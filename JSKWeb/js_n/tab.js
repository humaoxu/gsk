﻿function ms_dispnav()
{
	if(arguments.length < 2)return;
	var tell = arguments[0];
	for(var i = 1; i < arguments.length; i++)
	{
		var nav = document.getElementById(arguments[i] + "_nav");
		var content = document.getElementById(arguments[i] + "_content");
		if(!nav || !content)continue;
		if(tell == i)
		{
			nav.className = i == arguments.length - 1 ? "active end" : "active";
			content.style.display = "";
		}
		else
		{
			nav.className = i == arguments.length - 1 ? "normal end" : "normal";
			content.style.display = "none";
		}
	}
}
function updataip(ip)
{
	ms_dispnav(2,'ipsearch','ipsubmit','websearch');
	var theform = document.forms['ipsubmit'];
	theform.elements["upip"].value = ip;
	theform.elements["upaddr"].focus();
}