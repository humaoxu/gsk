﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.ComponentModel;

namespace JSKWeb.Controls
{
    public class GridViewKit:GridView
    {
      //private bool _enableEmptyContentRender = true;

        [DefaultValue("true")]
        [Browsable(true)]
        [Category("ExpandProperty")]
        [Description("July 7th incident of 1937, Don't forget the national humiliation")]
        //public bool EnableEmptyContentRender
        //{
        //    set { _enableEmptyContentRender = value; }

        //    get { return _enableEmptyContentRender; }
        //}

        public GridViewKit()
        {
            this.CellPadding = 0;
            this.HeaderStyle.CssClass = "header";
            this.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;
            this.RowStyle.CssClass = "normal";
            this.AlternatingRowStyle.CssClass = "across";
            this.CssClass = "gridview";
        }

        protected virtual void RenderEmptyContent(HtmlTextWriter writer)
        {
            Table table = new Table();
            table.CssClass = this.CssClass;
            table.GridLines = this.GridLines;
            table.BorderStyle = this.BorderStyle;
            table.BorderWidth = this.BorderWidth;
            table.CellPadding = this.CellPadding;
            table.CellSpacing = this.CellSpacing;
            table.HorizontalAlign = this.HorizontalAlign;
            table.Width = this.Width;
            table.CopyBaseAttributes(this);

            TableRow row = new TableRow();
            row.CssClass = "header";
            table.Rows.Add(row);

            foreach (DataControlField field in this.Columns)
            {
                    TableCell cell = new TableCell();
                    cell.Text = field.HeaderText; 
                    row.Cells.Add(cell); 
            }

            TableRow row2 = new TableRow();
            table.Rows.Add(row2);

            TableCell none = new TableCell();
            if (this.EmptyDataTemplate != null)
            {
                this.EmptyDataTemplate.InstantiateIn(none);
            }
            else
            {
                none.Text = "NO DATA";
            }
            none.ColumnSpan = this.Columns.Count;
            row2.Cells.Add(none);

            table.RenderControl(writer);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            //if (_enableEmptyContentRender && (this.Rows.Count == 0 || this.Rows[0].RowType == DataControlRowType.EmptyDataRow))

            if (this.Rows.Count == 0 || this.Rows[0].RowType == DataControlRowType.EmptyDataRow)
            {
                RenderEmptyContent(writer);
            }
            else
            {
                base.Render(writer);
            }
        }
    }
}