// JavaScript Document
function focusBox(o){
	if($.browser.msie && $.browser.version=='5.0'){
		$('#rotator').html('<div style="font-size:16;">IE5.0不支持此效果 ！</div>')
	}
	if(!o) return;
	var o='#'+o; i=0,arr= [],t= null,$a= $(o).find('.rotator_cont a');
	len= $a.length;
	$(o).find('.rotator_cont ul').width(len*940);
	
	//增加按钮
	arr.push('<div class="pages"><div class="pages_cont">');
	$a.each(function(i){
		if(i==0){
			arr.push('<span class="titre current" rel="'+ $(this).attr('href') +'"></span>');
		}else{
			arr.push('<span class="titre" rel="'+ $(this).attr('href') +'"></span>');
		}
	});
	arr.push('</div><div class="end"></div></div>');
	$(o).append(arr.join(''));

	//鼠标点击
	$('.pages span').click(function(){
		i = $('.pages span').index($(this));
		addCurrent(i);
		return false;
	})
	$(o).children('a.prev').click(function(){
		i = $('.pages span').index($('.pages span.current'));
		if(i==0){
			i=len-1;
		}else{
			i-=1;
		}
		addCurrent(i);
		return false;
	})
	$(o).children('a.next').click(function(){
		i = ($('.pages span').index($('.pages span.current'))+1)%len;
		addCurrent(i);
		return false;
	})
	$('.overlay').click(function(){
		var url = $('.pages span.current').attr('rel');
		location.href = url;
	})
	
	//自动
	t = setInterval(init,2500);
	$(o).hover(function(){
		clearInterval(t);
	}, function(){
		t = setInterval(init,2500);
	});
	
	function init(){
		addCurrent(i);
		setTimeout(loader,2500);
		i = (i+1)%len;
	}
	//触发焦点
	function addCurrent(i){
		$('.pages span').removeClass('current')
			.eq(i).addClass('current');
		$(o).find('.rotator_cont ul').animate({'margin-left': -940*i}, 500);
	}
	
	function loader(){
		var b = parseInt($('#loader_pic').css('backgroundPosition'));
		if(b>= -552){
			$('#loader_pic').show().animate({backgroundPosition:'-=24px'},0);
			setTimeout(loader,35);
		}else{
			$('#loader_pic').fadeOut(2500,function(){
				$(this).hide().css('backgroundPosition','0 0');
			}); 
		}
	}
}

$(function(){
	focusBox('rotator');
})