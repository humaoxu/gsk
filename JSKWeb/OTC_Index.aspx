﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OTC_Index.aspx.cs" Inherits="JSKWeb.OTC_Index" MasterPageFile="~/Master/Top.Master" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div style="margin: 0px; width: 100%; height: 100%">
        <table width="100%">
            <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>分数报告</span><a href="OTCREPORT/OTC_SCORERPT.aspx"><img width="140px" height="140px" src="otcimg/icon01.png" /></a></li>
                            <li class="smallbutton"><span>区域呈现</span><a href="OTCREPORT/OTC_CITY.aspx"><img  width="140px" height="140px" src="otcimg/icon02.png" /></a></li>
                            
                            <li class="smallbutton"><span>指标说明</span><a href="http://106.14.254.140:8080/otc_help.pptx"><img  width="140px" height="140px" src="otcimg/help.png" /></a></li>
                            
<%--                            <li class="smallbutton"><span>全国分项</span><a href="OTCREPORT/GSK_OTC_QGFX.aspx"><img src="otcimg/icon04.png" /></a></li>--%>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>分销</span><a href="Report/GSK_OTC_ITEM_FX_New.aspx"><img  width="140px" height="140px"  src="otcimg/icon03.png" /></a></li>
                            <li class="smallbutton"><span>价格</span><a href="Report/GSK_OTC_ITEM_JG_New.aspx"><img  width="140px" height="140px"  src="otcimg/icon04.png" /></a></li>
                            <li class="smallbutton"><span>陈列面</span><a href="Report/GSK_OTC_ITEM_CL_New.aspx"><img  width="140px" height="140px"  src="otcimg/icon05.png" /></a></li>
                            <li class="smallbutton"><span>陈列位置</span><a href="Report/GSK_OTC_ITEM_WZ_New.aspx"><img  width="140px" height="140px"  src="otcimg/icon06.png" /></a></li>
                            <li class="smallbutton"><span>POSM</span><a href="Report/GSK_OTC_ITEM_POSM.aspx"><img  width="140px" height="145px"  src="otcimg/icon07.png" /></a></li>
                            <li class="smallbutton"><span>推荐</span><a href="Report/GSK_OTC_ITEM_TJ_New.aspx"><img  width="140px" height="140px"  src="otcimg/icon08.png" /></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
