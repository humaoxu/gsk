﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="PPTIndex.aspx.cs" Inherits="JSKWeb.PPTIndex" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div style="margin: 0px; width: 100%; height: 100%">
        <table width="100%">
 <%--           <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>分数报告</span><a href="REPORTDDT/RptDDTSCORE.aspx"><img width="140px" height="140px" src="otcimg/icon01.png" /></a></li>
                            <li class="smallbutton"><span>指标说明</span><a href="http://106.14.254.140:8080/ddt_help.pptx"><img  width="140px" height="140px" src="otcimg/help.png" /></a></li>
                            
                        </ul>
                    </div>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>柱状图PPT生成</span><a href="PPTGenerate/bargraph.aspx"><img  width="140px" height="140px"  src="otcimg/icon03.png" /></a></li>
                            <li class="smallbutton"><span>饼图生成</span><a href="#"><img  width="140px" height="140px"  src="otcimg/ddt01.png" /></a></li>
                            <li class="smallbutton"><span>折线图生成</span><a href="#"><img  width="140px" height="140px"  src="otcimg/icon07.png" /></a></li>
                            <li class="smallbutton"><span>散点图生成</span><a href="#"><img  width="140px" height="140px"  src="otcimg/ddt01.png" /></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
