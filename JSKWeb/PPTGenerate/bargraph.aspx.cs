﻿using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSKWeb.PPTGenerate
{
    public partial class bargraph : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            HttpPostedFile file = this.fileUploadCtr.PostedFile;
            string fileName = file.FileName;
            string tempPath = Server.MapPath(InputText.GetConfig("TicketTempFile")); //获取系统临时文件路径
            fileName = System.IO.Path.GetFileName(fileName); //获取文件名（不带路径）
            string currFileExtension = System.IO.Path.GetExtension(fileName); //获取文件的扩展名
            string currFilePath = tempPath + fileName; //获取上传后的文件路径 记录到前面声明的全局变量
            file.SaveAs(currFilePath); //上传

            ISheet sheet = null;
            DataTable data = new DataTable();
            int startRow = 0;

            if (currFileExtension.ToUpper() != ".XLSX")
            {
                JavaScript.Alert(this, "文件格式不正确，上传的文件后缀名必须为XLSX还请确认");
                return;
            }

            FileStream fs = new FileStream(currFilePath, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            sheet = workbook.GetSheetAt(0);
            data = new DataTable();
            if (sheet != null)
            {
                IRow firstRow = sheet.GetRow(0);
                int cellCount = firstRow.LastCellNum; //一行最后一个cell的编号 即总的列数
                if (true)
                {
                    for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                    {
                        DataColumn column = new DataColumn(firstRow.GetCell(i).StringCellValue);
                        data.Columns.Add(column);
                    }
                    startRow = sheet.FirstRowNum + 1;
                }
                else
                {
                    startRow = sheet.FirstRowNum;
                }
                //最后一列的标号
                int rowCount = sheet.LastRowNum;
                for (int i = startRow; i <= rowCount; ++i)
                {
                    IRow row = sheet.GetRow(i);
                    if (row == null) continue; //没有数据的行默认是null　　　　　　　

                    DataRow dataRow = data.NewRow();
                    for (int j = row.FirstCellNum; j < cellCount; ++j)
                    {
                        if (row.GetCell(j) != null) //同理，没有数据的单元格都默认是null
                            dataRow[j] = row.GetCell(j).ToString();
                    }
                    data.Rows.Add(dataRow);
                }
            }


            JavaScript.Alert(this, data.Rows.Count.ToString());

        }
    }
}