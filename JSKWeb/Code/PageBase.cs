﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JSKWeb.Enum;
using System.Text;
using DBUtility;
using JSKWeb.Utility;

namespace JSKWeb.Code
{
    public class PageBase : System.Web.UI.Page
    {
        SqlHelper sqlHelper = new SqlHelper();
        public string CookieID
        {
            get
            {
                return Request["bwlsanmei"];
            }
        }

        public string _LinxSanmpleUserCode
        {
            get {
                if (Request.Cookies["linxsanmpleuser"] != null && Request.Cookies["linxsanmpleuser"].Value.Trim().Length > 0)
                {
                    return Request.Cookies["linxsanmpleuser"].Value.ToString().Trim().ToLower();
                }
                else
                {
                    ///跳转登录页
                }
                return null;
            }
        }
       
        /// <summary>
        ///  获取权限用户
        /// </summary>
        public string _GetPowerID()
        {
            string _sql = "";
            string loginId= Request.Cookies["linxsanmpleuser"].Value.ToString().Trim().ToLower();
            DataSet ds = null;
            try
            {
                if (loginId == InputText.GetConfig("gsk_user"))
                {
                    return loginId;
                }

                _sql = "select top 1 * from OTC_TEMP_USER where muid = '" + loginId + "'";
                ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, _sql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    loginId =  ds.Tables[0].Rows[0][1].ToString().Trim().ToLower();
                }

                //总监
                _sql = "select top 1 col30 as loginid,'GSKGLOBAL' from SurveyObj where projectid = 11 and col30 ='" + loginId + "'";
                ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, _sql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0][1].ToString().Trim().ToLower();
                }
                else
                {
                    //大区
                    _sql = "select top 1 col32 as loginid,col30 as repid from SurveyObj where projectid = 11 and col32 ='" + loginId + "' ";
                    ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, _sql);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].Rows[0][1].ToString().Trim().ToLower();
                    }
                    else
                    {
                        //团队
                        _sql = "select top 1 col34 as loginid,col32 as repid from SurveyObj where projectid = 11 and col34  ='" + loginId + "' ";
                        ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, _sql);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            return ds.Tables[0].Rows[0][1].ToString().Trim().ToLower();
                        }
                        else
                        {
                            //业代
                            _sql = "select top 1 col17 as loginid,col34 as repid from SurveyObj where projectid = 11 and col17   ='" + loginId + "' ";
                            ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, _sql);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                return ds.Tables[0].Rows[0][1].ToString().Trim().ToLower();
                            }
                            else
                            {
                                return loginId;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string _UserRole
        {
            get
            {
                if (Request.Cookies["_UserRole"] != null && Request.Cookies["_UserRole"].Value.Trim().Length > 0)
                {
                    
                    return Request.Cookies["_UserRole"].Value;
                }
                return null;
            }
        }

        public string _UserName
        {
            get
            {
                if (Request.Cookies["_UserName"] != null && Request.Cookies["_UserName"].Value.Trim().Length > 0)
                {
                    return Request.Cookies["_UserName"].Value;
                }
                return null;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            string s_user = this._LinxSanmpleUserCode;
            #region 营业所5位编码处理
            string s_role = "";
            if (Request.Cookies["_UserRole"] != null && Request.Cookies["_UserRole"].Value != null)
            {
                s_role = Request.Cookies["_UserRole"].Value.ToString();
            }
            #endregion
            base.OnLoad(e);
        }

        public bool IsUserLoggedOut(string a)
        {
            try
            {

                ///更新当前登录人的操作时间
                string sql = @"update sys_user_control set login_date=getdate() where user_code = '" + a + "' ";
                sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                /// nielsen 和管理员账户直接返回真
                if (this._UserRole == "6" || this._UserRole == "1")
                {
                    return false;
                }
                ///删除30分钟内不操作的非nielsen 和 管理员用户
                sql = @"delete from sys_user_control where DateDiff(minute,login_date,getdate())>10 
                         and user_role!=6 and user_code!='sanmei' and user_code!='1' ";
                sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                ///查询当前登录人是否已经超时。
                string sql1 = @"select * from sys_user_control where user_code = '" + a + "' ";
               
                DataSet ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql1);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }


            }
            catch (Exception)
            {
                return false;
            }
        }

        private void LoadUserInfo()
        {
            string cookieName = FormsAuthentication.FormsCookieName;
            HttpCookie authCookie = Context.Request.Cookies[cookieName];

            authCookie.Expires = DateTime.Now.AddDays(1);

            if (null == authCookie)
            {
                return;
            }

            FormsAuthenticationTicket authTicket = null;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch
            {
                return;
            }
            if (null == authTicket)
            {
                return;
            }

            string user = authTicket.UserData;
            FormsIdentity id = new FormsIdentity(authTicket);
            Principal principal = new Principal(id, user);
            Context.User = principal;
        }

        public Principal UserData
        {
            get
            {
                return User as Principal;
            }
        }



        private static string unLockUser;

        public static string UnLockUser
        {
            get { return unLockUser; }
            set { unLockUser = value; }
        }


    }
}
