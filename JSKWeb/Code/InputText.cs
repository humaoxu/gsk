﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Web;
using JSKWeb.Code;
using DBUtility;
using System.Data.SqlClient;
namespace JSKWeb.Utility
{
    public class InputText
    {
        public static decimal? GetDecimalNull(string aText)
        {
            if (aText.Trim() == string.Empty)
            {
                return null;
            }
            else
            {
                return decimal.Parse(aText.Trim());
            }
        }

        public static bool IsNumber(object obj)
        {
            try
            {
                int n = 0;
                return Int32.TryParse(obj.ToString(), out n);
            }
            catch (Exception)
            {

                return false;
            }

        }

        public static decimal GetDouble2(string date)
        {
            try
            {
                return decimal.Round(decimal.Parse(date) * 100, 2);
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }
        public static decimal GetDecimal(string aText)
        {
            return decimal.Parse(aText.Trim());
        }

        public static int? GetIntNull(string aText)
        {
            if (aText.Trim() == string.Empty)
            {
                return null;
            }
            else
            {
                return int.Parse(aText.Trim());
            }
        }


        public static int GetInt0(object aText)
        {
            try
            {
                if (aText.ToString().Trim() == string.Empty)
                {
                    return 20;
                }
                else
                {
                    return Convert.ToInt32(aText.ToString().Trim());
                }
            }
            catch (Exception)
            {
                return 20;
            }

        }
        public static string ReplaceFullChar(object obj)
        {
            string stra = "";
            if (obj != null)
            {
                stra = obj.ToString();
            }
            else
            {
                return stra;
            }
            stra = stra.Replace("'", "''");
            stra = stra.Replace("delete", "");
            stra = stra.Replace("update", "");
            stra = stra.Replace("insert", "");
            return stra;
        }

        public static string ReplaceSQL(object obj)
        {
            string stra = "";
            if (obj != null)
            {
                stra = obj.ToString();
            }
            else
            {
                return stra;
            }
            stra = stra.Replace("'", "''");
            return stra;
        }

        public static int GetInt(string aText)
        {
            return int.Parse(aText.Trim());
        }

        public static int GetInt(object aText)
        {
            try
            {
                return Convert.ToInt32(aText);
            }
            catch (Exception)
            {

                return 0;
            }

        }

        public static short GetShort(string aText)
        {
            return short.Parse(aText.Trim());
        }

        public static char? GetCharNull(string aText)
        {
            if (aText.Trim() == string.Empty)
            {
                return null;
            }
            else
            {
                return char.Parse(aText.Trim());
            }
        }

        public static char GetChar(string aText)
        {
            return char.Parse(aText);
        }

        public static DateTime? GetDateTimeNull(string aText)
        {
            if (aText.Trim() == string.Empty)
            {
                return null;
            }
            else
            {
                return DateTime.Parse(aText.Trim());
            }
        }

        public static DateTime GetDateTime(string aText)
        {
            try
            {
                return DateTime.Parse(aText);
            }
            catch
            {
                return DateTime.Now;
            }

            

        }



        public static string GetString(string aText, string aDefault)
        {
            if (aText == null || aText == string.Empty)
            {
                return aDefault;
            }
            else
            {
                return aText;
            }
        }

        public static string GetStrByObj(object aObj)
        {
            return string.Format("{0}", (aObj == null ? string.Empty : aObj));
        }

        public static string GetStrByObj(object aObj, string aDefault)
        {
            return (aObj == null ? aDefault : aObj.ToString());
        }

        public static string GetStrByDate(DateTime? aValue, string aFormat)
        {
            return (aValue != null ? (Convert.ToDateTime(aValue).ToString(aFormat)) : string.Empty);
        }

        public static Decimal GetDecimal(decimal? aValue, decimal aDefValue)
        {
            return aValue == null ? aDefValue : Convert.ToDecimal(aValue);
        }

        public static bool GetBool(bool? aValue)
        {
            return (aValue == true ? true : false);
        }

        public static Guid GetGuid(string aValue)
        {
            return new Guid(aValue);
        }

        public static Guid? GetGuidNull(string aValue)
        {
            if (string.IsNullOrEmpty(aValue))
            {
                return null;
            }
            else
            {
                return new Guid(aValue);
            }
        }

        public static int GetDropDownListIndex(DropDownList aDDL, string aValue)
        {
            return aDDL.Items.IndexOf(aDDL.Items.FindByValue(aValue));
        }

        public static string GetConfig(string obj)
        {
            try
            {
                return ConfigurationManager.AppSettings[obj].ToString();
            }
            catch (Exception)
            {
                return "";
                throw;
            }
        }
        /// <summary>
        /// 获得商品结构
        /// </summary>
        /// <returns></returns>
        public static DataTable GetProTable()
        {
            DataTable dt = new DataTable();
            DataColumn cl = new DataColumn("Product_Code", Type.GetType("System.String"));
            dt.Columns.Add(cl);
            DataColumn cl1 = new DataColumn("Product_Name", Type.GetType("System.String"));
            dt.Columns.Add(cl1);
            DataColumn cl2 = new DataColumn("Product_Bar_Code", Type.GetType("System.String"));
            dt.Columns.Add(cl2);
            DataColumn cl3 = new DataColumn("Product_Unit", Type.GetType("System.String"));
            dt.Columns.Add(cl3);
            DataColumn cl4 = new DataColumn("Product_Spec", Type.GetType("System.String"));
            dt.Columns.Add(cl4);
            return dt;
        }

        public static string GetString(object p)
        {
            try
            {
                return p.ToString();
            }
            catch (Exception)
            {
                return "";
                throw;
            }
        }


        public static double GetDouble(object p)
        {
            try
            {
                return Convert.ToDouble(p);
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }

        public static double GetDouble(object p, int n)
        {
            try
            {
                return Convert.ToDouble(Convert.ToDouble(p).ToString("f" + n));
            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p1">分子</param>
        /// <param name="p2">分母</param>
        /// <returns></returns>
        public static double GetDivisionRatio(object p1, object p2)
        {
            try
            {
                if (p2 == null || p2 == "" || GetDouble(p2) == 0)
                {
                    return 0;
                }
                return ((GetDouble(p1) / GetDouble(p2)));


            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }

        public static string GetExcelTitle(int number)
        {
            int n = 0;
            int x = number / 26;
            int y = number % 26;
            if (x > 0)
            {
                return ((char)('A' + (x - 1))).ToString().ToUpper() + ((char)('A' + (y))).ToString().ToUpper();
            }
            else
            {
                return ((char)('A' + (y))).ToString().ToUpper();
            }
            return "";
        }

        public static int GetRowsCount(DataRow[] dr)
        {
            try
            {
                if (dr != null)
                {
                    return dr.Count();
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                return 0;
                throw;
            }
        }

        public static string GetDateTimeBY_YYYYMMDD(string s)
        {
            try
            {
                return s.Substring(0, 4) + "-" + s.Substring(4, 2) + "-" + s.Substring(6, 2);
            }
            catch (Exception)
            {
                return DateTime.Now.ToString("yyyy-MM-DD");
                throw;
            }

        }

        public static string GetDateTimeEBY_YYYYMMDD(string s)
        {
            try
            {
                return s.Substring(0, 4) + "-" + s.Substring(4, 2) + "-" + s.Substring(6, 2);
            }
            catch (Exception)
            {
                return "";
                throw;
            }

        }

        public static void DelCookie()
        {
            Cookie.Delete("StarDate");
            Cookie.Delete("EndDate");
            Cookie.Delete("Ou");
            Cookie.Delete("Yedai");
            Cookie.Delete("Dianpu");
            Cookie.Delete("Qudao");
            Cookie.Delete("Zhuren");
            Cookie.Delete("Yingyesuo");
            Cookie.Delete("Dianpucode");
        }

        public static void InsertLog(string user, string category, string message)
        {
            //HttpRequest sys = HttpContext.Current.Request;


            //SqlParameter[] pars = {
            //                      new SqlParameter("@category",SqlDbType.VarChar),
            //                      new SqlParameter ("@user",SqlDbType.VarChar),
            //                      new SqlParameter ("@FromUrl",SqlDbType.VarChar),
            //                      new SqlParameter("@message",SqlDbType.VarChar),
            //                      new SqlParameter ("@ClientIP",SqlDbType.VarChar),
            //                      new SqlParameter ("@Browser",SqlDbType.VarChar),  
            //                      new SqlParameter ("@ClientOS",SqlDbType.VarChar),
            //                      new SqlParameter ("@ClientLang",SqlDbType.VarChar),
            //                      };
            //pars[0].Value = category;
            //pars[1].Value = user;
            //pars[2].Value = sys.Url.PathAndQuery;
            //pars[3].Value = message;
            //pars[4].Value = sys.UserHostAddress;
            //pars[5].Value = String.Concat(sys.Browser.Browser, sys.Browser.Version);
            //pars[6].Value = sys.Browser.Platform;
            //pars[7].Value = (sys.UserLanguages == null ? string.Empty : (sys.UserLanguages.Count() > 0 ? sys.UserLanguages[0] : string.Empty));

            //string sql = @"insert into Sys_Log([Category] 
            //      ,[UserName]
            //      ,[FromUrl]
            //      ,[Message]
            //      ,[ClientIP]
            //      ,[Browser]
            //      ,[ClientOS]
            //      ,[ClientLang])values(@category,@user,@FromUrl,@message,@ClientIP,@Browser,@ClientOS,@ClientLang)";
             
            //SqlHelper sqlhelper = new SqlHelper();
            //sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql, pars);

        }

        public static string[] GetEchartColors()
        {
            //string colors = @"#009DD9,#00D050,#ff0000,#ffc000,#be0059,#ff6600,#83f3ff,#7f7f7f,#ba55d3,#cd5c5c,#ffa500,#40e0d0";
            string colors = @"#FFC000,#009DFF,#ff7f50,#87cefa,#da70d6,#32cd32,#6495ed,#ff69b4,#ba55d3,#cd5c5c,#ffa500,#40e0d0";
            return colors.Split(',');
        }
    }
}
