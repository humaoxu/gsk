﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Principal;
using JSKWeb.Enum;

namespace JSKWeb.Code
{
    public class Principal : IPrincipal
    {
        private IIdentity _identity;
        private string[] _user;

        public Principal(IIdentity identity, string user)
        {
            _identity = identity;
            _user = user.Split('|');
        }

        public Principal(HttpCookie aFormsAuthenticationCookie)
        {
            FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(aFormsAuthenticationCookie.Value);
            _user = authTicket.UserData.Split('|');
        }

        public string this[PrincipalEnum pos]
        {
            get
            {
                int opos = (int)pos;
                return opos >= _user.Length ? string.Empty : _user[opos];
            }
        }

        public IIdentity Identity
        {
            get
            {
                return _identity;
            }
        }

        public bool IsInRole(string role)
        {
            return false;
        }

        public bool IsInAction(string className, long action)
        {
            return true;
        }
    }
}
