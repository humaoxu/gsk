﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.SS.UserModel;

namespace JSKWeb.Code
{
    public class PicturesInfo
    {
        public int MinRow {get;set; }

        public int MaxRow{get;set;}
        public int MinCol{get;set;}
        public int MaxCol {get;set;}
        public PictureType PictureType { get; set; }
        public Byte[] PictureData{get;set;}

        public PicturesInfo(int minRow, int maxRow, int minCol, int maxCol, PictureType pictureType, Byte[] pictureData)
        { 
            this.MaxCol = maxCol;
            this.MinCol = minCol;
            this.MaxRow = maxRow;
            this.MinRow = minRow;
            this.PictureData = pictureData;
            this.PictureType = pictureType;
        }
    }
}