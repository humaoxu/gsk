﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JSKWeb.Code
{
    public class TicketFilterInfo
    {
        private string starDate;
        private string endDate;
        private string ou;
        private string yedai;
        private string dianpu;
        private string qudao;
        private string zhuangtai;
        private string renling;
        private string tkid;
        private string storeid;
        private string page;
        private string zhuren;
        private string yingyesuo;
        private string dianpucode;
        private string shixiao;
        private string round;

        public string Round
        {
            get { return round; }
            set { round = value; }
        }

        public string Shixiao
        {
            get { return shixiao; }
            set { shixiao = value; }
        }
        private int pagenum;

        public int Pagenum
        {
            get { return pagenum; }
            set { pagenum = value; }
        }



        public string Dianpucode
        {
            get { return dianpucode; }
            set { dianpucode = value; }
        }

        public string Yingyesuo
        {
            get { return yingyesuo; }
            set { yingyesuo = value; }
        }

        public string Zhuren
        {
            get { return zhuren; }
            set { zhuren = value; }
        }

        public string Page
        {
            get { return page; }
            set { page = value; }
        }

        public string Storeid
        {
            get { return storeid; }
            set { storeid = value; }
        }

        public string Tkid
        {
            get { return tkid; }
            set { tkid = value; }
        }

        public string Renling
        {
            get { return renling; }
            set { renling = value; }
        }

        public string Zhuangtai
        {
            get { return zhuangtai; }
            set { zhuangtai = value; }
        }

        public string Qudao
        {
            get { return qudao; }
            set { qudao = value; }
        }

        public string Dianpu
        {
            get { return dianpu; }
            set { dianpu = value; }
        }

        public string Yedai
        {
            get { return yedai; }
            set { yedai = value; }
        }

        public string Ou
        {
            get { return ou; }
            set { ou = value; }
        }



        public string EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }


        public string StarDate
        {
            get { return starDate; }
            set { starDate = value; }
        }
    }
}