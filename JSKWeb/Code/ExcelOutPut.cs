﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using JSKWeb.Utility;
using System.Collections;
using System.Drawing;
using DBUtility;
using System.Drawing.Imaging;
using NPOI.HSSF.UserModel;

namespace JSKWeb.Code
{
    public class ExcelOutPut
    {
        SqlHelper sqlHelper = new SqlHelper();
        /// <summary>
        /// 导出DATATABLE 为 EXCEL 自定义列头
        /// COLNAME 为空：则用datatable列头
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="colname"></param>
        /// <returns></returns>
        public MemoryStream ExportOutExcel(DataTable dataTable, string colnames)
        {
            try
            {

                ICell cell_x = null;
                IRow row_x = null;
                XSSFWorkbook hssfworkbook = new XSSFWorkbook();

                //定义模板颜色
                ISheet sheet_data = hssfworkbook.CreateSheet(DateTime.Now.ToString("yyMMdd"));
                string colstr = InputText.GetConfig(colnames);
                row_x = sheet_data.CreateRow(0);
                string[] cloS = colstr.Split(',');
                if (cloS.Count() > 0)
                {
                    for (int i = 0; i < cloS.Count(); i++)
                    {
                        cell_x = row_x.CreateCell(i);
                        cell_x.SetCellValue(cloS[i].ToString());
                    }
                }
                else 
                {
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        cell_x = row_x.CreateCell(i);
                        cell_x.SetCellValue(dataTable.Columns[i].ToString());
                    }
                }
                
                short dateFormatter = hssfworkbook.CreateDataFormat().GetFormat("YYYY/MM/DD");
                ICellStyle dateStyle = hssfworkbook.CreateCellStyle();
                IDataFormat dateFormat = hssfworkbook.CreateDataFormat();
                dateStyle.DataFormat = dateFormat.GetFormat("yyyy/m/d");
                #region
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {

                    row_x = sheet_data.CreateRow(i + 1);
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    { 
                        cell_x = row_x.CreateCell(j);

                        if (dataTable.Columns[j].DataType.Name.ToString().ToUpper() == "DATETIME")
                        {
                            cell_x.CellStyle = dateStyle;
                            if (dataTable.Rows[i][j] != DBNull.Value)
                            {
                                cell_x.SetCellValue(InputText.GetDateTime(dataTable.Rows[i][j].ToString())); 
                            }
                           
                        }
                        else 
                        {
                            cell_x.SetCellValue(dataTable.Rows[i][j].ToString());
                        }
                    }
                }

                #endregion

                //生成文件
                using (MemoryStream ms = new MemoryStream())
                {
                    hssfworkbook.Write(ms);
                    sheet_data = null;
                    hssfworkbook = null;

                    //ms.Flush();
                    //ms.Position = 0; 
                    return ms;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public MemoryStream ExportOutExcel(DataTable dataTable, string[] colnames)
        {
            try
            {

                ICell cell_x = null;
                IRow row_x = null;
                XSSFWorkbook hssfworkbook = new XSSFWorkbook();

                //定义模板颜色
                ISheet sheet_data = hssfworkbook.CreateSheet(DateTime.Now.ToString("yyMMdd"));
                int r_Start = 0;
                for (int i = 0; i < colnames.Count(); i++)
                { 
                    string[] colname = colnames[i].Split(',');
                    row_x = sheet_data.CreateRow(i);
                    for (int j = 0; j < colname.Count(); j++) 
                    {
                        cell_x = row_x.CreateCell(j);
                        cell_x.SetCellValue(colname[j].ToString());
                    }
                    r_Start++;
                } 
                

                short dateFormatter = hssfworkbook.CreateDataFormat().GetFormat("YYYY/MM/DD");
                ICellStyle dateStyle = hssfworkbook.CreateCellStyle();
                IDataFormat dateFormat = hssfworkbook.CreateDataFormat();
                dateStyle.DataFormat = dateFormat.GetFormat("0.00%");

               
                #region
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {

                    row_x = sheet_data.CreateRow(i + r_Start);
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        cell_x = row_x.CreateCell(j);

                        if (j>0)
                        {
                            cell_x.CellStyle = dateStyle;
                            if (dataTable.Rows[i][j] != DBNull.Value)
                            {
                                cell_x.SetCellValue(Convert.ToDouble(dataTable.Rows[i][j]));
                            } 
                        }
                        else
                        {
                            cell_x.SetCellValue(dataTable.Rows[i][j].ToString());
                        }
                    }
                }

                #endregion

                //生成文件
                using (MemoryStream ms = new MemoryStream())
                {
                    hssfworkbook.Write(ms);
                    sheet_data = null;
                    hssfworkbook = null;

                    //ms.Flush();
                    //ms.Position = 0; 
                    return ms;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public MemoryStream ExportOutExcelWithNoHeader(DataTable dataTable, string colname)
        {
            try
            {

                ICell cell_x = null;
                IRow row_x = null;
                XSSFWorkbook hssfworkbook = new XSSFWorkbook();

                //定义模板颜色
                ISheet sheet_data = hssfworkbook.CreateSheet(DateTime.Now.ToString("yyMMdd"));
                string colstr = InputText.GetConfig(colname);
                //row_x = sheet_data.CreateRow(0);
                //string[] cloS = colstr.Split(',');
                //if (cloS.Count() > 0)
                //{
                //    for (int i = 0; i < cloS.Count(); i++)
                //    {
                //        cell_x = row_x.CreateCell(i);
                //        cell_x.SetCellValue(cloS[i].ToString());
                //    }
                //}
                //else
                //{
                //    for (int i = 0; i < dataTable.Columns.Count; i++)
                //    {
                //        cell_x = row_x.CreateCell(i);
                //        cell_x.SetCellValue(dataTable.Columns[i].ToString());
                //    }
                //}

                short dateFormatter = hssfworkbook.CreateDataFormat().GetFormat("YYYY/MM/DD");
                ICellStyle dateStyle = hssfworkbook.CreateCellStyle();
                IDataFormat dateFormat = hssfworkbook.CreateDataFormat();
                dateStyle.DataFormat = dateFormat.GetFormat("yyyy/m/d");
                #region
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {

                    row_x = sheet_data.CreateRow(i);
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        cell_x = row_x.CreateCell(j);

                        if (dataTable.Columns[j].DataType.Name.ToString().ToUpper() == "DATETIME")
                        {
                            cell_x.CellStyle = dateStyle;
                            if (dataTable.Rows[i][j] != DBNull.Value)
                            {
                                cell_x.SetCellValue(InputText.GetDateTime(dataTable.Rows[i][j].ToString()));
                            }

                        }
                        else
                        {
                            cell_x.SetCellValue(dataTable.Rows[i][j].ToString());
                        }
                    }
                }

                #endregion

                //生成文件
                using (MemoryStream ms = new MemoryStream())
                {
                    hssfworkbook.Write(ms);
                    sheet_data = null;
                    hssfworkbook = null;

                    //ms.Flush();
                    //ms.Position = 0; 
                    return ms;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// 导出DATATABLE 为 EXCEL 以查询列为列头
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        public MemoryStream ExportOutExcel(DataTable dataTable)
        {
            try
            {

                ICell cell_x = null;
                IRow row_x = null;
                XSSFWorkbook hssfworkbook = new XSSFWorkbook();

                //定义模板颜色
                ISheet sheet_data = hssfworkbook.CreateSheet(DateTime.Now.ToString("yyMMdd"));

                row_x = sheet_data.CreateRow(0);

                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    cell_x = row_x.CreateCell(i);
                    cell_x.SetCellValue(dataTable.Columns[i].ToString());
                }

                #region
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {

                    row_x = sheet_data.CreateRow(i + 1);
                    for (int j = 0; j < dataTable.Columns.Count; j++)
                    {
                        cell_x = row_x.CreateCell(j);
                        cell_x.SetCellValue(dataTable.Rows[i][j].ToString());
                    }
                }

                #endregion

                //生成文件
                using (MemoryStream ms = new MemoryStream())
                {
                    hssfworkbook.Write(ms);
                    sheet_data = null;
                    hssfworkbook = null;

                    //ms.Flush();
                    //ms.Position = 0; 
                    return ms;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        } 

        private bool CheckExcelData(string[] compname, string p, string filename, string p_2)
        {
            throw new NotImplementedException();
        }

        public string GetcodeByStr(string str)
        {
            try
            {
                str = str.Replace("-", "_").Replace("＿", "_").Replace("－", "_").Replace("—", "_").Replace("-", "_").Replace("-", "_");
                string[] codenameStr = new string[2];
                if (str != null)
                {
                    if (str.IndexOf("_", 0) > 0)
                    {
                        return str.Substring(0, str.IndexOf("_")).Replace(" ", "");
                    }
                    else
                    {
                        return str;
                    }
                }
                else
                {
                    return str;
                }
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public string[] Getcodename(string code_name)
        {
            try
            {
                code_name = code_name.Replace("-", "_").Replace("＿", "_").Replace("－", "_").Replace("—", "_").Replace("-", "_").Replace("-", "_");
                string[] codenameStr = new string[2];
                if (code_name != null)
                {
                    if (code_name.IndexOf("_", 0) > 0)
                    {
                        codenameStr[0] = code_name.Substring(0, code_name.IndexOf("_")).Replace(" ", "");
                        codenameStr[1] = code_name.Substring(code_name.IndexOf("_") + 1, code_name.Length - code_name.IndexOf("_") - 1).Replace(" ", "");
                    }
                    else
                    {
                        codenameStr[0] = code_name;
                    }
                }
                return codenameStr;
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        public bool CheckExcelData(string[] str)
        {
            if (str == null || str[0] == null || str[1] == null || str[0].Trim().Length == 0 || str[1].Trim().Length == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="t"></param>
        /// <param name="filename"></param>
        /// <param name="shensuren"></param>
        /// <returns></returns> 
        public bool CheckExcelData(string[] str, string t, string filename, string shensuxiang, out string message_erro)
        {

            message_erro = "";
            if (str == null || str[0] == null || str[1] == null || str[0].Trim().Length == 0 || str[1].Trim().Length == 0)
            {
                message_erro = t + "不符合[编码_名称]规则！";
                return false;
            }
            else
            {
                return true;
                #region 检查数据库内是否存在
                DataSet ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, GetSQLByType(t, str));
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    message_erro = shensuxiang + "编码或名称错误";
                    return false;
                }
                #endregion
            }
        }

        public string GetSQLByType(string t, string[] str)
        {
            string sql = "";

            if (t.Equals("OU"))
            {
                sql = "select max(SALES_ORGA_CODE) from vip_contranct_local where SALES_ORGA_CODE='" + str[0].ToString() + "' and COMP_NAME='" + str[1].ToString() + "'";
            }
            else if (t.Equals("店铺编号_店铺名称"))
            {
                sql = "select max(STORE_CODE) from vip_contranct_local where STORE_CODE='" + str[0].ToString() + "' and STORE_NAME='" + str[1].ToString() + "'";
            }
            else if (t.Equals("渠道编号_渠道名称"))
            {
                sql = "select max(RED_CHAN_NAME) from vip_contranct_local where RED_CHAN_CODE='" + str[0].ToString() + "' and RED_CHAN_NAME='" + str[1].ToString() + "'";
            }
            else if (t.Equals("业代编号_业代姓名"))
            {
                sql = "select max(SALES_REP_CODE) from vip_contranct_local where SALES_REP_CODE='" + str[0].ToString() + "' and SALES_REP_NAME='" + str[1].ToString() + "'";
            }
            else if (t.Equals("合同大项"))
            {
                sql = "select max(CON_MAXTERM_NAME) from vip_contranct_local where CON_MAXTERM='" + str[0].ToString() + "' and CON_MAXTERM_NAME='" + str[1].ToString() + "'";
            }
            else if (t.Equals("合同条款"))
            {
                sql = "select max(ITEM_META_CODE) from vip_contranct_local where ITEM_META_CODE='" + InputText.GetInt0(str[0].ToString()) + "' and ITEM_META_NAME='" + str[1].ToString() + "'";
            }

            return sql;
        }


        public bool CheckExcelData_new(string str, string t, string filename, string shensuxiang, out string message_erro, out string[] allStr)
        {
            message_erro = "";
            allStr = new string[2];
            #region 检查数据库内是否存在
            DataSet ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, GetSQLByType_new(t, str));
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                allStr[0] = ds.Tables[0].Rows[0][0].ToString();
                allStr[1] = ds.Tables[0].Rows[0][1].ToString();
                return true;
            }
            else
            {
                allStr[0] = "错误信息";
                message_erro = shensuxiang + "编码错误";
                return false;
            }
            #endregion
        }
        public string GetSQLByType_new(string t, string str)
        {
            string sql = "";

            if (t.Equals("OU"))
            {
                sql = "select top 1 SALES_ORGA_CODE,COMP_NAME from vip_contranct_local where SALES_ORGA_CODE='" + str.ToString() + "'";
            }
            else if (t.Equals("店铺编号_店铺名称"))
            {
                if (str.Contains("_"))
                {
                    string storecode = str.Split('_')[0];
                    string storeRound = str.Split('_')[1];
                    sql = "select STORE_CODE,STORE_NAME from PIC_TICKET_BY_ROUND where STORE_CODE='" + str.ToString() + "' and round ='" + storeRound + "'";
                }
                else 
                {
                    sql = "select top 1 STORE_CODE,STORE_NAME from vip_contranct_local where STORE_CODE='" + str.ToString() + "'";
                } 
            }
            else if (t.Equals("渠道编号_渠道名称"))
            {
                sql = "select top 1 RED_CHAN_CODE,RED_CHAN_NAME from vip_contranct_local where RED_CHAN_CODE='" + str.ToString() + "'";
            }
            else if (t.Equals("业代编号_业代姓名"))
            {
                sql = "select  top 1 SALES_REP_CODE,SALES_REP_NAME from vip_contranct_local where SALES_REP_CODE='" + str.ToString() + "'";
            }
            else if (t.Equals("合同大项"))
            {
                sql = "select max(CON_MAXTERM_NAME) from vip_contranct_local where CON_MAXTERM='" + str.ToString() + "'";
            }
            else if (t.Equals("合同条款"))
            {
                sql = "select top 1 CONT_TEMP_CLAU_CODE,CONT_TEMP_CLAU_NAME from vip_contranct_local where CONT_TEMP_CLAU_CODE='" + str.ToString() + "'";
            }
            return sql;
        }

        #region 图片压缩(降低质量)Compress
        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
        /// <summary>
        /// 图片压缩(降低质量以减小文件的大小)
        /// </summary>
        /// <param name="srcBitmap">传入的Bitmap对象</param>
        /// <param name="destStream">压缩后的Stream对象</param>
        /// <param name="level">压缩等级，0到100，0 最差质量，100 最佳</param>
        public void Compress(Bitmap srcBitmap, Stream destStream, long level)
        {
            ImageCodecInfo myImageCodecInfo;
            Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;

            // Get an ImageCodecInfo object that represents the JPEG codec.
            myImageCodecInfo = GetEncoderInfo("image/jpeg");

            // Create an Encoder object based on the GUID

            // for the Quality parameter category.
            myEncoder = Encoder.Quality;

            // Create an EncoderParameters object.
            // An EncoderParameters object has an array of EncoderParameter
            // objects. In this case, there is only one

            // EncoderParameter object in the array.
            myEncoderParameters = new EncoderParameters(1);

            // Save the bitmap as a JPEG file with 给定的 quality level
            myEncoderParameter = new EncoderParameter(myEncoder, level);
            myEncoderParameters.Param[0] = myEncoderParameter;
            srcBitmap.Save(destStream, myImageCodecInfo, myEncoderParameters);
        }
        /// <summary>
        /// 图片压缩(降低质量以减小文件的大小)
        /// </summary>
        /// <param name="srcBitMap">传入的Bitmap对象</param>
        /// <param name="destFile">压缩后的图片保存路径</param>
        /// <param name="level">压缩等级，0到100，0 最差质量，100 最佳</param>
        public void Compress(Bitmap srcBitMap, string destFile, long level)
        {
            Stream s = new FileStream(destFile, FileMode.Create);
            Compress(srcBitMap, s, level);
            s.Close();
        }
        /// <summary>
        /// 图片压缩(降低质量以减小文件的大小)
        /// </summary>
        /// <param name="srcFile">传入的Stream对象</param>
        /// <param name="destFile">压缩后的图片保存路径</param>
        /// <param name="level">压缩等级，0到100，0 最差质量，100 最佳</param>
        public void Compress(Stream srcStream, string destFile, long level)
        {
            Bitmap bm = new Bitmap(srcStream);
            Compress(bm, destFile, level);
            bm.Dispose();
        }
        /// <summary>
        /// 图片压缩(降低质量以减小文件的大小)
        /// </summary>
        /// <param name="srcFile">传入的Image对象</param>
        /// <param name="destFile">压缩后的图片保存路径</param>
        /// <param name="level">压缩等级，0到100，0 最差质量，100 最佳</param>
        public void Compress(Image srcImg, string destFile, long level)
        {
            Bitmap bm = new Bitmap(srcImg);
            Compress(bm, destFile, level);
            bm.Dispose();
        }
        /// <summary>
        /// 图片压缩(降低质量以减小文件的大小)
        /// </summary>
        /// <param name="srcFile">待压缩的BMP文件名</param>
        /// <param name="destFile">压缩后的图片保存路径</param>
        /// <param name="level">压缩等级，0到100，0 最差质量，100 最佳</param>
        public void Compress(string srcFile, string destFile, long level)
        {
            // Create a Bitmap object based on a BMP file.
            Bitmap bm = new Bitmap(srcFile);
            Compress(bm, destFile, level);
            bm.Dispose();
        }

        #endregion 图片压缩(降低质量)

    }
}