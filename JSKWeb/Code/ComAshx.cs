﻿using System;
using System.Data;
using System.Text;

namespace JSKWeb.Code
{
    public class ComAshx
    {        
        /// <summary>
             /// 把dataset数据转换成json的格式
             /// </summary>
             /// <param name="ds">dataset数据集</param>
             /// <returns>json格式的字符串</returns>
        public  string GetJsonByDataset(DataSet ds)
        {
            if (ds == null || ds.Tables.Count <= 0 || ds.Tables[0].Rows.Count <= 0)
            {
                //如果查询到的数据为空则返回标记ok:false
                //return "{\"ok\":false}";
            }
            StringBuilder sb = new StringBuilder();
            //sb.Append("{\"ok\":true,");
            foreach (DataTable dt in ds.Tables)
            {
                //sb.Append(string.Format("\"{0}\":[", dt.TableName));
                sb.Append("[");
                foreach (DataRow dr in dt.Rows)
                {
                    sb.Append("{");
                    for (int i = 0; i < dr.Table.Columns.Count; i++)
                    {
                        sb.AppendFormat("\"{0}\":\"{1}\",", dr.Table.Columns[i].ColumnName.Replace("\"", "\\\"").Replace("\'", "\\\'"), ObjToStr(dr[i]).Replace("\"", "\\\"").Replace("\'", "\\\'")).Replace(Convert.ToString((char)13), "\\r\\n").Replace(Convert.ToString((char)10), "\\r\\n");
                    }
                    sb.Remove(sb.ToString().LastIndexOf(','), 1);
                    sb.Append("},");
                }

                sb.Remove(sb.ToString().LastIndexOf(','), 1);
                sb.Append("]");
            }
            //sb.Remove(sb.ToString().LastIndexOf(','), 1);
            //sb.Append("}");
            return sb.ToString();
        }

        /// <summary>
        /// 将object转换成为string
        /// </summary>
        /// <param name="ob">obj对象</param>
        /// <returns></returns>
        public string ObjToStr(object ob)
        {
            if (ob == null)
            {
                return string.Empty;
            }
            else
                return ob.ToString();
        }
    }
}