﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JSKWeb.Code
{
    public class CommonStirng
    {
        public static string StrPagerCustomerInfo = "CurrentPage:{0}/{1} TotalRecord:{2} Rows Per Page:{3}";
        
        public static string StrPagerCustomerInfo_N = "Rows:{4}-{5}/{2} Page:{0}/{1} Rows Per Page:{3}";

        public string GetUserName(string strUsCode)
        {
            return strUsCode;
        }

        public string GetSelectIdHtml(int iPageSize)
        {
            string per_page_size = System.Configuration.ConfigurationManager.AppSettings["PerPageSize"];
            string[] arrPageSize = per_page_size.Split(',');
            string stmp = "  <select id='sel_load' width='100px'>";
            for (int i = 0; i < arrPageSize.Length; i++)
            {
                if (arrPageSize[i].ToString().Trim() == iPageSize.ToString())
                {
                    stmp = stmp + "<option selected='selected'>" + arrPageSize[i].ToString().Trim() + "</option>";
                }
                else
                {
                    stmp = stmp + "<option>" + arrPageSize[i].ToString().Trim() + "</option>";
                }
            }
            stmp = stmp + "</select>";

            return stmp;
        }
    }
}