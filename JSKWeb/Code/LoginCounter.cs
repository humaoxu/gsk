﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using DBUtility;
using JSKWeb.Utility;

namespace JSKWeb.Code
{
    public static class LoginCounter
    {
        #region Helper Class Definitions

        public class LoginInfo
        {
            public String UserCode { get; set; }
            public DateTime LoginTime { get; set; }

            public LoginInfo(String userCode, DateTime loginTime)
            {
                this.UserCode = userCode;
                this.LoginTime = loginTime;
            }
        }

        public class GroupLoggedFullException : Exception
        {
            public override string Message
            {
                get { return "所在的用户组在线用户数已达上限"; }
            }
        }

        #endregion

        #region Static & Const

        private static readonly TimeSpan LoginExpireTime = new TimeSpan(0, 30, 0); // 30 min

        private static readonly Dictionary<Int32, Int32> RoleLoggedUserLimit = new Dictionary<int, int>()
        {
            // { RoleId, LimitCount }
            {3, 2},
            {4, 2}
        };

        private static readonly int MaxUserNum = InputText.GetInt0(InputText.GetConfig("maxUser"));

        /// <summary>
        /// LoggedInDictionary =
        /// [
        ///     { Key: RoleId, Value: [LoginInfo, LoginInfo, LoginInfo ...] },
        ///     { Key: RoleId, Value: [LoginInfo, LoginInfo] },
        ///     { Key: RoleId, Value: [LoginInfo, LoginInfo, LoginInfo, LoginInfo ...] }
        /// ]
        /// </summary>
        public static readonly Dictionary<Int32, List<LoginInfo>> LoggedInDictionary = new Dictionary<int, List<LoginInfo>>();

        #endregion

        public static void UserLogin(String userCode)
        {
            LoginCounter.CheckLoggedUserExpired();

            // User login is not allowed between 0-8 A.M. 
            var datetimeNow = DateTime.Now;
            if (datetimeNow.Hour > 0 && datetimeNow.Hour < 8)
            {
                throw new Exception("0点-8点不允许登录");
            }


            var roleId = LoginCounter.GetUserRoleId(userCode);
            if (!LoginCounter.RoleLoggedUserLimit.ContainsKey(roleId))
            {
                // RoleGroup doesn't create in dictionary yet
                var loginInfos = new List<LoginInfo> { new LoginInfo(userCode, DateTime.Now) };
                if (!LoginCounter.LoggedInDictionary.ContainsKey(roleId))
                {
                    LoginCounter.LoggedInDictionary.Add(roleId, loginInfos);
                }
                else
                { 
                    var roleGroup = LoginCounter.LoggedInDictionary[roleId]; 
                    if (roleGroup.Exists(u => u.UserCode == userCode))
                    {
                        // user already in roleGroup
                        var user = roleGroup.First(u => u.UserCode == userCode);
                        // update user.LoginTime to Now
                        user.LoginTime = DateTime.Now;
                        // login successfully
                    }
                    else
                    {
                        roleGroup.Add(new LoginInfo(userCode, DateTime.Now));
                    }
                }
            }
            else
            {
                if (!LoginCounter.LoggedInDictionary.ContainsKey(roleId))
                {
                    var loginInfos = new List<LoginInfo> { new LoginInfo(userCode, DateTime.Now) };
                    LoginCounter.LoggedInDictionary.Add(roleId, loginInfos);
                }
                else
                {
                    var roleGroup = LoginCounter.LoggedInDictionary[roleId];
                    if (roleGroup.Exists(u => u.UserCode == userCode))
                    {
                        // user already in roleGroup
                        var user = roleGroup.First(u => u.UserCode == userCode);
                        // update user.LoginTime to Now
                        user.LoginTime = DateTime.Now;
                        // login successfully
                    }
                    else
                    {
                        // if limit is not set in dictionary, use Int32.MaxValue as default
                        // then following statment will always false
                        if (LoginCounter.LoggedInDictionary.Count >= MaxUserNum)
                        {
                            // users in group reach it's limit, current login action is not permitted
                            throw new GroupLoggedFullException();
                            // login failed
                        }
                        else
                        {
                            // limit not reached, add user to roleGroup in dictionary
                            roleGroup.Add(new LoginInfo(userCode, DateTime.Now));
                            // login succesfully
                        }
                    }
                }
            }
        }

        public static void UserLogout(String userCode)
        {
            var roleId = LoginCounter.GetUserRoleId(userCode);
            if (LoginCounter.LoggedInDictionary.ContainsKey(roleId))
            {
                var roleGroup = LoginCounter.LoggedInDictionary[roleId];
                try
                {
                    var user = roleGroup.First(u => u.UserCode == userCode);
                    roleGroup.Remove(user);
                }
                catch (ArgumentException)
                {
                    // specified User does not exists
                    // Do nothing
                }
            }
            else
            {
                // user does not exist in login dictionary
                // Do nothing
            }
        }

        /// <summary>
        /// Traversal LoggedInDictionary and remove loginInfos expired
        /// </summary>
        public static void CheckLoggedUserExpired()
        {
            foreach (var roleGroup in LoggedInDictionary)
            {
                foreach (var userInRole in roleGroup.Value)
                {
                    if (DateTime.Now >= userInRole.LoginTime + LoginExpireTime)
                    {
                        LoginCounter.UserLogout(userInRole.UserCode); 
                    }
                }
            }
        }


        /// <summary>
        /// Determine if user is logged out
        /// </summary>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static Boolean IsUserLoggedOut(String userCode)
        {
            try
            {
                var userRoleId = GetUserRoleId(userCode);
                LoginInfo userInfo = (LoginInfo)LoginCounter.LoggedInDictionary[userRoleId].Where(v => v.UserCode == userCode).FirstOrDefault();
                if (userInfo!=null)
                {
                    if (DateTime.Now >= userInfo.LoginTime + LoginExpireTime)
                    {
                        LoginCounter.UserLogout(userCode);
                        return true;
                    }
                    else 
                    {
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                // Exception throwed by GetUserRoleId(), means User not found
                return false;
            }
        }

        /// <summary>
        /// Determine if user is logged
        /// </summary>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static Boolean IsUserLogged(String userCode)
        {
            try
            {
                var userRoleId = GetUserRoleId(userCode);
                return LoginCounter.LoggedInDictionary[userRoleId].Exists(v => v.UserCode == userCode);
            }
            catch (ArgumentException)
            {
                // Exception throwed by GetUserRoleId(), means User not found
                return false;
            }
        }

        #region Private methods

        /// <summary>
        /// Get user's RoleId by user code
        /// </summary>
        /// <param name="userCode"></param>
        /// <returns>RoleId</returns>
        private static Int32 GetUserRoleId(String userCode)
        {
            var userDataSet = (new SqlHelper()).ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text,
                String.Format(@"SELECT [USER_ROLE] FROM [SYS_USER_INFO] WHERE [USER_Code]='{0}'", userCode));
            if (userDataSet == null || userDataSet.Tables[0].Rows.Count == 0)
            {
                // Cannot find specified user with the UserCode given
                throw new ArgumentException("用户不存在");
            }
            else
            {
                return Int32.Parse(userDataSet.Tables[0].Rows[0]["USER_ROLE"].ToString());
            }
        }

        #endregion
    }
}