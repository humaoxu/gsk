﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JSKWeb.Code
{
    public class JsAlertMessage
    {
        /// <summary>
        /// 没有设置MODELSeq
        /// </summary>
        public static string message1 = "the file is no Set ModelSeq";

        /// <summary>
        /// 已经进行import导入到BASE_CENSUS_LOAD
        /// </summary>
        public static string message2 = "import err";

        /// <summary>
        /// 已经进行import导入到BASE_CENSUS_LOAD
        /// </summary>
        public static string message3 = "import basecensus err";

        /// <summary>
        /// 操作成功
        /// </summary>
        public static string message4 = "Successful!";

        /// <summary>
        /// 操作失败
        /// </summary>
        public static string message5 = "Error!";

        /// <summary>
        /// 操作失败
        /// </summary>
        public static string message6 = "Please Check File,LinxCode have repeate data .Import basecensus table error.";

        public static string message7 = "Period is null.";

        public static string message8 = "Please select readRow.";

        public static string message9 = "Please select readTitle.";

        public static string message10 = "Fileseq not exist.";

        public static string message11 = "Please select uselessstore file first.";

        public static string message12 = "FiletypeErr,Please select 'xls','xlxs' file. ";

        public static string message13 = "FiletypeErr,Selected file must have census_linxcode,final_status columns.";

        public static string message14 = "选择字段数量与实际导入列数不符，请重新导入";


        public static string message15 = "字段名称不匹配，请重新导入";


        /// <summary>
        /// 必须设置SAMPLE CLIENT NAME
        /// </summary>
        public static string message16 = "Please input Client Name!";
        /// <summary>
        /// 请选择SAMPLE CLIENT FILE 
        /// </summary>
        public static string message17 = "Please select Client File!";
        /// <summary>
        /// Client Name 已存在
        /// </summary>
        public static string message18 = "The client Name already exists!";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message19 = "Please Set Filter";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message20 = "File Status err";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message21 = "Please select client first..";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message22 = "Please select department first..";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message23 = "Please select datasource first..";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message24 = "Please select readrow first..";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message25 = "Please select readtitle first..";

        /// <summary>
        /// 必须设置Filter 已存在
        /// </summary>
        public static string message26 = "upload file err..";
    }
}