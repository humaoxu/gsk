﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using JSKWeb.Code;
using System.Text;
using System.IO; 
using System.Web.UI.HtmlControls;
using System.Collections;
using JSKWeb.Utility;
using DBUtility;
namespace JSKWeb.Code
{
    public class CommonTitleBar   
    {
        SqlHelper sqlhelper = new SqlHelper();

        #region
        ///绘制Left菜单 (带参数)
        public string WriteLeft(string menu_id ,string userCode)
        {
            string parent_id = "";
            //获取parent_id
            SysMenuManager syMenuManager = new SysMenuManager();
            string strSQL = "select * from sys_menu   where menu_id ='" + menu_id + "' order by order_id";
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                parent_id = dr["parent_id"].ToString();
            }

            StringBuilder sbMenu = new StringBuilder();

            DataSet dsMenu = new DataSet();

            strSQL = "select * from sys_user_info where is_admin ='1' and user_name='" + userCode + "'";
            DataSet dsuser = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);
            if (dsuser == null || dsuser.Tables[0].Rows.Count == 0)
            {
                dsMenu = syMenuManager.GetUserMenu(userCode);
            }
            else
            {
                strSQL = "select * from sys_menu    ";
                dsMenu = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);
            }

            if (dsMenu != null && dsMenu.Tables[0].Rows.Count > 0)
            {
                DataTable dtMenu = dsMenu.Tables[0];
                //顶级菜单
                DataRow[] drTopMenu = dtMenu.Select("parent_id='0'", "order_id");
                if (drTopMenu.Count() > 0)
                {
                    int i = 0;
                    foreach (DataRow dr in drTopMenu)
                    {
                        if (i == 0)
                        {
                            sbMenu.Append("<li >");
                        }
                        else
                        {
                            sbMenu.Append("<li >");
                        }
                        //sbMenu.Append(" <a href='" + dr["linkurl"] + "'><span class='txt'>" + dr["module_name"] + "</span></a><span class='arrow'></span></div>");
                        sbMenu.Append(" <a href='" + dr["linkurl"] + "'> " + dr["module_name"] + " </a> ");
                        if (dr["menu_id"].ToString() == parent_id)
                        {
                            sbMenu.Append(WriteSubMenu(dr["menu_id"].ToString(), dtMenu, i, menu_id));
                        }
                        else
                        {
                            sbMenu.Append(WriteSubMenu_N(dr["menu_id"].ToString(), dtMenu, i, menu_id));
                        }
                        sbMenu.Append("</li>");
                        i++;
                    }
                }
            }

            return sbMenu.ToString();
        }

        /// <summary>
        /// 绘制子菜单(重新加载)
        /// </summary>
        /// <param name="fatherId"></param>
        /// <param name="dsMenu"></param>
        /// <returns></returns>
        public static string WriteSubMenu(string fatherId, DataTable dsMenu, int j, string menu_name)
        {
            try
            {
                string strSubMenu = "";
                DataRow[] drSubMenu = dsMenu.Select("parent_id='" + fatherId + "'", "order_id");

                strSubMenu += "<ul >";

                int i = 0;
                foreach (DataRow dr in drSubMenu)
                {
                    if (i == 0)
                    {
                        if (dr["linkurl"].ToString().Contains("sve_loading"))
                        {
                            strSubMenu += "<li   ><a href='" + dr["linkurl"] + "' target='_blank'>" + dr["module_name"] + "</a></li>";
                        }
                        else
                        {
                            strSubMenu += "<li  ><a href='" + dr["linkurl"] + "'>" + dr["module_name"] + "</a></li>";
                        }
                    }
                    else
                    {
                        if (dr["linkurl"].ToString().Contains("sve_loading"))
                        {
                            strSubMenu += "<li><a href='" + dr["linkurl"] + "' target='_blank'>" + dr["module_name"] + "</a></li>";
                        }
                        else
                        {
                            strSubMenu += "<li><a href='" + dr["linkurl"] + "'>" + dr["module_name"] + "</a></li>";
                        }
                    }
                    i++;
                }
                strSubMenu += "</ul>";
                return strSubMenu;
            }
            catch (Exception)
            {

                throw;
            }
            return "";
        }

        public static string WriteSubMenu_N(string fatherId, DataTable dsMenu, int j, string menu_name)
        {
            try
            {
                string strSubMenu = "";
                DataRow[] drSubMenu = dsMenu.Select("parent_id='" + fatherId + "'", "order_id");

                strSubMenu += "<ul >";

                int i = 0;
                foreach (DataRow dr in drSubMenu)
                {
                    if (i == 0)
                    {
                        if (dr["linkurl"].ToString().Contains("sve_loading"))
                        {
                            strSubMenu += "<li  ><a href='" + dr["linkurl"] + "' target='_blank'>" + dr["module_name"] + "</a></li>";
                        }
                        else
                        {
                            strSubMenu += "<li ><a href='" + dr["linkurl"] + "'>" + dr["module_name"] + "</a></li>";
                        }
                    }
                    else
                    {
                        if (dr["linkurl"].ToString().Contains("sve_loading"))
                        {
                            strSubMenu += "<li><a href='" + dr["linkurl"] + "' target='_blank'>" + dr["module_name"] + "</a></li>";
                        }
                        else
                        {
                            strSubMenu += "<li><a href='" + dr["linkurl"] + "'>" + dr["module_name"] + "</a></li>";
                        }
                    }
                    i++;
                }
                strSubMenu += "</ul>";
                return strSubMenu;
            }
            catch (Exception)
            {

                throw;
            }
            return "";
        }
        #endregion
    }
}