﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="DDT_Index.aspx.cs" Inherits="JSKWeb.DDT_Index" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div style="margin: 0px; width: 100%; height: 100%">
        <table width="100%">
            <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>分数报告</span><a href="REPORTDDT/RptDDTSCORE.aspx"><img width="140px" height="140px" src="otcimg/icon01.png" /></a></li>
                            <%--<li class="smallbutton"><span>区域呈现</span><a href="OTCREPORT/OTC_CITY.aspx"><img  width="140px" height="140px" src="otcimg/icon02.png" /></a></li>--%>
                            <li class="smallbutton"><span>指标说明</span><a href="http://106.14.254.140:8080/ddt_help.pptx"><img  width="140px" height="140px" src="otcimg/help.png" /></a></li>
                            
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>产品分销</span><a href="REPORTDDT/DDTQG_Fx.aspx"><img  width="140px" height="140px"  src="otcimg/icon03.png" /></a></li>
                            <li class="smallbutton"><span>产品分销率</span><a href="REPORTDDT/DDTQG_Fxlv.aspx"><img  width="140px" height="140px"  src="otcimg/ddt01.png" /></a></li>
                            <li class="smallbutton"><span>立招</span><a href="REPORTDDT/DDTQG_Lz.aspx"><img  width="140px" height="140px"  src="otcimg/icon07.png" /></a></li>
                            <li class="smallbutton"><span>立招陈列率</span><a href="REPORTDDT/DDTQG_LzClv.aspx"><img  width="140px" height="140px"  src="otcimg/ddt01.png" /></a></li>
                            <li class="smallbutton"><span>产品活跃</span><a href="REPORTDDT/DDTQG_Hy.aspx"><img  width="140px" height="145px"  src="otcimg/ddt03.png" /></a></li>
                            <li class="smallbutton"><span>产品活跃率</span><a href="REPORTDDT/DDTQG_Hylv.aspx"><img  width="140px" height="140px"  src="otcimg/ddt01.png" /></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
