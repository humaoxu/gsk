﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true"
    CodeBehind="SysTicketReBack.aspx.cs" Inherits="JSKWeb.Ticket.SysTicketReBack" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                document.getElementById("<%=hfstorecode.ClientID %>").value = akey;
                document.getElementById("<%=hfdateid.ClientID %>").value = $(row.cells[2]).text();
                document.getElementById("<%=hfRound.ClientID %>").value = $(row.cells[1]).text();
                // row.cells[0].innerHTML = "<input type=\"radio\" value=\"\" name=\"rd\" checked='checked' />";
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {
                var json = { "oo": [
                { "url": "Application_Add.aspx", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                { "url": "Application_Edit.aspx?ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                {},
                { "url": "Logic_Rule.aspx?TYPE=Default&ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" }
                ]

                };

                if (index == 0) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 1) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 2) {
                    return confirm("Confirm delete？");
                } else if (index == 3) {
                    return confirm("Confirm Run？");
                } else if (index == 5) {//Set out
                    var result = window.open(json.oo[4].url + key, window, json.oo[4].parm);

                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                else {
                    return false;
                }
            }
        }
    
    </script>
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>筛选条件</legend>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="">
            <tbody>
               <tr>
                    <td align="right" width="80px" class="contrlFontSize">
                        项目：
                    </td>
                    <td align="left" width="180px">
                        <asp:DropDownList ID="ddlProject" CssClass="inputtext15" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="ddlProject_OnSelectedIndexChanged">
                        </asp:DropDownList>
                    </td>

                    <td align="right" width="80px" class="contrlFontSize">
                        问卷期数：
                    </td>
                    <td align="left" width="180px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        状态：
                    </td>
                    <td align="left" >
                        <asp:DropDownList ID="ddlZhuangtai" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right" width="80px" class="contrlFontSize">
                        店铺编码： 
                    </td>
                    <td align="left"  width="180px" >
                        <asp:TextBox ID="txtStoreCode" CssClass="inputtext15" runat="server" Width="157px"></asp:TextBox>
                    </td>
                       <td align="right" width="80px" class="contrlFontSize">
                        门店名称：
                    </td>
                    <td align="left" width="250px">
                        <asp:TextBox ID="txtStoreName" CssClass="inputtext15" runat="server" Width="157px"></asp:TextBox>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize"  >
                        页面行数： </td>
                    <td align="left" >
                        <asp:TextBox ID="txtPageSise" CssClass="inputtext15" runat="server" Width="40px">20</asp:TextBox>
                    &nbsp;&nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn" 
                            runat="server"  Text="查询" Width="80px" /> &nbsp;   <asp:Button ID="btnOutPut"  CssClass="btn" 
                            runat="server"  Text="导出" Width="80px" OnClick="btnOutPut_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="dataMian">
        <div>
            <cc1:GridViewKit ID="gvMain" runat="server" AutoGenerateColumns="False" Width="100%"
                OnRowDataBound="gvMain_RowDataBound" Font-Size="9pt" CssClass="tablestyle" EnableEmptyContentRender="True"
                CellPadding="2">
                <RowStyle />
                <Columns>
                    <asp:BoundField DataField="shopcode" HeaderText="店铺编码">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="shopname" HeaderText="店铺名称">
                        <HeaderStyle HorizontalAlign="Center" Width="350px"  />
                        <ItemStyle HorizontalAlign="left" Width="350px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="t_score" HeaderText="得分">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="right" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="submitdate" HeaderText="上传时间">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="id" HeaderText="申诉票据No">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="csdate" HeaderText="申诉时间">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="godate" HeaderText="反馈时间">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="backuser" HeaderText="处理人">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Ticketstaus" HeaderText="状态">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="操作">
                        <HeaderStyle HorizontalAlign="Center"  Width="150px"  />
                        <ItemStyle HorizontalAlign="left"  Width="150px"  />
                        <ItemTemplate  > 
                            &nbsp;<asp:HyperLink ID="lbtnMingxi" runat="server">问卷明细</asp:HyperLink>&nbsp;&nbsp;
                            <asp:HyperLink ID="lbtnShensu"   runat="server">申诉反馈</asp:HyperLink> 
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle />
                <AlternatingRowStyle />
            </cc1:GridViewKit>
        </div>
        <div>
            <asp:HiddenField ID="hfMinDate" runat="server" />
            <asp:HiddenField ID="hfMaxDate" runat="server" />
            <asp:HiddenField ID="hfstorecode" runat="server" />
            <asp:HiddenField ID="hfdateid" runat="server" />
            <asp:HiddenField ID="hfKey" runat="server" />
            <asp:HiddenField ID="hfRound" runat="server" />
            <webdiyer:AspNetPager ID="Pager" runat="server" HorizontalAlign="Right" Width="100%"
                Style="font-size: 14px; color: Black;" AlwaysShow="true" FirstPageText="137"
                LastPageText="138" NextPageText="131" PrevPageText="132" SubmitButtonClass=""
                CustomInfoStyle="font-size:12px;text-align:Left;" TextBeforeInputBox="" TextAfterInputBox=""
                PageIndexBoxType="TextBox" ShowPageIndexBox="Always" TextAfterPageIndexBox="页"
                TextBeforePageIndexBox="转到" Font-Size="12px" ShowCustomInfoSection="Left" CustomInfoSectionWidth=""
                PagingButtonSpacing="3px" CustomInfoHTML="" CssClass="paginator" CurrentPageButtonClass="cpb"
                ShowInputBox="Never" CustomInfoClass="" OnPageChanged="pager_PageChanged" ButtonImageExtension=".png"
                ImagePath="~/images/" PagingButtonType="Image" NumericButtonType="Text" MoreButtonType="Text">
            </webdiyer:AspNetPager>
        </div>
        <div style="display: none;" runat="server" id="viewstatesql">
        </div>
    </div>
</asp:Content>
