﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;

namespace JSKWeb.Ticket
{
    public partial class SysTicket_FMCG : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        int iPageSize = 30;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_fmcg");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}  and isReleased = 1 order by paperId desc ", ddlProjectId);
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}   order by paperId desc ", ddlProjectId);
            }

            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            ///绑定状态
            string sql = @" select * from v_PubReference where parentcode='TickCode' ";
            DataSet ds = new DataSet();
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            ddlZhuangtai.DataSource = ds;
            ddlZhuangtai.DataValueField = "code";
            ddlZhuangtai.DataTextField = "name";
            ddlZhuangtai.DataBind();
            ddlZhuangtai.Items.Insert(0, new ListItem("全部", "NULL"));
            ddlZhuangtai.SelectedValue = "NULL";

            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            var select_sql2 = string.Empty;
            DataSet select_ds2 = new DataSet();


            ddltype0.DataSource = null;
            ddlType1.DataSource = null;
            ddlType2.DataSource = null;
            ddlType3.DataSource = null;

            select_sql2 = "SELECT DISTINCT COL5 CODE,COL5 NAME  FROM  SURVEYOBJ WHERE PROJECTID=12 AND COL5 IS NOT NULL ";
            select_ds2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql2);
            if (select_ds2 != null)
            {
                ddltype0.DataSource = select_ds2;
                ddltype0.DataValueField = "code";
                ddltype0.DataTextField = "name";
                ddltype0.DataBind();
            }

            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                //Loading...
                select_sql = "SELECT DISTINCT COL21 CODE,COL22 NAME  FROM  SURVEYOBJ WHERE PROJECTID=12 AND COL21 IS NOT NULL ";
                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType1.DataSource = select_ds;
                    ddlType1.DataValueField = "code";
                    ddlType1.DataTextField = "name";
                    ddlType1.DataBind();
                }
            }
            else
            {
                //Loading...
                select_sql = "SELECT DISTINCT SURVEYOBJ.COL21 CODE,SURVEYOBJ.COL22 NAME  FROM  SURVEYOBJ inner join (select * from V_FMCG_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and SURVEYOBJ.PROJECTID=12 AND SURVEYOBJ.COL21 IS NOT NULL ";
                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType1.DataSource = select_ds;
                    ddlType1.DataValueField = "code";
                    ddlType1.DataTextField = "name";
                    ddlType1.DataBind();
                }
            }
            ddltype0.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType1.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        private bool checkDateTimeFormat(string txtTime)
        {
            try
            {
                string sDate = txtTime.Substring(0, 4) + "-" + txtTime.Substring(4, 2) + "-" + txtTime.Substring(6, 2);
                DateTime.Parse(sDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void LoadInfo()
        {
            Pager.PageSize = iPageSize;
            int recCnt;

            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }

                //modify
                if (ddlZhuangtai.SelectedIndex > 0)
                {
                    ViewState["whereName"] += " and Ticketstaus ='" + ddlZhuangtai.SelectedItem.Text.Trim() + "' ";
                }

                if (txtStoreCode.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopcode like'%" + txtStoreCode.Text + "%' ";
                }

                if (txtStoreName.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopname like'%" + txtStoreName.Text + "%' ";
                }

                string strUsercode = this._LinxSanmpleUserCode;

                //dr 添加筛选

                if (!ddlType3.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType3.SelectedValue;
                }

                else if (!ddlType2.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType2.SelectedValue;
                }

                else if (!ddlType1.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType1.SelectedValue;
                }

                string strType = "";
                if (!ddltype0.SelectedItem.Text.Equals("全部"))
                {

                    strType = ddltype0.SelectedValue;
                }

                viewstatesql.InnerText = ViewState["whereName"].ToString();

                StringBuilder sb = new StringBuilder();
                sb.Append(@"(");
                sb.Append("select shopid, shopcode, shopname, paperId, paperTitle, submitdate, scdate, resultId,t_score,SSDFX,BLJFX");
                sb.Append(",(case when YZSFX is null then '-' when YZSFX = '' then '-' else convert(varchar(20),YZSFX) end) YZSFX ");
                //sb.Append(",(case when BLJPM is null then '-' when BLJPM = '' then '-' else convert(varchar(20),BLJPM) end) BLJPM ");
                sb.Append(",BLJPM as BLJPM ");

                sb.Append(",SSDPM ");
                sb.Append(",(case when YZSPM is null then '-' when YZSPM = '' then '-' else  convert(varchar(20),YZSPM) end) YZSPM ");
                sb.Append(", Ticketstaus,  t_status_cd,  t_id,  csdate,  godate,projectId, releaseDate  ");
                sb.Append("from (");
                if (strUsercode.ToUpper().Trim() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb.Append(" select V_FMCG_RESULT.* from V_FMCG_RESULT  ");
                    sb.Append(" where V_FMCG_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                    sb.Append("' ");
                    if (strType != "")
                    {
                        sb.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                    }
                    sb.Append(@") tb ) a ");
                }
                else
                {
                    sb.Append(" select V_FMCG_RESULT.* from V_FMCG_RESULT inner join (select * from V_FMCG_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on V_FMCG_RESULT.shopcode = VUSER.shopcode ");
                    sb.Append(" where V_FMCG_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                    if (this._LinxSanmpleUserCode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                    {
                        sb.Append("'  ");
                        if (strType != "")
                        {
                            sb.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                        }

                    }
                    else
                    {
                        sb.Append("' and V_FMCG_RESULT.shopcode in (select shopcode from V_FMCG_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");
                        if (strType != "")
                        {
                            sb.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                        }
                    }
                    sb.Append(@") tb ) a ");
                }
                
                StringBuilder sb1 = new StringBuilder();
                sb1.Append(@"(");
                sb1.Append("select * from (");
                if (strUsercode.ToUpper().Trim() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb1.Append(" select V_FMCG_RESULT.* from V_FMCG_RESULT  ");
                    sb1.Append(" where V_FMCG_RESULT.paperId ='");
                    sb1.Append(ddlTicketType.SelectedItem.Value.ToString());
                    sb1.Append("' ");
                    if (strType != "")
                    {
                        sb1.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                    }
                    sb1.Append(@") tb ) a ");
                }
                else
                {
                    sb1.Append(" select V_FMCG_RESULT.* from V_FMCG_RESULT inner join (select * from V_FMCG_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on V_FMCG_RESULT.shopcode = VUSER.shopcode ");
                    sb1.Append(" where V_FMCG_RESULT.paperId ='");
                    sb1.Append(ddlTicketType.SelectedItem.Value.ToString());
                    if (this._LinxSanmpleUserCode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                    {
                        sb1.Append("'  ");
                        if (strType != "")
                        {
                            sb1.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                        }

                    }
                    else
                    {
                        sb1.Append("' and V_FMCG_RESULT.shopcode in (select shopcode from V_FMCG_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");
                        if (strType != "")
                        {
                            sb1.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                        }
                    }
                    sb1.Append(@") tb ) a ");
                }

                int pageindex = 1;
                if (!IsPostBack)
                {
                    if (Request.QueryString["PAGENUM"] != null)
                    {
                        pageindex = Convert.ToInt32(Request.QueryString["PAGENUM"].ToString());
                    }
                }
                else
                {
                    pageindex = this.Pager.CurrentPageIndex;
                }

                DataTable dt = sqlhelper.GetPagingRecord(sb.ToString(), "releaseDate desc", Pager.PageSize, pageindex, ViewState["whereName"].ToString(), "*", out recCnt);
                Pager.RecordCount = recCnt;
                this.Pager.CurrentPageIndex = pageindex;
                Pager.Visible = recCnt > 0;

                this.Pager.CustomInfoHTML = string.Format(CommonStirng.StrPagerCustomerInfo, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount, this.Pager.RecordCount, iPageSize });
                gvMain.DataSource = dt;
                gvMain.DataBind();

                //获取所有的数据值,
                string ResultString = string.Empty;
                ResultString = sqlhelper.GetFMCGResult(sb1.ToString(), "releaseDate desc", ViewState["whereName"].ToString(), "*", out ResultString);
                if (ResultString.Equals(""))
                {
                    lblCount.Text = "No Data!";
                }
                else
                {
                    lblCount.Text = ResultString;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["resultId"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");

                HyperLink btnsm = (HyperLink)e.Row.FindControl("lbtnMingxi");
                StringBuilder sb = new StringBuilder();
                sb.Append(InputText.GetConfig("gsk_url"));
                sb.Append("resultId=");
                sb.Append(drv["resultId"].ToString());
                sb.Append("&paperId=");
                sb.Append(drv["paperId"].ToString());
                sb.Append("&nonedate=1");
                sb.Append("&isUser=1");
                btnsm.NavigateUrl = @"javascript:void window.open('" + sb.ToString() + "')";
                
                DateTime dtS = InputText.GetDateTime(drv["releaseDate"].ToString());
                DateTime dtS_CFG = InputText.GetDateTime(InputText.GetConfig("gsk_su_fmcg_date")); 
                DateTime d_tmp;
                if (dtS > dtS_CFG)
                {
                    d_tmp = dtS;
                }
                else
                {
                    d_tmp = dtS_CFG;
                } 
                string strPaper = InputText.GetConfig("gsk_su_fmcg_paper");
                string strflg = InputText.GetConfig("gsk_su_fmcg").ToString().Trim();

                //if (ddlTicketType.SelectedItem.Value.ToString() == strPaper && strflg == "1" && (InputText.GetInt(InputText.GetConfig("gsk_su_fmcg_days")) > DateTime.Compare(DateTime.Now, dtS)))
                if (ddlTicketType.SelectedItem.Value.ToString() == strPaper && strflg == "1" )
                {
                    ///屏蔽申诉 如果 超时则屏蔽 或 已申诉
                    if(drv["Ticketstaus"].ToString().Trim() != "未申诉") 
                    {
                        ///屏蔽申诉
                        HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                        btn.Visible = false;
                    }
                    else
                    {
                        ///申诉url
                        HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                        btn.Visible = true;
                        string strUrlsu = "SysTicketDet_FMCG.aspx?resultId=" + drv["resultId"] + "&projectId=" + drv["projectId"];
                        btn.NavigateUrl = @"javascript:void window.open('" + strUrlsu + "')";
                    }
                }
                else
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                    btn.Visible = false;
                }
                

                if (drv["Ticketstaus"].ToString().Trim() == "已处理")
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = true;
                    btn.NavigateUrl = @"javascript:void window.open('SysTicketReBackDet.aspx?resultId=" + drv["resultId"].ToString() + "')";
                }
                else
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = false;
                }
            }
        }

        protected void ddlType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType2.DataSource = null;
            ddlType3.DataSource = null;

            ddlType2.Items.Clear();
            ddlType3.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = " SELECT DISTINCT  COL19 CODE,COL20 NAME  FROM SURVEYOBJ  WHERE PROJECTID=12 AND COL19 IS NOT NULL   and surveyObjId in (select surveyObjId from result where paperid = " + ddlTicketType.SelectedValue + "  ) ";
                if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
                {

                    select_sql = select_sql + " and COL21 = '" + ddlType1.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType2.DataSource = select_ds;
                        ddlType2.DataValueField = "code";
                        ddlType2.DataTextField = "name";
                        ddlType2.DataBind();
                    }
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL19 CODE,SURVEYOBJ.COL20 NAME  FROM SURVEYOBJ   inner join (select * from V_FMCG_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=12 AND SURVEYOBJ.COL19 IS NOT NULL   and SURVEYOBJ.surveyObjId in (select surveyobjid from RESULT where paperid = " + ddlTicketType.SelectedValue + "  ) ";
                if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and SURVEYOBJ.COL21 = '" + ddlType1.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType2.DataSource = select_ds;
                        ddlType2.DataValueField = "code";
                        ddlType2.DataTextField = "name";
                        ddlType2.DataBind();
                    }
                }
            }


            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlType3.DataSource = null;

            ddlType3.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  COL17 CODE,COL18 NAME  FROM SURVEYOBJ  WHERE PROJECTID=12 AND COL17 IS NOT NULL AND COL18 <> '0'   and surveyObjId in (select surveyobjid from RESULT where paperid = " + ddlTicketType.SelectedValue + "  ) ";
                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and COL21 = '" + ddlType1.SelectedValue + "' and COL19 = '" + ddlType2.SelectedValue + "' ";


                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();

                    }
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL17 CODE,SURVEYOBJ.COL18 NAME  FROM SURVEYOBJ   inner join (select * from V_FMCG_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=12 AND SURVEYOBJ.COL17 IS NOT NULL  AND SURVEYOBJ.COL18 <> '0'    and SURVEYOBJ.surveyObjId in (select surveyobjid from RESULT where paperid = " + ddlTicketType.SelectedValue + "  ) ";
                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and SURVEYOBJ.COL21 = '" + ddlType1.SelectedValue + "'  and SURVEYOBJ.COL19 = '" + ddlType2.SelectedValue + "' ";
                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();
                    }
                }
            }
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void btnOutPut_Click(object sender, EventArgs e)
        {
            ViewState["whereName"] = null;

            if (ViewState["whereName"] == null)
            {
                ViewState["whereName"] = "1=1 ";
            }

            //modify
            if (ddlZhuangtai.SelectedIndex > 0)
            {
                ViewState["whereName"] += " and Ticketstaus ='" + ddlZhuangtai.SelectedItem.Text.Trim() + "' ";
            }

            if (txtStoreCode.Text.Trim().Length > 0)
            {
                ViewState["whereName"] += " and shopcode like'%" + txtStoreCode.Text + "%' ";
            }

            if (txtStoreName.Text.Trim().Length > 0)
            {
                ViewState["whereName"] += " and shopname like'%" + txtStoreName.Text + "%' ";
            }

            string strUsercode = this._LinxSanmpleUserCode;

            //dr 添加筛选

            if (!ddlType3.SelectedItem.Text.Equals("全部"))
            {

                strUsercode = ddlType3.SelectedValue;
            }

            else if (!ddlType2.SelectedItem.Text.Equals("全部"))
            {

                strUsercode = ddlType2.SelectedValue;
            }

            else if (!ddlType1.SelectedItem.Text.Equals("全部"))
            {

                strUsercode = ddlType1.SelectedValue;
            }

            string strType = "";
            if (!ddltype0.SelectedItem.Text.Equals("全部"))
            {

                strType = ddltype0.SelectedValue;
            }

            viewstatesql.InnerText = ViewState["whereName"].ToString();

            StringBuilder sb = new StringBuilder();
           
            sb.Append("select shopcode as 店铺编码,shopname as 门店名称,t_score as 总分,SSDFX as 舒适达分销,BLJFX as 保丽净分销,SSDPM as 舒适达排面,BLJPM as 保丽净排面,YZSPM as 益周适排面 ,csdate as 申诉时间 ,godate as 反馈时间,Ticketstaus as 申诉状态  from (");
            if (strUsercode.ToUpper().Trim() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
            {
                sb.Append(" select V_FMCG_RESULT.* from V_FMCG_RESULT  ");
                sb.Append(" where V_FMCG_RESULT.paperId ='");
                sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                sb.Append("' ");
                if (strType != "")
                {
                    sb.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                }
                sb.Append(@") tb ");
            }
            else
            {
                sb.Append(" select V_FMCG_RESULT.* from V_FMCG_RESULT inner join (select * from V_FMCG_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on V_FMCG_RESULT.shopcode = VUSER.shopcode ");
                sb.Append(" where V_FMCG_RESULT.paperId ='");
                sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                if (this._LinxSanmpleUserCode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb.Append("'  ");
                    if (strType != "")
                    {
                        sb.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                    }

                }
                else
                {
                    sb.Append("' and V_FMCG_RESULT.shopcode in (select shopcode from V_FMCG_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");
                    if (strType != "")
                    {
                        sb.Append(" and V_FMCG_RESULT.shopcode in (select objcode from SurveyObj where projectid = 12 and col5= '" + strType + "') ");
                    }
                }
                sb.Append(@") tb  ");
            }


            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string fileName = "FMCG申诉操作手册.pptx";//客户端保存的文件名
            string filePath = Server.MapPath("/" + "FMCG.pptx");//路径

            //以字符流的形式下载文件
            FileStream fs = new FileStream(filePath, FileMode.Open);
            byte[] bytes = new byte[(int)fs.Length];
            fs.Read(bytes, 0, bytes.Length);
            fs.Close();
            Response.ContentType = "application/octet-stream";
            //通知浏览器下载文件而不是打开
            Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}