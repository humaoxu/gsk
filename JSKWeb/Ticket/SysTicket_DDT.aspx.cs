﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;

namespace JSKWeb.Ticket
{
    public partial class SysTicket_DDT : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        int iPageSize = 30;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_ddt");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0} and isReleased = 1 order by paperId desc ", ddlProjectId);
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}  order by paperId desc ", ddlProjectId);
            }

            //var ddlTicketTypeSelectSql =  String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}  order by paperId desc ", ddlProjectId);

            //
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            ///绑定状态
            string sql = @" select * from v_PubReference where parentcode='TickCode' ";
            DataSet ds = new DataSet();
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            ddlZhuangtai.DataSource = ds;
            ddlZhuangtai.DataValueField = "code";
            ddlZhuangtai.DataTextField = "name";
            ddlZhuangtai.DataBind();
            ddlZhuangtai.Items.Insert(0, new ListItem("全部", "NULL"));
            ddlZhuangtai.SelectedValue = "NULL";

            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();
            
            var select_sql2 = string.Empty;
            DataSet select_ds2 = new DataSet();
            
            ddlType1.DataSource = null;
            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                //Loading...
                select_sql = "SELECT DISTINCT col13 CODE,col13 NAME  FROM  SURVEYOBJ WHERE PROJECTID=13 AND col13 IS NOT NULL and col13 <>'' ";
                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType1.DataSource = select_ds;
                    ddlType1.DataValueField = "code";
                    ddlType1.DataTextField = "name";
                    ddlType1.DataBind();
                }

                //select_sql2 = "SELECT DISTINCT col26 CODE,col27 NAME  FROM  SURVEYOBJ WHERE PROJECTID=13 AND col26 IS NOT NULL ";
                select_sql2 = "SELECT DISTINCT col27 CODE,col27 NAME  FROM  SURVEYOBJ WHERE PROJECTID=13 AND col26 IS NOT NULL  and col13 <>'' ";
                select_ds2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql2);
                if (select_ds2 != null)
                {
                    ddlType2.DataSource = select_ds2;
                    ddlType2.DataValueField = "code";
                    ddlType2.DataTextField = "name";
                    ddlType2.DataBind();
                }
            }
            else
            {
                //Loading...
                select_sql = "SELECT DISTINCT SURVEYOBJ.col13 CODE,SURVEYOBJ.col13 NAME  FROM  SURVEYOBJ inner join (select * from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and SURVEYOBJ.PROJECTID=13 AND SURVEYOBJ.col13 IS NOT NULL  and SURVEYOBJ.col13 <>'' ";
                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType1.DataSource = select_ds;
                    ddlType1.DataValueField = "code";
                    ddlType1.DataTextField = "name";
                    ddlType1.DataBind();
                }

                //select_sql2 = "SELECT DISTINCT SURVEYOBJ.col26 CODE,SURVEYOBJ.col27 NAME  FROM  SURVEYOBJ inner join (select * from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and SURVEYOBJ.PROJECTID=13 AND SURVEYOBJ.col26 IS NOT NULL ";
                select_sql2 = "SELECT DISTINCT SURVEYOBJ.col27 CODE,SURVEYOBJ.col27 NAME  FROM  SURVEYOBJ inner join (select * from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and SURVEYOBJ.PROJECTID=13 AND SURVEYOBJ.col26 IS NOT NULL and  SURVEYOBJ.col13 <>''  ";
                select_ds2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql2);
                if (select_ds2 != null)
                {
                    ddlType2.DataSource = select_ds2;
                    ddlType2.DataValueField = "code";
                    ddlType2.DataTextField = "name";
                    ddlType2.DataBind();
                }
            }
            ddlType1.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }

        private bool checkDateTimeFormate(string txtTime)
        {
            try
            {
                DateTime.Parse(txtTime);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool checkDateTimeFormat(string txtTime)
        {
            try
            {
                string sDate = txtTime.Substring(0, 4) + "-" + txtTime.Substring(4, 2) + "-" + txtTime.Substring(6, 2);
                DateTime.Parse(sDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void LoadInfo()
        {
            Pager.PageSize = iPageSize;
            int recCnt;

            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }

                //modify
                if (ddlZhuangtai.SelectedIndex > 0)
                {
                    ViewState["whereName"] += " and Ticketstaus ='" + ddlZhuangtai.SelectedItem.Text.Trim() + "' ";
                }

                if (txtStoreCode.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopcode like'%" + txtStoreCode.Text.Replace("'", "").Replace(";", "").Replace("(", "").Replace(")", "").Replace("*", "") + "%' ";
                }

                if (txtStoreName.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopname like'%" + txtStoreName.Text.Replace("'", "").Replace(";", "").Replace("(", "").Replace(")", "").Replace("*", "") + "%' ";
                }

                
                string strUsercode = this._LinxSanmpleUserCode;

                //dr 添加筛选
                
                if (!ddlType4.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType4.SelectedValue;
                }

                else if (!ddlType3.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType3.SelectedValue;
                }

                else if (!ddlType2.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType2.SelectedValue;
                }

                string strType = "";
                if (!ddlType1.SelectedItem.Text.Equals("全部"))
                {

                    strType = ddlType1.SelectedValue;
                }

                viewstatesql.InnerText = ViewState["whereName"].ToString();
                //string tableSql = @"(  select * from V_DDT_SCROERESULT where paperId ='" + ddlTicketType.SelectedItem.Value + "' " + ") a ";



                StringBuilder sb1 = new StringBuilder();
                sb1.Append(@"(");
                sb1.Append("select shopid, shopcode, shopname, paperId, paperTitle, submitdate, scdate, resultId, t_score, ");
                sb1.Append("(case when SSDPOSM is null then '-' else convert(varchar(20), SSDPOSM) end) SSDPOSM ,");
                sb1.Append("(case when BLJPOSM is null then '-'  else convert(varchar(20), BLJPOSM) end) BLJPOSM ,");
                sb1.Append("(case when SSDFX is null then '-'  else convert(varchar(20), SSDFX) end) SSDFX ,");
                sb1.Append("(case when BLJFX is null then '-'   else convert(varchar(20), BLJFX) end) BLJFX ,");
                sb1.Append("(case when SSDHR is null then '-'   else convert(varchar(20), SSDHR) end) SSDHR ,");
                sb1.Append("(case when BLJHR is null then '-'  else convert(varchar(20), BLJHR) end) BLJHR ,");
                sb1.Append("(case when BLJWGJFX is null then '-'  else convert(varchar(20), BLJWGJFX) end) BLJWGJFX ,");
                sb1.Append("(case when BLJQJPFX is null then '-'  else convert(varchar(20), BLJQJPFX) end) BLJQJPFX ,");
                sb1.Append("   Ticketstaus,  t_status_cd,"); 
                sb1.Append("t_id, csdate, godate");
                sb1.Append(", projectId, releaseDate from (");
                if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb1.Append(" select V_DDT_RESULT.* from V_DDT_RESULT  ");
                    sb1.Append(" where V_DDT_RESULT.paperId ='");
                    sb1.Append(ddlTicketType.SelectedItem.Value.ToString());
                    sb1.Append("' ");
                    if (strType != "")
                    {
                        sb1.Append(" and V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                    }
                    sb1.Append(@") tb ");
                }
                else
                {
                    sb1.Append(" select V_DDT_RESULT.* from V_DDT_RESULT inner join (select * from V_DDT_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on V_DDT_RESULT.shopcode = VUSER.shopcode ");
                    sb1.Append(" where V_DDT_RESULT.paperId ='");
                    sb1.Append(ddlTicketType.SelectedItem.Value.ToString());
                    if (this._LinxSanmpleUserCode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                    {
                        sb1.Append("'  ");
                        if (strType != "")
                        {
                            sb1.Append(" and  V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                        }

                    }
                    else
                    {
                        sb1.Append("' and V_DDT_RESULT.shopcode in (select shopcode from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");

                        if (strType != "")
                        {
                            sb1.Append(" and  V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                        }

                    }
                    sb1.Append(@") tb  ");
                }
                sb1.Append(" ) a ");


                StringBuilder sb = new StringBuilder();
                sb.Append(@"(");
                sb.Append("select * from (");
                if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb.Append(" select V_DDT_RESULT.* from V_DDT_RESULT  ");
                    sb.Append(" where V_DDT_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                    sb.Append("' ");
                    if (strType != "")
                    {
                        sb.Append(" and V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                    }
                    sb.Append(@") tb ");
                }
                else
                {
                    sb.Append(" select V_DDT_RESULT.* from V_DDT_RESULT inner join (select * from V_DDT_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on V_DDT_RESULT.shopcode = VUSER.shopcode ");
                    sb.Append(" where V_DDT_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                    if (this._LinxSanmpleUserCode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                    {
                        sb.Append("'  ");
                        if (strType != "")
                        {
                            sb.Append(" and  V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                        }

                    }
                    else
                    {
                        sb.Append("' and V_DDT_RESULT.shopcode in (select shopcode from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");

                        if (strType != "")
                        {
                            sb.Append(" and  V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                        }
                    }
                    sb.Append(@") tb  ");
                }
                sb.Append(" ) a ");

                int pageindex = 1;
                if (!IsPostBack)
                {
                    if (Request.QueryString["PAGENUM"] != null)
                    {
                        pageindex = Convert.ToInt32(Request.QueryString["PAGENUM"].ToString());
                    }
                }
                else
                {
                    pageindex = this.Pager.CurrentPageIndex;
                }

                DataTable dt = sqlhelper.GetPagingRecord(sb1.ToString(), "releaseDate desc", Pager.PageSize, pageindex, ViewState["whereName"].ToString(), "*", out recCnt);
                Pager.RecordCount = recCnt;
                this.Pager.CurrentPageIndex = pageindex;
                Pager.Visible = recCnt > 0;

                this.Pager.CustomInfoHTML = string.Format(CommonStirng.StrPagerCustomerInfo, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount, this.Pager.RecordCount, iPageSize });
                gvMain.DataSource = dt;
                gvMain.DataBind();

                //获取所有的数据值,
                //string ResultString = string.Empty;
                //ResultString = sqlhelper.GetDDTGResult(sb.ToString(), "submitdate desc", ViewState["whereName"].ToString(), "*", out ResultString);

                //获取所有的数据值,
                string ResultString2 = string.Empty;
                ResultString2 = sqlhelper.GetDDTGResult2(sb.ToString(), "releaseDate desc", ViewState["whereName"].ToString(), "*", out ResultString2);
                lblCount.Text = ResultString2;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["resultId"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");

                HyperLink btnsm = (HyperLink)e.Row.FindControl("lbtnMingxi");
                StringBuilder sb = new StringBuilder();
                sb.Append(InputText.GetConfig("gsk_url"));
                sb.Append("resultId=");
                sb.Append(drv["resultId"].ToString());
                sb.Append("&paperId=");
                sb.Append(drv["paperId"].ToString());
                sb.Append("&nonedate=1");
                sb.Append("&isUser=1");
                btnsm.NavigateUrl = @"javascript:void window.open('" + sb.ToString() + "')";

                DateTime dtS = InputText.GetDateTime(drv["releaseDate"].ToString());
                DateTime dtS_CFG = InputText.GetDateTime(InputText.GetConfig("gsk_su_ddt_date"));
                DateTime d_tmp;
                if (dtS > dtS_CFG)
                {
                    d_tmp = dtS;
                }
                else
                {
                    d_tmp = dtS_CFG;
                }
                string strPaper = InputText.GetConfig("gsk_su_ddt_paper");
                string strflg = InputText.GetConfig("gsk_su_ddt").ToString().Trim();

                //if (ddlTicketType.SelectedItem.Value.ToString() == strPaper && strflg == "1" && (InputText.GetInt(InputText.GetConfig("gsk_su_ddt_days")) > DateTime.Compare(DateTime.Now, dtS)))
                if (ddlTicketType.SelectedItem.Value.ToString() == strPaper && strflg == "1" )
                {

                    ///屏蔽申诉 如果 超时则屏蔽 或 已申诉
                    if (drv["Ticketstaus"].ToString().Trim() != "未申诉")
                    {
                        ///屏蔽申诉
                        HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                        btn.Visible = false;
                    }
                    else
                    {
                        ///申诉url
                        HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                        btn.Visible = true;
                        string strUrlsu = "SysTicketDet_cm.aspx?resultId=" + drv["resultId"] + "&projectId=" + drv["projectId"];
                        btn.NavigateUrl = @"javascript:void window.open('" + strUrlsu + "')";
                    }
                }
                else
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                    btn.Visible = false;
                }

                if (drv["Ticketstaus"].ToString().Trim() == "已处理")
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = true;
                    btn.NavigateUrl = @"javascript:void window.open('SysTicketReBackDet.aspx?resultId=" + drv["resultId"].ToString() + "')";
                }
                else
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = false;
                }
                
            }
        }

        protected void ddlType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ddlType2.DataSource = null;
            //ddlType3.DataSource = null;
            //ddlType4.DataSource = null;

            //ddlType2.Items.Clear();
            //ddlType3.Items.Clear();
            //ddlType4.Items.Clear();

            //DataSet select_ds = new DataSet();
            //var select_sql = "";
            //if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            //{
            //    select_sql = " SELECT DISTINCT  COL26 CODE,COL27 NAME  FROM SURVEYOBJ  WHERE PROJECTID=13 AND COL26 IS NOT NULL ";
            //    if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
            //    {

            //        select_sql = select_sql + " and COL13 = '" + ddlType1.SelectedValue + "' ";

            //        select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            //        if (select_ds != null)
            //        {
            //            ddlType2.DataSource = select_ds;
            //            ddlType2.DataValueField = "code";
            //            ddlType2.DataTextField = "name";
            //            ddlType2.DataBind();
            //        }
            //    }
            //}
            //else
            //{
            //    select_sql = "SELECT DISTINCT  SURVEYOBJ.COL26 CODE,SURVEYOBJ.COL27 NAME  FROM SURVEYOBJ   inner join (select * from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=13 AND SURVEYOBJ.COL26 IS NOT NULL ";
            //    if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
            //    {
            //        select_sql = select_sql + " and SURVEYOBJ.COL13 = '" + ddlType1.SelectedValue + "' ";

            //        select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            //        if (select_ds != null)
            //        {
            //            ddlType2.DataSource = select_ds;
            //            ddlType2.DataValueField = "code";
            //            ddlType2.DataTextField = "name";
            //            ddlType2.DataBind();
            //        }
            //    }
            //}


            //ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            //ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            //ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  COL24 CODE,COL25 NAME  FROM SURVEYOBJ  WHERE PROJECTID=13 AND COL24 IS NOT NULL and col13 <>''  ";
                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    //select_sql = select_sql + "   and COL26 = '" + ddlType2.SelectedValue + "'  ";
                    select_sql = select_sql + "   and COL27 = '" + ddlType2.SelectedValue + "'  ";
                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();
                    }
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL24 CODE,SURVEYOBJ.COL25 NAME  FROM SURVEYOBJ   inner join (select * from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=13 AND SURVEYOBJ.COL24 IS NOT NULL and  SURVEYOBJ.col13 <>''  ";
                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    //select_sql = select_sql + "  and SURVEYOBJ.COL26 = '" + ddlType2.SelectedValue + "' ";
                    select_sql = select_sql + "  and SURVEYOBJ.COL27 = '" + ddlType2.SelectedValue + "' ";
                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();
                    }
                }
            }

            

            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;

            ddlType4.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = " SELECT DISTINCT  COL22 CODE,COL23 NAME  FROM SURVEYOBJ  WHERE PROJECTID=13 AND COL22 IS NOT NULL  and  SURVEYOBJ.col13 <>''   and surveyObjId in (select surveyobjid from Result where paperid = " + ddlTicketType.SelectedValue+"  ) ";
                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "   and COL27 = '" + ddlType2.SelectedValue + "'  and COL24 = '" + ddlType3.SelectedValue + "' ";
                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType4.DataSource = select_ds;
                        ddlType4.DataValueField = "code";
                        ddlType4.DataTextField = "name";
                        ddlType4.DataBind();

                    }
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL22 CODE,SURVEYOBJ.COL23 NAME  FROM SURVEYOBJ   inner join (select * from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=13  and  SURVEYOBJ.col13 <>''  AND SURVEYOBJ.COL22 IS NOT NULL   and SURVEYOBJ.surveyObjId in (select surveyObjId from result where paperid = " + ddlTicketType.SelectedValue + "  ) ";
                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.COL27 = '" + ddlType2.SelectedValue + "'  and SURVEYOBJ.COL24 = '" + ddlType3.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType4.DataSource = select_ds;
                        ddlType4.DataValueField = "code";
                        ddlType4.DataTextField = "name";
                        ddlType4.DataBind();

                    }
                }
            }

            

            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void btnOutPut_Click(object sender, EventArgs e)
        {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }

                //modify
                if (ddlZhuangtai.SelectedIndex > 0)
                {
                    ViewState["whereName"] += " and Ticketstaus ='" + ddlZhuangtai.SelectedItem.Text.Trim() + "' ";
                }

                if (txtStoreCode.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopcode like'%" + txtStoreCode.Text.Replace("'","").Replace(";", "").Replace("(", "").Replace(")", "").Replace("*", "") + "%' ";
                }

                if (txtStoreName.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopname like'%" + txtStoreName.Text.Replace("'", "").Replace(";", "").Replace("(", "").Replace(")", "").Replace("*", "") + "%' ";
                }


                string strUsercode = this._LinxSanmpleUserCode;

                //dr 添加筛选

                if (!ddlType4.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType4.SelectedValue;
                }

                else if (!ddlType3.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType3.SelectedValue;
                }

                else if (!ddlType2.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType2.SelectedValue;
                }

                string strType = "";
                if (!ddlType1.SelectedItem.Text.Equals("全部"))
                {

                    strType = ddlType1.SelectedValue;
                }

                viewstatesql.InnerText = ViewState["whereName"].ToString();
                
                StringBuilder sb = new StringBuilder();
                sb.Append("select shopcode as 店铺编码,shopname as 门店名称,t_score as 总分,ssdposm as 舒适达POSM,bljposm as 保丽净POSM,ssdfx as 舒适达分销,bljfx as 保丽净分销,bljwgjfx as 保丽净稳固剂分销,bljqjpfx as 保丽净清洁片分销,ssdhr as 舒适达活跃,bljhr as 保丽净活跃,csdate as 申诉时间,godate as 反馈时间,Ticketstaus as 申诉状态 from (");
                if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb.Append(" select V_DDT_RESULT.* from V_DDT_RESULT  ");
                    sb.Append(" where V_DDT_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                    sb.Append("' ");
                    if (strType != "")
                    {
                        sb.Append(" and V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                    }
                    sb.Append(@") tb ");
                }
                else
                {
                    sb.Append(" select V_DDT_RESULT.* from V_DDT_RESULT inner join (select * from V_DDT_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on V_DDT_RESULT.shopcode = VUSER.shopcode ");
                    sb.Append(" where V_DDT_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                    if (this._LinxSanmpleUserCode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                    {
                        sb.Append("'  ");
                        if (strType != "")
                        {
                            sb.Append(" and  V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                        }

                    }
                    else
                    {
                        sb.Append("' and V_DDT_RESULT.shopcode in (select shopcode from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");

                        if (strType != "")
                        {
                            sb.Append(" and  V_DDT_RESULT.shopcode in (select objcode from SurveyObj where projectid = 13 and col13= '" + strType + "') ");
                        }

                    }
                    sb.Append(@") tb  ");
                }
            


                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                        byte[] bt = ms.ToArray();
                        //以字符流的形式下载文件  
                        Response.ContentType = "application/vnd.ms-excel";
                        //通知浏览器下载文件而不是打开
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                        Response.BinaryWrite(bt);

                        Response.Flush();
                        Response.End();
                        bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }




        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string fileName = "DDT申诉操作手册.pptx";//客户端保存的文件名
            string filePath = Server.MapPath("/" + "DDT.pptx");//路径

            //以字符流的形式下载文件
            FileStream fs = new FileStream(filePath, FileMode.Open);
            byte[] bytes = new byte[(int)fs.Length];
            fs.Read(bytes, 0, bytes.Length);
            fs.Close();
            Response.ContentType = "application/octet-stream";
            //通知浏览器下载文件而不是打开
            Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}