﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JSKWeb.Code;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using JSKWeb.Utility;


namespace JSKWeb.Ticket
{
    public partial class SysTicketDet_OTC : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        string resultId = "";
        string projectId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["resultId"] != null)
            {
                resultId = Request.QueryString["resultId"].ToString().Trim();
            }

            if (!IsPostBack)
            {
                CkPartDataBind();


                string sql = "select * from Sys_Ticket where resultId ='" + resultId + "'";
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    btnSubmit.Enabled = false;
                    JavaScript.Alert(this, "该店铺已申诉。");
                    return;
                }
            }
        }
        public void CkPartDataBind()
        {
            if (Request.QueryString["projectId"] != null)
            {
                projectId = Request.QueryString["projectId"].ToString().Trim();
            }

            string sql = @" select * from v_PubReference where 1=1 ";
            if (projectId == "11")
            {
                sql += " and parentcode='TICKET_OTC' ";
            }
            if (projectId == "12")
            {
                sql += " and parentcode='TICKET_FMCG' ";
            }
            if (projectId == "13")
            {
                sql += " and parentcode='TICKET_DDT' ";
            }
            DataSet ds = new DataSet();
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = ds.Tables[0].Rows[i];
                    cklistpartId.Items.Add(new ListItem(dr["NAME"].ToString(), dr["CODE"].ToString()));
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["resultId"] != null)
                {
                    resultId = Request.QueryString["resultId"].ToString().Trim();
                }

                if (Request.QueryString["projectId"] != null)
                {
                    projectId = Request.QueryString["projectId"].ToString().Trim();
                }

                string sql = "select * from Sys_Ticket where resultId ='" + resultId + "'";
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    btnSubmit.Enabled = false;

                    JavaScript.Alert(this, "该店铺已申诉。");
                    return;
                }

                bool partIdChkflg = true;
                for (int i = 0; i < cklistpartId.Items.Count; i++)
                {
                    if (cklistpartId.Items[i].Selected)
                    {
                        partIdChkflg = false;
                        break;
                    }
                }

                if (partIdChkflg)
                {
                    JavaScript.Alert(this, "必须选择至少一个申诉点，请输入。");
                    return;
                }

                if (this.txtReason.Text.Trim().Equals(""))
                {
                    JavaScript.Alert(this, "申诉理由为必填项目，请输入。");
                    return;
                }


                if (fuobjPic1.HasFile || fuobjPic2.HasFile)
                {

                }
                else
                {
                    JavaScript.Alert(this, "必须上传一张门头照，请确认。");
                    return;
                }

                {
                    string partId = "";
                    for (int i = 0; i < cklistpartId.Items.Count; i++)
                    {
                        if (cklistpartId.Items[i].Selected)
                        {
                            partId += cklistpartId.Items[i].Value + ",";
                        }
                    }
                    if (partId.Length > 0)
                    {
                        partId = partId.Remove(partId.Length - 1);
                    }

                    #region 上传门头照1
                    string objPic1 = "";
                    string pathobjPic1 = "";
                    if (fuobjPic1.HasFile)
                    {
                        if (txtobjPic1Date.Text.Trim().Length == 0)
                        {
                            JavaScript.Alert(this, "必须选择门头照1的拍摄日期！");
                            return;
                        }

                        objPic1 = Guid.NewGuid().ToString();
                        pathobjPic1 = InputText.GetConfig("TICKEYPHOTO") + "\\" + DateTime.Today.ToString("yyyyMMdd") + "\\" + resultId;
                        string filename = fuobjPic1.FileName;

                        if (System.IO.Directory.Exists(pathobjPic1) == false)
                        {
                            System.IO.Directory.CreateDirectory(pathobjPic1);
                        }
                        pathobjPic1 += "\\objPic1_" + objPic1 + ".jpg";
                        fuobjPic1.SaveAs(pathobjPic1);
                    }
                    #endregion
                    string objPic2 = "";
                    string pathobjPic2 = "";
                    #region 上传门头照2
                    if (fuobjPic2.HasFile)
                    {
                        objPic2 = Guid.NewGuid().ToString();
                        pathobjPic2 = InputText.GetConfig("TICKEYPHOTO") + "\\" + DateTime.Today.ToString("yyyyMMdd") + "\\" + resultId;

                        if (txtobjPic2Date.Text.Trim().Length == 0)
                        {
                            JavaScript.Alert(this, "必须选择门头照2拍摄日期！");
                            return;
                        }
                        string filename = fuobjPic2.FileName;

                        if (System.IO.Directory.Exists(pathobjPic2) == false)
                        {
                            System.IO.Directory.CreateDirectory(pathobjPic2);
                        }
                        pathobjPic2 += "\\objPic2_" + objPic2 + ".jpg";
                        fuobjPic2.SaveAs(pathobjPic2);
                    }
                    #endregion
                    string appealPic1 = "";
                    string pathappealPic1 = "";
                    
                    string appealPic2 = "";
                    string pathappealPic2 = "";
                    
                    string appealPic3 = "";
                    string pathappealPic3 = "";

                    string appealPic4 = "";
                    string pathappealPic4 = "";
                    string appealPic5 = "";

                    //#region 照片5
                    string pathappealPic5 = "";
   
                    SqlParameter[] parsDet = {
                                  new SqlParameter("@partId",SqlDbType.NVarChar),
                                  new SqlParameter ("@Reason",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic1",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic1Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic2",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic2Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic1",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic1Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic2",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic2Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic3",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic3Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic4",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic4Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic5",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic5Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@createUser",SqlDbType.NVarChar),
                                  new SqlParameter ("@createTime",SqlDbType.NVarChar),
                                  new SqlParameter ("@resultId",SqlDbType.NVarChar),
                                  new SqlParameter("@staus", SqlDbType.NVarChar),
                                  new SqlParameter("@projectId", SqlDbType.Int)
                                  };
                    parsDet[0].Value = partId;
                    parsDet[1].Value = txtReason.Text.Trim();
                    parsDet[2].Value = pathobjPic1.Trim().Length > 0 ? pathobjPic1 : "";
                    parsDet[3].Value = txtobjPic1Date.Text.Trim();
                    parsDet[4].Value = pathobjPic2.Trim().Length > 0 ? pathobjPic2 : "";
                    parsDet[5].Value = txtobjPic2Date.Text.Trim();
                    parsDet[6].Value = pathappealPic1.Trim().Length > 0 ? pathappealPic1 : "";
                    parsDet[7].Value = "";
                    parsDet[8].Value = pathappealPic2.Trim().Length > 0 ? pathappealPic2 : "";
                    parsDet[9].Value = "";
                    parsDet[10].Value = pathappealPic3.Trim().Length > 0 ? pathappealPic3 : "";
                    parsDet[11].Value = "";
                    parsDet[12].Value = pathappealPic4.Trim().Length > 0 ? pathappealPic4 : "";
                    parsDet[13].Value = "";
                    parsDet[14].Value = pathappealPic5.Trim().Length > 0 ? pathappealPic5 : "";
                    parsDet[15].Value = "";
                    parsDet[16].Value = this._LinxSanmpleUserCode;
                    parsDet[17].Value = DateTime.Now.ToString();
                    parsDet[18].Value = resultId;
                    parsDet[19].Value = 2;
                    parsDet[20].Value = projectId;
                    sql = @"insert into Sys_Ticket([partId]
      ,[reason] 
      ,[objPic1] ,[objPic1_time]
      ,[objPic2] ,[objPic2_time]
      ,[appealPic1] ,[appealPic1_time]
      ,[appealPic2] ,[appealPic2_time]
      ,[appealPic3] ,[appealPic3_time]
      ,[appealPic4]  ,[appealPic4_time]
      ,[appealPic5],[appealPic5_time],createUser,createTime,resultId,staus,projectId)values(@partId,@Reason,@objPic1,@objPic1Date,@objPic2,@objPic2Date,@appealPic1,@appealPic1Date
,@appealPic2,@appealPic2Date
,@appealPic3,@appealPic3Date
,@appealPic4,@appealPic4Date
,@appealPic5,@appealPic5Date,@createUser,@createTime,@resultId,@staus,@projectId)";

                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql, parsDet);
                    btnSubmit.Enabled = false;
                    JavaScript.Alert(this, "申诉提交成功");
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            JavaScript.ClosePage(this);
        }


    }
}