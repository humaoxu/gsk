﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true"
    CodeBehind="SysTicketReBackDet.aspx.cs" Inherits="JSKWeb.Ticket.SysTicketReBackDet" %>
<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">

        function operate(index) {
            if (index == 0) {

                return confirm("是否要提交？");
            }
            else {
                return false;
            }
        }
        //清空文件上传框
        function clearFileInput(file) {
            var form = document.createElement('form');
            document.body.appendChild(form);
            //记住file在旧表单中的的位置
            var pos = file.nextSibling;
            form.appendChild(file);
            form.reset();
            pos.parentNode.insertBefore(file, pos);
            document.body.removeChild(form);
        }
    </script>

    <style type="text/css">
        .image 
        {
            max-height: 120px;
        }
        .auto-style1 {
            width: 105px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
            <fieldset>
        <legend>&nbsp;申诉内容&nbsp;</legend>
                 <table>

                           <tr>
                  <td >
                      <asp:Label ID="lblSU" runat="server" Text="　　"></asp:Label> 
                  </td>
              </tr>
                 </table>
                    <div id="content" runat="server"  >

         
    </div>  
    <table width="1000px" >
            <tbody>
         
             
                <tr>
                    <td align="left">
                       <div id="storeImg" runat="server" ></div>
                    </td> 
                </tr>
                <%--              <tr>
                     <td align="right">
                        照片：
                    </td>
                    <td align="left"> 
                     <div id="appealImg" runat="server" style="width: 200px" ></div>
                    </td> 
                </tr>
                   <tr>
                    <td align="right">
                        申诉理由：
                    </td>
                    <td align="left">
                     
                        <asp:TextBox ID="txtReason" ReadOnly="true" runat="server" Height="135px" 
                            TextMode="MultiLine" Width="476px"></asp:TextBox>
                    </td> 
                </tr>--%>
              
            </tbody>
        </table>
                </fieldset>
          <fieldset runat="server" id="feedbacksu">
        <legend>&nbsp;申诉反馈&nbsp;</legend>
    <table width="1000px">
            
        <tr>

                   <td align="right">申诉点：</td>
              <td align="left">
                   <asp:DropDownList ID="ddl1" CssClass="inputtext15" runat="server" Width="450px"  meta:resourcekey="ddl2Resource1" OnSelectedIndexChanged="ddl1_SelectedIndexChanged">
                        </asp:DropDownList>&nbsp;&nbsp;
               </td>
        </tr> 
        <tr>
                    <td align="right">
                        反馈内容：
                    </td>
                    <td align="left">
                     
                        <asp:TextBox ID="txtbackInfo" runat="server" 
                             Width="450px" MaxLength="500"></asp:TextBox>
                    </td>
                </tr>
                   <tr>
                    <td align="right">
                        得分状态：
                    </td>
                    <td align="left">
                        <asp:RadioButton ID="rad1" runat="server" Text="不修改得分" 
                            GroupName="rad"  />
                        <asp:RadioButton ID="rad2" runat="server" Text="不修改得分无照片" GroupName="rad" />
                        
                        <asp:RadioButton ID="rad3" runat="server" Text="已修改得分" GroupName="rad" />

                        
                        <asp:RadioButton ID="rad5" runat="server" Text="客户要求修改得分" GroupName="rad" />
                        
                        <asp:RadioButton ID="rad4" runat="server" Text="待确认" GroupName="rad" /> &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnSubmit" CssClass="btn" Height="25" Width="105px" runat="server" Text="提交反馈"
                            OnClick="btnSubmit_Click" />
                     </td>
                </tr>
                <tr style="text-align: left;">
                    <td colspan="2" style="padding-left: 150px;">
                
                    </td>
                </tr>

    </table>
              </fieldset>


    <fieldset runat="server" id="fileticket">
        <legend>&nbsp;申诉票据状态&nbsp;</legend>
    <table width="1000px">
         
        
                   <tr>
                    <td align="right" class="auto-style1">
                        申诉票据状态：
                    </td>
                    <td align="left">
                        <asp:RadioButton ID="RadioButton1" runat="server" Checked="True" Text="未处理" 
                            GroupName="rad01"  />
                        <asp:RadioButton ID="RadioButton2" runat="server" Text="已处理" GroupName="rad01" />
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnStatus" CssClass="btn" Height="25" Width="100px" runat="server" Text="修改状态" OnClick="btnStatus_Click1"
                            />
                     </td>
                </tr>
                <tr style="text-align: left;">
                    <td colspan="2" style="padding-left: 150px;">
                
                    </td>
                </tr>

    </table>
              </fieldset>

        <script type="text/javascript">
            (function () {
                document.body.addEventListener('click', function (e) {
                    if (e.target.tagName.toLowerCase() == 'img') {
                        window.open(e.target.src, '_blank');
                    }
                });
            })();
        </script>
</asp:Content>
