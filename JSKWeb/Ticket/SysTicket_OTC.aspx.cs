﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;

namespace JSKWeb.Ticket
{
    public partial class SysTicket_OTC : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        int iPageSize = 30;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}  and isReleased = 1 order by paperId desc ", ddlProjectId);
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}   order by paperId desc ", ddlProjectId);
            }

            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            ///绑定状态
            string sql = @" select * from v_PubReference where parentcode='TickCode' ";
            DataSet ds = new DataSet();
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            ddlZhuangtai.DataSource = ds;
            ddlZhuangtai.DataValueField = "code";
            ddlZhuangtai.DataTextField = "name";
            ddlZhuangtai.DataBind();
            ddlZhuangtai.Items.Insert(0, new ListItem("全部", "NULL"));
            ddlZhuangtai.SelectedValue = "NULL";

            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;
            ddlType5.DataSource = null;

            ddlType2.Items.Clear();
            ddlType3.Items.Clear();
            ddlType4.Items.Clear();
            ddlType5.Items.Clear();

            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = " SELECT DISTINCT  col25 CODE,COL26 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col25 IS NOT NULL and col25 <> '' ";
          
            }
            else
            {
                select_sql = " SELECT DISTINCT  SURVEYOBJ.col25 CODE,SURVEYOBJ.COL26 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.col25 IS NOT NULL  and SURVEYOBJ.col25 <> ''  ";
            }

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }

            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlType1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;
            ddlType5.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();
            ddlType5.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  col27 CODE,COL28 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col27 IS NOT NULL  and col27 not like 'GSKOTC%'  and col25 <> ''   ";

                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();

                    }
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.col27 CODE,SURVEYOBJ.COL28 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.col27 IS NOT NULL  and col27 not like 'GSKOTC%'  and SURVEYOBJ.col25 <> ''   ";

                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.col25 = '" + ddlType2.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();

                    }
                }
            }
            
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;
            ddlType5.DataSource = null;
            
            ddlType4.Items.Clear();
            ddlType5.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            
                if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
                {
                    select_sql = "SELECT DISTINCT  col19 CODE,(COL33+'-'+COL20) NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col19 IS NOT NULL  and col25 <> ''  ";
                    if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                    {
                    //select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' and col27 = '" + ddlType3.SelectedValue + "' ";
                    select_sql = select_sql + "   and col28 = '" + ddlType3.SelectedItem.Text.Trim() + "'  and surveyObjId in (select surveyobjid from RESULT where paperid = "+ddlTicketType.SelectedValue+") ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                        if (select_ds != null)
                        {
                            ddlType4.DataSource = select_ds;
                            ddlType4.DataValueField = "code";
                            ddlType4.DataTextField = "name";
                            ddlType4.DataBind();

                        }
                    }
                }
                else
                {
                    select_sql = "SELECT DISTINCT SURVEYOBJ.col19 CODE,(SURVEYOBJ.COL33+'-'+SURVEYOBJ.COL20) NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL19 IS NOT NULL  and SURVEYOBJ.col25 <> ''  ";
                    if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                    {
                    //select_sql = select_sql + "   and SURVEYOBJ.col25 = '" + ddlType2.SelectedValue + "'  and SURVEYOBJ.col27 = '" + ddlType3.SelectedValue + "' ";
                    select_sql = select_sql + "   and SURVEYOBJ.col28 = '" + ddlType3.SelectedItem.Text.Trim() + "'  and SURVEYOBJ.surveyObjId in (select surveyobjid from RESULT where paperid = " + ddlTicketType.SelectedValue + ") ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                        if (select_ds != null)
                        {
                            ddlType4.DataSource = select_ds;
                            ddlType4.DataValueField = "code";
                            ddlType4.DataTextField = "name";
                            ddlType4.DataBind();
                        }
                    }
                }
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlType4_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlType5.DataSource = null;
            ddlType5.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
 
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  COL17 CODE,COL18 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND COL17 IS NOT NULL  and col25 <> ''  " + "  and surveyObjId in (select surveyobjid from RESULT where paperid = " + ddlTicketType.SelectedValue + ") "; ;
                if (!ddlType4.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "   and col19 = '" + ddlType4.SelectedValue + "' ";
                    if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                    {
                        select_sql = select_sql + "  and col25 = '" + ddlType2.SelectedValue + "' ";
                    }
                    if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                    {
                        select_sql = select_sql + "  and col27 = '" + ddlType3.SelectedValue + "'  ";
                    }

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType5.DataSource = select_ds;
                        ddlType5.DataValueField = "code";
                        ddlType5.DataTextField = "name";
                        ddlType5.DataBind();
                    }
                }

            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL17 CODE,SURVEYOBJ.COL18 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL17 IS NOT NULL  and SURVEYOBJ.col25 <> ''  " + "  and SURVEYOBJ.surveyObjId in (select surveyObjId from RESULT where paperid = " + ddlTicketType.SelectedValue + ") "; ; ;

                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "    and SURVEYOBJ.col27 = '" + ddlType3.SelectedValue + "'  ";
                }
                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.col25 = '" + ddlType2.SelectedValue + "'  ";
                }

                if (!ddlType4.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and SURVEYOBJ.col19 = '" + ddlType4.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType5.DataSource = select_ds;
                        ddlType5.DataValueField = "code";
                        ddlType5.DataTextField = "name";
                        ddlType5.DataBind();
                    }
                }

                
            }

            

            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));
        }



        private bool checkDateTimeFormat(string txtTime)
        {
            try
            {
                string sDate = txtTime.Substring(0, 4) + "-" + txtTime.Substring(4, 2) + "-" + txtTime.Substring(6, 2);
                DateTime.Parse(sDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void LoadInfo()
        {
            Pager.PageSize = iPageSize;
            int recCnt;

            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }

                //modify
                if (ddlZhuangtai.SelectedIndex > 0)
                {
                    ViewState["whereName"] += " and Ticketstaus ='" + ddlZhuangtai.SelectedItem.Text.Trim() + "' ";
                }

                if (txtStoreCode.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopcode like'%" + txtStoreCode.Text + "%' ";
                }

                if (txtStoreName.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopname like'%" + txtStoreName.Text + "%' ";
                }

                string strUsercode = this._LinxSanmpleUserCode;

                StringBuilder sb_t = new StringBuilder();
                sb_t.Append("SELECT distinct shopid,shopcode,shopname,paperid,papertitle,submitdate,scdate,dbo.SYS_OTC_RESULT.resultId, ");
                sb_t.Append("CAST((CASE WHEN t_score IS NULL THEN 0 ELSE t_score END) AS decimal(10, 3)) t_score , ");
                sb_t.Append("CAST((CASE WHEN STPF IS NULL THEN 0 ELSE STPF END) AS decimal(10, 3)) STPF , ");
                sb_t.Append("CAST((CASE WHEN CLPF IS NULL THEN 0 ELSE CLPF END) AS decimal(10, 3)) CLPF , ");
                sb_t.Append("CAST((CASE WHEN POSM IS NULL THEN 0 ELSE POSM END) AS decimal(10, 3)) POSM , ");
                sb_t.Append("CAST((CASE WHEN POSMFJ IS NULL THEN 0 ELSE POSMFJ END) AS decimal(10, 3)) POSMFJ , ");

                //sb_t.Append(" (CAST((CASE WHEN POSM IS NULL THEN 0 ELSE POSM END) AS decimal(10, 3)) + CAST((CASE WHEN POSMFJ IS NULL THEN 0 ELSE POSMFJ END) AS decimal(10, 3))) POSM , ");
                sb_t.Append("CAST((CASE WHEN JG IS NULL THEN 0 ELSE JG END) AS decimal(10, 3)) JG , ");
                sb_t.Append("CAST((CASE WHEN TJ IS NULL THEN 0 ELSE TJ END) AS decimal(10, 3)) TJ , ");
                sb_t.Append("CAST((CASE WHEN STFJ IS NULL THEN 0 ELSE STFJ END) AS decimal(10, 3)) STFJ,  ");
                sb_t.Append("CASE ");
                sb_t.Append("WHEN t.staus IS NULL THEN '未申诉' ");
                sb_t.Append("WHEN t .staus = '1' THEN '未申诉' ");
                sb_t.Append("WHEN t .staus = '2' THEN '已申诉' ");
                sb_t.Append("WHEN t .staus = '3' THEN '已处理' ");
                sb_t.Append("END AS Ticketstaus, ");
                sb_t.Append("t.staus AS t_status_cd, ");
                sb_t.Append("t.id AS t_id, ");
                sb_t.Append("CONVERT(varchar(100), t.createTime, 11) AS csdate, ");
                sb_t.Append("CONVERT(varchar(100), t.backTime, 11) AS godate, ");
                sb_t.Append(" dbo.SYS_OTC_RESULT.projectId FROM dbo.SYS_OTC_RESULT inner join SurveyObj on SYS_OTC_RESULT.shopid = SurveyObj.surveyObjId ");


                sb_t.Append(" and SYS_OTC_RESULT.paperid = " + ddlTicketType.SelectedItem.Value.ToString() +" ");

                // sb_t.Append(" and SYS_OTC_RESULT.shopid in (select surveyobjid from Result where resultid in (141677,141682,147156,147066,148022,147617) ) ");
                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    sb_t.Append("and SurveyObj.col25 = '"+ ddlType2.SelectedValue + "' ");
                }
                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    sb_t.Append("and SurveyObj.col27 = '" + ddlType3.SelectedValue + "' ");
                }
                if (!ddlType4.SelectedItem.Text.Trim().Equals("全部"))
                {
                    sb_t.Append("and SurveyObj.col19 = '" + ddlType4.SelectedValue + "' ");
                }
                if (!ddlType5.SelectedItem.Text.Trim().Equals("全部"))
                {
                    sb_t.Append("and SurveyObj.col17 = '" + ddlType5.SelectedValue + "' ");
                }
                sb_t.Append("LEFT JOIN dbo.Sys_Ticket AS t ON t.resultId = dbo.SYS_OTC_RESULT.resultId ");

                viewstatesql.InnerText = ViewState["whereName"].ToString(); 

                StringBuilder sb = new StringBuilder();
                sb.Append(@"(");
                sb.Append("select distinct * from (");
                if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb.Append(" select tb_V_OTC_RESULT.* from (" + sb_t + ") tb_V_OTC_RESULT  ");
                    sb.Append(" where tb_V_OTC_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                    sb.Append("' ");
                    sb.Append(@") tb ) a ");
                }
                else
                {
                    
                    sb.Append(" select tb_V_OTC_RESULT.* from (" + sb_t + ") tb_V_OTC_RESULT inner join (select * from V_OTC_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on tb_V_OTC_RESULT.shopcode = VUSER.shopcode ");
                    
                    sb.Append(" where tb_V_OTC_RESULT.paperId ='");
                    sb.Append(ddlTicketType.SelectedItem.Value.ToString());

                    sb.Append("' and tb_V_OTC_RESULT.shopcode in (select shopcode from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");

                    sb.Append(@") tb ) a ");
                }


                int pageindex = 1;

                if (!IsPostBack)
                {
                    if (Request.QueryString["PAGENUM"] != null)
                    {
                        pageindex = Convert.ToInt32(Request.QueryString["PAGENUM"].ToString());
                    }
                }
                else
                {
                    pageindex = this.Pager.CurrentPageIndex;
                }

                DataTable dt = sqlhelper.GetPagingRecord(sb.ToString(), "submitdate desc", Pager.PageSize, pageindex, ViewState["whereName"].ToString(), "*", out recCnt);
                Pager.RecordCount = recCnt;
                this.Pager.CurrentPageIndex = pageindex;
                Pager.Visible = recCnt > 0;

                this.Pager.CustomInfoHTML = string.Format(CommonStirng.StrPagerCustomerInfo, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount, this.Pager.RecordCount, iPageSize });
                gvMain.DataSource = dt;
                gvMain.DataBind();

                //decimal CountByStore, CountByST, CountByCL, CountByJG, CountByTJ, CountBySTFJ;
                //获取所有的数据值,
                string ResultString = string.Empty;
                ResultString = sqlhelper.GetOTCResult(sb.ToString(), "submitdate desc", ViewState["whereName"].ToString(), "*", out ResultString);
                if (ResultString.Equals(""))
                {
                    lblCount.Text = "No Data!";
                }
                else
                {
                    lblCount.Text = ResultString;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       /// <summary>
       /// 计算Datat列合值
       /// </summary>
       /// <param name="dt"></param>
       /// <param name="columnName"></param>
       /// <returns></returns>
        private double DataTableColumnSum(DataTable dt, string columnName)
        {
            double d = 0;
            foreach (DataRow dr in dt.Rows)
            {
                d += double.Parse(dr[columnName].ToString());
            }

            return d;
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["resultId"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");

                HyperLink btnsm = (HyperLink)e.Row.FindControl("lbtnMingxi");
                StringBuilder sb = new StringBuilder();
                sb.Append(InputText.GetConfig("gsk_url"));
                sb.Append("resultId=");
                sb.Append(drv["resultId"].ToString());
                sb.Append("&paperId=");
                sb.Append(drv["paperId"].ToString());
                sb.Append("&nonedate=1");
                sb.Append("&isUser=1 ");
                //sb.Append("&external=1&viewresult=1");
                btnsm.NavigateUrl = @"javascript:void window.open('" + sb.ToString() + "')";


                DateTime dtS = InputText.GetDateTime(drv["scdate"].ToString());

                ///屏蔽申诉 如果 超时则屏蔽 或 已申诉
                if (drv["Ticketstaus"].ToString().Trim() != "未申诉" )  
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                    btn.Visible = false;
                }
                else
                {
                    ///申诉url
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                    btn.Visible = true;
                    string strUrlsu = "SysTicketDet.aspx?resultId=" + drv["resultId"] + "&projectId=" + drv["projectId"];
                    btn.NavigateUrl = @"javascript:void window.open('" + strUrlsu + "')";
                }

                string strflg = InputText.GetConfig("gsk_su_otc").ToString().Trim();
                if (strflg == "0")
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                    btn.Visible = false;
                }

                if (drv["Ticketstaus"].ToString().Trim() == "已处理")
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = true;
                    btn.NavigateUrl = @"javascript:void window.open('SysTicketReBackDet.aspx?resultId=" + drv["resultId"].ToString() + "')";
                }
                else
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = false;
                }


            }
        }

        /// <summary>
        /// 数据导出
        /// </summary>
        public void btnOutput_Click(object sender, EventArgs e)
        {

            ViewState["whereName"] = null;

            if (ViewState["whereName"] == null)
            {
                ViewState["whereName"] = "1=1 ";
            }

            //modify
            if (ddlZhuangtai.SelectedIndex > 0)
            {
                ViewState["whereName"] += " and Ticketstaus ='" + ddlZhuangtai.SelectedItem.Text.Trim() + "' ";
            }

            if (txtStoreCode.Text.Trim().Length > 0)
            {
                ViewState["whereName"] += " and shopcode like'%" + txtStoreCode.Text + "%' ";
            }

            if (txtStoreName.Text.Trim().Length > 0)
            {
                ViewState["whereName"] += " and shopname like'%" + txtStoreName.Text + "%' ";
            }

            string strUsercode = this._LinxSanmpleUserCode;

            StringBuilder sb_t = new StringBuilder();
            sb_t.Append("SELECT  distinct  shopid,shopcode,shopname,paperid,papertitle,submitdate,scdate,dbo.SYS_OTC_RESULT.resultId, ");
            sb_t.Append("CAST((CASE WHEN t_score IS NULL THEN 0 ELSE t_score END) AS decimal(10, 3)) t_score , ");
            sb_t.Append("CAST((CASE WHEN STPF IS NULL THEN 0 ELSE STPF END) AS decimal(10, 3)) STPF , ");
            sb_t.Append("CAST((CASE WHEN CLPF IS NULL THEN 0 ELSE CLPF END) AS decimal(10, 3)) CLPF , ");
            //sb_t.Append(" (CAST((CASE WHEN POSM IS NULL THEN 0 ELSE POSM END) AS decimal(10, 3)) + CAST((CASE WHEN POSMFJ IS NULL THEN 0 ELSE POSMFJ END) AS decimal(10, 3)))  POSM , ");

            sb_t.Append("CAST((CASE WHEN POSM IS NULL THEN 0 ELSE POSM END) AS decimal(10, 3)) POSM , ");
            sb_t.Append("CAST((CASE WHEN POSMFJ IS NULL THEN 0 ELSE POSMFJ END) AS decimal(10, 3)) POSMFJ , ");
            
            sb_t.Append("CAST((CASE WHEN JG IS NULL THEN 0 ELSE JG END) AS decimal(10, 3)) JG , ");
            sb_t.Append("CAST((CASE WHEN TJ IS NULL THEN 0 ELSE TJ END) AS decimal(10, 3)) TJ , ");
            sb_t.Append("CAST((CASE WHEN STFJ IS NULL THEN 0 ELSE STFJ END) AS decimal(10, 3)) STFJ,  ");
            sb_t.Append("CASE ");
            sb_t.Append("WHEN t.staus IS NULL THEN '未申诉' ");
            sb_t.Append("WHEN t .staus = '1' THEN '未申诉' ");
            sb_t.Append("WHEN t .staus = '2' THEN '已申诉' ");
            sb_t.Append("WHEN t .staus = '3' THEN '已处理' ");
            sb_t.Append("END AS Ticketstaus, ");
            sb_t.Append("t.staus AS t_status_cd, ");
            sb_t.Append("t.id AS t_id, ");
            sb_t.Append("CONVERT(varchar(100), t.createTime, 11) AS csdate, ");
            sb_t.Append("CONVERT(varchar(100), t.backTime, 11) AS godate, ");
            sb_t.Append("dbo.SYS_OTC_RESULT.projectId FROM dbo.SYS_OTC_RESULT inner join SurveyObj on SYS_OTC_RESULT.shopid = SurveyObj.surveyObjId ");
            // sb_t.Append(" and SYS_OTC_RESULT.shopid in (select surveyobjid from Result where resultid in (141677,141682,147156,147066,148022,147617) ) ");

            sb_t.Append(" and SYS_OTC_RESULT.paperid = " + ddlTicketType.SelectedItem.Value.ToString() + " ");
            if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
            {
                sb_t.Append("and SurveyObj.col25 = '" + ddlType2.SelectedValue + "' ");
            }
            if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
            {
                sb_t.Append("and SurveyObj.col27 = '" + ddlType3.SelectedValue + "' ");
            }
            if (!ddlType4.SelectedItem.Text.Trim().Equals("全部"))
            {
                sb_t.Append("and SurveyObj.col19 = '" + ddlType4.SelectedValue + "' ");
            }
            if (!ddlType5.SelectedItem.Text.Trim().Equals("全部"))
            {
                sb_t.Append("and SurveyObj.col17 = '" + ddlType5.SelectedValue + "' ");
            }
            sb_t.Append("LEFT JOIN dbo.Sys_Ticket AS t ON t.resultId = dbo.SYS_OTC_RESULT.resultId ");


            viewstatesql.InnerText = ViewState["whereName"].ToString();
            //string tableSql = @"(  select * from V_DDT_SCROERESULT where paperId ='" + ddlTicketType.SelectedItem.Value + "' " + ") a ";

            StringBuilder sb = new StringBuilder();

            sb.Append("select  distinct  shopcode as 店铺编码,shopname as 门店名称,t_score as 总分,STPF as 分销含分销附加分,STFJ as 分销附加分,CLPF as 陈列评分,POSM as POSM含POSM附加分,POSMFJ as POSM附加评分,JG as 价格评分,TJ as 推荐评分,csdate as 申诉时间,godate as 反馈时间,Ticketstaus as 申诉状态 from (");

            if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
            {
                sb.Append(" select tb_V_OTC_RESULT.* from (" + sb_t + ") tb_V_OTC_RESULT  ");
                sb.Append(" where tb_V_OTC_RESULT.paperId ='");
                sb.Append(ddlTicketType.SelectedItem.Value.ToString());
                sb.Append("' ");
                sb.Append(@") tb  ");
            }
            else
            {

                sb.Append(" select tb_V_OTC_RESULT.* from (" + sb_t + ") tb_V_OTC_RESULT inner join (select * from V_OTC_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on tb_V_OTC_RESULT.shopcode = VUSER.shopcode ");

                sb.Append(" where tb_V_OTC_RESULT.paperId ='");
                sb.Append(ddlTicketType.SelectedItem.Value.ToString());

                sb.Append("' and tb_V_OTC_RESULT.shopcode in (select shopcode from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");

                sb.Append(@") tb  ");
            }


            //sb.Append("select shopcode as 店铺编码,shopname as 门店名称,t_score as 总分,STPF as 渗透评分,CLPF as 陈列评分,POSM as POSM评分,JG as 价格评分,TJ as 推荐评分,STFJ as 渗透附加分,csdate as 申诉时间,godate as 反馈时间,Ticketstaus as 申诉状态 from (");
            //if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
            //{
            //    sb.Append(" select V_OTC_RESULT.* from V_OTC_RESULT  ");
            //    sb.Append(" where V_OTC_RESULT.paperId ='");
            //    sb.Append(ddlTicketType.SelectedItem.Value.ToString());
            //    sb.Append("' ");
            //    sb.Append(@") tb ");
            //}
            //else
            //{

            //    sb.Append(" select V_OTC_RESULT.* from V_OTC_RESULT inner join (select * from V_OTC_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on V_OTC_RESULT.shopcode = VUSER.shopcode ");


            //    sb.Append(" where V_OTC_RESULT.paperId ='");
            //    sb.Append(ddlTicketType.SelectedItem.Value.ToString());

            //    if (this._LinxSanmpleUserCode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
            //    {
            //        sb.Append("'  ");

            //    }
            //    else
            //    {
            //        sb.Append("' and V_OTC_RESULT.shopcode in (select shopcode from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");

            //    }

            //    sb.Append(@") tb  ");
            //}



            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string fileName = "OTC申诉操作手册.pptx";//客户端保存的文件名
            string filePath = Server.MapPath("/" + "OTC.pptx");//路径

            //以字符流的形式下载文件
            FileStream fs = new FileStream(filePath, FileMode.Open);
            byte[] bytes = new byte[(int)fs.Length];
            fs.Read(bytes, 0, bytes.Length);
            fs.Close();
            Response.ContentType = "application/octet-stream";
            //通知浏览器下载文件而不是打开
            Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}