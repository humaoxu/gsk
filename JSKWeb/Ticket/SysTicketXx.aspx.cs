﻿using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSKWeb.Ticket
{
    public partial class SysTicketXx : PageBase
    {

        SqlHelper sqlhelper = new SqlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            HttpPostedFile file = this.fileUploadCtr.PostedFile;
            string fileName = file.FileName;
            string tempPath = Server.MapPath(InputText.GetConfig("TicketTempFile")); //获取系统临时文件路径
            fileName = System.IO.Path.GetFileName(fileName); //获取文件名（不带路径）
            string currFileExtension = System.IO.Path.GetExtension(fileName); //获取文件的扩展名
            string currFilePath = tempPath + fileName; //获取上传后的文件路径 记录到前面声明的全局变量
            file.SaveAs(currFilePath); //上传

            if (currFileExtension.ToUpper() != ".XLSX")
            {
                JavaScript.Alert(this, "文件格式不正确，上传的文件后缀名必须为XLSX还请确认");
                return;
            }

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            string NameTable = "";
            string ConText = "";
            //获取Excel路径
            FileInfo info = new FileInfo(currFilePath);
            //获取文件的扩展名
            string fileExt = info.Extension;
            //判断用哪种连接方式
            ConText = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + currFilePath + ";Extended Properties='excel 12.0 Xml;hdr=no;IMEX=1';Persist Security Info=False";
            //连接excel
            OleDbConnection conn = new OleDbConnection(ConText);
            try
            {
                //打开excel
                conn.Open();
                dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //获取sheet1表单的表名
                    NameTable = dt.Rows[0]["TABLE_NAME"].ToString();
                    //获取sheet2表单的表名
                    //NameTable = dt.Rows[1]["TABLE_NAME"].ToString();
                }
                string sql = "select * from [" + NameTable + "]";
                da = new OleDbDataAdapter(sql, conn);
                da.Fill(ds, NameTable); //把数据填充到Dataset

                GridView1.DataSource = ds;
                GridView1.DataBind();

                int count = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string id = ds.Tables[0].Rows[i][0].ToString().Trim();
                    string staus = ds.Tables[0].Rows[i][1].ToString().Trim();
                    string reason = ds.Tables[0].Rows[i][2].ToString().Trim();
                    if (staus == "不修改得分" || staus == "不修改得分无照片" || staus == "已修改得分" || staus == "客户要求修改得分" || staus == "待确认")
                    {
                        
                        
                        SqlParameter[] parsDet = {
                                          new SqlParameter("@id",SqlDbType.NVarChar),
                                          new SqlParameter("@backUserId",SqlDbType.NVarChar),
                                            new SqlParameter("@backReason",SqlDbType.NVarChar),
                                          new SqlParameter ("@score_status",SqlDbType.NVarChar),
                                          new SqlParameter ("@backTime",SqlDbType.NVarChar)
                                             };
                        parsDet[0].Value = id;
                        parsDet[1].Value = this._LinxSanmpleUserCode;
                        parsDet[2].Value = reason;
                        if (staus == "不修改得分")
                        {
                            parsDet[3].Value = 0;
                        }
                        else if (staus == "不修改得分无照片")
                        {
                            parsDet[3].Value = 1;
                        }
                        else if (staus == "已修改得分")
                        {
                            parsDet[3].Value = 2;
                        }
                        else if (staus == "待确认")
                        {
                            parsDet[3].Value = 3;
                        }
                        else if (staus == "客户要求修改得分")
                        {
                            parsDet[3].Value = 4;
                        }

                        parsDet[4].Value = DateTime.Now.ToString();

                        string sqlUp = @" update SyS_TicketDetails set backUserId=@backUserId, backTime =@backTime ,backReason=@backReason,score_status=@score_status where id=@id ";
                        sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sqlUp, parsDet);
                        count = count + 1;
                    }

                }
                JavaScript.Alert(this, "更新成功");

                conn.Close();
            }
            catch (Exception ex)
            {
                conn.Close();
                JavaScript.Alert(this, ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}