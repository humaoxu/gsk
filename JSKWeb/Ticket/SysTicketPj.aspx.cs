﻿using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSKWeb.Ticket
{
    public partial class SysTicketPj : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        SqlHelper sqlhelper = new SqlHelper();

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            HttpPostedFile file = this.fileUploadCtr.PostedFile;
            string fileName = file.FileName;
            string tempPath = Server.MapPath(InputText.GetConfig("TicketTempFile")); //获取系统临时文件路径
            fileName = System.IO.Path.GetFileName(fileName); //获取文件名（不带路径）
            string currFileExtension = System.IO.Path.GetExtension(fileName); //获取文件的扩展名
            string currFilePath = tempPath + fileName; //获取上传后的文件路径 记录到前面声明的全局变量
            file.SaveAs(currFilePath); //上传

            if (currFileExtension.ToUpper() != ".XLSX")
            {
                JavaScript.Alert(this,"文件格式不正确，上传的文件后缀名必须为XLSX还请确认");
                return;
            }

            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            string NameTable = "";
            string ConText = "";
            //获取Excel路径
            FileInfo info = new FileInfo(currFilePath);
            //获取文件的扩展名
            string fileExt = info.Extension;
            //判断用哪种连接方式
            ConText = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + currFilePath + ";Extended Properties='excel 12.0 Xml;hdr=no;IMEX=1';Persist Security Info=False";
            //连接excel
            OleDbConnection conn = new OleDbConnection(ConText);
            try
            {
                //打开excel
                conn.Open();
                dt = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //获取sheet1表单的表名
                    NameTable = dt.Rows[0]["TABLE_NAME"].ToString();
                    //获取sheet2表单的表名
                    //NameTable = dt.Rows[1]["TABLE_NAME"].ToString();
                }
                string sql = "select * from [" + NameTable + "]";
                da = new OleDbDataAdapter(sql, conn);
                da.Fill(ds, NameTable); //把数据填充到Dataset

                int count = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string id = ds.Tables[0].Rows[i][0].ToString().Trim();
                    string staus = ds.Tables[0].Rows[i][1].ToString().Trim();                    
                    if (staus == "已处理" || staus == "未处理")
                    {

                        SqlParameter[] parsDet = {
                                          new SqlParameter("@backUserId",SqlDbType.NVarChar),
                                          new SqlParameter ("@backTime",SqlDbType.NVarChar),
                                          new SqlParameter ("@backInfo",SqlDbType.NVarChar),
                                          new SqlParameter ("@score_status",SqlDbType.NVarChar),
                                          new SqlParameter ("@staus",SqlDbType.NVarChar),
                                          new SqlParameter ("@id",SqlDbType.NVarChar)
                                             };
                        parsDet[0].Value = this._LinxSanmpleUserCode;
                        parsDet[1].Value = DateTime.Now.ToString();
                        parsDet[2].Value = "";
                        parsDet[3].Value = "0";
                        if (staus == "未处理")
                        {
                            parsDet[4].Value = "2";
                        }
                        else
                        {
                            parsDet[4].Value = "3";
                        }

                        parsDet[5].Value = id;
                        string sqlUp = @" update Sys_Ticket  set backUserId=@backUserId, backTime =@backTime ,backInfo=@backInfo,score_status=@score_status ,staus=@staus 
                    where id =@id";
                        sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sqlUp, parsDet);
                        count = count + 1;
                    }

                }
                JavaScript.Alert(this, "更新成功");

                conn.Close();
            }
            catch(Exception ex)
            {
                conn.Close();
                JavaScript.Alert(this,ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }
    }
}