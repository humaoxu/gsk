﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using System.Collections;
using System.Data.SqlClient;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;

namespace JSKWeb.Ticket
{
    public partial class SysTicketReBackDet : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Loadsu();
                LoadData();
                PageBind();
            }
        }

        private void Loadsu()
        {
            string strResultId = "";
            if (Request.QueryString["resultId"] != null)
            {
                strResultId = Request.QueryString["resultId"].ToString().Trim();
            }
            string strSql = "select distinct SYS_TICKETCFG.id as id,SYS_TICKETCFG.cfgfact +'-'+SYS_TICKETCFG.cfgfacgvalue as suvalue  from Sys_Ticket inner join SyS_TicketDetails on Sys_Ticket.detailcode = SyS_TicketDetails.detailcode inner join SYS_TICKETCFG on SyS_TicketDetails.ticketcfgid = SYS_TICKETCFG.id and Sys_Ticket.resultId = "+ strResultId + "";

            DataSet ds = new DataSet();
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSql);
            ddl1.DataSource = ds;
            ddl1.DataValueField = "id";
            ddl1.DataTextField = "suvalue";
            ddl1.DataBind();
            ddl1.SelectedIndex = 0;
        }
        

        private void LoadData()
        {
            string strResultId = "";
            if (Request.QueryString["resultId"] != null)
            {
                strResultId = Request.QueryString["resultId"].ToString().Trim();
            }

            string strSqlShop = "select * from SurveyObj where surveyObjId in (select distinct surveyobjid from Result where resultid = " + strResultId + ")";
            DataSet dsShop = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSqlShop);
            if (dsShop != null && dsShop.Tables[0].Rows.Count > 0)
            {
                string objcode = dsShop.Tables[0].Rows[0]["objCode"].ToString().Trim();
                string objName = dsShop.Tables[0].Rows[0]["objName"].ToString().Trim();
                lblSU.Text = "门店编号：" + objcode + "  门店名称：" + objName;
            }

            string sqlProjectid = "select Paper.projectid from Result inner join Paper on Result.paperid = paper.paperid and resultId = '" + strResultId + "'";
            DataSet dsProject = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlProjectid);
            string strProject = "11";
            if (dsProject != null && dsProject.Tables[0].Rows.Count > 0)
            {
                strProject = dsProject.Tables[0].Rows[0]["projectid"].ToString().Trim();
            }

            string strTicked = "";

            string sql = "select * from Sys_Ticket where resultId ='" + strResultId + "'";
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                strTicked = ds.Tables[0].Rows[0]["detailcode"].ToString().Trim();
            }
            if (strProject == "11")
            {
                sql = "SELECT  SYS_TICKETCFG.cfgfact as 申诉点 ,SYS_TICKETCFG.cfgfacgvalue as 申诉项目,SyS_TicketDetails.detailreason as 申诉理由,(case when score_status is null then '待确认' when score_status='0' then '不修改得分'  when score_status='1' then '不修改得分'  when score_status='2' then '已修改得分'  when score_status='4' then '已修改得分'  else '待确认' end ) as 申诉反馈状态,backReason as 申诉反馈内容,backUserId as 申诉处理人  FROM SyS_TicketDetails inner join SYS_TICKETCFG on SyS_TicketDetails.ticketcfgid=SYS_TICKETCFG.id ";
            }
            else
            {
                sql = "SELECT  SYS_TICKETCFG.cfgfact as 申诉点 ,SYS_TICKETCFG.cfgfacgvalue as 申诉项目,SyS_TicketDetails.detailreason as 申诉理由,(case when score_status is null then '待确认' when score_status='0' then '不修改得分'  when score_status='1' then '不修改得分'  when score_status='2' then '已修改得分'   when score_status='4' then '已修改得分'  else '待确认' end ) as 申诉反馈状态,backReason as 申诉反馈内容,backUserId as 申诉处理人,pic1 as 申诉佐证照片1,pic2 as 申诉佐证照片2 FROM SyS_TicketDetails inner join SYS_TICKETCFG on SyS_TicketDetails.ticketcfgid=SYS_TICKETCFG.id ";
            }
            
            sql = sql + " where SyS_TicketDetails.detailcode = '" + strTicked + "' order by SyS_TicketDetails.id ";
            DataSet dsss = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
 
            DataTable dt = dsss.Tables[0];
            StringBuilder table_sb = new StringBuilder();
            if (dt != null && dt.Rows.Count > 0)
            {
                table_sb.Append("<table class ='tablestyle'  cellspacing = '0' cellpadding='2' style = 'font-size:10pt;'><tbody>");
                table_sb.Append("<tr class='header'>");
                table_sb.Append("<td align = 'center' style='width:60px;' scope='col'> No.</td>");
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    table_sb.Append("<td align = 'center' style='width:200px;' scope='col'> " + dt.Columns[i].ColumnName + "</td>");
                }
                table_sb.Append("</tr>");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    table_sb.Append("<tr style='cursor:pointer'>");
                    table_sb.Append("<td class='across' align='center'>" + (i + 1).ToString() + "</td>");
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        string strTmp = " align='left' ";

                        if (j == 6)
                        {
                            if (dt.Rows[i][j].ToString().Trim().Trim().Equals(""))
                            {
                                table_sb.Append("<td> <img class='image' src='../images/noimage.jpg' alt='NoImage'/> </td>");
                            }
                            else
                            {
                                table_sb.Append("<td> <img class='image' src='" + dt.Rows[i][j].ToString().Trim() + "' alt='NoImage' /> </td>");
                            }
                        }
                        else if (j == 7)
                        {
                            if (dt.Rows[i][j].ToString().Trim().Trim().Equals(""))
                            {
                                table_sb.Append("<td> <img class='image' src='../images/noimage.jpg' alt='NoImage'/> </td>");
                            }
                            else
                            {
                                table_sb.Append("<td> <img class='image' src='" + dt.Rows[i][j].ToString().Trim() + "' alt='NoImage' /> </td>");
                            }
                        }
                        else
                        {
                            table_sb.Append("<td class='across' " + strTmp + ">" + dt.Rows[i][j].ToString().Trim() + "</td>");
                        }
                        
                    }
                    table_sb.Append("</tr>");
                }

                table_sb.Append("</tbody></table>");
                content.InnerHtml = table_sb.ToString();
            }
        }


        public void PageBind()
        {
            if (Request.QueryString["resultId"] != null)
            {
                string resultId = Request.QueryString["resultId"].ToString();
                string sql = "select * from [Sys_Ticket] where [resultId] =" + resultId;
                DataSet ds = new DataSet();
                ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                  

                    #region 门头照
                    string tableobjPic = "<table><tr>";
                    //  if (dr["objPic1"] != null && dr["objPic1"].ToString().Trim().Length > 0 && dr["objPic2"] != null && dr["objPic2"].ToString().Trim().Length > 0)
                    {
                        if (dr["objPic1_time"].ToString().Trim() != String.Empty && dr["objPic1_time"].ToString().Trim() != String.Empty)
                        {
                            tableobjPic += @"<td> <img class='image' src='ImgHandler.ashx?path=" + dr["objPic1"].ToString() + "' alt='NoImage' /><br>上传日期：" + dr["objPic1_time"].ToString() + " </td>";
                        }

                        if (dr["objPic2_time"].ToString().Trim() != String.Empty && dr["objPic2_time"].ToString().Trim() != String.Empty)
                        {
                            tableobjPic += @"<td> <img class='image' src='ImgHandler.ashx?path=" + dr["objPic2"].ToString() + "' alt='NoImage' /><br>上传日期：" + dr["objPic2_time"].ToString() + " </td>";
                        }
                    }
                    tableobjPic += "</tr></table>";
                    storeImg.InnerHtml = tableobjPic;
                    #endregion

                    #region 照片
                    string appealPic = "<table><tr>";
                    //   if (dr["appealPic1"] != null && dr["appealPic1"].ToString().Trim().Length > 0 && dr["appealPic2"] != null && dr["appealPic2"].ToString().Trim().Length > 0)
                    {
                        var pictures = new List<KeyValuePair<String, String>>() {
                            new KeyValuePair<String, String>(dr["appealPic1"].ToString(), dr["appealPic1_time"].ToString()),
                            new KeyValuePair<String, String>(dr["appealPic2"].ToString(), dr["appealPic2_time"].ToString()),
                            new KeyValuePair<String, String>(dr["appealPic3"].ToString(), dr["appealPic3_time"].ToString()),
                            new KeyValuePair<String, String>(dr["appealPic4"].ToString(), dr["appealPic4_time"].ToString()),
                            new KeyValuePair<String, String>(dr["appealPic5"].ToString(), dr["appealPic5_time"].ToString())
                        };
                        foreach (var keyValue in pictures)
                        {
                            if (keyValue.Value.Trim() != String.Empty && keyValue.Key.Trim() != String.Empty)
                            {
                                appealPic += String.Format("<td><img class=\"image\" src=\"ImgHandler.ashx?path={0}\" /><br />上传日期：{1}</td>", keyValue.Key, keyValue.Value);
                            }
                        }
                    }
                    appealPic += "</tr></table>";
                    //appealImg.InnerHtml = appealPic;
                    #endregion

                    if (dr["staus"].ToString().Trim().Equals("3"))
                    {
                        RadioButton2.Checked = true;
                    }
                    else
                    {
                        RadioButton1.Checked = true;
                    }

                    ViewState["detailcode"] = dr["detailcode"].ToString().Trim();
                    //if (dr["backInfo"] != null)
                    //{
                    //    txtbackInfo.Text = dr["backInfo"].ToString().Trim();
                    //}
                    //if (dr["score_status"] != null)
                    //{
                    //    if (Request.QueryString["go"] != null)
                    //    {
                    //        if (dr["score_status"].ToString().Trim().Equals("0"))
                    //        {
                    //            rad1.Checked = true;
                    //        }
                    //        else if (dr["score_status"].ToString().Trim().Equals("1"))
                    //        {
                    //            rad2.Checked = true;
                    //        }
                    //        else if (dr["score_status"].ToString().Trim().Equals("2"))
                    //        {
                    //            rad3.Checked = true;
                    //        }
                    //        else
                    //        {
                    //            rad4.Checked = true;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        rad2.Visible = false;
                    //        if (dr["score_status"].ToString().Trim().Equals("0") || dr["score_status"].ToString().Trim().Equals("1"))
                    //        {
                    //            rad1.Checked = true;
                    //        }
                    //        else if (dr["score_status"].ToString().Trim().Equals("2"))
                    //        {
                    //            rad3.Checked = true;
                    //        }
                    //        else
                    //        {
                    //            rad4.Checked = true;
                    //        }
                    //    }
                    //} 
                }
            }

            if (Request.QueryString["go"] == null)
            {
                feedbacksu.Visible = false;
                fileticket.Visible = false;
            }
        }

   

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["resultId"] != null)
            {
                try
                {
                    string resultId = Request.QueryString["resultId"].ToString();
                    string strfactId = ddl1.SelectedValue;
                    string detailcode = ViewState["detailcode"].ToString();
                    
                    SqlParameter[] parsDet = {
                                          new SqlParameter("@detailcode",SqlDbType.NVarChar),
                                          new SqlParameter("@backUserId",SqlDbType.NVarChar),
                                            new SqlParameter("@backReason",SqlDbType.NVarChar),
                                          new SqlParameter("@ticketcfgid",SqlDbType.NVarChar),
                                          new SqlParameter ("@score_status",SqlDbType.NVarChar),
                                          new SqlParameter ("@backTime",SqlDbType.NVarChar)
                                             };
                    parsDet[0].Value = detailcode;
                    parsDet[1].Value = this._LinxSanmpleUserCode;
                    parsDet[2].Value = txtbackInfo.Text.ToString();
                    parsDet[3].Value = strfactId;

                    if (rad1.Checked)
                    {
                        parsDet[4].Value = "0";
                    }
                    else if(rad2.Checked)
                    {
                        parsDet[4].Value = "1";
                    }
                    else if(rad3.Checked)
                    {
                        parsDet[4].Value = "2";
                    }
                    else if (rad4.Checked)
                    {
                        parsDet[4].Value = "3";
                    }
                    else 
                    {
                        parsDet[4].Value = "4";
                    }

                    parsDet[5].Value = DateTime.Now.ToString();
                    
                    string sql = @" update SyS_TicketDetails set backUserId=@backUserId, backTime =@backTime ,backReason=@backReason,score_status=@score_status where detailcode = @detailcode and ticketcfgid=@ticketcfgid ";
                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql, parsDet);
                    JavaScript.Alert(this, "提交成功。");
                    rad1.Checked = false;
                    rad2.Checked = false;
                    rad3.Checked = false;
                    rad4.Checked = false;
                    rad5.Checked = false;
                    txtbackInfo.Text = "";
                    LoadData();
                }
                catch(Exception ex)
                {
                    JavaScript.Alert(this, "提交失败。");
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            JavaScript.ClosePage(this);
        }

        protected void rad1_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void ddl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnStatus_Click(object sender, EventArgs e)
        {
           
        }

        protected void btnStatus_Click1(object sender, EventArgs e)
        {
            if (Request.QueryString["resultId"] != null)
            {
                try
                {
                    string resultId = Request.QueryString["resultId"].ToString();

                    string status = "2";
                    if (RadioButton2.Checked)
                    {
                        status = "3";
                    }

                    //string sqlchk = "select * from [Sys_Ticket] where staus='3' and [resultId] =" + resultId;
                    //DataSet ds = new DataSet();
                    //ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlchk);
                    //if (ds != null && ds.Tables[0].Rows.Count > 0)
                    //{
                    //    JavaScript.Alert(this, "该申诉单已被处理，不可以重复处理。");
                    //    this.btnSubmit.Enabled = false;
                    //    return;
                    //}

                    SqlParameter[] parsDet = {
                                          new SqlParameter("@backUserId",SqlDbType.NVarChar),
                                          new SqlParameter ("@backTime",SqlDbType.NVarChar),
                                          new SqlParameter ("@backInfo",SqlDbType.NVarChar),
                                          new SqlParameter ("@score_status",SqlDbType.NVarChar),
                                          new SqlParameter ("@staus",SqlDbType.NVarChar)
                                             };
                    parsDet[0].Value = this._LinxSanmpleUserCode;
                    parsDet[1].Value = DateTime.Now.ToString();
                    parsDet[2].Value = txtbackInfo.Text.ToString();
                    parsDet[3].Value = "0";


                    parsDet[4].Value = status;
                    string sql = @" update Sys_Ticket  set backUserId=@backUserId, backTime =@backTime ,backInfo=@backInfo,score_status=@score_status ,staus=@staus 
                    where resultId =" + resultId;
                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql, parsDet);
                    JavaScript.Alert(this, "提交成功。");
                }
                catch
                {
                    JavaScript.Alert(this, "提交失败。");
                }
            }
        }
    }
}
