﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JSKWeb.Code;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using JSKWeb.Utility;
using System.Web.UI.HtmlControls;

namespace JSKWeb.Ticket
{
    public partial class SysTicketDet_FMCG : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        string resultId = "";
        string projectId = "";
        string tickedcode = "";
        string _objfile1 = "";
        string _objfile2 = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["resultId"] != null)
            {
                ViewState["resultId"] = Request.QueryString["resultId"].ToString().Trim();
            }
            if (Request.QueryString["projectId"] != null)
            {
                ViewState["projectId"] = Request.QueryString["projectId"].ToString().Trim();
            }
            if (!IsPostBack)
            {
                //if (ViewState["projectId"].ToString().Trim().Equals("11"))
                //{
                //    lblWarn.Visible = true;
                //}
                //else
                //{
                //    lblWarn.Visible = false;
                //}

                lblWarn.Visible = false;
                CkPartDataBind();

                string strSqlShop = "select * from SurveyObj where surveyObjId in (select distinct surveyobjid from Result where resultid = " + ViewState["resultId"].ToString() + ")";
                DataSet dsShop = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSqlShop);
                if (dsShop != null && dsShop.Tables[0].Rows.Count > 0)
                {
                    string objcode = dsShop.Tables[0].Rows[0]["objCode"].ToString().Trim();
                    string objName = dsShop.Tables[0].Rows[0]["objName"].ToString().Trim();
                    lblSU.Text = "门店编号：" + objcode + "  门店名称：" + objName;
                }


                string sql = "select * from Sys_Ticket where resultId ='" + ViewState["resultId"].ToString() + "'";
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    ViewState["tickedcode"] = ds.Tables[0].Rows[0]["detailcode"].ToString().Trim();
                    ViewState["_objfile1"] = ds.Tables[0].Rows[0]["objPic1"].ToString().Trim();
                    ViewState["_objfile2"] = ds.Tables[0].Rows[0]["objPic2"].ToString().Trim();

                    LoadData();
                }
                else
                {
                    ViewState["tickedcode"] = Guid.NewGuid().ToString();
                    ViewState["_objfile1"] = "";
                    ViewState["_objfile2"] = "";
                }

            }
        }

        public void CkPartDataBind()
        {
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            select_sql = " select fact from sys_tickedfact where projectid = " + ViewState["projectId"].ToString() + " order by orderno ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddl1.DataSource = select_ds;
                ddl1.DataValueField = "fact";
                ddl1.DataTextField = "fact";
                ddl1.DataBind();
            }

            ddl1.SelectedIndex = 0;

            var select_sql2 = string.Empty;
            DataSet select_ds2 = new DataSet();
            string strText = ddl1.SelectedValue;

            select_sql2 = "SELECT  id  ,cfgfacgvalue FROM SYS_TICKETCFG where projectid = " + ViewState["projectId"].ToString() + " and cfgfact = '" + strText + "' and openflag = 1 order by orderno ";
            select_ds2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql2);
            if (select_ds2 != null)
            {
                ddl2.DataSource = select_ds2;
                ddl2.DataValueField = "id";
                ddl2.DataTextField = "cfgfacgvalue";
                ddl2.DataBind();
            }
        }

        private void LoadData()
        {

            string sql = "SELECT  SyS_TicketDetails.id,SyS_TicketDetails.detailreason,SyS_TicketDetails.ticketcfgid,SYS_TICKETCFG.cfgfact,SYS_TICKETCFG.cfgfacgvalue,SyS_TicketDetails.pic1,SyS_TicketDetails.pic2  FROM SyS_TicketDetails inner join SYS_TICKETCFG on SyS_TicketDetails.ticketcfgid=SYS_TICKETCFG.id ";

            sql = sql + " where SyS_TicketDetails.detailcode = '" + ViewState["tickedcode"].ToString() + "' order by SyS_TicketDetails.id ";
            DataSet dsss = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            gvMain.DataSource = dsss;
            gvMain.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtReason.Text.Trim().Equals(""))
                {
                    JavaScript.Alert(this, "申诉理由不可以为空。");
                    return;
                }

                string objPic1 = "";
                string pathobjPic1 = "";
                string savpic1 = "";
                if (FileUpload1.HasFile)
                {

                    objPic1 = Guid.NewGuid().ToString();
                    pathobjPic1 = InputText.GetConfig("TICKEYIMAGE") + "\\" + DateTime.Today.ToString("yyyyMMdd") + "\\" + ViewState["resultId"].ToString();
                    savpic1 = InputText.GetConfig("TICKEYSSHOW") + @"/" + DateTime.Today.ToString("yyyyMMdd") + @"/" + ViewState["resultId"].ToString();
                    string filename = FileUpload1.FileName;

                    if (System.IO.Directory.Exists(pathobjPic1) == false)
                    {
                        System.IO.Directory.CreateDirectory(pathobjPic1);
                    }
                    pathobjPic1 += "\\su_" + objPic1 + ".jpg";
                    savpic1 += @"/su_" + objPic1 + ".jpg";
                    FileUpload1.SaveAs(pathobjPic1);
                }

                string objPic2 = "";
                string pathobjPic2 = "";
                string savpic2 = "";

                if (FileUpload2.HasFile)
                {
                    objPic2 = Guid.NewGuid().ToString();
                    pathobjPic2 = InputText.GetConfig("TICKEYIMAGE") + "\\" + DateTime.Today.ToString("yyyyMMdd") + "\\" + ViewState["resultId"].ToString();
                    savpic2 = InputText.GetConfig("TICKEYSSHOW") + @"/" + DateTime.Today.ToString("yyyyMMdd") + @"/" + ViewState["resultId"].ToString();


                    string filename = FileUpload2.FileName;

                    if (System.IO.Directory.Exists(pathobjPic2) == false)
                    {
                        System.IO.Directory.CreateDirectory(pathobjPic2);
                    }
                    pathobjPic2 += "\\su_" + objPic2 + ".jpg";
                    savpic2 += @"/su_" + objPic2 + ".jpg";
                    FileUpload2.SaveAs(pathobjPic2);
                }


                string ticfactid = ddl2.SelectedValue;
                string sql = "select * from SyS_TicketDetails where detailcode ='" + ViewState["tickedcode"].ToString() + "' and ticketcfgid = " + ticfactid;
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    JavaScript.Alert(this, "该申诉点已在申诉列表中。");
                    return;
                }
                else
                {
                    SqlParameter[] parsDet = {
                                              new SqlParameter("@detailcode",SqlDbType.NVarChar),
                                              new SqlParameter ("@detailreason",SqlDbType.NVarChar),
                                              new SqlParameter ("@detailfile",SqlDbType.NVarChar),
                                              new SqlParameter ("@backUserId",SqlDbType.NVarChar),
                                              new SqlParameter ("@backReason",SqlDbType.NVarChar),
                                              new SqlParameter ("@memo",SqlDbType.NVarChar),
                                              new SqlParameter ("@ticketcfgid",SqlDbType.NVarChar),
                                              new SqlParameter ("@pic1",SqlDbType.NVarChar),
                                              new SqlParameter ("@pic2",SqlDbType.NVarChar),};
                    parsDet[0].Value = ViewState["tickedcode"].ToString();
                    parsDet[1].Value = txtReason.Text.Trim();
                    parsDet[2].Value = "";
                    parsDet[3].Value = "";
                    parsDet[4].Value = "";
                    parsDet[5].Value = "";
                    parsDet[6].Value = ddl2.SelectedValue;

                    parsDet[7].Value = savpic1;

                    parsDet[8].Value = savpic2;
                    sql = @"insert into SyS_TicketDetails([detailcode]
                  ,[detailreason] 
                  ,[detailfile] ,[backUserId]
                  ,[backReason] ,[memo]
                  ,[ticketcfgid],[pic1],[pic2] )values(@detailcode,@detailreason,@detailfile,@backUserId,@backReason,@memo,@ticketcfgid,@pic1,@pic2)";

                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql, parsDet);

                    LoadData();
                    this.txtReason.Text = "";
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            JavaScript.ClosePage(this);
        }

        protected void ddl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl2.Items.Clear();

            var select_sql2 = string.Empty;
            DataSet select_ds2 = new DataSet();

            select_sql2 = "SELECT  id  ,cfgfacgvalue FROM SYS_TICKETCFG where projectid = " + ViewState["projectId"].ToString() + " and cfgfact = '" + ddl1.SelectedValue + "' and openflag = 1 order by orderno ";
            select_ds2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql2);
            if (select_ds2 != null)
            {
                ddl2.DataSource = select_ds2;
                ddl2.DataValueField = "id";
                ddl2.DataTextField = "cfgfacgvalue";
                ddl2.DataBind();
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["id"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");

                Image img1 = (Image)e.Row.FindControl("im1");
                Image img2 = (Image)e.Row.FindControl("im2");

                if (drv["pic1"].ToString().Trim().Equals(""))
                {
                    img1.ImageUrl = "../images/noimage.jpg";
                }
                else
                {
                    img1.ImageUrl = drv["pic1"].ToString().Trim();
                }

                if (drv["pic2"].ToString().Trim().Equals(""))
                {
                    img2.ImageUrl = "../images/noimage.jpg";
                }
                else
                {
                    img2.ImageUrl = drv["pic2"].ToString().Trim();
                }

            }
        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            try
            {

                string delSql1 = "delete from SyS_TicketDetails where id = " + hfKey.Value + "";

                sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, delSql1);


                LoadData();
            }
            catch
            {
                throw;
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "select * from SyS_TicketDetails where detailcode ='" + ViewState["tickedcode"].ToString() + "'";
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                }
                else
                {
                    JavaScript.Alert(this, "申诉点及申诉理由不可以为空。");
                    return;
                }

                //if (ViewState["projectId"].ToString().Trim().Equals("11"))
                //{
                //    string sqlCHKSHOP = "select * from SyS_TicketDetails where ticketcfgid in(61,62) and detailcode = '" + ViewState["tickedcode"].ToString() + "'";
                //    DataSet dsss = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlCHKSHOP);
                //    if (dsss.Tables[0].Rows.Count > 0)
                //    {
                //        if (file1.HasFile || file2.HasFile || ViewState["_objfile1"].ToString().Trim() != "" || ViewState["_objfile2"].ToString().Trim() != "")
                //        {

                //        }
                //        else
                //        {
                //            JavaScript.Alert(this, "必须上传一张门头照，请确认。");
                //            return;
                //        }
                //    }

                //}
                //else
                //{
                //    if (file1.HasFile || file2.HasFile || ViewState["_objfile1"].ToString().Trim() != "" || ViewState["_objfile2"].ToString().Trim() != "")
                //    {

                //    }
                //    else
                //    {
                //        JavaScript.Alert(this, "必须上传一张门头照，请确认。");
                //        return;
                //    }
                //}


                if (file1.HasFile && txtobjPic1Date.Text.Trim().Equals(""))
                {
                    JavaScript.Alert(this, "门头照1的拍摄日期不可以为空，请输入。");
                    return;
                }

                if (file2.HasFile && txtobjPic2Date.Text.Trim().Equals(""))
                {
                    JavaScript.Alert(this, "门头照2的拍摄日期不可以为空，请输入。");
                    return;
                }


                string partId = "";


                #region 上传门头照1
                string objPic1 = "";
                string pathobjPic1 = "";
                if (file1.HasFile)
                {

                    objPic1 = Guid.NewGuid().ToString();
                    pathobjPic1 = InputText.GetConfig("TICKEYPHOTO") + "\\" + DateTime.Today.ToString("yyyyMMdd") + "\\" + ViewState["resultId"].ToString();
                    string filename = file1.FileName;

                    if (System.IO.Directory.Exists(pathobjPic1) == false)
                    {
                        System.IO.Directory.CreateDirectory(pathobjPic1);
                    }
                    pathobjPic1 += "\\objPic1_" + objPic1 + ".jpg";
                    file1.SaveAs(pathobjPic1);
                }
                #endregion
                string objPic2 = "";
                string pathobjPic2 = "";
                #region 上传门头照2
                if (file2.HasFile)
                {
                    objPic2 = Guid.NewGuid().ToString();
                    pathobjPic2 = InputText.GetConfig("TICKEYPHOTO") + "\\" + DateTime.Today.ToString("yyyyMMdd") + "\\" + ViewState["resultId"].ToString();


                    string filename = file2.FileName;

                    if (System.IO.Directory.Exists(pathobjPic2) == false)
                    {
                        System.IO.Directory.CreateDirectory(pathobjPic2);
                    }
                    pathobjPic2 += "\\objPic2_" + objPic2 + ".jpg";
                    file2.SaveAs(pathobjPic2);
                }
                #endregion
                string appealPic1 = "";
                string pathappealPic1 = "";

                string appealPic2 = "";
                string pathappealPic2 = "";

                string appealPic3 = "";
                string pathappealPic3 = "";

                string appealPic4 = "";
                string pathappealPic4 = "";
                string appealPic5 = "";

                //#region 照片5
                string pathappealPic5 = "";

                SqlParameter[] parsDet = {
                                  new SqlParameter("@partId",SqlDbType.NVarChar),
                                  new SqlParameter ("@Reason",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic1",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic1_time",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic2",SqlDbType.NVarChar),
                                  new SqlParameter ("@objPic2_time",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic1",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic1Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic2",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic2Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic3",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic3Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic4",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic4Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic5",SqlDbType.NVarChar),
                                  new SqlParameter ("@appealPic5Date",SqlDbType.NVarChar),
                                  new SqlParameter ("@createUser",SqlDbType.NVarChar),
                                  new SqlParameter ("@createTime",SqlDbType.NVarChar),
                                  new SqlParameter ("@resultId",SqlDbType.NVarChar),
                                  new SqlParameter("@staus", SqlDbType.NVarChar),
                                  new SqlParameter("@projectId", SqlDbType.Int),
                                  new SqlParameter("@detailcode", SqlDbType.NVarChar)
                                  };
                parsDet[0].Value = partId;
                parsDet[1].Value = txtReason.Text.Trim();
                parsDet[2].Value = pathobjPic1.Trim().Length > 0 ? pathobjPic1 : "";
                parsDet[3].Value = txtobjPic1Date.Text.Trim();
                parsDet[4].Value = pathobjPic2.Trim().Length > 0 ? pathobjPic2 : "";
                parsDet[5].Value = txtobjPic2Date.Text.Trim();
                parsDet[6].Value = pathappealPic1.Trim().Length > 0 ? pathappealPic1 : "";
                parsDet[7].Value = "";
                parsDet[8].Value = pathappealPic2.Trim().Length > 0 ? pathappealPic2 : "";
                parsDet[9].Value = "";
                parsDet[10].Value = pathappealPic3.Trim().Length > 0 ? pathappealPic3 : "";
                parsDet[11].Value = "";
                parsDet[12].Value = pathappealPic4.Trim().Length > 0 ? pathappealPic4 : "";
                parsDet[13].Value = "";
                parsDet[14].Value = pathappealPic5.Trim().Length > 0 ? pathappealPic5 : "";
                parsDet[15].Value = "";
                parsDet[16].Value = this._LinxSanmpleUserCode;
                parsDet[17].Value = DateTime.Now.ToString();
                parsDet[18].Value = ViewState["resultId"].ToString();
                parsDet[19].Value = 2;
                parsDet[20].Value = ViewState["projectId"].ToString();
                parsDet[21].Value = ViewState["tickedcode"].ToString();


                string sqlchk = "select * from Sys_Ticket where resultId ='" + ViewState["resultId"].ToString() + "'";
                DataSet dschk = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlchk);
                if (dschk != null && dschk.Tables[0].Rows.Count > 0)
                {

                    sql = @"update Sys_Ticket set createUser=@createUser,createTime=@createTime ";

                    if (pathobjPic1 != "")
                    {
                        sql = sql + @"  ,objPic1 = @objPic1,objPic1_time=@objPic1_time ";
                    }

                    if (pathobjPic2 != "")
                    {
                        sql = sql + @" ,objPic2 = @objPic2,objPic2_time=@objPic2_time  ";
                    }
                    sql = sql + @"   where resultId=@resultId  ";
                }
                else
                {

                    sql = @"insert into Sys_Ticket([partId]
                          ,[reason] 
                          ,[objPic1] ,[objPic1_time]
                          ,[objPic2] ,[objPic2_time]
                          ,[appealPic1] ,[appealPic1_time]
                          ,[appealPic2] ,[appealPic2_time]
                          ,[appealPic3] ,[appealPic3_time]
                          ,[appealPic4]  ,[appealPic4_time]
                          ,[appealPic5],[appealPic5_time],createUser,createTime,resultId,staus,projectId,detailcode)values(@partId,@Reason,@objPic1,@objPic1_time,@objPic2,@objPic2_time,@appealPic1,@appealPic1Date
                        ,@appealPic2,@appealPic2Date
                        ,@appealPic3,@appealPic3Date
                        ,@appealPic4,@appealPic4Date
                        ,@appealPic5,@appealPic5Date,@createUser,@createTime,@resultId,@staus,@projectId,@detailcode)";
                }
                sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql, parsDet);

                JavaScript.Alert(this, "申诉提交成功");

            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }
    }
}