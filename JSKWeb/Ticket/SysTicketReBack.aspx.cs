﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;

namespace JSKWeb.Ticket
{
    public partial class SysTicketReBack : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        int iPageSize = 20;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DDLDataBind();

                TicketTypeDataBind();   
            }    
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = ddlProject.SelectedValue;
            var ddlTicketTypeSelectSql = "";
            if (ddlProjectId == "25")
            {

                 ddlTicketTypeSelectSql = "SELECT paperId, papername as paperTitle FROM Paper_GSKHZ order by paperId desc ";
            }
            else
            {

                 ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0} order by paperId desc ", ddlProjectId);
            }
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();
        }

        public void DDLDataBind()
        {
            var ddlProjectSelectSql = @"SELECT projectId, prjName FROM Project";
            var ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlProjectSelectSql);
            ddlProject.DataSource = ddlDataSet;
            ddlProject.DataValueField = "projectId";
            ddlProject.DataTextField = "prjName";
            ddlProject.DataBind();

            ///绑定状态
            string sql = @" select * from v_PubReference where parentcode='TickCode' ";
            DataSet ds = new DataSet();
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            ddlZhuangtai.DataSource = ds;
            ddlZhuangtai.DataValueField = "code";
            ddlZhuangtai.DataTextField = "name";
            ddlZhuangtai.DataBind();
            ddlZhuangtai.Items.Insert(0, new ListItem("全部", "NULL"));
            ddlZhuangtai.SelectedValue = "NULL";

            ////绑定类型 DDT
            //sql = @"  SELECT p.paperId, p.paperTitle FROM dbo.SurveyObj s INNER JOIN dbo.Paper p ON s.projectId = p.projectId and  s.col1='" + this._LinxSanmpleUserCode + "' join Project j on j.[projectId] = p.projectId and j.[prjName] ='DDT' ";
            //ds = new DataSet();
            //ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            //ddlTicketType.DataSource = ds;
            //ddlTicketType.DataValueField = "paperId";
            //ddlTicketType.DataTextField = "paperTitle";
            //ddlTicketType.DataBind();

            //ddlTicketType.Items.Insert(0, new ListItem("全部", "NULL"));
            //ddlTicketType.SelectedValue = "NULL";
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }

        private bool checkDateTimeFormat(string txtTime)
        {
            try
            {
                string sDate = txtTime.Substring(0, 4) + "-" + txtTime.Substring(4, 2) + "-" + txtTime.Substring(6, 2);
                DateTime.Parse(sDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void LoadInfo()
        {
            if (txtPageSise.Text.Trim().Length > 0)
            {
                iPageSize = InputText.GetInt0(txtPageSise.Text);
            }
            Pager.PageSize = iPageSize;
            int recCnt;

            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }

                //if (ddlTicketType.SelectedItem != null)
                //{
                //    ViewState["whereName"] += " and paperId ='" + ddlTicketType.SelectedItem.Value + "' ";
                //}

                //modify
                if (ddlZhuangtai.SelectedIndex > 0)
                {
                    ViewState["whereName"] += " and Ticketstaus ='" + ddlZhuangtai.SelectedItem.Text.Trim() + "' ";
                }

                if (txtStoreCode.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopcode like'%" + txtStoreCode.Text + "%' ";
                }

                if (txtStoreName.Text.Trim().Length > 0)
                {
                    ViewState["whereName"] += " and shopname like'%" + txtStoreName.Text + "%' ";
                }

                viewstatesql.InnerText = ViewState["whereName"].ToString();

                string str1 = InputText.GetConfig("gsk_su_otc_paper");
                string str2 = InputText.GetConfig("gsk_su_ddt_paper");
                string str3 = InputText.GetConfig("gsk_su_fmcg_paper");
                string str4 = InputText.GetConfig("gsk_su_otchz_paper"); 
                string strPaperid = ddlTicketType.SelectedItem.Value;

                StringBuilder sb = new StringBuilder();
                if (strPaperid == str1 || strPaperid == str2 || strPaperid == str3 )
                {
                    sb.Append("(");
                    sb.Append(" select a.shopid,a.shopcode,a.shopname,a.paperId,t.backUserId as backuser ");
                    sb.Append(" , a.t_score ");
                    sb.Append(" , a.submitdate, CASE WHEN t .staus IS NULL ");
                    sb.Append(" THEN '未申诉' WHEN t .staus = '1' THEN '未申诉' WHEN t .staus = '2' THEN '已申诉' WHEN t .staus = '3' THEN '已处理' END AS Ticketstaus,a.resultId,t.staus as ticketstatuscode,t.id,t.createTime as csdate ,t.backTime as godate ");
                    sb.Append(" from SYS_SHENSU a  ");
                    sb.Append("  left join sys_ticket t on a.resultid = t.resultid  where paperid = " + ddlTicketType.SelectedItem.Value + "  ");
                    sb.Append(") a ");
                }
                else if(ddlProject.SelectedValue == "25")
                {
                    sb.Append("(");
                    sb.Append(" select a.surveyobjid as shopid,s.objcode as shopcode,s.objname as shopname,a.Paperid as paperId,t.backUserId as backuser ");
                    sb.Append(" , v.t_score as t_score ");
                    sb.Append(" , a.appSubmitDate submitdate, CASE WHEN t .staus IS NULL ");
                    sb.Append(" THEN '未申诉' WHEN t .staus = '1' THEN '未申诉' WHEN t .staus = '2' THEN '已申诉' WHEN t .staus = '3' THEN '已处理' END AS Ticketstaus,a.resultId,t.staus as ticketstatuscode,t.id,t.createTime as csdate ,t.backTime as godate ");
                    sb.Append(" from Result a  ");
                    sb.Append(" inner join  SYS_OTC_RESULT_HZ v on a.resultId = v.resultId and  v.paperid = " + ddlTicketType.SelectedItem.Value + "   inner ");
                    sb.Append(" join Userlogin b on a.userLoginId = b.userLoginId and  a.isRelease = 'Y' and b.loginId not like 'qc%' and b.loginId not like 'test%'  left join ");
                    sb.Append(" sys_ticket t on a.resultid = t.resultid inner join surveyobj s on a.surveyobjid = s.surveyobjid ");
                    sb.Append(") a ");
                }
                else
                {
                    sb.Append("(");
                    sb.Append(" select a.surveyobjid as shopid,s.objcode as shopcode,s.objname as shopname,a.Paperid as paperId,t.backUserId as backuser ");
                    sb.Append(" , v.t_score as t_score ");
                    sb.Append(" , a.appSubmitDate submitdate, CASE WHEN t .staus IS NULL ");
                    sb.Append(" THEN '未申诉' WHEN t .staus = '1' THEN '未申诉' WHEN t .staus = '2' THEN '已申诉' WHEN t .staus = '3' THEN '已处理' END AS Ticketstaus,a.resultId,t.staus as ticketstatuscode,t.id,t.createTime as csdate ,t.backTime as godate ");
                    sb.Append(" from Result a  ");
                    sb.Append(" left join v_sys_score v on a.resultId = v.resultId inner ");
                    sb.Append(" join Userlogin b on a.userLoginId = b.userLoginId and  a.isRelease = 'Y' and b.loginId not like 'qc%' and b.loginId not like 'test%'  left join ");
                    sb.Append(" sys_ticket t on a.resultid = t.resultid inner join surveyobj s on a.surveyobjid = s.surveyobjid where paperid = " + ddlTicketType.SelectedItem.Value + "  ");
                    sb.Append(") a ");
                }
   
                int pageindex = 1;
                if (!IsPostBack)
                {
                    if (Request.QueryString["PAGENUM"] != null)
                    {
                        pageindex = Convert.ToInt32(Request.QueryString["PAGENUM"].ToString());
                    }
                }
                else
                {
                    pageindex = this.Pager.CurrentPageIndex;
                }
                

                DataTable dt = sqlhelper.GetPagingRecord(sb.ToString(), "csdate ", Pager.PageSize, pageindex, ViewState["whereName"].ToString(), "*", out recCnt);
                Pager.RecordCount = recCnt;
                this.Pager.CurrentPageIndex = pageindex;
                Pager.Visible = recCnt > 0;

                this.Pager.CustomInfoHTML = string.Format(CommonStirng.StrPagerCustomerInfo, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount, this.Pager.RecordCount, iPageSize });
                gvMain.DataSource = dt;
                gvMain.DataBind();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            var ddlProjectId = ddlProject.SelectedValue;
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["resultId"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");
                HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                
                HyperLink btnsm = (HyperLink)e.Row.FindControl("lbtnMingxi");
                StringBuilder sb = new StringBuilder();
                sb.Append(InputText.GetConfig("gsk_url"));
                if (this._LinxSanmpleUserCode.ToUpper().ToString() == "CARL")
                {
                    sb.Append("viewresult=1&");
                }
                sb.Append("resultId=");
                sb.Append(drv["resultId"].ToString());
                if(ddlProjectId == "25")
                {

                    sb.Append("&paperId=77");
                }
                else
                {

                    sb.Append("&paperId=");
                    sb.Append(drv["paperId"].ToString());
                }
                btnsm.NavigateUrl = @"javascript:void window.open('" + sb.ToString() + "')";
                if (drv["Ticketstaus"].ToString().Trim().Equals("未申诉"))
                {

                    btn.Visible = false;

                    btn.NavigateUrl = @"#";
                }
                else
                {
                    btn.NavigateUrl = @"javascript:void window.open('SysTicketReBackDet.aspx?resultId=" + drv["resultId"].ToString() + "&go=1')";
                }
            }
        }

        protected void ddlProject_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            TicketTypeDataBind();
        }

        protected void btnOutPut_Click(object sender, EventArgs e)
        {
            string projectid = ddlProject.SelectedValue;
            string strPaperId = ddlTicketType.SelectedValue;

            string str1 = InputText.GetConfig("gsk_su_otc_paper");
            string str2 = InputText.GetConfig("gsk_su_ddt_paper");
            string str3 = InputText.GetConfig("gsk_su_fmcg_paper");

            string str4 = InputText.GetConfig("gsk_su_otchz_paper");
            string strPaperid = ddlTicketType.SelectedItem.Value;

            StringBuilder sb = new StringBuilder();
            sb.Append("");
            if (strPaperid == str1 || strPaperid == str2 || strPaperid == str3 )
            {
                sb.Append("select Sys_Ticket.id as 申诉票据No,Sys_Ticketdetails.id as 申诉子项No, SurveyObj.objCode as 计划店铺编码,SurveyObj.objName as 计划店铺名称, SYS_SHENSU.shopcode as 执行店铺编码,SYS_SHENSU.shopname as 执行店铺名称,surveyobj.col1 尼尔森区域,surveyobj.col2 尼尔森省,surveyobj.col3 尼尔森市,surveyobj.col4 尼尔森县级市, SYS_TICKETCFG.cfgfact as 申诉点 ,SYS_TICKETCFG.cfgfacgvalue as 申诉项目 , SyS_TicketDetails.detailreason as 申诉理由 ,SyS_Ticket.createTime as 申诉时间 ,SyS_Ticket.createUser as 申诉用户ID,SyS_TicketDetails.backUserId as 申诉处理用户ID,");
                sb.Append("(case when SyS_Ticket.staus = '3' then '已处理' else '已申诉' end) as 申诉票据状态,");
                sb.Append("sys_ticketdetails.backreason as 反馈内容 ,sys_ticketdetails.backtime 反馈时间,");
                sb.Append("(case when sys_ticketdetails.score_status = '0' then '不修改得分' when sys_ticketdetails.score_status = '1' then '不修改得分无照片'  when sys_ticketdetails.score_status = '2' then '已修改得分'  when sys_ticketdetails.score_status = '4' then '客户要求修改得分'  else '待确定' end) as 申诉小项状态  from Sys_Ticket inner join SyS_TicketDetails on Sys_Ticket.detailcode = SyS_TicketDetails.detailcode and  Sys_Ticket.resultId in (select resultId from SYS_SHENSU where paperId = " + strPaperId + " ) inner join SYS_TICKETCFG on SyS_TicketDetails.ticketcfgid = SYS_TICKETCFG.id  inner join SYS_SHENSU on Sys_Ticket.resultId = SYS_SHENSU.resultId   and SYS_SHENSU.paperId = " + strPaperId + " inner join ResultView on SYS_SHENSU.resultid = ResultView.resultid inner join surveyobj on ResultView.plansurveyobjid = surveyobj.surveyobjid order by  SYS_SHENSU.shopcode ");
            }
            else
            {
                if(ddlProject.SelectedValue == "25")
                {
                    sb.Append("select Sys_Ticket.id as 申诉票据No,Sys_Ticketdetails.id as 申诉子项No, SurveyObj.objCode as 店铺编码,SurveyObj.objname as 店铺名称,surveyobj.col1 尼尔森区域,surveyobj.col2 尼尔森省,surveyobj.col3 尼尔森市,surveyobj.col4 尼尔森县级市, SYS_TICKETCFG.cfgfact as 申诉点 ,SYS_TICKETCFG.cfgfacgvalue as 申诉项目 , SyS_TicketDetails.detailreason as 申诉理由 ,SyS_Ticket.createTime as 申诉时间 ,SyS_Ticket.createUser as 申诉用户ID,SyS_TicketDetails.backUserId as 申诉处理用户ID,");
                    sb.Append("(case when SyS_Ticket.staus = '3' then '已处理' else '已申诉' end) as 申诉票据状态,");
                    sb.Append("sys_ticketdetails.backreason as 反馈内容 ,sys_ticketdetails.backtime 反馈时间,");
                    sb.Append("(case when sys_ticketdetails.score_status = '0' then '不修改得分' when sys_ticketdetails.score_status = '1' then '不修改得分无照片'  when sys_ticketdetails.score_status = '2' then '已修改得分'  when sys_ticketdetails.score_status = '4' then '客户要求修改得分'  else '待确定' end) as 申诉小项状态  from Sys_Ticket inner join SyS_TicketDetails on Sys_Ticket.detailcode = SyS_TicketDetails.detailcode and  Sys_Ticket.resultId in (select resultId from Result where surveyObjId in (select surveyObjId from SurveyObj where projectid = " + projectid + ")) inner join SYS_TICKETCFG on SyS_TicketDetails.ticketcfgid = SYS_TICKETCFG.id  inner join Result on Sys_Ticket.resultId = Result.resultId and Result.isRelease = 'Y' inner join SurveyObj on SurveyObj.surveyObjId = Result.surveyObjId inner join SYS_OTC_RESULT_HZ on Result.resultid=SYS_OTC_RESULT_HZ.resultid  and SYS_OTC_RESULT_HZ.paperId = " + strPaperId + " order by  SurveyObj.objCode");

                }
                else
                {
                    sb.Append("select Sys_Ticket.id as 申诉票据No,Sys_Ticketdetails.id as 申诉子项No, SurveyObj.objCode as 店铺编码,SurveyObj.objname as 店铺名称,surveyobj.col1 尼尔森区域,surveyobj.col2 尼尔森省,surveyobj.col3 尼尔森市,surveyobj.col4 尼尔森县级市, SYS_TICKETCFG.cfgfact as 申诉点 ,SYS_TICKETCFG.cfgfacgvalue as 申诉项目 , SyS_TicketDetails.detailreason as 申诉理由 ,SyS_Ticket.createTime as 申诉时间 ,SyS_Ticket.createUser as 申诉用户ID,SyS_TicketDetails.backUserId as 申诉处理用户ID,");
                    sb.Append("(case when SyS_Ticket.staus = '3' then '已处理' else '已申诉' end) as 申诉票据状态,");
                    sb.Append("sys_ticketdetails.backreason as 反馈内容 ,sys_ticketdetails.backtime 反馈时间,");
                    sb.Append("(case when sys_ticketdetails.score_status = '0' then '不修改得分' when sys_ticketdetails.score_status = '1' then '不修改得分无照片'  when sys_ticketdetails.score_status = '2' then '已修改得分'  when sys_ticketdetails.score_status = '4' then '客户要求修改得分'  else '待确定' end) as 申诉小项状态  from Sys_Ticket inner join SyS_TicketDetails on Sys_Ticket.detailcode = SyS_TicketDetails.detailcode and  Sys_Ticket.resultId in (select resultId from Result where surveyObjId in (select surveyObjId from SurveyObj where projectid = " + projectid + ")) inner join SYS_TICKETCFG on SyS_TicketDetails.ticketcfgid = SYS_TICKETCFG.id  inner join Result on Sys_Ticket.resultId = Result.resultId and Result.isRelease = 'Y' inner join SurveyObj on SurveyObj.surveyObjId = Result.surveyObjId and Result.paperId = " + strPaperId + " order by  SurveyObj.objCode");

                }

            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }

        }
    }
}