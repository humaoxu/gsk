﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="SysTicketDet_cm.aspx.cs" Inherits="JSKWeb.Ticket.SysTicketDet_cm" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<base target="_self" />
        <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('请先选择需要删除的列。');
                return false;
            }
            else {
                var json = { "oo": [
                { "url": "user_add.aspx", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                { "url": "user_add.aspx?id=", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no"}]
                };

                if (index == 0) {//新增
                    window.location.href = json.oo[index].url; 
                    return false; 
                }
                else if (index == 1) {//修改
                    window.location.href = json.oo[index].url + key; 
                    return false;
 
                }
                else if (index == 2) {//删除
                    return confirm("确认要删除吗？");
                }
                else {
                    return false;
                }
            }
        }
        $(document).ready(function () {
            $("#sel_load").change(function () {
                var p = $(this).children('option:selected').val();
                window.location.href = "user_index.aspx?p=" + p;

            });
        })

    </script>
    <style type="text/css">
        .tablestyleDet
        {
            margin-top: 30px;
        }
        .tablestyleDet > tbody > tr > td
        {
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .tablestyleDet > tbody > tr > td > .btn
        {
            margin-top: 15px;
            margin-bottom: 5px;
        }
        .tablestyleDet > tbody > tr > td > input:first-child
        {
            margin-right:10px;
        }
        #topTool_cklistpartId > tbody > tr > td > input[type="checkbox"]
        {
            vertical-align: middle;
            margin-right: 3px;
        }
        .auto-style1 {
            height: 29px;
        }
    </style>

    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
                        <asp:HiddenField ID="hfKey" runat="server" />
        <fieldset>
        <legend>&nbsp;申诉点&nbsp;</legend>
          <table    >
            <tbody> 
              <tr>
                  <td colspan="4">
                      <asp:Label ID="lblSU" runat="server" Text="　　"></asp:Label> 
                  </td>
              </tr>
              <tr>
              <td align="right">申诉点：</td>
              <td align="left">
                  <asp:DropDownList ID="ddl1" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddl1_SelectedIndexChanged" meta:resourcekey="ddl1Resource1">
                        </asp:DropDownList>&nbsp;&nbsp;
                   <asp:DropDownList ID="ddl2" CssClass="inputtext15" runat="server" Width="200px" AutoPostBack="True"  meta:resourcekey="ddl2Resource1">
                        </asp:DropDownList>&nbsp;&nbsp;
               </td>
                  
                     <td  align="right"> 
                     申诉理由： </td><td align="left">
                        <asp:TextBox ID="txtReason" runat="server" 
                             Width="300px" MaxLength="1000" meta:resourcekey="txtReasonResource1"></asp:TextBox>&nbsp;
              <asp:Button ID="btnSubmit"  CssClass="btn" Height ="25px"  Width="70px"   runat="server" Text="添加" 
                           onclick="btnSubmit_Click" meta:resourcekey="btnSubmitResource1" /> &nbsp; <asp:Button ID="btnDel"  CssClass="btn" Height ="25px"  Width="70px"   runat="server" Text="删除" OnClick="btnDel_Click" OnClientClick="return operate(2)" /> 
                     </td></tr> 
                <%--<tr> 
                     <td > 
                     申诉点佐证照片1： </td>
                     <td >
                         <asp:FileUpload ID="FileUpload1" runat="server" meta:resourcekey="file1Resource1" Width="300px" /> 
                         
                     </td> 
                    <td align="right" class="auto-style1"> 
                     申诉点佐证照片2： </td><td align="left" class="auto-style1">
                         <asp:FileUpload ID="FileUpload2" runat="server" meta:resourcekey="file2Resource1" Width="300px" /> 
                          &nbsp; 
                         
                     </td>
                </tr>
                --%>
            </tbody>
        </table>
            <table>
                <tr><td>
                <cc1:GridViewKit ID="gvMain" runat="server" AutoGenerateColumns="False" Width="1000px"
                Font-Size="9pt"  CssClass="tablestyle" EnableEmptyContentRender="True"   DataKeyNames="id,pic1,pic2"  
                CellPadding="2" OnRowDataBound="gvMain_RowDataBound"  >
                <RowStyle />
                <Columns>
                    <asp:BoundField DataField="cfgfact" HeaderText="申诉点" meta:resourcekey="BoundFieldResource1">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="cfgfacgvalue" HeaderText="申诉项目" meta:resourcekey="BoundFieldResource2">
                        <HeaderStyle HorizontalAlign="Center" Width="150px"  />
                        <ItemStyle HorizontalAlign="left" Width="150px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="detailreason" HeaderText="申诉理由" meta:resourcekey="BoundFieldResource3">
                        <HeaderStyle HorizontalAlign="Center" Width="250px" />
                        <ItemStyle HorizontalAlign="left" Width="250px" />
                    </asp:BoundField>
             
              <%--      <asp:TemplateField HeaderText="申诉照片" >
                        <HeaderStyle HorizontalAlign="Center" Width="220"  />
                        <ItemStyle HorizontalAlign="left" Width="220" />
                        <ItemTemplate> 
                            <asp:Image ID="im1" runat="server" Width="100px"  Height="100px"  />&nbsp;
                            <asp:Image ID="im2" runat="server" Width="100px"  Height="100px"  />
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <HeaderStyle />
                <AlternatingRowStyle />
            </cc1:GridViewKit>
                    </td></tr>
        
      
            </table>
            </fieldset>
         <fieldset>
        <legend>&nbsp;门头照&nbsp;</legend>
        <table  >
            <tbody> 
 
    
                 <tr> 
                     <td > 
                     门头照1： </td>
                     <td >
                         <asp:FileUpload ID="file1" runat="server" meta:resourcekey="file1Resource1" /> 
                         拍摄日期： 
                         <asp:TextBox ID="txtobjPic1Date"  onClick="WdatePicker({dateFmt:'yyyyMMdd'})"  CssClass="inputtext15" runat="server" Width="85px" meta:resourcekey="txtobjPic1DateResource1" ></asp:TextBox>&nbsp;&nbsp;
                          <asp:Label ID="lblWarn" runat="server" ForeColor="Red" Text="*当申诉店包含是否正确门店项目时，门头照为必填选项"></asp:Label>
                          <asp:Label ID="lbl1" Text="  " runat="server" Width="272px"  ForeColor="Red" meta:resourcekey="lbl1Resource1" ></asp:Label>
                     </td> 
                </tr>
                 <tr> 
                     <td align="right" class="auto-style1"> 
                     门头照2： </td><td align="left" class="auto-style1">
                         <asp:FileUpload ID="file2" runat="server" meta:resourcekey="file2Resource1" /> 
                         拍摄日期： 
                         <asp:TextBox ID="txtobjPic2Date"  onClick="WdatePicker({dateFmt:'yyyyMMdd'})"  CssClass="inputtext15"  runat="server" Width="85px" meta:resourcekey="txtobjPic2DateResource1" ></asp:TextBox>&nbsp;&nbsp;
                          <asp:Label ID="Label1"  Text="  " runat="server" Width="272px"  ForeColor="Red" meta:resourcekey="Label1Resource1" ></asp:Label>
                     </td> 
                </tr>
                               <tr   style=" text-align:left;">
                  <td colspan="2" >
                      <asp:Button ID="btnConfirm"  CssClass="btn" Height ="25px"  Width="200px"   runat="server" Text="提交申诉" OnClick="btnConfirm_Click" 
                            /> 
                  </td>
             
                </tr>
            </tbody>
        </table>
                </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">



</asp:Content>
