﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="SysTicketDet_OTC.aspx.cs" Inherits="JSKWeb.Ticket.SysTicketDet_OTC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<base target="_self" />
    <style type="text/css">
        .tablestyleDet
        {
            margin-top: 30px;
        }
        .tablestyleDet > tbody > tr > td
        {
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .tablestyleDet > tbody > tr > td > .btn
        {
            margin-top: 15px;
            margin-bottom: 5px;
        }
        .tablestyleDet > tbody > tr > td > input:first-child
        {
            margin-right:10px;
        }
        #topTool_cklistpartId > tbody > tr > td > input[type="checkbox"]
        {
            vertical-align: middle;
            margin-right: 3px;
        }
    </style>

    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        function operate(index) {   
                if (index == 0) {
                    
                    return confirm("是否要提交？");
                } 
                else {
                    return false;
                } 
        }
        //清空文件上传框
        function clearFileInput(file) {
            var form = document.createElement('form');
            document.body.appendChild(form);
            //记住file在旧表单中的的位置
            var pos = file.nextSibling;
            form.appendChild(file);
            form.reset();
            pos.parentNode.insertBefore(file, pos);
            document.body.removeChild(form);
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">

          <table class="tablestyleDet" width="100%" >
            <tbody> 
              <tr>
              <td align="right">申诉点：</td><td align="left">
              <asp:CheckBoxList ID="cklistpartId" runat="server" RepeatDirection="Horizontal">
                  </asp:CheckBoxList>
               </td>
              </tr> 
                <tr> 
                     <td  align="right"> 
                     申诉理由： </td><td align="left">
                        <asp:TextBox ID="txtReason" runat="server" Height="135px" TextMode="MultiLine" 
                             Width="476px" MaxLength="500"></asp:TextBox> 
                    </td> 
                </tr>
                 <tr> 
                     <td align="right"> 
                     门头照1： </td><td align="left">
                         <asp:FileUpload ID="fuobjPic1" runat="server" /> 
                         拍摄日期： 
                         <asp:TextBox ID="txtobjPic1Date"  onClick="WdatePicker({dateFmt:'yyyyMMdd'})"  CssClass="inputtext15"  runat="server" Width="85px" ></asp:TextBox>
                     </td> 
                </tr>
                 <tr> 
                     <td align="right"> 
                     门头照2： </td><td align="left">
                         <asp:FileUpload ID="fuobjPic2" runat="server" /> 
                         拍摄日期： 
                         <asp:TextBox ID="txtobjPic2Date"  onClick="WdatePicker({dateFmt:'yyyyMMdd'})"  CssClass="inputtext15"  runat="server" Width="85px" ></asp:TextBox>
                     </td> 
                </tr>
               
                <tr   style=" text-align:left;">
                  <td colspan="2" style="padding-left:150px;">
                      <asp:Button ID="btnSubmit"  CssClass="btn" Height ="25"  Width="70"   runat="server" Text="提交" 
                           onclick="btnSubmit_Click" />  &nbsp;&nbsp;&nbsp;&nbsp;
                      <asp:Button ID="btnClose"  CssClass="btn" Height ="25"   Width="70"   
                          runat="server" Text="关闭" onclick="btnClose_Click" />
                  </td>
                </tr>
            </tbody>
        </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">



</asp:Content>
