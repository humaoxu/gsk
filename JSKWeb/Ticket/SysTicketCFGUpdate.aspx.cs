﻿using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSKWeb.Ticket
{
    public partial class SysTicketCFGUpdate : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            HttpPostedFile file = this.fileUploadCtr.PostedFile;
            string fileName = file.FileName;
            string tempPath = Server.MapPath(InputText.GetConfig("TicketTempFile")); //获取系统临时文件路径
            fileName = System.IO.Path.GetFileName(fileName); //获取文件名（不带路径）
            string currFileExtension = System.IO.Path.GetExtension(fileName); //获取文件的扩展名
            string currFilePath = tempPath + fileName; //获取上传后的文件路径 记录到前面声明的全局变量
            file.SaveAs(currFilePath); //上传

            ISheet sheet = null;
            DataTable data = new DataTable();
            int startRow = 0;

            if (currFileExtension.ToUpper() != ".XLSX")
            {
                JavaScript.Alert(this, "文件格式不正确，上传的文件后缀名必须为XLSX还请确认");
                return;
            }

            FileStream fs = new FileStream(currFilePath, FileMode.Open, FileAccess.Read);

            XSSFWorkbook workbook = new XSSFWorkbook(fs);
            sheet = workbook.GetSheetAt(0);
            data = new DataTable();
            if (sheet != null)
            {
                IRow firstRow = sheet.GetRow(0);
                int cellCount = firstRow.LastCellNum; //一行最后一个cell的编号 即总的列数
                if (true)
                {
                    for (int i = firstRow.FirstCellNum; i < cellCount; ++i)
                    {
                        DataColumn column = new DataColumn(firstRow.GetCell(i).StringCellValue);
                        data.Columns.Add(column);
                    }
                    startRow = sheet.FirstRowNum + 1;
                }
                else
                {
                    startRow = sheet.FirstRowNum;
                }
                //最后一列的标号
                int rowCount = sheet.LastRowNum;
                for (int i = startRow; i <= rowCount; ++i)
                {
                    IRow row = sheet.GetRow(i);
                    if (row == null) continue; //没有数据的行默认是null　　　　　　　

                    DataRow dataRow = data.NewRow();
                    for (int j = row.FirstCellNum; j < cellCount; ++j)
                    {
                        if (row.GetCell(j) != null) //同理，没有数据的单元格都默认是null
                            dataRow[j] = row.GetCell(j).ToString();
                    }
                    data.Rows.Add(dataRow);
                }
            }
  
            try
            {
                int count = 0;
                bool bCheck = false;
                string strPj = "";
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    strPj = data.Rows[i][0].ToString().Trim();
                    string strPrj = data.Rows[i][0].ToString().Trim();
                    if (strPrj == "11" || strPrj == "12" || strPrj == "13" || strPrj == "25")
                    {
                        
                    }
                    else
                    {
                        JavaScript.Alert(this, strPrj + "并不是项目的ID编号，数据格式异常，请检查数据后重新导入");
                        bCheck = true;
                        break;
                    }

                    try
                    {
                        Int32.Parse(data.Rows[i][3].ToString());
                    }
                    catch
                    {
                        JavaScript.Alert(this, "最后一列必须为整数，数据格式异常，请检查数据后重新导入");
                        bCheck = true;
                        break;
                    }
                }
                if (bCheck)
                {
                    return;
                }

                string sqlUp = @" update SYS_TICKETCFG  set openflag = 0 where projectid =" + strPj;
                sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sqlUp, null);

                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string projectid = data.Rows[i][0].ToString();
                    string cfgfact = data.Rows[i][1].ToString();
                    string cfgfacgvalue = data.Rows[i][2].ToString();
                    string orderno = data.Rows[i][3].ToString();

                    string sqlChek = "select * from SYS_TICKETCFG where projectid=" + projectid + " and cfgfact='" +
                                     cfgfact + "' and cfgfacgvalue='" + cfgfacgvalue + "' ";
                    DataSet dsTmp = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlChek);
                    if (dsTmp.Tables[0].Rows.Count > 0)
                    {
                        string s_id = dsTmp.Tables[0].Rows[0][0].ToString();
                        sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, @" update SYS_TICKETCFG  set openflag = 1, orderno = "+ orderno + " where id =" + s_id, null);

                    }
                    else
                    {
                        SqlParameter[] parsDet = {
                                              new SqlParameter ("@projectid",SqlDbType.NVarChar),
                                              new SqlParameter ("@cfgfact",SqlDbType.NVarChar),
                                              new SqlParameter ("@cfgfacgvalue",SqlDbType.NVarChar),
                                              new SqlParameter ("@orderno",SqlDbType.NVarChar)
                                                 };
                        parsDet[0].Value = projectid;
                        parsDet[1].Value = cfgfact;
                        parsDet[2].Value = cfgfacgvalue;
                        parsDet[3].Value = orderno;

                        string s = @" insert into SYS_TICKETCFG values(@projectid,@cfgfact,@cfgfacgvalue,@orderno,1)";
                        sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, s, parsDet);
                    }
                }
                JavaScript.Alert(this, "更新成功");

                //conn.Close();
            }
            catch (Exception ex)
            {
                //conn.Close();
                JavaScript.Alert(this, ex.Message);
            }
            finally
            {
               // conn.Close();
            }

        }

        protected void btnOutPut_Click(object sender, EventArgs e)
        {
            string sql = "select projectid,cfgfact,cfgfacgvalue,orderno from SYS_TICKETCFG where projectid= "+DropDownList1.SelectedValue+" and openflag=1 order by cfgfact,orderno ";

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }
    }
}