﻿using DBUtility;
using JSKWeb.Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace JSKWeb.Ticket
{
    public partial class dataoutput : System.Web.UI.Page
    {

        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnOutPut_Click(object sender, EventArgs e)
        {
            if (this.txtSql.Text.Trim().ToLower().StartsWith("select"))
            {
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, this.txtSql.Text.Trim());
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    ExcelOutPut excelOutPut = new ExcelOutPut();
                    System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                    try
                    {
                        byte[] bt = ms.ToArray();
                        //以字符流的形式下载文件  
                        Response.ContentType = "application/vnd.ms-excel";
                        //通知浏览器下载文件而不是打开
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                        Response.BinaryWrite(bt);

                        Response.Flush();
                        Response.End();
                        bt = null;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (ms != null) ms.Dispose();
                    }
                }
                else
                {
                    JavaScript.Alert(this, "无符合查询条件的数据。");
                }
            }
            else
            {
                JavaScript.Alert(this,"脚本格式不正确，请联系系统管理员");
            }
            
        }
    }
}