﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Drawing;

namespace JSKWeb.Ticket
{
    /// <summary>
    /// ImgHandler 的摘要说明
    /// </summary>
    public class ImgHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //在此写入您的处理程序实现。
            string path = context.Request.QueryString["path"];
            if (!string.IsNullOrEmpty(path))
            {
                FileStream fs = new FileStream(@path, FileMode.Open, FileAccess.Read);
                Bitmap myImage = new Bitmap(fs);
                MemoryStream ms = new MemoryStream();
                myImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                context.Response.ClearContent();
                context.Response.ContentType = "image/Jpeg";
                context.Response.BinaryWrite(ms.ToArray());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}