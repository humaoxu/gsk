﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.Collections;


namespace JSKWeb.REPORTFMCG
{
    public partial class RPTFMCG_PMDBL : System.Web.UI.Page
    {
        public string item_Name = "[]";
        public string item_series = "[]";
        public string item_legend = "[]";



        SqlHelper sqlhelper = new SqlHelper();
        ArrayList qsList = new ArrayList();  //期数
        ArrayList qdList = new ArrayList();  //渠道
        ArrayList dqList = new ArrayList();  //大区
        ArrayList qyList = new ArrayList();  //区域
        DataTable nkaList = new DataTable(); ///NKA objclient

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataBinds();

                BindQD();

            }
        }

        public void DataBinds()
        {
            var ddlProjectId = InputText.GetConfig("gsk_fmcg");
            var ddlTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId ", ddlProjectId);


            //绑定期数
           // var ddlTypeSelectSql = String.Format("select paperid,paperTitle from Paper where paperid in (27,51) order by paperid desc ");
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qsDropDownCheckBoxes.DataSource = ddlTypeDataSet;
            qsDropDownCheckBoxes.DataValueField = "paperId";
            qsDropDownCheckBoxes.DataTextField = "paperTitle";
            qsDropDownCheckBoxes.DataBind();

            for (int i = 0; i < (qsDropDownCheckBoxes.Items.Count); i++)
            {
                ///期数超过1期
                if (qsDropDownCheckBoxes.Items.Count > 1)
                {
                    if (i >= (qsDropDownCheckBoxes.Items.Count - 2))
                    {
                        qsDropDownCheckBoxes.Items[i].Selected = true;
                    }
                }
                else
                {
                    qsDropDownCheckBoxes.Items[i].Selected = true;
                }
            }
        }

        public string GetSKUFx(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select '" + fxGroup + "' 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG z where 1=1 " + ppfx + strWhere + " ) /COUNT(1)))*100 as numeric(10,2))  分值,2 'orderid' from RPT_FMCG m where 1=1 "
                + strWhere;

            return sql;
        }

        private void BindQD()
        {
            //获取渠道
            var ddlTypeSelectSql = "SELECT DISTINCT QD FROM RPT_FMCG_ALL ";
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qdDownCheckBoxes.DataSource = ddlTypeDataSet;
            qdDownCheckBoxes.DataValueField = "QD";
            qdDownCheckBoxes.DataTextField = "QD";
            qdDownCheckBoxes.DataBind();
            foreach (ListItem item in qdDownCheckBoxes.Items)
            {
                item.Selected = true;
            }

            //获取大区
            ddlTypeSelectSql = "select '全国' dq ,0 orderid union select distinct dq,1 orderid from RPT_FMCG_all where 1=1 ";

            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql  + " order by orderid ");
            dqDownCheckBoxes.DataSource = ddlTypeDataSet;
            dqDownCheckBoxes.DataValueField = "DQ";
            dqDownCheckBoxes.DataTextField = "DQ";
            dqDownCheckBoxes.DataBind();

            //获取区域
            ddlTypeSelectSql = "SELECT DISTINCT QY FROM RPT_FMCG_ALL ";
            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "QY";
            qyDownCheckBoxes.DataTextField = "QY";
            qyDownCheckBoxes.DataBind();
        }


        //期数
        protected void qsDropDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                    qsList.Add(item.Value);
            }
        }

        //渠道
        protected void qdDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    qdList.Add(item.Value);
                }
            }

            var sql = "";
            var sqlWhere = "";
            DataSet ds = new DataSet();

            //if (ddlfw.SelectedItem.Text.Equals("必查门店"))
            //{
            //    sqlWhere = " and objbc = 'Y' ";
            //} 

            //if (ddlqd.Visible)
            //{
            //    sqlWhere += " and qd = '" + ddlqd.SelectedItem.Text + "' ";
            //}

            sql = "select '全国' dq ,0 orderid union select distinct dq,1 orderid from RPT_FMCG_all where 1=1 ";

            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere + " order by orderid ");

            dqDownCheckBoxes.DataSource = ds;
            dqDownCheckBoxes.DataTextField = "dq";
            dqDownCheckBoxes.DataValueField = "dq";
            dqDownCheckBoxes.DataBind();

            sql = "select distinct qy from RPT_FMCG_all where 1=1 ";

            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere);

            qyDownCheckBoxes.DataSource = ds;
            qyDownCheckBoxes.DataTextField = "qy";
            qyDownCheckBoxes.DataValueField = "qy";
            qyDownCheckBoxes.DataBind();
        }

        //大区
        protected void dqDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //如果大区全选
            int select_sum = Int32.Parse(dqDownCheckBoxes.Items.Capacity.ToString());
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    dqList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            if (dqList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - dqList.Count == 1)
                {
                    str_code = "(SELECT DISTINCT DQ FROM RPT_FMCG_ALL)";
                }
                else
                {
                    for (int i = 0; i < dqList.Count; i++)
                    {
                        str_code += "'" + dqList[i].ToString() + "',";
                    }
                }
            }

            //获取区域
            var ddlTypeSelectSql = "";
            if (str_code.Trim().Length == 0)
            {
                ddlTypeSelectSql = "SELECT DISTINCT QY FROM RPT_FMCG_ALL ";
            }
            else
            {

                if (select_sum - qyList.Count == 1)
                {
                    ddlTypeSelectSql = "SELECT DISTINCT QY from RPT_FMCG_All where dq in (" + str_code + ")  order by QY";
                }
                else
                {

                    ddlTypeSelectSql = "SELECT DISTINCT QY from RPT_FMCG_All where dq in (" + str_code.TrimEnd(',').ToString() + ")  order by QY ";
                }
            }

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "QY";
            qyDownCheckBoxes.DataTextField = "QY";
            qyDownCheckBoxes.DataBind();
        }

        protected void ddldim_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sql = "";
            var sqlWhere = "";
            DataSet ds = new DataSet();

            //if (ddlfw.SelectedItem.Text.Equals("必查门店"))
            //{
            //    sqlWhere = " and objbc = 'Y' ";
            //}

            //MBD
            if (ddldim.SelectedIndex.ToString().Trim() == "0")
            {
                //ddlqd.Visible = false;
                //dqDownCheckBoxes.Visible = true;
                //qyDownCheckBoxes.Visible = true;
                //qd_td.Visible = false;
                //dq_td.Visible = true;
                //qy_td.Visible = true;


                sql = "select '全国' dq ,0 orderid union select distinct dq,1 orderid from RPT_FMCG_all where 1=1 ";

                ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere + " order by orderid ");

                dqDownCheckBoxes.DataSource = ds;
                dqDownCheckBoxes.DataTextField = "dq";
                dqDownCheckBoxes.DataValueField = "dq";
                dqDownCheckBoxes.DataBind();

                sql = "select distinct qy from RPT_FMCG_all where 1=1 ";

                ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere);

                qyDownCheckBoxes.DataSource = ds;
                qyDownCheckBoxes.DataTextField = "qy";
                qyDownCheckBoxes.DataValueField = "qy";
                qyDownCheckBoxes.DataBind();
            }
            else if (ddldim.SelectedIndex.ToString().Trim() == "2")
            {
                //ddlqd.Visible = true;

                //dqDownCheckBoxes.Visible = true;
                //qyDownCheckBoxes.Visible = true;

                //qd_td.Visible = true;
                //dq_td.Visible = true;
                //qy_td.Visible = true;

                //if (ddlqd.Visible)
                //{
                //    sqlWhere += " and qd = '"+ddlqd.SelectedItem.Text+"' ";
                //}

                sql = "select '全国' dq ,0 orderid union select distinct dq,1 orderid from RPT_FMCG_all where 1=1 ";

                ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere + " order by orderid ");

                dqDownCheckBoxes.DataSource = ds;
                dqDownCheckBoxes.DataTextField = "dq";
                dqDownCheckBoxes.DataValueField = "dq";
                dqDownCheckBoxes.DataBind();

                sql = "select distinct qy from RPT_FMCG_all where 1=1 ";

                ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere);

                qyDownCheckBoxes.DataSource = ds;
                qyDownCheckBoxes.DataTextField = "qy";
                qyDownCheckBoxes.DataValueField = "qy";
                qyDownCheckBoxes.DataBind();

            }
            else
            {


            }

        }

        protected void ddlqd_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sql = "";
            var sqlWhere = "";
            DataSet ds = new DataSet();

            //if (ddlfw.SelectedItem.Text.Equals("必查门店"))
            //{
            //    sqlWhere = " and objbc = 'Y' ";
            //}

            //if (ddlqd.Visible)
            //{
            //    sqlWhere += " and qd = '" + ddlqd.SelectedItem.Text + "' ";
            //}

            sql = "select '全国' dq ,0 orderid union select distinct dq,1 orderid from RPT_FMCG_all where 1=1 ";

            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere + " order by orderid ");

            dqDownCheckBoxes.DataSource = ds;
            dqDownCheckBoxes.DataTextField = "dq";
            dqDownCheckBoxes.DataValueField = "dq";
            dqDownCheckBoxes.DataBind();

            sql = "select distinct qy from RPT_FMCG_all where 1=1 ";

            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql + sqlWhere);

            qyDownCheckBoxes.DataSource = ds;
            qyDownCheckBoxes.DataTextField = "qy";
            qyDownCheckBoxes.DataValueField = "qy";
            qyDownCheckBoxes.DataBind();
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {


            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = DataLoad();
                if (dt.Rows.Count == 0)
                {
                    item_Name = "[]";
                    item_series = "[]";
                    item_legend = "[]";
                    content.InnerHtml = "请检查MBD筛选";
                    return;
                }
                else
                {
                    item_Name = "";
                    item_series = "";
                    item_legend = "";
                }
                //动态定义样式
                sb.Append(@"<table class='tablestyle_otc' cellspacing='0' cellpadding='2' rules='all' enableemptycontentrender='True' 
border='1' style='width: 100 %; border - collapse:collapse; ' ><tbody >");
                sb.Append("<tr class='header'>");
                int w = 75 / (dt.Columns.Count - 1);


                //echart 值
                string[] item_series_value_tmp = new string[dt.Columns.Count - 2];
                //echart 名称
                string[] item_series_name_tmp = new string[dt.Columns.Count - 2];

                string header2 = "";  

                if (ddldim.SelectedIndex>0)
                {
                    int t = 0;
                    ArrayList qishuList = new ArrayList();
                    foreach (ListItem item in qsDropDownCheckBoxes.Items)
                    {
                        if (item.Selected)
                        {
                            qishuList.Add(item.Text);
                        }

                    }
                    foreach (DataRow item in nkaList.Rows)
                    { 
                        {
                            if (t == 0)
                            {
                                sb.Append("<td  rowspan='2'>渠道</td><td rowspan='2' >销售区域</td>");
                                sb.Append("<td  colspan='" + qishuList.Count + "'>" + item[0] + "</td>");
                            }
                            else
                            {
                                sb.Append("<td  colspan='" + qishuList.Count + "' >" + item[0] + "</td>");
                            }

                            for (int i = 0; i < qishuList.Count; i++)
                            {
                                header2 += "<td>" + qishuList[i].ToString() + "</td>";
                            } 
                            t++;
                        } 
                    }

                    header2 = "</tr><tr  class='header'>" + header2 + "</tr>";
                }
                else
                {
                    for (int i = 0; i < dt.Columns.Count - 2; i++)
                    {
                        header2 += ("<td >" + dt.Columns[i].ToString() + "</td>");
                        if (i > 0)
                        {
                            item_legend += "'" + dt.Columns[i].ToString() + "',";
                        }
                    }

                    header2 = "</tr><tr  class='header'>" + header2 + "</tr>";
                } 

               

                sb.Append(header2);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    sb.Append("<tr onclick='gv_selectRow(this)' onmouseover='gv_mouseHover(this)' style='cursor: pointer;' >");
                    for (int j = 0; j < dt.Columns.Count - 2; j++)
                    {
                        if (j > 1)
                        {
                            sb.Append("<td class='across'  >" + Math.Round(InputText.GetDouble(dr[j].ToString()), 2) + "%</td>");

                            item_series_value_tmp[j - 1] += InputText.GetDouble(dr[j].ToString()).ToString("##0.00") + ",";
                            item_series_name_tmp[j - 1] = "'" + dt.Columns[j].ToString() + "'";
                        }
                        else
                        {
                            sb.Append("<td class='across'  >" + dr[j].ToString() + "</td>");

                            item_Name += "'" + dr[j].ToString() + "',";
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");



                content.InnerHtml = sb.ToString();



                ///echart  legend
                //item_legend = "[" + item_legend.TrimEnd(',') + "]";
                #region echart series
                //{
                //    name: "" + $(this).text() + "", data: s_itemValue, type: 'bar', itemStyle:
                //    {
                //        normal:
                //        {
                //            color: colorList[i],
                //                        label:
                //            {
                //                show: true,
                //                            position: 'top',
                //                            textStyle:
                //                {
                //                    color: 'black'
                //                            }
                //            }
                //        }
                //    }
                //}
                ///echart series

                //for (int i = 0; i < item_series_name_tmp.Length; i++)
                //{
                //    item_series += "{name:" + item_series_name_tmp[i]
                //        + ",data:[" + item_series_value_tmp[i].TrimEnd(',')
                //        + "],type:'bar',itemStyle:{normal:{color: '" + InputText.GetEchartColors()[i] + "',label:{show: true,position: 'top',textStyle:{color:'black'}}}}},";

                //}
                //item_series = "[" + item_series.TrimEnd(',') + "]";
                #endregion

                ///echart item_Name
                //item_Name = "[" + item_Name.TrimEnd(',') + "]";


            }
            catch (Exception)
            {

                item_Name = "[]";
                item_series = "[]";
                item_legend = "[]";
                content.InnerHtml = "请检查MBD筛选";
                return;
            }



        }

        /// <summary>
        /// 数据查询
        /// </summary>
        /// <returns></returns>
        private DataTable DataLoad()
        {
            
            DataTable dt = new DataTable();
            try
            {
                int i = 0;
                string sql = "";

                string fenshuCol = "";
                string pinPaiConfig = "";
                string fenShuJingPinCol = ""; 

                //fenshuCol += ddlPP.SelectedItem.Text.Equals("舒适达") ? " and ssdpmzs >= ssdpm " : " and bljpmzs >= bljpm";
                fenshuCol += ddlPP.SelectedItem.Text.Equals("舒适达") ? " and ssdpmyx>= ssdpm " : " and bljpmyx>= bljpm ";

                //fenshuCol = ddlPP.SelectedItem.Text.Equals("舒适达") ? "ssdpmzs" : "ssdpmzs";

                //fenShuJingPinCol = ddlPP.SelectedItem.Text.Equals("舒适达") ? "ssdjppm" : "bljjppm";

                //pinPaiConfig = ddlPP.SelectedItem.Text.Equals("舒适达") ? "ssdjppm" : "bljjppm";

                ///
                ArrayList sqlList = new ArrayList();
                ///分析维度
                ArrayList fenXiWeiDu = new ArrayList();

                string sqlWhere_quDao = "";
                string sqlWhere_daQu = "";
                string sqlWhere_quYu = "";
                string sqlWhere_chengShi = "";
                string sqlWhere_xiaoShouDaiBiao = "";

                var sqlTmp = "";
                var sqlWhere = "";
                
                

                if (ddlfw.SelectedItem.Text.Equals("必查门店"))
                {
                    sqlWhere = " and objbc = 'Y' ";
                }
                sqlTmp = "";
                foreach (ListItem item in qsDropDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        sqlTmp += "" + item.Value + ",";
                    }
                }
                if (sqlTmp.Length > 0)
                {
                    sqlWhere += " and paperid in (" + sqlTmp.TrimEnd(',') + ")";
                }

                sqlTmp = "";
                foreach (ListItem item in qdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
                if (sqlTmp.Length > 0)
                {
                    sqlWhere_quDao += " and qd in (" + sqlTmp.TrimEnd(',') + ")";
                }


                ///NKA判断
                if (ddldim.SelectedItem.Value.Equals("1"))
                {
                    sqlWhere += " and objtype = 'NKA'";
                    sql = "select distinct objclient from RPT_FMCG_all where 1=1 " + sqlWhere + " and  objclient <>'无' and objtype = 'NKA' " + sqlWhere_quDao;
                    nkaList = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql).Tables[0];
                }


                ///渠道
                sqlTmp = "";

                //if (ddlqd.Visible)
                //{
                //    sqlWhere += " and qd ='"+ddlqd.SelectedItem.Text+"'";
                //}
                int quDaoCheckItem = 0;
                



                ///大区
                sqlTmp = "";
                foreach (ListItem item in dqDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        if (item.Value.Equals("全国"))
                        {
                            int t = 0;
                            foreach (ListItem qditem in qdDownCheckBoxes.Items)
                            {
                                if (qditem.Selected)
                                {
                                    sqlList.Add(string.Format(GetSqlQuanGuoByCol(qditem.Text, fenshuCol, fenShuJingPinCol, (t+1),0), sqlWhere_quYu + sqlWhere)); 
                                    quDaoCheckItem++;
                                }
                                t++;
                            }
                            //if (qdDownCheckBoxes.Items.Count == quDaoCheckItem)
                            {
                                sqlList.Add(string.Format(GetSqlQuanGuoByCol("全渠道", fenshuCol, fenShuJingPinCol, 0,0), sqlWhere_quYu + sqlWhere));
                            }
                            
                        }
                        else
                        {
                            sqlTmp += "'" + item.Value + "',";
                        }
                       
                    }
                }
                if (sqlTmp.Length > 0)
                {
                    sqlWhere_daQu += " and dq in (" + sqlTmp.TrimEnd(',') + ")";
                }




                ///区域
                sqlTmp = "";
                foreach (ListItem item in qyDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
                if (sqlTmp.Length > 0)
                {
                    sqlWhere_quYu += " and qy in (" + sqlTmp.TrimEnd(',') + ")" + sqlWhere_daQu ;
                }
                

               
               
                for (i=0; i < qdDownCheckBoxes.Items.Count; i++)
                {
                    if (qdDownCheckBoxes.Items[i].Selected)
                    {
                        if (sqlWhere_quYu.Trim().Length > 0)
                        {
                            sqlList.Add(string.Format(GetSqlByCol(qdDownCheckBoxes.Items[i].Text, fenshuCol, fenShuJingPinCol, "qy",  (i+1),3), sqlWhere_quYu + sqlWhere));
                            if (i==0)
                            {
                                sqlList.Add(string.Format(GetSqlByCol("全渠道", fenshuCol, fenShuJingPinCol, "qy", 0, 3), sqlWhere_quYu + sqlWhere));
                            }
                            
                        }
                        if (sqlWhere_daQu.Trim().Length > 0)
                        {
                            sqlList.Add(string.Format(GetSqlByCol(qdDownCheckBoxes.Items[i].Text, fenshuCol, fenShuJingPinCol, "dq", (i + 1), 2), sqlWhere_daQu + sqlWhere));
                            if (i == 0)
                                sqlList.Add(string.Format(GetSqlByCol("全渠道", fenshuCol, fenShuJingPinCol, "dq", 0, 2), sqlWhere_daQu + sqlWhere));
                        }
                        if (sqlWhere_quDao.Trim().Length > 0)
                        {
                            //sqlList.Add(string.Format(GetSqlByCol(qdDownCheckBoxes.Items[i].Text, fenshuCol, fenShuJingPinCol, "qd", 1, i), sqlWhere_quDao + sqlWhere));
                            //sqlList.Add(string.Format(GetSqlByCol("全部", fenshuCol, fenShuJingPinCol, "qd", 0, i), sqlWhere_quDao + sqlWhere));
                        }

                        //if (ddldim.SelectedItem.Value.Equals("1"))
                        //{
                        //    sqlList.Add(string.Format(GetSqlByCol(qdDownCheckBoxes.Items[i].Text, fenshuCol, fenShuJingPinCol, "objclient", 0, i), " and  objclient <>'无' and objtype = 'NKA'" + sqlWhere));
                        //}
                    }
                }
                #region 全渠道
                //if (qdDownCheckBoxes.Items.Count == quDaoCheckItem)
                {
                    if (sqlWhere_daQu.Trim().Length > 0)
                    {
                        sqlList.Add(string.Format(GetSqlByCol("全渠道", fenshuCol, fenShuJingPinCol, "dq", 0, 2), sqlWhere_daQu + sqlWhere));
                    }
                    if (sqlWhere_quYu.Trim().Length > 0)
                    {
                        sqlList.Add(string.Format(GetSqlByCol("全渠道", fenshuCol, fenShuJingPinCol, "qy", 0, 3), sqlWhere_quYu + sqlWhere));
                    }
                } 
                #endregion

                if (sqlList.Count > 0)
                {
                    sql = "select * from (";
                    for ( i = 0; i < sqlList.Count; i++)
                    {
                        if (i > 0)
                        {
                            sql += " union ";
                        }
                        sql += sqlList[i];
                    }

                    sql += " ) b order by orderid1,orderid2 ";
                }


                dt = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql).Tables[0];

            }
            catch
            {

            }


            return dt;
        }

        /// <summary>
        /// 全国SQL
        /// </summary>
        /// <param name="fenshuCol"></param>
        /// <param name="pinPaiConfig"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        private string GetSqlQuanGuoByCol(string qudaoWeiDuCol,string fenshuCol,  string fenShuJingPinCol, int c1 , int c2 )
        {
            string qudaoWeiDuWhere = "";
            if (qudaoWeiDuCol != "全渠道")
            {
                qudaoWeiDuWhere = " and qd='" + qudaoWeiDuCol + "' ";
            }

            var sql = @"select '"+ qudaoWeiDuCol + "' 渠道,'全国' 销售区域, ";

            int i = 1;
            var paperidSql = "";
            ///NKA处理方法
            if (ddldim.SelectedIndex > 0 )
            {
                for (int n = 0; n < nkaList.Rows.Count; n++)
                {
                    foreach (ListItem item in qsDropDownCheckBoxes.Items)
                    {
                        if (item.Selected)
                        {
                            paperidSql += "'" + item.Value + "',";

                            sql += @"case when
(sum(case when paperid = " + item.Value + qudaoWeiDuWhere + @" and  objclient = '" + nkaList.Rows[n][0].ToString() + @"'  then (1.0) else 0 end) = 0)
then 0
else (sum(case when paperid = " + item.Value + qudaoWeiDuWhere + fenshuCol + @" and  objclient = '" + nkaList.Rows[n][0].ToString() + @"' then (1.0) else 0 end) / sum(case when paperid = " + item.Value + qudaoWeiDuWhere + " and  objclient = '" + nkaList.Rows[n][0].ToString() + @"' then (1.0) else 0 end) *100) end [" + nkaList.Rows[n][0].ToString()+ item.Text + "],";
                        }
                        i++;
                    }
                }
            }
            else
            {
                foreach (ListItem item in qsDropDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        paperidSql += "'" + item.Value + "',";

                        sql += @"case when
(sum(case when paperid = " + item.Value + qudaoWeiDuWhere + @" then (1.0) else 0 end) = 0)
then 0
else (sum(case when paperid = " + item.Value + qudaoWeiDuWhere + fenshuCol + @" then (1.0) else 0 end) / sum(case when paperid = " + item.Value + qudaoWeiDuWhere + " then (1.0) else 0 end) *100) end [" + item.Text + "],";
                    }
                    i++;
                }
            }
            
            sql += c1 + @" 'orderid1',"+c2+" 'orderid2' from RPT_FMCG_all where 1=1 {0}" + qudaoWeiDuWhere;

            return sql;
        }

        /// <summary>
        /// 明细SQL
        /// </summary>
        /// <param name="fenshuCol"></param>
        /// <param name="pinPaiConfig"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        private string GetSqlByCol(string qudaoWeiDuCol, string fenshuCol, string fenShuJingPinCol, string weiDuCol, int c1,int c2)
        {
            string qudaoWeiDuWhere = "";
            if (qudaoWeiDuCol != "全渠道")
            {
                qudaoWeiDuWhere = " and qd='" + qudaoWeiDuCol + "' ";
            }


            var sql = @"select '" + qudaoWeiDuCol + "' 渠道," + weiDuCol + " 销售区域, ";

            int i = 1;
            var paperidSql = "";

            if (ddldim.SelectedIndex > 0)
            {
                for (int n = 0; n < nkaList.Rows.Count; n++)
                {
                    foreach (ListItem item in qsDropDownCheckBoxes.Items)
                    {
                        if (item.Selected)
                        {
                            paperidSql += "'" + item.Value + "',";

                            sql += @"case when
(sum(case when paperid = " + item.Value + qudaoWeiDuWhere + @" and  objclient = '" + nkaList.Rows[n][0].ToString() + @"'  then (1.0) else 0 end) = 0)
then 0
else (sum(case when paperid = " + item.Value + qudaoWeiDuWhere + fenshuCol + @" and  objclient = '" + nkaList.Rows[n][0].ToString() + @"'  then (1.0) else 0 end) / sum(case when paperid = " + item.Value + qudaoWeiDuWhere + " and  objclient = '" + nkaList.Rows[n][0].ToString() + @"'  then (1.0) else 0 end) *100) end [" + nkaList.Rows[n][0].ToString()+ item.Text + "],";
                        }
                        i++;
                    }
                }
            }
            else
            {
                foreach (ListItem item in qsDropDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        paperidSql += "'" + item.Value + "',";

                        sql += @"case when
(sum(case when paperid = " + item.Value + qudaoWeiDuWhere + @" then (1.0) else 0 end) = 0)
then 0
else (sum(case when paperid = " + item.Value + qudaoWeiDuWhere + fenshuCol + @" then (1.0) else 0 end) / sum(case when paperid = " + item.Value + qudaoWeiDuWhere + " then (1.0) else 0 end) *100) end [" + item.Text + "],";
                    }
                    i++;
                }
            }

                  
            sql += c1 + @" 'orderid1'," + c2 + " 'orderid2' from RPT_FMCG_all where 1=1 " + qudaoWeiDuWhere + " {0} group by " + weiDuCol;

            return sql;
        }

        public void btnOutput_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string htmlOutPut = "";
            string[] header_Col = new string[2];

            dt = DataLoad();
            dt.Columns.Remove(dt.Columns[dt.Columns.Count - 1]);
            ExcelOutPut excelOutPut = new ExcelOutPut();
            System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(dt);
            try
            {
                byte[] bt = ms.ToArray();
                //以字符流的形式下载文件  
                Response.ContentType = "application/vnd.ms-excel";
                //通知浏览器下载文件而不是打开
                Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                Response.BinaryWrite(bt);

                Response.Flush();
                Response.End();
                bt = null;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (ms != null) ms.Dispose();
            }
        }



    }
}