﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="RPTFMCGSCORE.aspx.cs" Inherits="JSKWeb.REPORTFMCG.RPTFMCGSCORE" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
      
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            
        }
    
    </script>
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        .auto-style1 {
            width: 161px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
      <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0"  class="">
            <tbody>
                <tr>
                    <td  style="padding-left: 5px;" >
                        <asp:DropDownList ID="ddlTicketType" CssClass="dd_chk_select" runat="server" Width="120px">
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 5px;" >
                        <asp:DropDownList ID="ddldim" CssClass="dd_chk_select"  runat="server" Width="100px" >
                            <asp:ListItem Value="0">大区</asp:ListItem>
                            <asp:ListItem Value="1">区域</asp:ListItem>
                            <asp:ListItem Value="2">渠道</asp:ListItem>
                            <asp:ListItem Value="3">RSM</asp:ListItem>
                            <asp:ListItem Value="4">TSM</asp:ListItem>
                            <asp:ListItem Value="5">CSE</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px;" id="dq_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="dqDownCheckBoxes" runat="server" OnSelectedIndexChanged="dqDownCheckBoxes_SelcetedIndexChanged" 
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="大区" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 10px;" id="qy_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qyDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 10px;" id="Td1" runat="server" >
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qdDownCheckBoxes" runat="server" 
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td>&nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn" 
                            runat="server"  Text="查询" Width="80px" /> &nbsp;
                        <asp:Button ID="btnOutPut"  CssClass="btn" 
                            runat="server"  Text="导出" Width="70px" OnClick="btnOutPut_Click" />  </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">

        <div id="content" runat="server" style="text-align:center">
            
        </div>

</asp:Content>
