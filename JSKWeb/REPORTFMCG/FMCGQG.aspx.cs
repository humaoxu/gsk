﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;
using System.Collections;

namespace JSKWeb.REPORTFMCG
{
    public partial class FMCGQG : System.Web.UI.Page
    {
        public string itemName = "[]";
        public string itemValue = "[]";
        public string itemTitle = "[]";
        public string itemFormate = "[]";

        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();

                //ddlDim.SelectedIndex = 0;
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_fmcg");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            string strFormType = "";
            //request form type
            if (Request.QueryString["strFormType"] != null)
            {
                strFormType = Request.QueryString["strFormType"].ToString();
            }

            if (strFormType == "")
            {
                strFormType = "1";
            }

            Dictionary<string, string> Ht = new Dictionary<string, string>();
            

            //分销率
            if (strFormType == "1")
            {
                Ht.Add("0", "全国");
                Ht.Add("1", "大区");
                Ht.Add("2", "区域");
                Ht.Add("3", "渠道");
                Ht.Add("4", "系统");
                Ht.Add("5", "SKU");
            }
            else if(strFormType == "2")
            {
                Ht.Add("0", "全国");
                Ht.Add("1", "大区");
                Ht.Add("2", "区域");
                Ht.Add("3", "渠道");
                Ht.Add("4", "系统");
            }
            else if (strFormType == "3")
            {
                Ht.Add("0", "全国");
                Ht.Add("1", "大区");
                Ht.Add("2", "区域");
                Ht.Add("3", "渠道");
                Ht.Add("4", "系统");
            }
            else if (strFormType == "4")
            {
                Ht.Add("0", "全国");
                Ht.Add("1", "大区");
                Ht.Add("2", "区域");
                Ht.Add("3", "渠道");
                Ht.Add("4", "系统");
            }
            else if (strFormType == "5")
            {
                Ht.Add("0", "全国");
                Ht.Add("1", "大区");
                Ht.Add("2", "区域");
                Ht.Add("3", "渠道");
                Ht.Add("4", "系统");
            }

            this.ddldim.DataSource = Ht.OrderBy(o=>o.Key);
            this.ddldim.DataValueField = "key";
            this.ddldim.DataTextField = "value";
            this.ddldim.DataBind();
        }

        public void RestForm()
        {
            tbqd.Visible = false;
            tbqy.Visible = false; 
        }

        public DataSet DataLoad()
        {
            DataSet ds = new DataSet();
            try
            {
                //string strFormType = Request.QueryString["strFormType"].ToString();
                string strFormType = "";
                //request form type
                if (Request.QueryString["strFormType"] != null)
                {
                    strFormType = Request.QueryString["strFormType"].ToString();
                }

                if (strFormType == "")
                {
                    strFormType = "1";
                }

                string sql = "";
                string sqlwhere = " and paperid = " + ddlTicketType.SelectedValue;
                string sqlorderid = " orderid ";
                if (ddlfw.SelectedIndex != 0)
                {
                    sqlwhere = sqlwhere + " and objbc = 'Y' ";
                }
                string qgFx = "  ";
                #region MyRegion
                switch (strFormType)
                {
                    ///分销率
                    case "1":
                        itemTitle = "'分销率'";
                        itemFormate = "'%'";

                        #region 全国


                        if (ddldim.SelectedItem.Text.Equals("全国"))
                        {
                            if (ddlPP.SelectedIndex == 0)
                            {
                                qgFx = qgFx + " and ssdfx_cfg = '1' ";
                            }
                            else if (ddlPP.SelectedIndex == 1)
                            {
                                qgFx = qgFx + " and bljfx_cfg = '1' ";
                            }

                            sql = GetQuGuoFx(qgFx, sqlwhere);
                        }
                        #endregion
                        #region 大区
                        else if (ddldim.SelectedItem.Text.Equals("大区"))
                        {
                            if (ddlPP.SelectedIndex == 0)
                            {
                                qgFx = qgFx + " and ssdfx_cfg = '1' ";
                            }
                            else if (ddlPP.SelectedIndex == 1)
                            {
                                qgFx = qgFx + " and bljfx_cfg = '1' ";
                            }

                            sql = GetQuGuoFx(qgFx, sqlwhere);
                            sql += " union ";
                            sql += GetDaQuFx(qgFx, "dq", sqlwhere);
                        }
                        #endregion
                        #region 区域
                        else if (ddldim.SelectedItem.Text.Equals("区域"))
                        {
                            if (ddlPP.SelectedIndex == 0)
                            {
                                qgFx = qgFx + " and ssdfx_cfg = '1' ";
                            }
                            else if (ddlPP.SelectedIndex == 1)
                            {
                                qgFx = qgFx + " and bljfx_cfg = '1' ";
                            }

                            string sqlWhereQy = sqlwhere;
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qy_dq.SelectedValue + "' ";
                            }
                             
                            if (ddl_qy_qd.SelectedIndex !=0 )
                            {
                                sqlwhere  = sqlwhere + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                                sqlWhereQy = sqlWhereQy + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                            }

                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sql = GetDaQuFx(qgFx, "dq", sqlWhereQy);
                                sql += " union ";
                                sql += GetQyYuFx(qgFx, "qy", sqlWhereQy);
                            }
                            else
                            {
                                sql = GetQuGuoFx(qgFx, sqlwhere);
                                sql += " union ";
                                sql += GetQyYuFx(qgFx, "qy", sqlWhereQy);
                            }
                        }
                        #endregion
                        #region 渠道
                        else if (ddldim.SelectedItem.Text.Equals("渠道"))
                        {
                            if (ddlPP.SelectedIndex == 0)
                            {
                                qgFx = qgFx + " and ssdfx_cfg = '1' ";
                            }
                            else if (ddlPP.SelectedIndex == 1)
                            {
                                qgFx = qgFx + " and bljfx_cfg = '1' ";
                            }

                            string sqlWhereQy = sqlwhere;
                            if (ddl_qd_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qd_dq.SelectedValue + "' ";
                            }
                            if (ddl_qd_qy.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and qy = '" + ddl_qd_qy.SelectedValue + "' ";
                            }
                            sql = GetQyYuFx(qgFx, "qd", sqlWhereQy);
                        }
                        #endregion
                        #region 系统
                        else if (ddldim.SelectedItem.Text.Equals("系统"))
                        {
                            if (ddlPP.SelectedIndex == 0)
                            {
                                qgFx = qgFx + " and ssdfx_cfg = '1' ";
                            }
                            else if (ddlPP.SelectedIndex == 1)
                            {
                                qgFx = qgFx + " and bljfx_cfg = '1' ";
                            }

                            string sqlWhereQy = sqlwhere;
                            sqlWhereQy = sqlWhereQy + " and objtype = 'NKA' ";
                            //if (ddl_xt_dq.SelectedIndex != 0)
                            //{
                            //    sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_xt_dq.SelectedValue + "' ";
                            //}
                            //if (ddl_xt_qy.SelectedIndex != 0)
                            //{
                            //    sqlWhereQy = sqlWhereQy + " and qy = '" + ddl_xt_qy.SelectedValue + "' ";
                            //}
                            sql = GetQyYuFx(qgFx, "objclient", sqlWhereQy);
                        }
                        #endregion
                        #region SKU
                        else if (ddldim.SelectedItem.Text.Equals("SKU"))
                        {

                            string skustrSQL = String.Format("select * from RPT_FMCG_ITEM where paperid={0} order by orderno", ddlTicketType.SelectedValue);
                            DataSet dsSKU = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, skustrSQL);
                            string stritmename = "";
                            for (int i = 0; i < dsSKU.Tables[0].Rows.Count; i++)
                            {
                                stritmename = dsSKU.Tables[0].Rows[i]["itemname"].ToString().Trim();
                                qgFx = " and " + dsSKU.Tables[0].Rows[i]["colname"].ToString().Trim() + "fx = '有分销' ";
                                if (i == 0)
                                {
                                    sql = GetSKUFx(qgFx, stritmename, sqlwhere);
                                }
                                else
                                {
                                    sql = sql + " Union " + GetSKUFx(qgFx, stritmename, sqlwhere);
                                } 
                            }
                            sqlorderid = " 分值 desc ,orderid";
                        }
                        #endregion

                        break;
                    ///分销达标率
                    case "2":

                        itemTitle = "'分销达标率'";
                        itemFormate = "'%'";

                        if (ddlPP.SelectedIndex == 0)
                        {
                            qgFx = qgFx + " and ssdfx_zs >= ssdfx_yq ";
                        }
                        else if (ddlPP.SelectedIndex == 1)
                        {
                            qgFx = qgFx + " and bljfx_zs >= bljfx_yq ";
                        }

                        #region 全国
                        if (ddldim.SelectedItem.Text.Equals("全国"))
                        {
                            sql = GetQG_FXDBL(qgFx, sqlwhere);
                        }
                        #endregion
                        #region 大区
                        else if (ddldim.SelectedItem.Text.Equals("大区"))
                        {
                            sql = GetQG_FXDBL(qgFx, sqlwhere);
                            sql += " union ";
                            sql += GetQY_FXDBL(qgFx, "dq", sqlwhere);
                        }
                        #endregion
                        #region 区域 

                        else if (ddldim.SelectedItem.Text.Equals("区域"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qy_dq.SelectedValue + "' ";
                            }
                            if (ddl_qy_qd.SelectedIndex != 0)
                            {
                                sqlwhere = sqlwhere + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                                sqlWhereQy = sqlWhereQy + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                            }

                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sql = GetQY_FXDBL(qgFx, "dq", sqlWhereQy);
                                sql += " union ";
                                sql += GetQY_FXDBL(qgFx, "qy", sqlWhereQy);
                            }
                            else
                            {
                                sql = GetQG_FXDBL(qgFx, sqlwhere);
                                sql += " union ";
                                sql += GetQY_FXDBL(qgFx, "qy", sqlWhereQy);
                            }
                        }
                        #endregion
                        #region 渠道

                       
                        else if (ddldim.SelectedItem.Text.Equals("渠道"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qd_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qd_dq.SelectedValue + "' ";
                            }
                            if (ddl_qd_qy.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and qy = '" + ddl_qd_qy.SelectedValue + "' ";
                            }
                            sql = GetQY_FXDBL(qgFx, "qd", sqlWhereQy);
                        }
                        #endregion
                        #region 系统
                        else if (ddldim.SelectedItem.Text.Equals("系统"))
                        {
                            string sqlWhereQy = sqlwhere;
                           // sqlWhereQy = sqlWhereQy + " and objtype = 'NKA' ";
                            sql = GetQY_FXDBL_NKA(qgFx, "objclient", sqlWhereQy);
                        }

                        #endregion

                        break;
                    ///有效排面数
                    case "3":
                        itemTitle = "'有效排面数'";
                        itemFormate = "[]";

                        string item_yxpms = "";
                        if (ddlPP.SelectedIndex == 0)
                        {
                            item_yxpms = "ssdpmzs";
                        }
                        else if (ddlPP.SelectedIndex == 1)
                        {

                            item_yxpms = "bljpmzs";
                        }
                        #region 全国
                        if (ddldim.SelectedItem.Text.Equals("全国"))
                        {
                            sql = GetQG_YXPMS(item_yxpms, qgFx, sqlwhere);
                        }
                        #endregion
                        #region 大区
                        else if (ddldim.SelectedItem.Text.Equals("大区"))
                        {
                            sql = GetQG_YXPMS(item_yxpms, qgFx, sqlwhere);
                            sql += " union ";
                            sql += GetQY_YXPMS(item_yxpms, qgFx, "dq", sqlwhere);
                        }
                        #endregion
                        #region 区域
                        else if (ddldim.SelectedItem.Text.Equals("区域"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qy_dq.SelectedValue + "' ";
                            }
                            if (ddl_qy_qd.SelectedIndex != 0)
                            {
                                sqlwhere = sqlwhere + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                                sqlWhereQy = sqlWhereQy + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                            }
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sql = GetQY_YXPMS(item_yxpms, qgFx, "dq", sqlWhereQy);
                                sql += " union ";
                                sql += GetQY_YXPMS(item_yxpms, qgFx, "qy", sqlWhereQy);
                            }
                            else
                            {
                                sql = GetQG_YXPMS(item_yxpms, qgFx, sqlwhere);
                                sql += " union ";
                                sql += GetQY_YXPMS(item_yxpms, qgFx, "qy", sqlWhereQy);
                            }
                        }
                        #endregion
                        #region 渠道
                        else if (ddldim.SelectedItem.Text.Equals("渠道"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qd_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qd_dq.SelectedValue + "' ";
                            }
                            if (ddl_qd_qy.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and qy = '" + ddl_qd_qy.SelectedValue + "' ";
                            }
                            sql = GetQY_YXPMS(item_yxpms, qgFx, "qd", sqlWhereQy);
                        }
                        #endregion
                        #region 系统
                        else if (ddldim.SelectedItem.Text.Equals("系统"))
                        {
                            string sqlWhereQy = sqlwhere;
                            sqlWhereQy = sqlWhereQy + " and objtype = 'NKA' ";
                            sql = GetQY_YXPMS(item_yxpms, qgFx, "objclient", sqlWhereQy);

                        }
                        #endregion

                        break;
                    ///排面份额
                    case "4":
                        itemTitle = "'排面份额'";
                        itemFormate = "[]";

                        string itemjp = "";

                        string itemzs = "";
                        if (ddlPP.SelectedIndex == 0)
                        {
                            itemzs = "ssdpmzs";
                            itemjp = "ssdjppm";
                        }
                        else if (ddlPP.SelectedIndex == 1)
                        {
                            itemzs = "bljpmzs";
                            itemjp = "bljjppm";
                        }
                        #region 全国
                        if (ddldim.SelectedItem.Text.Equals("全国"))
                        {
                            sql = GetQG_PMFE2423(itemzs, itemjp, qgFx, sqlwhere);
                        }
                        #endregion
                        #region 大区
                        else if (ddldim.SelectedItem.Text.Equals("大区"))
                        {
                            sql = GetQG_PMFE2423(itemzs, itemjp, qgFx, sqlwhere);
                            sql += " union ";
                            sql += GetQY_PMFE2423(itemzs, itemjp, qgFx, "dq", sqlwhere);
                        }
                        #endregion
                        #region 区域
                        else if (ddldim.SelectedItem.Text.Equals("区域"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qy_dq.SelectedValue + "' ";
                            }
                            if (ddl_qy_qd.SelectedIndex != 0)
                            {
                                sqlwhere = sqlwhere + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                                sqlWhereQy = sqlWhereQy + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                            }
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sql = GetQY_PMFE2423(itemzs, itemjp, qgFx, "dq", sqlWhereQy);
                                sql += " union ";
                                sql += GetQY_PMFE2423(itemzs, itemjp, qgFx, "qy", sqlWhereQy);
                            }
                            else
                            {
                                sql = GetQG_PMFE2423(itemzs, itemjp, qgFx, sqlwhere);
                                sql += " union ";
                                sql += GetQY_PMFE2423(itemzs, itemjp, qgFx, "qy", sqlWhereQy);
                            }
                        }
                        #endregion
                        #region 渠道
                        else if (ddldim.SelectedItem.Text.Equals("渠道"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qd_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qd_dq.SelectedValue + "' ";
                            }
                            if (ddl_qd_qy.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and qy = '" + ddl_qd_qy.SelectedValue + "' ";
                            }
                            sql = GetQY_PMFE2423(itemzs, itemjp, qgFx, "qd", sqlWhereQy);
                        }
                        #endregion
                        #region 系统
                        else if (ddldim.SelectedItem.Text.Equals("系统"))
                        {
                            string sqlWhereQy = sqlwhere;
                            sqlWhereQy = sqlWhereQy + " and objtype = 'NKA' ";
                            //sql = GetQY_PMFE(itemzs, itemjp, qgFx, "objclient", sqlWhereQy);
                            sql = GetQY_PMFE_NKA(itemzs, itemjp, qgFx, "objclient", sqlWhereQy);
                        }
                        #endregion

                        break; 
                    ///排面达标率
                    case "5":
                        itemTitle = "'排面达标率'";
                        itemFormate = "'%'";

                        if (ddlPP.SelectedIndex == 0)
                        {
                            qgFx = qgFx + " and ssdpmzs >= ssdpm ";
                        }
                        else if (ddlPP.SelectedIndex == 1)
                        {
                            qgFx = qgFx + " and bljpmzs >= bljpm ";
                        }

                        #region 全国
                        if (ddldim.SelectedItem.Text.Equals("全国"))
                        {
                            sql = GetQG_PMDBL(qgFx, sqlwhere);
                        }
                        #endregion
                        #region 大区
                        else if (ddldim.SelectedItem.Text.Equals("大区"))
                        {
                            sql = GetQG_PMDBL(qgFx, sqlwhere);
                            sql += " union ";
                            sql += GetQY_PMDBL(qgFx, "dq", sqlwhere);
                        }
                        #endregion
                        #region 区域
                        else if (ddldim.SelectedItem.Text.Equals("区域"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qy_dq.SelectedValue + "' ";
                            }
                            if (ddl_qy_qd.SelectedIndex != 0)
                            {
                                sqlwhere = sqlwhere + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                                sqlWhereQy = sqlWhereQy + " and qd = '" + ddl_qy_qd.SelectedValue + "' ";
                            }
                            if (ddl_qy_dq.SelectedIndex != 0)
                            {
                                sql = GetQY_PMDBL(qgFx, "dq", sqlWhereQy);
                                sql += " union ";
                                sql += GetQY_PMDBL(qgFx, "qy", sqlWhereQy);
                            }
                            else
                            {
                                sql = GetQG_PMDBL(qgFx, sqlwhere);
                                sql += " union ";
                                sql += GetQY_PMDBL(qgFx, "qy", sqlWhereQy);
                            }
                        }
                        #endregion
                        #region 渠道
                        else if (ddldim.SelectedItem.Text.Equals("渠道"))
                        {
                            string sqlWhereQy = sqlwhere;
                            if (ddl_qd_dq.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and dq = '" + ddl_qd_dq.SelectedValue + "' ";
                            }
                            if (ddl_qd_qy.SelectedIndex != 0)
                            {
                                sqlWhereQy = sqlWhereQy + " and qy = '" + ddl_qd_qy.SelectedValue + "' ";
                            }
                            sql = GetQY_PMDBL(qgFx, "qd", sqlWhereQy);
                        }
                        #endregion
                        #region 系统
                        else if (ddldim.SelectedItem.Text.Equals("系统"))
                        {
                            string sqlWhereQy = sqlwhere;
                           // sqlWhereQy = sqlWhereQy + " and objtype = 'NKA' ";
                            sql = GetQY_PMDBL_NKA(qgFx, "objclient", sqlWhereQy);
                        }
                        #endregion 
                       
                        break;
                    default:
                        break;
                }
                #endregion

                sql += " order by "+sqlorderid;

                this.TextBox1.Text = sql;
                ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            }
            catch (Exception)
            {

                throw;
            }
            return ds;
        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            content.InnerHtml = "";
            DataSet ds = DataLoad();
            DataTable dt = ds.Tables[0];
            itemValue = "";
            itemName = "";
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (i == 0)
                    {
                        itemValue += "";
                        itemName += "";
                    }
                    else
                    {
                        itemValue += ",";
                        itemName += ",";
                    }
                    itemName = itemName + "'" + dt.Rows[i][0].ToString().Trim() + "'";
                    itemValue = itemValue + "" + (InputText.GetDouble(dt.Rows[i][1]).ToString("##0.0").Trim()) + "";
                }
                itemName = "[" + itemName + "]";
                itemValue = "[" + itemValue + "]";
            }
            else
            {
                itemValue = "[]";
                itemName = "[]";
            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            itemName = "''";
            itemValue = "''";

            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = DataLoad().Tables[0];
                if (dt.Rows.Count == 0)
                {
                    return;
                }
                //动态定义样式
                sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");

                for (int i = 0; i < dt.Columns.Count - 1; i++)
                {

                    sb.Append("<th align='left' style='width:25%;' scope='col'>" + dt.Columns[i].ToString() + "</th>");
                }
                sb.Append("</tr>");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    sb.Append("<tr style='cursor:pointer;' >");
                    for (int j = 0; j < dt.Columns.Count - 1; j++)
                    {
                        if (j == 1)
                        {
                            sb.Append("<td class='across' style='width:75%;'>" + InputText.GetDouble(dr[j].ToString()).ToString("##0.0") + itemFormate.Replace("[]", "").Replace("'", "") + "</td>");
                        }
                        else
                        {
                            sb.Append("<td class='across' style='width:25%;'>" + dr[j].ToString() + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");

                content.InnerHtml = sb.ToString();

            }
            catch (Exception)
            {

                throw;
            }



        }


        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RestForm();
        }

        protected void ddldim_SelectedIndexChanged(object sender, EventArgs e)
        {
            RestForm();
            switch (ddldim.SelectedItem.Text)
            {
                case "全国":
                    break;
                case "大区":
                    break;
                case "区域":
                    RestQy();
                    tbqy.Visible = true;
                    break;
                case "渠道":
                    RestQd();
                    tbqd.Visible = true;
                    break;
                case "系统":
                    RestXT();
                    break;
                case "SKU":
                    RestSKU();
                    break;
                default:
                    break;
            }
        }

        #region 设置区域的DDL
        public void RestQy()
        {
            string strSQL = String.Format("select distinct dq from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);
            
            this.ddl_qy_dq.DataSource = ds;
            this.ddl_qy_dq.DataValueField = "dq";
            this.ddl_qy_dq.DataTextField = "dq";
            this.ddl_qy_dq.DataBind();

            this.ddl_qy_dq.Items.Insert(0,(new ListItem("全部", "全部")));

            ddl_qy_dq.SelectedIndex = 0;


            strSQL = String.Format("select distinct qd from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            this.ddl_qy_qd.DataSource = ds;
            this.ddl_qy_qd.DataValueField = "qd";
            this.ddl_qy_qd.DataTextField = "qd";
            this.ddl_qy_qd.DataBind();
            this.ddl_qy_qd.Items.Insert(0, (new ListItem("全部", "全部")));
            ddl_qy_qd.SelectedIndex = 0;
        }
        #endregion

        private void RestQd()
        {
            string strSQL = String.Format("select distinct dq from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            this.ddl_qd_dq.DataSource = ds;
            this.ddl_qd_dq.DataValueField = "dq";
            this.ddl_qd_dq.DataTextField = "dq";
            this.ddl_qd_dq.DataBind();
            this.ddl_qd_dq.Items.Insert(0, (new ListItem("全部", "全部")));
            ddl_qd_dq.SelectedIndex = 0;

            strSQL = String.Format("select distinct qy from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            this.ddl_qd_qy.DataSource = ds;
            this.ddl_qd_qy.DataValueField = "qy";
            this.ddl_qd_qy.DataTextField = "qy";
            this.ddl_qd_qy.DataBind();
            this.ddl_qd_qy.Items.Insert(0, (new ListItem("全部", "全部")));
            ddl_qd_qy.SelectedIndex = 0;
        }

        public void RestXT()
        {
            //string strSQL = String.Format("select distinct dq from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            //DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            //this.ddl_xt_dq.DataSource = ds;
            //this.ddl_xt_dq.DataValueField = "dq";
            //this.ddl_xt_dq.DataTextField = "dq";
            //this.ddl_xt_dq.DataBind();
            //this.ddl_xt_dq.Items.Insert(0, (new ListItem("全部", "全部")));
            //ddl_xt_dq.SelectedIndex = 0;

            //strSQL = String.Format("select distinct qy from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            //ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            //this.ddl_xt_qy.DataSource = ds;
            //this.ddl_xt_qy.DataValueField = "qy";
            //this.ddl_xt_qy.DataTextField = "qy";
            //this.ddl_xt_qy.DataBind();
            //this.ddl_xt_qy.Items.Insert(0, (new ListItem("全部", "全部")));
            //ddl_xt_qy.SelectedIndex = 0;
        }

        private void RestSKU()
        {
            string strSQL = String.Format("select distinct dq from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            //this.ddl_sku_dq.DataSource = ds;
            //this.ddl_sku_dq.DataValueField = "dq";
            //this.ddl_sku_dq.DataTextField = "dq";
            //this.ddl_sku_dq.DataBind();
            //this.ddl_sku_dq.Items.Insert(0, (new ListItem("全部", "全部")));
            //ddl_sku_dq.SelectedIndex = 0;

            //strSQL = String.Format("select distinct qy from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            //ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            //this.ddl_sku_qy.DataSource = ds;
            //this.ddl_sku_qy.DataValueField = "qy";
            //this.ddl_sku_qy.DataTextField = "qy";
            //this.ddl_sku_qy.DataBind();
            //this.ddl_sku_qy.Items.Insert(0, (new ListItem("全部", "全部")));
            //ddl_sku_qy.SelectedIndex = 0;

            //strSQL = String.Format("select * from RPT_FMCG_ITEM where paperid={0} order by orderno", ddlTicketType.SelectedValue);
            //ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);
            //this.ddlSKU.DataSource = ds;
            //this.ddlSKU.DataValueField = "colname";
            //this.ddlSKU.DataTextField = "itemname";
            //this.ddlSKU.DataBind();
            ////this.ddlSKU.Items.Insert(0, (new ListItem("全部", "全部")));
            //ddlSKU.SelectedIndex = 0;

        }
 

        protected void ddl_qd_dq_SelectedIndexChanged(object sender, EventArgs e)
        {
            string strSQLQd = "";
            if (ddl_qd_dq.SelectedValue.ToString().Trim().Equals("全部"))
            {
                strSQLQd = String.Format("select distinct qy from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            }
            else
            {
                strSQLQd = String.Format("select distinct qy from RPT_FMCG where paperid = {0} and dq = '"+ ddl_qd_dq.SelectedValue.ToString().Trim() + "'", ddlTicketType.SelectedValue);
            }
            
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQLQd);

            this.ddl_qd_qy.DataSource = ds;
            this.ddl_qd_qy.DataValueField = "qy";
            this.ddl_qd_qy.DataTextField = "qy";
            this.ddl_qd_qy.DataBind();
            this.ddl_qd_qy.Items.Insert(0, (new ListItem("全部", "全部")));
            ddl_qd_qy.SelectedIndex = 0;
        }

        protected void ddl_sku_dq_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string strSQLQd = "";
            //if (ddl_sku_dq.SelectedValue.ToString().Trim().Equals("全部"))
            //{
            //    strSQLQd = String.Format("select distinct qy from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            //}
            //else
            //{
            //    strSQLQd = String.Format("select distinct qy from RPT_FMCG where paperid = {0} and dq = '" + ddl_sku_dq.SelectedValue.ToString().Trim() + "'", ddlTicketType.SelectedValue);
            //}

            //DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQLQd);
            //this.ddl_sku_qy.DataSource = ds;
            //this.ddl_sku_qy.DataValueField = "qy";
            //this.ddl_sku_qy.DataTextField = "qy";
            //this.ddl_sku_qy.DataBind();
            //this.ddl_sku_qy.Items.Insert(0, (new ListItem("全部", "全部")));
            //ddl_sku_qy.SelectedIndex = 0;
        }

        #region 分销率（%） 
        public string GetQuGuoFx(string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG where 1=1 " + ppfx + strWhere + " ) /COUNT(1))*100) as numeric(10,2)) 分值,0 'orderid' from RPT_FMCG  where 1=1 " + strWhere;
            return sql;
        }

        public string GetDaQuFx(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG z where 1=1 " + ppfx  + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup 
                + " ) /COUNT(1)))*100 as numeric(10,2))  分值,1 'orderid' from RPT_FMCG m where 1=1 "
                + strWhere + " group by " + fxGroup;

            return sql;
        }

        public string GetMBDQGFX(string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL where 1=1 " + ppfx + strWhere + " ) /COUNT(1))*100) as numeric(10,2)) 分值,0 'orderid' from RPT_FMCG_ALL  where 1=1 " + strWhere;
            return sql;
        }

        public string GetMBDFX(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where 1=1 " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /COUNT(1)))*100 as numeric(10,2))  分值,1 'orderid' from RPT_FMCG_ALL m where 1=1 "
                + strWhere + " group by " + fxGroup;
            return sql;
        }

        public string GetQyYuFx(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG z where 1=1 " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /COUNT(1)))*100 as numeric(10,2))  分值,2 'orderid' from RPT_FMCG m where 1=1 "
                + strWhere + " group by " + fxGroup;

            return sql;
        }

        public string GetQyYuFx_NKA(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG_DABIAO z where 1=1 " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /COUNT(1)))*100 as numeric(10,2))  分值,2 'orderid' from RPT_FMCG_DABIAO m where 1=1 "
                + strWhere + " group by " + fxGroup;

            return sql;
        }

        public string GetSKUFx(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select '" + fxGroup + "' 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG z where 1=1 " + ppfx + strWhere + " ) /COUNT(1)))*100 as numeric(10,2))  分值,2 'orderid' from RPT_FMCG m where 1=1 "
                + strWhere ;

            return sql;
        }

        #endregion

        #region 分销达标率

        public string GetQG_FXDBL(string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG where 1=1 " + ppfx + strWhere + " ) /COUNT(1))*100) as numeric(10,2)) 分值,0 'orderid' from RPT_FMCG  where 1=1 " + strWhere;
            return sql;
        }

        public string GetQY_FXDBL(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG z where 1=1 " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /COUNT(1)))*100 as numeric(10,2))  分值,1 'orderid' from RPT_FMCG m where 1=1 "
                + strWhere + " group by " + fxGroup;

            return sql;
        }
        public string GetQG_FXDBL_NKA(string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG_DABIAO where 1=1 " + ppfx + strWhere + " ) /COUNT(1))*100) as numeric(10,2)) 分值,0 'orderid' from RPT_FMCG_DABIAO  where 1=1 " + strWhere;
            return sql;
        }

        public string GetQY_FXDBL_NKA(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG_DABIAO z where 1=1 " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /COUNT(1)))*100 as numeric(10,2))  分值,1 'orderid' from RPT_FMCG_DABIAO m where 1=1 "
                + strWhere + " group by " + fxGroup;

            return sql;
        }

        #endregion

        #region 有效排面数

        public string GetQG_YXPMS(string itmename,string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast(((sum(" + itmename + ")/count(1))*1) as numeric(10,0)) 有效排面数,0 'orderid' from RPT_FMCG  where 1=1 and  SSDJPPM > SSDPMZS " + strWhere + ppfx ;
            return sql;
        }

        public string GetQY_YXPMS(string itmename,string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select sum(" + itmename + ")+0.0  from RPT_FMCG z where 1=1 and  SSDJPPM > SSDPMZS " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /count(1)))*1 as numeric(10,2))  有效排面数,1 'orderid' from RPT_FMCG m where 1=1 and  SSDJPPM > SSDPMZS "
                + strWhere + " group by " + fxGroup;

            return sql;
        }

        public string GetQG_YXPMS_NKA(string itmename, string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast(((sum(" + itmename + ")/count(1))*1) as numeric(10,0)) 有效排面数,0 'orderid' from RPT_FMCG_DABIAO  where 1=1 and  SSDJPPM > SSDPMZS " + strWhere + ppfx;
            return sql;
        }

        public string GetQY_YXPMS_NKA(string itmename, string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select sum(" + itmename + ")+0.0  from RPT_FMCG_DABIAO z where 1=1 and  SSDJPPM > SSDPMZS " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /count(1)))*1 as numeric(10,2))  有效排面数,1 'orderid' from RPT_FMCG_DABIAO m where 1=1 and  SSDJPPM > SSDPMZS "
                + strWhere + " group by " + fxGroup;

            return sql;
        }

        #endregion

        #region 排面份额

        public string GetQG_PMFE(string itmename, string itmejp, string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast((((select sum(" + itmename + ")+0.0 分值 from RPT_FMCG where 1=1 and  SSDJPPM > SSDPMZS " + ppfx + strWhere + " ) /sum("+ itmejp + "))*100) as numeric(10,2)) 排面份额,0 'orderid' from RPT_FMCG  where 1=1 " + strWhere;
            return sql;
        }

        public string GetQG_PMFE2423(string itmename, string itmejp, string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, sum(SSDPM+0.0)/sum (YGZPM) *100 分值 ,0 orderid from RPT_FMCG_PMFE where 1=1 " + ppfx + strWhere;
            return sql;
        }

        public string GetQY_PMFE(string itmename, string itmejp, string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select sum(" + itmename + ")+0.0 分值 from RPT_FMCG z where 1=1 and  SSDJPPM > SSDPMZS " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /sum(" + itmejp + ")))*100 as numeric(10,2))  排面份额,1 'orderid' from RPT_FMCG m where 1=1 and  SSDJPPM > SSDPMZS "
                + strWhere + " group by " + fxGroup;
            return sql;
        }

        public string GetQY_PMFE2423(string itmename, string itmejp, string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, sum(SSDPM+0.0)/sum(YGZPM) *100 分值 ,1 orderid from RPT_FMCG_PMFE where 1=1 " + ppfx + strWhere + " group by " + fxGroup;
            return sql;
        }

        public string GetQY_PMFE_NKA(string itmename, string itmejp, string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
             
            sql = @" select " + fxGroup + " 维度, case when sum(ssdpmzs1)=0 then 0 else ( sum(ssdpm+0.0)/ sum(ssdpmzs1) ) end*100 分值, 1 orderid  from RPT_FMCG_DABIAO where 1=1 " + ppfx +  " group by " + fxGroup;
            return sql;
        }
        #endregion

        #region 排面达标率

        public string GetQG_PMDBL(string ppfx, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG where 1=1 " + ppfx + strWhere + " ) /COUNT(1))*100) as numeric(10,2)) 排面达标率,0 'orderid' from RPT_FMCG  where 1=1 " + strWhere;
            return sql;
        }

        public string GetQY_PMDBL(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG z where 1=1 " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /COUNT(1)))*100 as numeric(10,2))  排面达标率,1 'orderid' from RPT_FMCG m where 1=1 "
                + strWhere + " group by " + fxGroup;

            return sql;
        }

        public string GetQY_PMDBL_NKA(string ppfx, string fxGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + fxGroup + " 维度, Cast((((select COUNT(1)+0.0  from RPT_FMCG_DABIAO z where 1=1 " + ppfx + strWhere + " group by z." + fxGroup + " having z." + fxGroup + " = m." + fxGroup
                + " ) /COUNT(1)))*100 as numeric(10,2))  排面达标率,1 'orderid' from RPT_FMCG_DABIAO m where 1=1 "
                + strWhere + " group by " + fxGroup;
            return sql;
        }

        #endregion
        protected void ddl_xt_dq_SelectedIndexChanged(object sender, EventArgs e)
        {
            //string strSQLQd = "";
            //if (ddl_xt_dq.SelectedValue.ToString().Trim().Equals("全部"))
            //{
            //    strSQLQd = String.Format("select distinct qy from RPT_FMCG where paperid = {0}", ddlTicketType.SelectedValue);
            //}
            //else
            //{
            //    strSQLQd = String.Format("select distinct qy from RPT_FMCG where paperid = {0} and dq = '" + ddl_xt_dq.SelectedValue.ToString().Trim() + "'", ddlTicketType.SelectedValue);
            //}

            //DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQLQd);
            //this.ddl_xt_qy.DataSource = ds;
            //this.ddl_xt_qy.DataValueField = "qy";
            //this.ddl_xt_qy.DataTextField = "qy";
            //this.ddl_xt_qy.DataBind();
            //this.ddl_xt_qy.Items.Insert(0, (new ListItem("全部", "全部")));
            //ddl_xt_qy.SelectedIndex = 0;
        }
    }
}