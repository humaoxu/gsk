﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="RPTFMCG_FXDBL.aspx.cs" Inherits="JSKWeb.REPORTFMCG.RPTFMCG_FXDBL" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <link rel="Stylesheet" type="text/css" href="../css/CustomDDStyles.css" />
      <script type="text/javascript">
          var lastRowSelected;
          var lastRowClassName;
          var s_sum = 0;
          var s_itemName = [];
          var s_itemValue = [];
          var s_itemTitle = [];
          var series_tmp = [];
          var s_itemColor = [];
          var s_xAxis_data = [];
          function gv_selectRow(row) {
              if (lastRowSelected != row) {
                  if (lastRowSelected != null) {
                      lastRowSelected.className = lastRowClassName;
                  }
                  lastRowClassName = row.className;
                  row.className = 'select';
                  lastRowSelected = row;

                  $('html,body').animate({ scrollTop: $("#main_echar").offset().top }, 1200);
                  $("#main_echar").width($(document.body).width());


                  if ($("#topTool_ddldim option:selected").text() == "销售区域划分") {
                      bindEchartShiChang();
                  }
                  else {
                      bindEChartNKA();
                  }


              }
          }



          function bindEchartShiChang() {
             
              var colorList = [
             '#FFC000', '#009DFF', '#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#6495ed',
             '#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0'
              ];

              s_itemColor = [];
              series_tmp = [];
              s_itemName = [];
              s_itemValue = [];
              s_legend = [];
              s_itemTitle = ["分销达标率—" + $(".select td:eq(0)").text()];
              var i = 0;
              var r = 0;
              var qs = 0;
              var step = $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").length;

              $(".tablestyle_otc tr:eq(0) td:gt(1)").each(
                 function () {
                     s_itemName[i] = "" + $(this).text() + "";
                     i++;
                 })
              var selectRow = $(".select td:eq(0)").text();
              var gridWidth = $(document.body).width();
              var qd = 0;

              $("#topTool_qsDropDownCheckBoxes_sl input[type=checkbox]:checked").each(
              function () {
                  s_itemColor = [];
                  s_itemValue = [];
                  qd = 0;
                  $(".tablestyle_otc tr:gt(1)").each(
                  function () {

                      if ($(this).find("td").eq(0).text() == selectRow) {
                          s_itemName[qd] = $(this).find("td").eq((1)).text();
                          s_itemValue[qd] = parseFloat($(this).find("td").eq((qs + 2)).text().replace("%", ""));

                          qd++;
                      }


                  })




                  series_tmp[qs] = {
                      name: "" + $(this).next().text() + "期" + "",
                      data: s_itemValue,
                      type: 'bar',
                      itemStyle: {
                          normal: {
                              color: colorList[qs],
                              label: {
                                  show: true,
                                  position: 'top',
                                  textStyle: {
                                      color: 'black'
                                  }
                              }
                          }
                      }
                  }
                  s_legend[qs] = $(this).next().text() + "期";
                  qs++;

              })


              if (s_itemName.length > 10)
                  s_sum = 45;
              else
                  s_sum = 0;

              option = {
                  title: {
                      x: 'left',
                      text: s_itemTitle
                  },
                  tooltip: {
                      trigger: 'axis'
                  },
                  legend: {
                      data: s_legend
                  },
                  toolbox: {
                      show: true,
                      feature: {
                          mark: { show: true },
                          dataView: { show: true, readOnly: false },
                          magicType: { show: true, type: ['line', 'bar'] },
                          restore: { show: true },
                          saveAsImage: { show: true }
                      }
                  },
                  calculable: true,
                  xAxis: [
                       {
                           type: 'category',
                           axisLabel: {
                               interval: 0,
                               rotate: s_sum,
                               margin: 2,
                               textStyle: {
                                   color: "#222"
                               }
                           },
                           data: s_itemName
                       }
                  ],
                  grid: {
                      x: 40,
                      x2: 20,
                      y2: 100,
                  },
                  yAxis: [
                        {
                            type: 'value',
                            //min: 0,
                            //max: 100,
                            //interval: 20,
                            axisLabel: {
                                formatter: '{value} %'
                            }
                        }
                  ],
                  series: series_tmp
              };

              var chart = echarts.init(document.getElementById('main_echar'));

              chart.setOption(option);

          }

          function bindEChartNKA() {
            
              var colorList = [
             '#FFC000', '#009DFF', '#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#6495ed',
             '#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0'
              ];

              s_itemColor = [];
              series_tmp = [];
              s_itemName = [];
              s_itemValue = [];
              s_legend = [];
              s_itemTitle = ["分销达标率—" + $(".select td:eq(0)").text() + "—" + $(".select td:eq(1)").text()];
              var i = 0;
              var r = 0;
              var qs = 0;
              var step = $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").length;

              $(".tablestyle_otc tr:eq(0) td:gt(1)").each(
                 function () {
                     s_itemName[i] = "" + $(this).text() + "";
                     i++;
                 })
              var selectRow = $(".select td:eq(0)").text();
              var gridWidth = $(document.body).width();
              var qd = 0;

              $("#topTool_qsDropDownCheckBoxes_sl input[type=checkbox]:checked").each(
              function () {
                  qd = 0;
                  s_itemValue = [];

                  $(".tablestyle_otc tr:eq(0) td:gt(1)").each(
                  function () {
                      s_itemValue[qd] = parseFloat($(".select td:eq(" + (qd * step + 2 + qs) + ")").text().replace("%", ""));
                      qd++;
                  })

                  series_tmp[qs] = {
                      name: "" + $(this).next().text() + "期" + "",
                      data: s_itemValue,
                      type: 'bar',
                      itemStyle: {
                          normal: {
                              color: colorList[qs],
                              label: {
                                  show: true,
                                  position: 'top',
                                  textStyle: {
                                      color: 'black'
                                  }
                              }
                          }
                      }
                  }
                  s_legend[qs] = $(this).next().text() + "期";
                  qs++;

              })


              if (s_itemName.length > 10)
                  s_sum = 45;
              else
                  s_sum = 0;

              option = {
                  title: {
                      x: 'left',
                      text: s_itemTitle
                  },
                  tooltip: {
                      trigger: 'axis'
                  },
                  legend: {
                      data: s_legend
                  },
                  toolbox: {
                      show: true,
                      feature: {
                          mark: { show: true },
                          dataView: { show: true, readOnly: false },
                          magicType: { show: true, type: ['line', 'bar'] },
                          restore: { show: true },
                          saveAsImage: { show: true }
                      }
                  },
                  calculable: true,
                  xAxis: [
                       {
                           type: 'category',
                           axisLabel: {
                               interval: 0,
                               rotate: s_sum,
                               margin: 2,
                               textStyle: {
                                   color: "#222"
                               }
                           },
                           data: s_itemName
                       }
                  ],
                  grid: {
                      x: 40,
                      x2: 20,
                      y2: 100,
                  },
                  yAxis: [
                        {
                            type: 'value',
                            //min: 0,
                            //max: 100,
                            //interval: 20,
                            axisLabel: {
                                formatter: '{value} %'
                            }
                        }
                  ],
                  series: series_tmp
              };

              var chart = echarts.init(document.getElementById('main_echar'));

              chart.setOption(option);

          }


          function gv_mouseHover(row) {
              row.style.cursor = 'pointer';
          }


          function QueryChecked() {

              var daquLen = $("#topTool_dqDownCheckBoxes_sl input:[type=checkbox]:checked").length;
              var quyuLen = $("#topTool_qyDownCheckBoxes_sl input:[type=checkbox]:checked").length;

              if ((daquLen + quyuLen) == 0) {
                  alert("请选择MBD！");
                  return false;
              }
          }

      </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;MBD筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td style="padding-left: 10px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qsDropDownCheckBoxes" runat="server" OnSelectedIndexChanged="qsDropDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="问卷期数" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding: 10px 0px 10px 10px;"></td>
                    <td style="padding-left: 5px;">
                        <asp:DropDownList ID="ddldim" CssClass="ddl_select"  runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="ddldim_SelectedIndexChanged">
                            <asp:ListItem Value="0">销售区域划分</asp:ListItem>
                            <asp:ListItem Value="1">NKA</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding: 10px 0px 10px 10px;"></td>
                    <td style="padding-left: 5px;">
                        <asp:DropDownList ID="ddlfw" CssClass="ddl_select" runat="server" Width="120px" AutoPostBack="True">
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="1">必查门店</asp:ListItem>
                        </asp:DropDownList>
                    </td>

 
                    <td style="padding-left: 10px;" id="dq_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="dqDownCheckBoxes" runat="server" OnSelectedIndexChanged="dqDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="大区" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 10px;" id="qy_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qyDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 10px;" id="Td1" runat="server" >
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qdDownCheckBoxes" runat="server" 
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
         

                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>
    </fieldset>
    <fieldset>
        <legend>&nbsp;产品筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                     
                    <td style="padding-left: 10px;">
                        <asp:DropDownList ID="ddlPP" CssClass="ddl_select" runat="server" Width="160px">
                            <asp:ListItem Value="0">舒适达</asp:ListItem>
                            <asp:ListItem Value="1">保丽净</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                               <td style="padding-left: 10px;" align="left" width="150px">
                        <asp:Button ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn_s" OnClientClick="return QueryChecked()"
                            runat="server" Text="数据查询" Width="60px" />
                        <asp:Button runat="server" OnClick="btnOutput_Click" CssClass="btn_s" Width="60px" ID="btnOutput" Text="数据导出" />
                    </td>
                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>

    </fieldset>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center;">
    </div>
    <div id="main_echar" style="width: 100%; height: 500px; overflow: hidden;"></div>
    
</asp:Content>