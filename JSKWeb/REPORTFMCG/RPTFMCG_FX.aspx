﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="RPTFMCG_FX.aspx.cs" Inherits="JSKWeb.REPORTFMCG.RPTFMCG_FX" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <link rel="Stylesheet" type="text/css" href="../css/CustomDDStyles.css" />

    <style>
        select {
            border-color: #cccccc;
            border-width: 1px;
            border-style: solid;
        }

    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;MBD筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td style="padding-left: 10px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qsDropDownCheckBoxes" runat="server" OnSelectedIndexChanged="qsDropDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="问卷期数" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding: 10px 0px 10px 10px;">分析维度</td>
                    <td style="padding-left: 5px;">
                        <asp:DropDownList ID="ddldim" CssClass="dd_chk_select"  runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="ddldim_SelectedIndexChanged">
                            <asp:ListItem Value="0">市场划分</asp:ListItem>
                            <asp:ListItem Value="1">NKA</asp:ListItem>
                            <asp:ListItem Value="2">SKU</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding: 10px 0px 10px 10px;"">门店范围</td>
                    <td style="padding-left: 5px;">
                        <asp:DropDownList ID="ddlfw" CssClass="dd_chk_select" runat="server" Width="120px"  >
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="1">必查门店</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 10px;" id="dq_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="dqDownCheckBoxes" runat="server" OnSelectedIndexChanged="dqDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="大区" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 10px;" id="qy_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qyDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    
                    <td style="padding-left: 10px;" id="qd_td" runat="server" >
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qdDownCheckBoxes" runat="server" OnSelectedIndexChanged="qdDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 10px;" align="left" width="150px">&nbsp;
                        <asp:Button ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />
                        <asp:Button runat="server" OnClick="btnOutput_Click" CssClass="btn_s" Width="60px" ID="btnOutput" Text="数据导出" />
                    </td>

                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>
    </fieldset>
    <fieldset>
        <legend>&nbsp;产品筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td style="padding: 10px 0px 10px 10px;">&nbsp;&nbsp;品&nbsp;&nbsp;&nbsp;牌&nbsp;&nbsp;

                    </td>
                    <td style="padding-left: 5px;">
                        <asp:DropDownList ID="ddlPP" CssClass="dd_chk_select" runat="server" Width="110px">
                            <asp:ListItem Value="0">舒适达</asp:ListItem>
                            <asp:ListItem Value="1">保丽净</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>

    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center;">
    </div>
    <div id="main" style="width: 100%; height: 500px; overflow: hidden;"></div>
    
</asp:Content>
