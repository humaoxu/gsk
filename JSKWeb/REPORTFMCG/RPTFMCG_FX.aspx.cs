﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.Collections;

namespace JSKWeb.REPORTFMCG
{
    public partial class RPTFMCG_FX : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();
        ArrayList qsList = new ArrayList();  //期数
        ArrayList qdList = new ArrayList();  //渠道
        ArrayList dqList = new ArrayList();  //大区
        ArrayList qyList = new ArrayList();  //区域

        //定义chart变量
        public string item_Name = "[]";
        public string item_series = "[]";
        public string item_legend = "[]";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tdSKUITEM.Visible = false;
                ddlSKUITEM.Visible = false;
                DataBinds();
                BindQD();

            }
        }

        public void DataBinds()
        {
            var ddlProjectId = InputText.GetConfig("gsk_fmcg");
            var ddlTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId  ", ddlProjectId);

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qsDropDownCheckBoxes.DataSource = ddlTypeDataSet;
            qsDropDownCheckBoxes.DataValueField = "paperId";
            qsDropDownCheckBoxes.DataTextField = "paperTitle";
            qsDropDownCheckBoxes.DataBind();

            for (int i = 0; i < (qsDropDownCheckBoxes.Items.Count); i++)
            {
                ///期数超过1期
                if (qsDropDownCheckBoxes.Items.Count > 1)
                {
                    if (i >= (qsDropDownCheckBoxes.Items.Count - 2))
                    {
                        qsDropDownCheckBoxes.Items[i].Selected = true;
                    }
                }
                else
                {
                    qsDropDownCheckBoxes.Items[i].Selected = true;
                }
            }

            ddlTypeSelectSql = "select * from RPT_FMCG_ITEM where 1=1 order by orderno";  //固定sku
            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);

            ddlSKUITEM.DataSource = ddlTypeDataSet;
            ddlSKUITEM.DataValueField = "colname";
            ddlSKUITEM.DataTextField = "itemname";
            ddlSKUITEM.DataBind();
            
        }

        public string GetSKUFx(string ppfx, string fxGroup, string strWhere, ArrayList qs)
        {
            string sql = "";
            sql = @"select '" + fxGroup + "' 维度,";
            if (qs.Count > 1)
            {
                foreach (var item in qs)
                {
                    sql += @" (case COUNT(1) when 0 then 0 else Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where 1=1 " + ppfx + strWhere + "  and paperid =" + item.ToString() + " ) /sum(case when paperid = " + item.ToString() + " then 1 else 0 end ) ))*100 as numeric(10,2)) end) 分值" + item.ToString() + ", ";
                }
                sql += " 2 'orderid'  from RPT_FMCG_ALL m";
            }
            else
            {
                sql += @" (case COUNT(1) when 0 then 0 else Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where 1=1 " + ppfx + strWhere + "  and paperid =" + qs[0].ToString() + " ) /sum(case when paperid = " + qs[0].ToString() + " then 1 else 0 end ) ))*100 as numeric(10,2)) end) 分值" + qs[0].ToString() + ", ";
                sql += " 2 'orderid'  from RPT_FMCG_ALL m where  paperid =" + qs[0].ToString();
            }

            return sql;
        }

        private void BindQD()
        {
            //获取渠道
            var ddlTypeSelectSql = "SELECT DISTINCT QD FROM RPT_FMCG_ALL ";
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qdDownCheckBoxes.DataSource = ddlTypeDataSet;
            qdDownCheckBoxes.DataValueField = "QD";
            qdDownCheckBoxes.DataTextField = "QD";
            qdDownCheckBoxes.DataBind();

            //获取大区
            ddlTypeSelectSql = "SELECT DISTINCT DQ FROM RPT_FMCG_ALL ";

            var ddlTypeDataSet_dq = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            dqDownCheckBoxes.DataSource = ddlTypeDataSet_dq;
            dqDownCheckBoxes.DataValueField = "DQ";
            dqDownCheckBoxes.DataTextField = "DQ";
            dqDownCheckBoxes.DataBind();
            dqDownCheckBoxes.Items.Insert(0, (new ListItem("全国", "全国")));


            //获取区域
            ddlTypeSelectSql = "SELECT DISTINCT QY FROM RPT_FMCG_ALL ";
            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "QY";
            qyDownCheckBoxes.DataTextField = "QY";
            qyDownCheckBoxes.DataBind();
        }

        //期数
        protected void qsDropDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //qdDownCheckBoxes.Visible = true;

            //qdDownCheckBoxes.DataSource = null;
            //qyDownCheckBoxes.DataSource = null;

            //qdDownCheckBoxes.Items.Clear();
            //qyDownCheckBoxes.Items.Clear();

            //获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                    qsList.Add(item.Value);
            }

            //获取渠道
            //var ddlTypeSelectSql = "SELECT DISTINCT QD FROM RPT_FMCG_ALL ";
            //var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            //qdDownCheckBoxes.DataSource = ddlTypeDataSet;
            //qdDownCheckBoxes.DataValueField = "QD";
            //qdDownCheckBoxes.DataTextField = "QD";
            //qdDownCheckBoxes.DataBind();


        }

        //渠道
        protected void qdDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    qdList.Add(item.Value);
                }
            }
        }

        //大区
        protected void dqDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qyDownCheckBoxes.Items.Clear();
            qyDownCheckBoxes.DataSource = null;

            //如果大区全选
            int select_sum = Int32.Parse(dqDownCheckBoxes.Items.Capacity.ToString());
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    dqList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            if (dqList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - dqList.Count == 1)
                {
                    str_code = "(SELECT DISTINCT DQ FROM RPT_FMCG_ALL)";
                }
                else
                {
                    for (int i = 0; i < dqList.Count; i++)
                    {
                        str_code += "'" + dqList[i].ToString() + "',";
                    }
                }
            }

            //获取区域
            var ddlTypeSelectSql = "";
            if (str_code.Trim().Length == 0)
            {
                ddlTypeSelectSql = "SELECT DISTINCT QY FROM RPT_FMCG_ALL ";
            }
            else
            {
                if (str_code.Contains("全国"))
                {
                    ddlTypeSelectSql = " SELECT DISTINCT QY FROM RPT_FMCG_ALL  order by qy ";
                }
                else
                {
                    if (select_sum - qyList.Count == 1)
                    {
                     }
                    else
                    {

                        ddlTypeSelectSql = "SELECT DISTINCT QY from RPT_FMCG_All where dq in (" + str_code.TrimEnd(',').ToString() + ")  order by QY ";
                    }
                }
                
            }

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "QY";
            qyDownCheckBoxes.DataTextField = "QY";
            qyDownCheckBoxes.DataBind();
        }

        protected void ddldim_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MBD
            if (ddldim.SelectedIndex.ToString().Trim() == "2")
            {
                tdSKUITEM.Visible = true;
                ddlSKUITEM.Visible = true;
            }
            else
            {
                tdSKUITEM.Visible = false;
                ddlSKUITEM.Visible = false;
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (qsDropDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择问卷期数");
                    return;
                }

                ArrayList plsQS = new ArrayList();  //期数
                ArrayList plsQD = new ArrayList();  //渠道
                ArrayList plsDQ = new ArrayList();  //大区
                ArrayList plsQY = new ArrayList();  //区域

                StringBuilder sb = new StringBuilder();
                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();

                //获取问卷期数选择值
                foreach (ListItem item in qsDropDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQS.Add(item.Value);
                    }
                }

                //根据区域筛选where
                foreach (ListItem item in qdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQD.Add(item.Value);
                    }
                }

                //获取大区选择值
                foreach (ListItem item in dqDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsDQ.Add(item.Value);
                    }
                }

                //获取区域选择值
                foreach (ListItem item in qyDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQY.Add(item.Value);
                    }
                }
                string str_where = " 1=1 ";
                string str_where1 = " 1=1 ";
                //根据分析唯独判断
                if (ddldim.SelectedValue.ToString().Trim() == "0")  //MBD
                {
                    if (ddlPP.SelectedIndex == 0)
                    {
                        str_where = str_where + " and ssdfx_cfg = '1' ";
                    }
                    else if (ddlPP.SelectedIndex == 1)
                    {
                        str_where = str_where + " and bljfx_cfg = '1' ";
                    }

                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where = str_where + " and objbc = 'Y' ";
                        str_where1 = str_where1 + " and m.objbc = 'Y' ";
                    }

                    FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "销售区域划分", ddlfw.SelectedValue, str_where, str_where1);
                }
                else if (ddldim.SelectedValue.ToString().Trim() == "1")  //系统
                {
                    if (ddlPP.SelectedIndex == 0)
                    {
                        str_where = str_where + " and ssdfx_cfg = '1'  and objtype = 'NKA'  and objClient <> '无' ";
                    }
                    else if (ddlPP.SelectedIndex == 1)
                    {
                        str_where = str_where + " and bljfx_cfg = '1'  and objtype = 'NKA'  and objClient <> '无' ";
                    }
                    str_where1 = " m.objtype = 'NKA'  and m.objClient <> '无' ";
                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where = str_where + " and objbc = 'Y' ";
                        str_where1 = str_where1 + " and m.objbc = 'Y' ";
                    }
                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where1 = str_where1 + " and m.objbc = 'Y'  ";
                        str_where = str_where + " and objbc = 'Y'  ";
                    }
                    FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "NKA", ddlfw.SelectedValue, str_where, str_where1);
                }
                else if (ddldim.SelectedValue.ToString().Trim() == "2")  //SKU
                {
                    if (ddlPP.SelectedIndex == 0)
                    {
                        str_where = str_where + " and ssdfx_cfg = '1' ";
                    }
                    else if (ddlPP.SelectedIndex == 1)
                    {
                        str_where = str_where + " and bljfx_cfg = '1' ";
                    }

                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where = str_where + " and objbc = 'Y' ";
                        str_where1 = str_where1 + " and m.objbc = 'Y' ";
                    }

                    FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "渠道", ddlfw.SelectedValue, str_where, str_where1);

                    //FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "SKU", ddlfw.SelectedValue, str_where, str_where1);
                }

                else if (ddldim.SelectedValue.ToString().Trim() == "3")  //SKU
                {
                    FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "SKU", ddlfw.SelectedValue, str_where, str_where1);
                }

            }
            catch 
            {
               
            }
        }

        protected void btnOutput_Click(object sender, EventArgs e)
        {
            try
            {
                if (qsDropDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择问卷期数");
                    return;
                }

                ArrayList plsQS = new ArrayList();  //期数
                ArrayList plsQD = new ArrayList();  //渠道
                ArrayList plsDQ = new ArrayList();  //大区
                ArrayList plsQY = new ArrayList();  //区域

                StringBuilder sb = new StringBuilder();
                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();

                //获取问卷期数选择值
                foreach (ListItem item in qsDropDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQS.Add(item.Value);
                    }
                }

                //获取大区选择值
                foreach (ListItem item in dqDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsDQ.Add(item.Value);
                    }
                }

                //获取区域选择值
                foreach (ListItem item in qyDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQY.Add(item.Value);
                    }
                }
                string str_where = " 1=1 ";
                string str_where1 = " 1=1 ";

                DataSet ds = new DataSet();

                //根据分析唯独判断
                if (ddldim.SelectedValue.ToString().Trim() == "0")  //MBD
                {
                    if (ddlPP.SelectedIndex == 0)
                    {
                        str_where = str_where + " and ssdfx_cfg = '1' ";
                    }
                    else if (ddlPP.SelectedIndex == 1)
                    {
                        str_where = str_where + " and bljfx_cfg = '1' ";
                    }

                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where = str_where + " and objbc = 'Y' ";
                        str_where1 = str_where1 + " and m.objbc = 'Y' ";
                    }

                    ds = FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "销售区域划分", ddlfw.SelectedValue, str_where, str_where1);
                }
                else if (ddldim.SelectedValue.ToString().Trim() == "1")  //系统
                {
                    if (ddlPP.SelectedIndex == 0)
                    {
                        str_where = str_where + " and ssdfx_cfg = '1'  and objtype = 'NKA'  and objClient <> '无' ";
                    }
                    else if (ddlPP.SelectedIndex == 1)
                    {
                        str_where = str_where + " and bljfx_cfg = '1'  and objtype = 'NKA'  and objClient <> '无' ";
                    }
                    str_where1 = " m.objtype = 'NKA'  and m.objClient <> '无' ";
                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where = str_where + " and objbc = 'Y' ";
                        str_where1 = str_where1 + " and m.objbc = 'Y' ";
                    }
                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where1 = str_where1 + " and m.objbc = 'Y'  ";
                        str_where = str_where + " and objbc = 'Y'  ";
                    }
                    ds = FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "NKA", ddlfw.SelectedValue, str_where, str_where1);
                }
                else if (ddldim.SelectedValue.ToString().Trim() == "2")  //SKU
                {
                    if (ddlPP.SelectedIndex == 0)
                    {
                        str_where = str_where + " and ssdfx_cfg = '1' ";
                    }
                    else if (ddlPP.SelectedIndex == 1)
                    {
                        str_where = str_where + " and bljfx_cfg = '1' ";
                    }

                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where = str_where + " and objbc = 'Y' ";
                        str_where1 = str_where1 + " and m.objbc = 'Y' ";
                    }

                    ds = FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "渠道", ddlfw.SelectedValue, str_where, str_where1);

                    //FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "SKU", ddlfw.SelectedValue, str_where, str_where1);
                }

                else if (ddldim.SelectedValue.ToString().Trim() == "3")  //SKU
                {
                    ds = FMCGTableShow(plsQS, plsQD, plsDQ, plsQY, "SKU", ddlfw.SelectedValue, str_where, str_where1);
                }

                DataTable dt = ds.Tables[0];
                dt.Columns.Remove(dt.Columns[dt.Columns.Count - 1]);
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(dt);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            catch
            {

            }
        }

        private string GetPaperNameByVal(string pid)
        {
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (pid == item.Value)
                {
                    return item.Text;
                }
            }
            return "";
        }

        public DataSet FMCGTableShow(ArrayList plsQS, ArrayList plsQD, ArrayList plsDQ, ArrayList plsQY, string leixing , string fenshuCol ,string str_where,string str_where1)
        {
            DataSet ds_qs = new DataSet();
            DataTable dt_qs = new DataTable();
            DataSet ds_lx = new DataSet();
            DataTable dt_lx = new DataTable();
            DataSet ds_data = new DataSet();
            string strPname = "";
           StringBuilder sb_html = new StringBuilder();
            StringBuilder sb_header1 = new StringBuilder();
            StringBuilder sb_header2 = new StringBuilder();
            StringBuilder sb_body = new StringBuilder();
            sb_html.Append(@"<table class='tablestyle_otc' cellspacing='0' cellpadding='2' rules='all' enableemptycontentrender='True' 
                                border='1' style='width: 100 %; border - collapse:collapse; ' ><tbody >");

            sb_header1.Append("<tr class='header'>");
            sb_header2.Append("<tr class='header'>");

            StringBuilder sb_sql_qg = new StringBuilder();
            StringBuilder sb_sql_qd = new StringBuilder();
            StringBuilder sb_sql_dq = new StringBuilder();
            StringBuilder sb_sql_qy = new StringBuilder();
            StringBuilder sb_sql_All = new StringBuilder();
            StringBuilder sb_sql_base = new StringBuilder();
            StringBuilder sb_sql_where = new StringBuilder();

            if (leixing == "销售区域划分")
            {
                bool bUinoin = true;
                string strQdWhereSql = " ";
                string strQdWD = " ";
                //大区plsDQ
                if (plsDQ.Count > 0)
                {
                    if (plsQD.Count == 3 || plsQD.Count == 0)
                    {
                        strQdWhereSql = " ";
                        strQdWD = " '全部' 渠道 , ";

                        foreach (ListItem item in dqDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_dq.Append("   union  ");
                                }

                                if (item.Value.Equals("全国"))
                                {
                                    sb_sql_dq.Append(" select " + strQdWD + " '全国' 销售区域划分, ");
                                    string strqs_temp = "";
                                    for (int qs = 0; qs < plsQS.Count; qs++)
                                    {
                                        strPname = GetPaperNameByVal(plsQS[qs].ToString().Trim());
                                        if (qs == 0)
                                        {
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0 from RPT_FMCG_ALL a  where " + str_where + strQdWhereSql + " and paperid = " + plsQS[qs].ToString() + " ) / ");
                                            sb_sql_dq.Append(" sum(case when paperid = " + plsQS[qs].ToString() + " then 1 else 0 end)))*100 as numeric(10,2))  [" + strPname + "] ");
                                            strqs_temp = strqs_temp + plsQS[qs].ToString();
                                        }
                                        else
                                        {
                                            sb_sql_dq.Append(",  Cast((((select COUNT(1)+0.0 from RPT_FMCG_ALL a  where " + str_where + strQdWhereSql + " and paperid = " + plsQS[qs].ToString() + " ) / ");
                                            sb_sql_dq.Append(" sum(case when paperid = " + plsQS[qs].ToString() + " then 1 else 0 end)))*100 as numeric(10,2))  [" + strPname + "] ");
                                            strqs_temp = strqs_temp + "," + plsQS[qs].ToString();
                                        }
                                    }
                                    sb_sql_dq.Append(", 0 'orderid' , 0 'orderidx' ");
                                    sb_sql_dq.Append(" from RPT_FMCG_ALL m  where  " + str_where1 + "   and paperid in(" + strqs_temp + ") " + strQdWhereSql );
                                }
                                else
                                {
                                    sb_sql_dq.Append("  select " + strQdWD + " dq 销售区域划分,  ");
                                    if (plsQS.Count > 1)
                                    {
                                        for (int qs = 0; qs < plsQS.Count; qs++)
                                        {
                                            strPname = GetPaperNameByVal(plsQS[qs].ToString().Trim());
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[qs] + " group by z.dq having z.dq = m.dq ) ");
                                            sb_sql_dq.Append("   /sum(case when paperid = " + plsQS[qs] + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                        }
                                        sb_sql_dq.Append("  2 'orderid' , 0 'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and dq = '" + item.Value + "' group by dq");
                                    }
                                    else
                                    {
                                        strPname = GetPaperNameByVal(plsQS[0].ToString().Trim());
                                        sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[0] + " group by z.dq having z.dq = m.dq ) ");
                                        sb_sql_dq.Append("   /sum(case when paperid = " + plsQS[0] + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                        sb_sql_dq.Append("  2 'orderid' , 0 'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and dq = '" + item.Value + "'  group by dq");
                                    }
                                }
                            }
                        }
                    }
                    for (int i_qd = 0; i_qd < plsQD.Count; i_qd++)
                    {
                        string strQd = plsQD[i_qd].ToString();
                        strQdWhereSql = " and qd = '" + strQd + "' ";
                        strQdWD = " '" + strQd + "' 渠道 , ";
                        int iOrder = i_qd + 1;


                        foreach (ListItem item in dqDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_dq.Append("   union  ");
                                }

                                if (item.Value.Equals("全国"))
                                {
                                    sb_sql_dq.Append(" select "+ strQdWD + " '全国' 销售区域划分, ");
                                    string strqs_temp = "";
                                    for (int qs = 0; qs < plsQS.Count; qs++)
                                    {

                                        strPname = GetPaperNameByVal(plsQS[qs].ToString().Trim());
                                        if (qs == 0)
                                        {
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0 from RPT_FMCG_ALL a  where " + str_where + strQdWhereSql + " and paperid = " + plsQS[qs].ToString() + " ) / ");
                                            sb_sql_dq.Append(" sum(case when paperid = " + plsQS[qs].ToString() + " then 1 else 0 end)))*100 as numeric(10,2))  [" + strPname + "] ");
                                            strqs_temp = strqs_temp + plsQS[qs].ToString();
                                        }
                                        else
                                        {
                                            sb_sql_dq.Append(",  Cast((((select COUNT(1)+0.0 from RPT_FMCG_ALL a  where " + str_where + strQdWhereSql + " and paperid = " + plsQS[qs].ToString() + " ) / ");
                                            sb_sql_dq.Append(" sum(case when paperid = " + plsQS[qs].ToString() + " then 1 else 0 end)))*100 as numeric(10,2))  [" + strPname + "] ");
                                            strqs_temp = strqs_temp + "," + plsQS[qs].ToString();
                                        }
                                    }
                                    sb_sql_dq.Append(", 0 'orderid' , "+ iOrder.ToString()+ " 'orderidx'  ");
                                    sb_sql_dq.Append(" from RPT_FMCG_ALL m  where " + str_where1 + " and paperid in(" + strqs_temp + ") " + strQdWhereSql);
                                }
                                else
                                {
                                    sb_sql_dq.Append("  select " + strQdWD + " dq 销售区域划分,  ");
                                    if (plsQS.Count > 1)
                                    {
                                        for (int qs = 0; qs < plsQS.Count; qs++)
                                        {
                                            
                                            strPname = GetPaperNameByVal(plsQS[qs].ToString().Trim());
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[qs] + " group by z.dq having z.dq = m.dq ) ");
                                            sb_sql_dq.Append("   /sum(case when paperid = " + plsQS[qs] + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                        }
                                        sb_sql_dq.Append("  2 'orderid' ,  " + iOrder.ToString() + "  'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and dq = '" + item.Value + "' group by dq");
                                    }
                                    else
                                    {
                                        strPname = GetPaperNameByVal(plsQS[0].ToString().Trim());
                                        sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[0] + " group by z.dq having z.dq = m.dq ) ");
                                        sb_sql_dq.Append("   /sum(case when paperid = " + plsQS[0] + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                        sb_sql_dq.Append("  2 'orderid' ,  " + iOrder.ToString() + "  'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and dq = '" + item.Value + "'  group by dq");
                                    }
                                }
                            }
                        }
                    }
                }
                //区域plsQY
                if (plsQY.Count > 0)
                {
                    if (plsQD.Count == 3 || plsQD.Count == 0)
                    {
                        strQdWhereSql = " ";
                        strQdWD = " '全部' 渠道 , ";
                        foreach (ListItem item in qyDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_qy.Append("   union  ");
                                }

                                sb_sql_qy.Append("  select" + strQdWD + " qy 销售区域划分,  ");

                                if (plsQS.Count > 1)
                                {
                                    for (int qs = 0; qs < plsQS.Count; qs++)
                                    {

                                        strPname = GetPaperNameByVal(plsQS[qs].ToString().Trim());
                                        sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[qs] + " group by z.qy having z.qy = m.qy ) ");
                                        sb_sql_qy.Append("   /sum(case when paperid = " + plsQS[qs] + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "] , ");
                                    }
                                    sb_sql_qy.Append("  3 'orderid' , 0 'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and qy = '" + item.Value + "'  group by qy");
                                }
                                else
                                {
                                    strPname = GetPaperNameByVal(plsQS[0].ToString().Trim());
                                    sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[0] + " group by z.qy having z.qy = m.qy ) ");
                                    sb_sql_qy.Append("   /sum(case when paperid = " + plsQS[0] + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "] , ");
                                    sb_sql_qy.Append("  3 'orderid', 0 'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and qy = '" + item.Value + "'  group by qy");
                                }
                            }
                        }
                    }
                    for (int i_qd = 0; i_qd < plsQD.Count; i_qd++)
                    {
                        string strQd = plsQD[i_qd].ToString();
                        strQdWhereSql = " and qd = '" + strQd + "' ";
                        strQdWD = " '" + strQd + "' 渠道 , ";
                        int iOrder = i_qd + 1;

                        foreach (ListItem item in qyDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_qy.Append("   union  ");
                                }

                                sb_sql_qy.Append("  select"+ strQdWD + " qy 销售区域划分,  ");

                                if (plsQS.Count > 1)
                                {
                                    for (int qs = 0; qs < plsQS.Count; qs++)
                                    {

                                        strPname = GetPaperNameByVal(plsQS[qs].ToString().Trim());
                                        sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[qs] + " group by z.qy having z.qy = m.qy ) ");
                                        sb_sql_qy.Append("   /sum(case when paperid = " + plsQS[qs] + " then 1 else 0 end )))*100 as numeric(10,2))  ["+ strPname + "] , ");
                                    }
                                    sb_sql_qy.Append("  3 'orderid',  " + iOrder.ToString() + "  'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and qy = '" + item.Value + "'  group by qy");
                                }
                                else
                                {
                                    strPname = GetPaperNameByVal(plsQS[0].ToString().Trim());
                                    sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + plsQS[0] + " group by z.qy having z.qy = m.qy ) ");
                                    sb_sql_qy.Append("   /sum(case when paperid = " + plsQS[0] + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "] , ");
                                    sb_sql_qy.Append("  3 'orderid',  " + iOrder.ToString() + "  'orderidx'  from   RPT_FMCG_ALL m where  " + str_where1 + strQdWhereSql + " and qy = '" + item.Value + "'  group by qy");
                                }
                            }
                        }
                    }
                }
                string sqlMBD =  sb_sql_dq.ToString() + sb_sql_qy.ToString();
                //sqlMBD = sqlMBD.Substring(9, sqlMBD.Length - 1);
                sb_sql_All.Append("  select b.* from (" + sqlMBD + ") b order by  orderidx,orderid ");
            }
            else if (leixing == "NKA")
            {
                bool bUinoin = true;
                string strQdWhereSql = " ";
                string strQdWD = " ";
                //大区plsDQ
                if (plsDQ.Count > 0)
                {
                    if (plsQD.Count == 3 || plsQD.Count == 0)
                    {
                        strQdWhereSql = " ";
                        strQdWD = " '全部' 渠道 , ";
                        foreach (ListItem item in dqDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_dq.Append("   union  ");
                                }

                                if (item.Value.Equals("全国"))
                                {
                                    sb_sql_dq.Append(" select " + strQdWD + " '全国' 销售区域划分, objClient NKA,");
                                    string strqs_temp = "";

                                    bool bAllflag = true;
                                    foreach (var item_qs in plsQS)
                                    {
                                        strPname = GetPaperNameByVal(item_qs.ToString().Trim());
                                  
                                        if (bAllflag)
                                        {
                                            strqs_temp = strqs_temp + "" + item_qs.ToString();
                                            bAllflag = false;
                                        }
                                        else
                                        {
                                            strqs_temp = strqs_temp + "," + item_qs.ToString();
                                        }
                                        
                                        sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where  " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by z.objClient having z.objClient = m.objClient   ) ");
                                        sb_sql_dq.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");


                                    }
                                    sb_sql_dq.Append("   0 'orderid' , 0 'orderidx' from   RPT_FMCG_ALL m  where  " + str_where1 + strQdWhereSql + "   and paperid in(" + strqs_temp + ") " + " group by objClient ");
                                    
                                }
                                else
                                {
                                    sb_sql_dq.Append("  select " + strQdWD + " dq 销售区域划分,objClient NKA,  ");
                                    foreach(var item_qs in plsQS)
                                    {
                                        strPname = GetPaperNameByVal(item_qs.ToString().Trim());
                                        if (plsQS.Count > 1)
                                        {
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by z.dq,z.objClient having z.objClient = m.objClient and z.dq =m.dq  ) ");
                                            sb_sql_dq.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                            //strqs_temp = strqs_temp + "" + item_qs.ToString();
                                        }
                                        else
                                        {
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where  " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by  z.dq,z.objClient having z.objClient = m.objClient and z.dq =m.dq  ) ");
                                            sb_sql_dq.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                            //strqs_temp = strqs_temp + "," + item_qs.ToString();
                                        }
                                    }
                                    sb_sql_dq.Append("   1 'orderid' , 0 'orderidx' from   RPT_FMCG_ALL m  where  " + str_where1 + strQdWhereSql + " group by dq,objClient ");
                                }
                            }
                        }
                    }
                    for (int i_qd = 0; i_qd < plsQD.Count; i_qd++)
                    {
                        string strQd = plsQD[i_qd].ToString();
                        strQdWhereSql = " and qd = '" + strQd + "' ";
                        strQdWD = " '" + strQd + "' 渠道 , ";
                        int iOrder = i_qd + 1;

                        foreach (ListItem item in dqDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_dq.Append("   union  ");
                                }

                                if (item.Value.Equals("全国"))
                                {
                                    sb_sql_dq.Append(" select " + strQdWD + " '全国' 销售区域划分, objClient NKA,");
                                    string strqs_temp = "";

                                    bool bAllflag = true;
                                    foreach (var item_qs in plsQS)
                                    {
                                        strPname = GetPaperNameByVal(item_qs.ToString().Trim());

                                        if (bAllflag)
                                        {
                                            strqs_temp = strqs_temp + "" + item_qs.ToString();
                                            bAllflag = false;
                                        }
                                        else
                                        {
                                            strqs_temp = strqs_temp + "," + item_qs.ToString();
                                        }

                                        sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where  " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by z.objClient having z.objClient = m.objClient   ) ");
                                        sb_sql_dq.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");


                                    }
                                    sb_sql_dq.Append("   0 'orderid' , "+ iOrder.ToString()+ " 'orderidx' from   RPT_FMCG_ALL m  where  " + str_where1 + strQdWhereSql + "   and paperid in(" + strqs_temp + ") " + " group by objClient ");

                                }
                                else
                                {
                                    sb_sql_dq.Append("  select " + strQdWD + " dq 销售区域划分,objClient NKA,  ");
                                    foreach (var item_qs in plsQS)
                                    {
                                        strPname = GetPaperNameByVal(item_qs.ToString().Trim());
                                        if (plsQS.Count > 1)
                                        {
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by z.dq,z.objClient having z.objClient = m.objClient and z.dq =m.dq  ) ");
                                            sb_sql_dq.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                             
                                        }
                                        else
                                        {
                                            sb_sql_dq.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where  " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by  z.dq,z.objClient having z.objClient = m.objClient and z.dq =m.dq  ) ");
                                            sb_sql_dq.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                     
                                        }
                                    }
                                    sb_sql_dq.Append("   1 'orderid' ,  " + iOrder.ToString() + "  'orderidx' from   RPT_FMCG_ALL m  where  " + str_where1 + strQdWhereSql + " group by dq,objClient ");
                                }
                            }
                        }
                    }
                }

                //区域plsQY
                if (plsQY.Count > 0)
                {
                    if (plsQD.Count == 3 || plsQD.Count == 0)
                    {
                        strQdWhereSql = " ";
                        strQdWD = " '全部' 渠道 , ";
                        foreach (ListItem item in qyDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_qy.Append("   union  ");
                                }
                                sb_sql_qy.Append("  select " + strQdWD + " qy 销售区域划分,objClient NKA,  ");
                                foreach (var item_qs in plsQS)
                                {
                                    strPname = GetPaperNameByVal(item_qs.ToString().Trim());
                                    if (plsQS.Count > 1)
                                    {
                                        sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by z.qy,z.objClient having z.objClient = m.objClient and z.qy =m.qy  ) ");
                                        sb_sql_qy.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                      
                                    }
                                    else
                                    {
                                        sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where  " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by  z.qy,z.objClient having z.objClient = m.objClient and z.qy =m.qy  ) ");
                                        sb_sql_qy.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                       
                                    }
                                }
                                sb_sql_qy.Append("   3 'orderid' , 0 'orderidx' from   RPT_FMCG_ALL m  where  " + str_where1 + strQdWhereSql + "  and qy = '" + item.Value + "'   group by qy,objClient ");
                            }
                        }
                    }
                    for (int i_qd = 0; i_qd < plsQD.Count; i_qd++)
                    {
                        string strQd = plsQD[i_qd].ToString();
                        strQdWhereSql = " and qd = '" + strQd + "' ";
                        strQdWD = " '" + strQd + "' 渠道 , ";
                        int iOrder = i_qd + 1;
                        foreach (ListItem item in qyDownCheckBoxes.Items)
                        {
                            if (item.Selected)
                            {
                                if (bUinoin)
                                {
                                    bUinoin = false;
                                }
                                else
                                {
                                    sb_sql_qy.Append("   union  ");
                                }
                                sb_sql_qy.Append("  select " + strQdWD + " qy 销售区域划分,objClient NKA,  ");
                                foreach (var item_qs in plsQS)
                                {
                                    strPname = GetPaperNameByVal(item_qs.ToString().Trim());
                                    if (plsQS.Count > 1)
                                    {
                                        sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by z.qy,z.objClient having z.objClient = m.objClient and z.qy =m.qy  ) ");
                                        sb_sql_qy.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                    }
                                    else
                                    {
                                        sb_sql_qy.Append("  Cast((((select COUNT(1)+0.0  from RPT_FMCG_ALL z where  " + str_where + strQdWhereSql + "  and paperid = " + item_qs.ToString() + " group by  z.qy,z.objClient having z.objClient = m.objClient and z.qy =m.qy  ) ");
                                        sb_sql_qy.Append("   /sum(case when paperid = " + item_qs.ToString() + " then 1 else 0 end )))*100 as numeric(10,2))  [" + strPname + "], ");
                                    }
                                }
                                sb_sql_qy.Append("   3 'orderid' , "+ iOrder.ToString() + " 'orderidx' from   RPT_FMCG_ALL m  where  " + str_where1 + strQdWhereSql + "  and qy = '" + item.Value + "'   group by qy,objClient ");
                            }
                        }
                    }
                }
                string sqlMBD = sb_sql_dq.ToString() + sb_sql_qy.ToString();
                sb_sql_All.Append("  select b.* from (" + sqlMBD.ToString() + ") b order by  orderidx,orderid ");
            }
            else if (ddldim.SelectedItem.Text.Equals("SKU"))
            {
                string qgFx = "";
                string sql = "";
                string skustrSQL = "select * from RPT_FMCG_ITEM where 1=1 order by orderno";  //固定sku
                DataSet dsSKU = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, skustrSQL);
                string stritmename = "";
                str_where1 = "and  " + str_where1;
                for (int i = 0; i < dsSKU.Tables[0].Rows.Count; i++)
                {
                    stritmename = dsSKU.Tables[0].Rows[i]["itemname"].ToString().Trim();
                    qgFx = " and " + dsSKU.Tables[0].Rows[i]["colname"].ToString().Trim() + "fx = '有分销' ";

                    if (ddlfw.SelectedIndex != 0)
                    {
                        str_where1 = str_where1 + " and objbc = 'Y' ";
                    }

                    if (i == 0)
                    {
                        sql = GetSKUFx(qgFx, stritmename, str_where1, plsQS);
                    }
                    else
                    {
                        foreach (var item_qs in plsQS)
                        {
                            sql = sql + " Union " + GetSKUFx(qgFx, stritmename, str_where1, plsQS);
                        }
                    }
                }
                sb_sql_All.Append(sql);
            }

            try
            {
                ds_data = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql_All.ToString());
            }
            catch (Exception)
            {

                content.InnerHtml = "";
            }

            //div_sql.InnerText = sb_sql_All.ToString();

            //if (leixing == "销售区域划分")
            //{
            //    ds_data.Tables[0].DefaultView.Sort = "orderid desc";
            //}
            if (ds_data != null && ds_data.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds_data.Tables[0];

                for (int h = 0; h < dt.Columns.Count - 2; h++)
                {
                    //if (h == 0)
                    //{
                    //    sb_header1.Append("<td>" + InputText.GetStrByObj(dt.Columns[h].ToString()) + "</td>");
                    //}
                    //else
                    //{
                    //    if (dt.Columns[h].ToString().Equals("分值26"))
                    //    {
                    //        sb_header1.Append("<td>2016.Q1</td>");
                    //    }
                    //    else if (dt.Columns[h].ToString().Equals("分值45"))
                    //    {

                    //        sb_header1.Append("<td>2016.Q2</td>");
                    //    }
                    //}
                    sb_header1.Append("<td>" + InputText.GetStrByObj(dt.Columns[h].ToString()) + "</td>");
                }

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    sb_body.Append("<tr style='cursor: pointer; '>");
                    ///查询结果包含 TypeCode 因此从1开始
                    for (int j = 0; j < dt.Columns.Count - 2; j++)
                    {
                        sb_body.Append("<td>" + InputText.GetStrByObj(dt.Rows[i][j]) + "</td>");
                        //if (j == 0 || j==1)
                        //{
                        //    sb_body.Append("<td>" + InputText.GetStrByObj(dt.Rows[i][j]) + "</td>");
                        //}
                        //else
                        //{
                        //    sb_body.Append("<td>" + (InputText.GetDouble(dt.Rows[i][j])).ToString("0.00") + "%" + "</td>");
                        //}
                    }
                    sb_body.Append("</tr>");
                }
                sb_header1.Append("</tr>");
                sb_header2.Append("</tr>");
                sb_body.Append("</tbody></table> ");
                content.InnerHtml = sb_html.ToString() + sb_header1.ToString() + sb_header2.ToString() + sb_body.ToString();

                ////添加charts值
                //if (dt.Rows.Count == 0)
                //{
                //    item_Name = "[]";
                //    item_series = "[]";
                //    item_legend = "[]";
                //}
                //else
                //{
                //    item_Name = "";
                //    item_series = "";
                //    item_legend = "";
                //}

                ////echart 值
                //string[] item_series_value_tmp = new string[dt.Columns.Count - 2];
                ////echart 名称
                //string[] item_series_name_tmp = new string[dt.Columns.Count - 2];
                //for (int i = 0; i < dt.Columns.Count - 1; i++)
                //{
                //    if (dt.Columns[i].ToString().Equals("分值26"))
                //    {
                //        dt.Columns["分值26"].ColumnName = "2016.Q1";
                //    }
                //    else if (dt.Columns[i].ToString().Equals("分值45"))
                //    {
                //        dt.Columns["分值45"].ColumnName = "2016.Q2";
                //    }
                //}
                //for (int i = 0; i < dt.Columns.Count - 1; i++)
                //{
                //    if (i > 0)
                //    {
                //        item_legend += "'" + dt.Columns[i].ToString() + "',";
                //    }
                //}

                //for (int i = 0; i < dt.Rows.Count; i++)
                //{
                //    DataRow dr = dt.Rows[i];
                //    for (int j = 0; j < dt.Columns.Count - 1; j++)
                //    {
                //        if (j >= 1)
                //        {
                //            item_series_value_tmp[j - 1] += InputText.GetDouble(dr[j].ToString()).ToString("##0.00") + ",";
                //            item_series_name_tmp[j - 1] = "'" + dt.Columns[j].ToString() + "'";
                //        }
                //        else
                //        {
                //            item_Name += "'" + dr[j].ToString() + "',";
                //        }
                //    }
                //}

                /////echart  legend
                //item_legend = "[" + item_legend.TrimEnd(',') + "]";

                //for (int i = 0; i < item_series_name_tmp.Length; i++)
                //{
                //    item_series += "{name:" + item_series_name_tmp[i]
                //        + ",data:[" + item_series_value_tmp[i].TrimEnd(',')
                //        + "],type:'bar',itemStyle:{normal:{color: '" + InputText.GetEchartColors()[i] + "',label:{show: true,position: 'top',textStyle:{color:'black'}}}}},";

                //}
                //item_series = "[" + item_series.TrimEnd(',') + "]";

                /////echart item_Name
                //item_Name = "[" + item_Name.TrimEnd(',') + "]";
            }
            else
            {
                item_Name = "";
                item_series = "";
                item_legend = "";

            }

            return ds_data;
        }
    }
}