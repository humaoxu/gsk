﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Collections;

namespace JSKWeb.REPORTFMCG
{
    public partial class RPTFMCGSCORE : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();
        int iPageSize = 30;
        ArrayList dqList = new ArrayList();  //大区

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();

                BindQD();
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_fmcg");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}  and isRPTReleased = 1 order by paperId desc ", ddlProjectId);
            //var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}   order by paperId desc ", ddlProjectId);

            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();
        }

        private void BindQD()
        {
            //获取渠道
            var ddlTypeSelectSql = "SELECT DISTINCT QD FROM RPT_FMCG_ALL ";
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qdDownCheckBoxes.DataSource = ddlTypeDataSet;
            qdDownCheckBoxes.DataValueField = "QD";
            qdDownCheckBoxes.DataTextField = "QD";
            qdDownCheckBoxes.DataBind();

            //获取大区
            ddlTypeSelectSql = "SELECT DISTINCT DQ FROM RPT_FMCG_ALL ";

            var ddlTypeDataSet_dq = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            dqDownCheckBoxes.DataSource = ddlTypeDataSet_dq;
            dqDownCheckBoxes.DataValueField = "DQ";
            dqDownCheckBoxes.DataTextField = "DQ";
            dqDownCheckBoxes.DataBind();
            //dqDownCheckBoxes.Items.Insert(0, (new ListItem("全国", "全国")));


            //获取区域
            ddlTypeSelectSql = "SELECT DISTINCT QY FROM RPT_FMCG_ALL ";
            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "QY";
            qyDownCheckBoxes.DataTextField = "QY";
            qyDownCheckBoxes.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();
        }
        
        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }

        protected void dqDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qyDownCheckBoxes.Items.Clear();
            qyDownCheckBoxes.DataSource = null;

            //如果大区全选
            int select_sum = Int32.Parse(dqDownCheckBoxes.Items.Capacity.ToString());
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    dqList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            if (dqList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - dqList.Count == 1)
                {
                    str_code = "(SELECT DISTINCT DQ FROM RPT_FMCG_ALL)";
                }
                else
                {
                    for (int i = 0; i < dqList.Count; i++)
                    {
                        str_code += "'" + dqList[i].ToString() + "',";
                    }
                }
            }

            //获取区域
            var ddlTypeSelectSql = "";
            if (str_code.Trim().Length == 0)
            {
                ddlTypeSelectSql = "SELECT DISTINCT QY FROM RPT_FMCG_ALL ";
            }
            else
            {
                if (str_code.Contains("全国"))
                {
                    ddlTypeSelectSql = " SELECT DISTINCT QY FROM RPT_FMCG_ALL  order by qy ";
                }
                else
                {
                    if (select_sum - dqList.Count == 1)
                    {
                        ddlTypeSelectSql = " SELECT DISTINCT QY FROM RPT_FMCG_ALL  order by qy ";
                    }
                    else
                    {

                        ddlTypeSelectSql = "SELECT DISTINCT QY from RPT_FMCG_All where dq in (" + str_code.TrimEnd(',').ToString() + ")  order by QY ";
                    }
                }

            }

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "QY";
            qyDownCheckBoxes.DataTextField = "QY";
            qyDownCheckBoxes.DataBind();
        }


        private string GetSql(string strWd)
        {
            string str = " select ";
            if (strWd == "全国" || strWd == "全渠道")
            {
                str = str + " '" + strWd + "'  '名称',";
                str = str + " avg(t_score) as 'AVG(总分)',avg(SSDFX) as 'AVG(舒适达分销)',avg(BLJFX) as 'AVG(保丽净分销)',avg(YZSFX) as 'AVG(益周适分销)',avg(SSDPM) as 'AVG(舒适达排面)',avg(BLJPM) as 'AVG(保丽净排面)',avg(YZSPM) as 'AVG(益周适排面)',0 orderid  from SYS_FMCG_RESULT inner join surveyobj on SYS_FMCG_RESULT.shopid = surveyobj.surveyobjid ";

            }
            else
            {
                str = str + "surveyobj." + strWd + " '名称',";
                str = str + " avg(t_score) as 'AVG(总分)',avg(SSDFX) as 'AVG(舒适达分销)',avg(BLJFX) as 'AVG(保丽净分销)',avg(YZSFX) as 'AVG(益周适分销)',avg(SSDPM) as 'AVG(舒适达排面)',avg(BLJPM) as 'AVG(保丽净排面)',avg(YZSPM) as 'AVG(益周适排面)',1 orderid  from SYS_FMCG_RESULT inner join surveyobj on SYS_FMCG_RESULT.shopid = surveyobj.surveyobjid ";
            }
            str = str + " where SYS_FMCG_RESULT.paperid = " + this.ddlTicketType.SelectedValue + " and surveyobj.projectId = 12 ";
            if (strWd == "全国" || strWd == "全渠道")
            {
            }
            else
            {
                str = str + " and surveyobj." + strWd + " is not null ";
            }

            bool bflag = true;
            string strDq = "";
            for (int i = 0; i < dqDownCheckBoxes.Items.Count; i++)
            {
                if (dqDownCheckBoxes.Items[i].Selected)
                {
                    if (bflag)
                    {
                        strDq = strDq + "'" + dqDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {
                        strDq = strDq + ",'" + dqDownCheckBoxes.Items[i].Text + "'";
                    }
                    bflag = false;
                }
            }
            if (strDq != "")
            {
                str = str + " and surveyobj.area in (" + strDq + ")";
            }

            bflag = true;
            string strQy = "";
            for (int i = 0; i < qyDownCheckBoxes.Items.Count; i++)
            {
                if (qyDownCheckBoxes.Items[i].Selected)
                {
                    if (bflag)
                    {
                        strQy = strQy + "'" + qyDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {
                        strQy = strQy + ",'" + qyDownCheckBoxes.Items[i].Text + "'";
                    }
                    bflag = false;
                }
            }
            if (strQy != "")
            {
                str = str + " and surveyobj.col11 in (" + strQy + ")";
            }


            bflag = true;
            string strQd = "";
            for (int i = 0; i < qdDownCheckBoxes.Items.Count; i++)
            {
                if (qdDownCheckBoxes.Items[i].Selected)
                {
                    if (bflag)
                    {
                        strQd = strQd + "'" + qdDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {
                        strQd = strQd + ",'" + qdDownCheckBoxes.Items[i].Text + "'";
                    }
                    bflag = false;
                }
            }
            if (strQd != "")
            {
                str = str + " and surveyobj.col5 in (" + strQd + ")";
            }
            
            if (strWd == "全国" || strWd == "全渠道")
            {
                // str = str + " '" + strWd + "',";
            }
            else
            {
                str = str + " group by surveyobj." + strWd + " ";
            }
            return str;
        }

        protected void LoadInfo()
        {
            
            try
            {
                StringBuilder sb = new StringBuilder();
                string strSql = "";

                if (ddldim.SelectedIndex == 0)
                {
                    strSql = strSql + "select * from (";
                    strSql = strSql + GetSql("全国");

                    strSql = strSql + " Union  ";

                    strSql = strSql + GetSql("area");

                    strSql = strSql + " ) b order by orderid ";
                }
                else if (ddldim.SelectedIndex == 1)
                {
                    //strSql = strSql + GetSql("全国");

                    //strSql = strSql + GetSql(" Union  ");

                    strSql = strSql + GetSql("col11");
                }
                else if (ddldim.SelectedIndex == 2)
                {
                    strSql = strSql + "select * from (";
                    strSql = strSql + GetSql("全渠道");

                    strSql = strSql + " Union  ";

                    strSql = strSql + GetSql("col5");
                    strSql = strSql + " ) b  order by orderid ";
                }
                else if (ddldim.SelectedIndex == 3)
                {
                    strSql = strSql + GetSql("COL22");   
                }

                else if (ddldim.SelectedIndex == 4)
                {
                    strSql = strSql + GetSql("COL20");
                }

                else if (ddldim.SelectedIndex == 5)
                {
                    strSql = strSql + GetSql("COL18");
                }

                ////生成Dataset
                //DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(strSql.ToString(), con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle' cellspacing='0' cellpadding='2' style='font-size: 9pt; width: 100%;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                for (int i = 0; i < dr.FieldCount-1; i++)
                {
                    order_str = dr.GetName(0);

                    sb.Append("<th align='center' style='width:200px;' scope='col'>" + dr.GetName(i) + "</th>");
                }
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer'>");
                    for (int i = 0; i < dr.FieldCount-1; i++)
                    {
                        try
                        {
                            double dtemp = Double.Parse(dr[i].ToString());
                            sb.Append("<td class='across'>" + dtemp.ToString("0.###") + "</td>");
                        }
                        catch
                        {
                            sb.Append("<td class='across'>" + dr[i].ToString() + "</td>");
                        }


                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();
                
            }
            catch
            {
                content.InnerHtml = "";
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }


        protected void btnOutPut_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            string strSql = "";

            if (ddldim.SelectedIndex == 0)
            {
                strSql = strSql + "select * from (";
                strSql = strSql + GetSql("全国");

                strSql = strSql + " Union  ";

                strSql = strSql + GetSql("area");

                strSql = strSql + " ) b order by orderid ";
            }
            else if (ddldim.SelectedIndex == 1)
            {
                //strSql = strSql + GetSql("全国");

                //strSql = strSql + GetSql(" Union  ");

                strSql = strSql + GetSql("col11");
            }
            else if (ddldim.SelectedIndex == 2)
            {
                strSql = strSql + "select * from (";
                strSql = strSql + GetSql("全渠道");

                strSql = strSql + " Union  ";

                strSql = strSql + GetSql("col5");
                strSql = strSql + " ) b  order by orderid ";
            }
            else if (ddldim.SelectedIndex == 3)
            {
                strSql = strSql + GetSql("COL22");
            }

            else if (ddldim.SelectedIndex == 4)
            {
                strSql = strSql + GetSql("COL20");
            }

            else if (ddldim.SelectedIndex == 5)
            {
                strSql = strSql + GetSql("COL18");
            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }
        
    }
}