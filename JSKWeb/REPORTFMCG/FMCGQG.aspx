﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="FMCGQG.aspx.cs" Inherits="JSKWeb.REPORTFMCG.FMCGQG" %>
<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    
    <script src="../js/echarts.min.js" type="text/javascript"></script>
    
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }

        select {
            border-color: #cccccc;
        }
        
    </style>
    <script language="javascript" type="text/javascript">
        function change()
        {
            document.getElementById('main').hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td align="right" width="75px" class="contrlFontSize_s">问卷期数：
                    </td>
                    <td align="left" width="120px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketType_SelectedIndexChanged" >
                        </asp:DropDownList>
                    </td>
                              <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="td_qd1">门店范围：
                    </td>
                    <td align="left" runat="server" id="td_qd2">
                            <asp:DropDownList ID="ddlfw" CssClass="inputtext15" runat="server" Width="120px" AutoPostBack="True" >
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="1">必查门店</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s">品牌：
                    </td>
                    <td align="left" width="100px">
                        <asp:DropDownList ID="ddlPP" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" >
                            <asp:ListItem Value="0">舒适达</asp:ListItem>
                            <asp:ListItem Value="1">保丽净</asp:ListItem>
                        </asp:DropDownList>
                    </td>
          
                    <td align="right" runat="server" id="td_dq1" width="80px" class="contrlFontSize_s">分析维度：
                    </td>
                    <td align="left" runat="server" id="td_dq2">
                         <asp:DropDownList ID="ddldim" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddldim_SelectedIndexChanged" >
                         
                        </asp:DropDownList>
                    </td>
                <td align="right" width="80px">&nbsp;<asp:Button ID="btnChart" OnClick="btnChart_Click" CssClass="btn_s"
                            runat="server" Text="图表展示" Width="60px" />
                        </td>
                <td align="left" width="300px">&nbsp;<asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />                    
                    <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
                </td>
                </tr>
            </tbody>
        </table>
        <div style="height:5px"></div>
           <table cellpadding="0" cellspacing="0" border="0"  runat="server" visible="false" id="tbqy">
                <tbody>
                    <tr>
                        <td align="right" width="75px" class="contrlFontSize_s">大区：
                        </td>
                        <td align="left" width="120px">
                            <asp:DropDownList ID="ddl_qy_dq" CssClass="inputtext15" runat="server" Width="120px"   >
                            </asp:DropDownList>
                        </td>   
                        <td align="right" width="75px" class="contrlFontSize_s">
                             渠道： 
                        </td>
                        <td align="left" width="120px">
                            <asp:DropDownList ID="ddl_qy_qd" CssClass="inputtext15" runat="server" Width="120px" >
                            </asp:DropDownList>
                        </td>   
                    </tr>
                </tbody>
            </table>
               <table cellpadding="0" cellspacing="0" border="0"  runat="server" visible="false" id="tbqd">
                <tbody>
                    <tr>
                        <td align="right" width="75px" class="contrlFontSize_s">大区：
                        </td>
                        <td align="left" width="120px">
                            <asp:DropDownList ID="ddl_qd_dq" CssClass="inputtext15" runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="ddl_qd_dq_SelectedIndexChanged" >
                            </asp:DropDownList>
                        </td>
                        
                        <td align="right" width="75px" class="contrlFontSize_s">区域：
                        </td>
                        <td align="left" width="120px">
                            <asp:DropDownList ID="ddl_qd_qy" CssClass="inputtext15" runat="server" Width="120px" AutoPostBack="True" >
                            </asp:DropDownList>
                        </td>   
                    </tr>
                </tbody>
            </table>

           

    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align:center">
            
    </div>
     <div id="main" style="width: 100%; height: 500px; overflow: hidden;"></div>
    <script src="echarts.min.js" type="text/javascript"></script>
    <script src="map/china.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        var s_itemName  = <%= this.itemName %>;
        var s_itemValue = '';
        if (s_itemName != '[0]' && s_itemName !='') {
            var s_itemFormatte = <%= this.itemFormate %>;
            var s_itemValue = <%= this.itemValue %>;
            var s_itemTitle = <%= this.itemTitle %>;
            var s_maxY = 40;
            var s_interval = 5;
            if (s_itemFormatte == '%') {
                s_maxY = 100;
                s_interval = 20;
            }
            option = {
                title: {
                    x: 'center',
                    text: s_itemTitle 
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'
                    }
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                legend: {
                    data: [s_itemTitle]
                },
                xAxis : [
     {
         type :'category',
         axisLabel:{
             interval:0,
             rotate:45,
             margin:2,
             textStyle:{
                 color:"#222"
             }
         },
         data : s_itemName
     }
                ],
                grid: {
                    x: 40,
                    x2: 20,
                    y2: 100,
                },
                yAxis: [
                      {
                          type: 'value',
                          name: '',
                          min: 0,
                          max: s_maxY,
                          interval: s_interval,
                          axisLabel: {
                              formatter: '{value} '+s_itemFormatte
                          }
                      }
                ],
                series: [
                    {
                        name: '分值',
                        type: 'bar',
                        data: s_itemValue,
                        seriesLabel: {
                            formatter: '{value} '+s_itemFormatte
                        },
                        itemStyle: {
                            normal: {
                                color: function(params) { 
                                    var colorList = ['#C1232B'];
                                    return colorList[0]
                                },
                                label: {
                                    show: true,
                                    position: 'top',
                                    formatter: '{c}'
                                }
                            }
                        },
                        data: s_itemValue,
                        markPoint: {
                            tooltip: {
                                trigger: 'item',
                                backgroundColor: 'rgb(50,216,234)',
                                formatter: function(params){
                                    return '<img src="' 
                                            + params.data.symbol.replace('image://', '')
                                            + '"/>';
                                }
                            },
                            data: s_itemValue
                
                        }
                    }
                ]
            };

            var chart = echarts.init(document.getElementById('main'));
       
            chart.showLoading({
                text: '正在加载中..... ',
            });
            chart.hideLoading();

            chart.setOption(option);

            $("#main").resize(function(){
                $("#main").resize();
            })
        }
        
    </script>
</asp:Content>