﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="ChangePwd.aspx.cs" Inherits="JSKWeb.ChangePwd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
// <![CDATA[

        function btnback_onclick() {

        }

        function btnback_onclick() {

        }

        function btnback_onclick() {

        }

        function btnback_onclick() {

        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div style="width: 100%" align="left">
        <table width="50%" style="text-align: left; font-size: 12px;" cellspacing="2px">
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <font style="font-size: 12pt"><b><asp:Label ID="lbltitle" runat="server" Text="修改密码"></asp:Label></b> </font>
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
           
                        <table width="85%" style="text-align: center">
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        旧密码：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtOldPwd" runat="server" MaxLength="50" Width="250px" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        新密码：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPwd" runat="server" MaxLength="50" Width="250px" TextMode="Password"></asp:TextBox>
                                 </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        确认新密码：
                                    </label>
                                </td>
                                <td>
                                   <asp:TextBox ID="txtRePwd" runat="server" MaxLength="50" Width="250px" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                           
                        </table>
               
                </td>
            </tr>
          
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn" Text="保存" Width="60px" OnClick="btnSave_Click"
                                     />
                                &nbsp;&nbsp;&nbsp;&nbsp
                                &nbsp;&nbsp;&nbsp;&nbsp
                                <asp:Button ID="btnBack" runat="server" CssClass="btn" Text="返回" Width="60px" 
                                    onclick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
