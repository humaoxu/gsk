﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace DBUtility
{
    public class JavaScript
    {
        public static void Show(Page page, string message)
        {
            page.ClientScript.RegisterStartupScript(typeof(Page), "JavascriptShow", "<script type='text/javascript'>alert(\"" + message + "\");</script>");
        }

        public static void ShowAndClose(string message)
        {
            HttpContext.Current.Response.Write("<script type='text/javascript'>window.alert('" + message.Replace("'", "\"").Replace("\n", @"\n").Replace("\r", @"\t") + "');window.opener = null; window.close()</script>");
        }

        public static void Register(Page page, string script)
        {
            try
            {
                page.ClientScript.RegisterStartupScript(typeof(Page), "JavascriptExec", "<script type='text/javascript'>" + script + "</script>");
            }
            catch
            {
                throw;
            }
        }

        public static void Alert(Page page, string message)
        {
            try
            {
                page.ClientScript.RegisterStartupScript(typeof(Page), "JavascriptAlert", "<script type='text/javascript'>alert(\"" + message + "\");</script>");
            }
            catch
            {
                throw;
            }
        }

        public static void Href(Page page, string url)
        {
            try
            {
                page.ClientScript.RegisterStartupScript(typeof(Page), "Href", "<script language=\"javascript\"> document.location.href = \"" + url + "\"; </script>");
            }
            catch
            {
                throw;
            }
        }

        public static void ClosePage(Page page)
        {
            try
            {
                page.ClientScript.RegisterStartupScript(typeof(Page), "BD27E9CB", "<script language=\"javascript\">window.opener=null;window.open('','_self');window.close();</script>");
            }
            catch
            {
                throw;
            }
        }

        public static void AlertToUrl(Page page, string msg, string url)
        {
            try
            {
                page.ClientScript.RegisterStartupScript(typeof(Page), "JavascriptAlert", "<script type='text/javascript'> document.body.style.display = \"none\";alert(\"" + msg + "\"); document.location.href = \"" + url + "\"; </script>");
            }
            catch
            {
                throw;
            }
        }

        public static void ClosePage(Page aPage, string aRstValue)
        {
            JavaScript.Register(aPage, string.Format("window.returnValue='{0}';window.close()", aRstValue));
        }

        public static void ClosePage(Page aPage, string aRstValue, string aMsg)
        {
            aPage.ClientScript.RegisterStartupScript(typeof(Page),
                "JavascriptAlert", "<script type='text/javascript'>   alert(\"" + aMsg + "\");   this.close(); window.opener.location.href=window.opener.location.href; </script>");
           
           // JavaScript.Register(aPage, string.Format("window.opener.location.reload(); this.alert('{1}'); this.close();)", aRstValue, aMsg));
           // JavaScript.Register(aPage, string.Format("window.returnValue='{0}';window.alert('{1}');window.close()", aRstValue, aMsg));
        }
    }
}