﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="OTC_CITY.aspx.cs" Inherits="JSKWeb.OTCREPORT.OTC_CITY" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
   
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0"  class="">
            <tbody>
                <tr>
                    <td align="right" width="100px" class="contrlFontSize">
                        问卷期数：
                    </td>
                    <td align="left" width="140px">
                        <asp:DropDownList ID="ddlTicketType"  CssClass="dd_chk_select"  runat="server" Width="140px">
                              
                        </asp:DropDownList>
                    </td>
                    
                    <td style="padding-left: 25px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qdDownCheckBoxes" runat="server"  OnSelectedIndexChanged="qdDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td id="qy_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qyDownCheckBoxes" runat="server" AddJQueryReference="True"  OnSelectedIndexChanged="dqDownCheckBoxes_SelcetedIndexChanged" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        得分点：
                    </td>
                    <td align="left" >
                        <asp:DropDownList ID="ddlType2"  CssClass="dd_chk_select"  runat="server" Width="140px" >
                        <asp:ListItem Value="0">总分</asp:ListItem>
                            <asp:ListItem Value="1">分销评分</asp:ListItem>
                            <asp:ListItem Value="2">陈列评分</asp:ListItem>
                            <asp:ListItem Value="3">POSM评分</asp:ListItem>
                            <asp:ListItem Value="4">价格评分</asp:ListItem>
                            <asp:ListItem Value="5">推荐评分</asp:ListItem>
                        </asp:DropDownList>
                        </td>
                    <td align="left" >
                        
                        &nbsp;<asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn" 
                            runat="server"  Text="查询" Width="80px" />
                        </td>
                    

                   
                </tr>
               
            </tbody>
        </table>
    </fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="chart" style="width: 100%; height: 600px"></div>
    
    <script src="../Content/libs/echarts/js/echarts.min.js" type="text/javascript"></script>
    <script src="../Content/libs/echarts/js/map/china.js" type="text/javascript"></script>
    <script type="text/javascript">
        var top5Json = '<%= this.TopFiveJsonContent %>';
        var restJson = '<%= this.RestResultJsonContent %>';

        var option = {
            backgroundColor: '#404a59',
            title: {
                text: '',
                left: 'center',
                textStyle: {
                    color: '#fff'
                }
            },
            tooltip: {
                trigger: 'item',
                formatter: function(params) {
                    return params.name + ": " + params.value[2];
                }
            },
            legend: {
                orient: 'vertical',
                y: 'bottom',
                x: 'right',
                data: ['pm2.5'],
                textStyle: {
                    color: '#fff'
                }
            },
            geo: {
                map: 'china',
                label: {
                    emphasis: {
                        show: false
                    }
                },
                roam: true,
                itemStyle: {
                    normal: {
                        areaColor: '#323c48',
                        borderColor: '#111'
                    },
                    emphasis: {
                        areaColor: '#2a333d'
                    }
                }
            },
            series: [
                {
                    type: 'scatter',
                    coordinateSystem: 'geo',
                    data: JSON.parse(restJson),
                    symbolSize: function(val) {
                        return val[2] * <%= this.DisplayCoefficient %>;
                    },
                    label: {
                        normal: {
                            formatter: '{b}',
                            position: 'right',
                            show: false
                        },
                        emphasis: {
                            show: true
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#ddb926'
                        }
                    }
                },
                {
                    name: 'Top 5',
                    type: 'effectScatter',
                    coordinateSystem: 'geo',
                    data: JSON.parse(top5Json),
                    symbolSize: function(val) {
                        return val[2] * <%= this.DisplayCoefficient %>;
                    },
                    showEffectOn: 'render',
                    rippleEffect: {
                        brushType: 'stroke'
                    },
                    hoverAnimation: true,
                    label: {
                        normal: {
                            formatter: '{b}',
                            position: 'right',
                            show: true
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: '#f4e925',
                            shadowBlur: 10,
                            shadowColor: '#333'
                        }
                    },
                    zlevel: 1
                }
            ]
        };

        var chart = echarts.init(document.getElementById('chart'));
        chart.setOption(option);
    </script>
</asp:Content>
