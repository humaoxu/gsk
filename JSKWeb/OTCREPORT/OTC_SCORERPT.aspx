﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="OTC_SCORERPT.aspx.cs" Inherits="JSKWeb.OTCREPORT.OTC_SCORERPT" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <link rel="Stylesheet" type="text/css" href="../css/CustomDDStyles.css" />
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                document.getElementById("<%=hfstorecode.ClientID %>").value = akey;
                document.getElementById("<%=hfdateid.ClientID %>").value = $(row.cells[2]).text();
                document.getElementById("<%=hfRound.ClientID %>").value = $(row.cells[1]).text();
                // row.cells[0].innerHTML = "<input type=\"radio\" value=\"\" name=\"rd\" checked='checked' />";
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {
                var json = { "oo": [
                { "url": "Application_Add.aspx", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                { "url": "Application_Edit.aspx?ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                {},
                { "url": "Logic_Rule.aspx?TYPE=Default&ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" }
                ]

                };

                if (index == 0) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 1) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 2) {
                    return confirm("Confirm delete？");
                } else if (index == 3) {
                    return confirm("Confirm Run？");
                } else if (index == 5) {//Set out
                    var result = window.open(json.oo[4].url + key, window, json.oo[4].parm);

                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                else {
                    return false;
                }
            }
        }
    
    </script>
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0"  class="">
            <tbody>
                <tr>
                    <td align="right" width="75px" class="contrlFontSize_s">
                        问卷期数：
                    </td>
                    <td align="left" width="80px">
                        <asp:DropDownList ID="ddlTicketType"  CssClass="ddl_select"  runat="server" Width="80px">
                        </asp:DropDownList>
                    </td>
                    
                    <td align="right" width="80px" class="contrlFontSize_s">
                        分析维度：
                    </td>
                    <td align="left"  width="100px" >
                        <asp:DropDownList ID="ddlDim"  CssClass="dd_chk_select"  runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlDim_SelectedIndexChanged">
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="3">大区</asp:ListItem>
                            <asp:ListItem Value="4">团队</asp:ListItem>
                            <asp:ListItem Value="5">代表</asp:ListItem>
                            <asp:ListItem Value="6">城市</asp:ListItem>
                        </asp:DropDownList>
                      
                    </td>
                    <td align="right" width="50px" class="contrlFontSize_s" runat="server" id="td_qd1">
                        渠道：
                    </td>
                    <td align="left"  runat="server" id="td_qd2" >
                        <asp:DropDownList ID="ddlType2"  CssClass="dd_chk_select"  runat="server" Width="80px" AutoPostBack="true" OnSelectedIndexChanged="ddlType2_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right"  runat="server" id="td_dq1" width="50px" class="contrlFontSize_s">
                        大区：
                    </td>
                    <td align="left"  runat="server" id="td_dq2" >
                        <asp:DropDownList ID="ddlType3" CssClass="dd_chk_select" runat="server" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="ddlType3_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="50px" class="contrlFontSize_s" runat="server" id="td_td1">
                        团队：
                    </td>
                    <td align="left" width="120px" runat="server" id="td_td2">
                        <asp:DropDownList ID="ddlType4" CssClass="dd_chk_select" runat="server" Width="120px" >
                        </asp:DropDownList>
                    </td>
                      <td align="left"  width="150px" >
                          &nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s" 
                            runat="server"  Text="数据查询" Width="60px" /> &nbsp;
                        <asp:Button runat="server"  OnClick="btnOutput_Click" CssClass="btn_s" Width="60px"   ID="btnOutput" Text="数据导出" />
                        </td>
              
                </tr>
              
            </tbody>
        </table>
    </fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">

    <div id="dataMian">
        <div id="content" runat="server" style="text-align:center">
            
        </div>
   
        <div style="display: none;" runat="server" id="viewstatesql">
        </div>
            <asp:HiddenField ID="hfMinDate" runat="server" />
            <asp:HiddenField ID="hfMaxDate" runat="server" />
            <asp:HiddenField ID="hfstorecode" runat="server" />
            <asp:HiddenField ID="hfdateid" runat="server" />
            <asp:HiddenField ID="hfKey" runat="server" />
            <asp:HiddenField ID="hfRound" runat="server" />
    </div>
</asp:Content>

