﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;
namespace JSKWeb.OTCREPORT
{
    public partial class GSK_CLM : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();
        public static string ConnStrEagles = ConfigurationManager.ConnectionStrings["DTTEST"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            ddlType2.DataSource = null;

            
            select_sql = " SELECT DISTINCT  col25 CODE,COL26 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col25 IS NOT NULL ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();

        }

        /// <summary>
        ///  Author: Dr
        ///  修改数据绑定（权限）
        ///  2016-4-26
        /// </summary>
        protected void LoadData()
        {
            string className = string.Empty;  //css
            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }


                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb = new StringBuilder();

                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_sql.Append(" select 产品名称, SUM(cast(陈列面 as decimal(18,2))) / COUNT(*) 陈列面");
                    sb_sql.Append(" from v_sys_otc_data where 品牌 <> '竞品'   and 陈列面 <> ''  ");
                    if (ddlType2.SelectedIndex != 0)
                    {
                        sb_sql.Append(" and col26 = '" + ddlType2.SelectedItem.Text + "'");
                    }
                    sb_sql.Append(" group by 产品名称  ");
                }
                
                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_sql.ToString(), con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt; width: 100%;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    order_str = dr.GetName(0);
                    sb.Append("<th align='left' style='width:200px;' scope='col'>" + dr.GetName(i) + "</th>");
                }
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer'>");
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        if (i == 1)
                        {
                            double a = double.Parse(dr[i].ToString());
                            sb.Append("<td class='across'>" + a.ToString("f2") + "</td>");
                        }
                        else
                        {
                            sb.Append("<td class='across'>" + dr[i].ToString() + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 导出数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnOutput_Click(object sender, EventArgs e)
        {

            ViewState["whereName"] = null;

            if (ViewState["whereName"] == null)
            {
                ViewState["whereName"] = "1=1 ";
            }

            if (ddlDim.SelectedIndex > 0)
            {
                ViewState["whereName"] += " and Ticketstaus ='" + ddlDim.SelectedItem.Text.Trim() + "' ";
            }

            StringBuilder sb_sql = new StringBuilder();
            StringBuilder sb = new StringBuilder();

            if (ddlDim.SelectedIndex == 0)  //全国
            {
                sb_sql.Append(" select 产品名称, SUM(cast(陈列面 as decimal(18,2))) / COUNT(*) 陈列面");
                sb_sql.Append(" from v_sys_otc_data where 品牌 <> '竞品'   and 陈列面 <> ''  ");
                if (ddlType2.SelectedIndex != 0)
                {
                    sb_sql.Append(" and col26 = '" + ddlType2.SelectedItem.Text + "'");
                }
                sb_sql.Append(" group by 产品名称  ");
            }
            

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }

        protected void ddlDim_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDim.SelectedValue.ToString().Trim() == "0" || ddlDim.SelectedValue.ToString().Trim() == "1" || ddlDim.SelectedValue.ToString().Trim() == "2")
            {
                td_qd1.Visible = false;
                td_qd2.Visible = false;
            }
            ddlType2.SelectedIndex = 0;

        }

    }
}