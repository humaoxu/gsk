﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Collections;

namespace JSKWeb.OTCREPORT
{
    public partial class OTC_CITY : PageBase
    {
        protected String TopFiveJsonContent = "[]";
        protected String RestResultJsonContent = "[]";
        protected Double DisplayCoefficient = 10;

        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");
                var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
                var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
                ddlTicketType.DataSource = ddlTicketTypeDataSet;
                ddlTicketType.DataValueField = "paperId";
                ddlTicketType.DataTextField = "paperTitle";
                ddlTicketType.DataBind();

                var ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level in (1) ";
                var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
                qdDownCheckBoxes.DataSource = ddlTypeDataSet;
                qdDownCheckBoxes.DataValueField = "dimcode";
                qdDownCheckBoxes.DataTextField = "dimname";
                qdDownCheckBoxes.DataBind();

                // 获取区域
                ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level = 2 order by dimname";
                ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
                qyDownCheckBoxes.DataSource = ddlTypeDataSet;
                qyDownCheckBoxes.DataValueField = "dimcode";
                qyDownCheckBoxes.DataTextField = "dimname";
                qyDownCheckBoxes.DataBind();

                LoadData();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void LoadData()
        {
            ViewState["whereName"] = null;

            if (ViewState["whereName"] == null)
            {
                ViewState["whereName"] = "1=1 ";
            }

            
            var parameter = String.Empty;      
            switch (this.ddlType2.SelectedIndex)
            {
                case 0:
                    parameter = 
                        "(CASE WHEN SYS_OTC_RESULT.t_score IS NULL " +
                            "THEN 0.00 " +
                            "ELSE SYS_OTC_RESULT.t_score " +
                        "END)";
                    break;
                case 1:
                    parameter = "(CASE WHEN (SYS_OTC_RESULT.STPF) is null then 0.00 else (SYS_OTC_RESULT.STPF) end ) ";
                    break;
                case 2:
                    parameter = "(CASE WHEN SYS_OTC_RESULT.CLPF IS NULL " +
                      "THEN 0.00 " +
                      "ELSE SYS_OTC_RESULT.CLPF " +
                  "END)";
                    break;
                case 3:
                    parameter = "(CASE WHEN SYS_OTC_RESULT.POSM IS NULL " +
                         "THEN 0.00 " +
                         "ELSE SYS_OTC_RESULT.POSM " +
                     "END)";
                    break;
                case 4:
                    parameter = "(CASE WHEN SYS_OTC_RESULT.JG IS NULL " +
                            "THEN 0.00 " +
                            "ELSE SYS_OTC_RESULT.JG " +
                        "END)";
                    break;
                case 5:
                    parameter = "(CASE WHEN SYS_OTC_RESULT.TJ IS NULL " +
                            "THEN 0.00 " +
                            "ELSE SYS_OTC_RESULT.TJ " +
                        "END)";
                    break;
            }

            ArrayList plsQD = new ArrayList();  //渠道
            ArrayList plsQY = new ArrayList();  //区域

            string sql = " 1=1 ";
            string strqd = "";
            bool bflg = true;
            for (int i= 0; i < qdDownCheckBoxes.Items.Count;i++)
            {
                if (qdDownCheckBoxes.Items[i].Selected)
                {
                    if (qdDownCheckBoxes.Items[i].Text == "全国")
                    {
                        break;
                    }
                    if (bflg)
                    {
                        bflg = false;
                        strqd = strqd + " '" + qdDownCheckBoxes.Items[i].Text + "' ";
                    }
                    else
                    {

                        strqd = strqd + " ,'" + qdDownCheckBoxes.Items[i].Text + "' ";
                    }
                }
            }

            string strdq = "";
            bflg = true;
            for (int i = 0; i < qyDownCheckBoxes.Items.Count; i++)
            {
                if (qyDownCheckBoxes.Items[i].Selected)
                {
                    if (bflg)
                    {
                        bflg = false;
                        strdq = strdq + " '" + qyDownCheckBoxes.Items[i].Text + "' ";
                    }
                    else
                    {

                        strdq = strdq + " ,'" + qyDownCheckBoxes.Items[i].Text + "' ";
                    }
                }
            }


            string formatedSql =
                            " " +
                            "SELECT [CityLocation].*, [ResultTable].* " +
                            "FROM [CityLocation] " +
                            "INNER JOIN (" +
                                "SELECT " +
                                    "[SurveyObj].[city] AS [City], " +
                                    "CAST(AVG(" + parameter + ") AS DECIMAL(10, 2)) AS [Score] " +
                                "FROM [SYS_OTC_RESULT] " +
                                    "INNER JOIN [SurveyObj] ON [SYS_OTC_RESULT].[shopid] = [SurveyObj].[surveyObjId] " +
                                "WHERE [paperid] = '" + ddlTicketType.SelectedItem.Value + "' AND [SurveyObj].[projectId] = 11 ";

            if (strqd != "")
            {
                formatedSql = formatedSql + " and [SurveyObj].[col26] in (" + strqd+") ";
            }

            if (strdq != "")
            {
                formatedSql = formatedSql + " and [SurveyObj].[col28] in (" + strdq + ") ";
            }
            formatedSql = formatedSql +
                                " GROUP BY [SurveyObj].[city] " +
                            ") AS [ResultTable] ON [ResultTable].[City] = [CityLocation].[City] " +
                            "ORDER BY [Score] DESC ";

            //生成Dataset
            var resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, formatedSql);

        if (resultDataSet.Tables.Count > 0)
            {
                var resultList = resultDataSet.Tables[0]
                    .AsEnumerable()
                    .Select(r => new
                    {
                        City = r.Field<String>("City"),
                        Average = (Double)r.Field<Decimal>("Score"),
                        Lng = r.Field<Double>("Lng"),
                        Lat = r.Field<Double>("Lat")
                    }).OrderByDescending(r => r.Average).ToArray();
                int iLen = resultList.Length;
                if (iLen > 5)
                {
                    iLen = 5;
                }
                var topFiveList = resultList.Take(iLen).ToArray();
                var restList = resultList.Skip(iLen).ToArray();

                try
                {
                    this.DisplayCoefficient = 10.0 / restList.Average(l => l.Average);
                    if (this.DisplayCoefficient > 15.0)
                    {
                        this.DisplayCoefficient = 15.0;
                    }
                }
                catch
                {
                    var restList_short = resultList.Take(iLen).ToArray();
                    this.DisplayCoefficient = 10.0 / restList_short.Average(l => l.Average);
                    if (this.DisplayCoefficient > 15.0)
                    {
                        this.DisplayCoefficient = 15.0;
                    }
                }
                
                try
                {
                    this.TopFiveJsonContent =
                        (new JavaScriptSerializer()).Serialize(
                            topFiveList.Select(l => new { name = l.City, value = new[] { l.Lng, l.Lat, l.Average } }));
                }
                catch
                {
                    this.TopFiveJsonContent = "";
                }
                try
                {
                    this.RestResultJsonContent =
                        (new JavaScriptSerializer()).Serialize(
                            restList.Select(l => new { name = l.City, value = new[] { l.Lng, l.Lat, l.Average } }));
                }
                catch
                {
                    this.RestResultJsonContent = "";
                }

            }
        }

        protected void btnOutput_Click(object sender, EventArgs e)
        {

        }


        protected void qdDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qyDownCheckBoxes.Items.Clear();
            string str = "";

            bool flag = true;
            for (int i = 0; i < qdDownCheckBoxes.Items.Count;i++)
            {
                if (qdDownCheckBoxes.Items[i].Selected)
                {
                    if (flag)
                    {
                        flag = false;
                        str = str + "'"+ qdDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {

                        str = str + ",'" + qdDownCheckBoxes.Items[i].Text + "'";
                    }
                }
            }

            var ddlTypeSelectSql = "";
            if (str == "" || str.Contains("全国"))
            {

                ddlTypeSelectSql = "select distinct col27,col28 from SurveyObj where projectid = 11 and col26 = '经销网'";
            }
            else
            {

                ddlTypeSelectSql = "select distinct col27,col28 from SurveyObj where projectid = 11 and col26 in ("+str+")";
            }
            DataSet ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "col27";
            qyDownCheckBoxes.DataTextField = "col28";
            qyDownCheckBoxes.DataBind();

            LoadData();
        }

        protected void dqDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            LoadData();

        }

    }
}