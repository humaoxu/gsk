﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/Top.Master" CodeBehind="GSK_CLM.aspx.cs" Inherits="JSKWeb.OTCREPORT.GSK_CLM" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                document.getElementById("<%=hfstorecode.ClientID %>").value = akey;
                document.getElementById("<%=hfdateid.ClientID %>").value = $(row.cells[2]).text();
                document.getElementById("<%=hfRound.ClientID %>").value = $(row.cells[1]).text();
                // row.cells[0].innerHTML = "<input type=\"radio\" value=\"\" name=\"rd\" checked='checked' />";
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {
                var json = {
                    "oo": [
                    { "url": "Application_Add.aspx", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                    { "url": "Application_Edit.aspx?ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                    {},
                    { "url": "Logic_Rule.aspx?TYPE=Default&ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" }
                    ]

                };

                if (index == 0) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 1) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 2) {
                    return confirm("Confirm delete？");
                } else if (index == 3) {
                    return confirm("Confirm Run？");
                } else if (index == 5) {//Set out
                    var result = window.open(json.oo[4].url + key, window, json.oo[4].parm);

                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                else {
                    return false;
                }
            }
        }

    </script>
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0"  class="">
            <tbody>
                <tr>
                    <td align="right" width="75px" class="contrlFontSize_s">
                        问卷期数：
                    </td>
                    <td align="left" width="80px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="80px">
                        </asp:DropDownList>
                    </td>
                    
                    <td align="right" width="80px" class="contrlFontSize_s">
                        分析维度：
                    </td>
                    <td align="left"  width="100px" >
                        <asp:DropDownList ID="ddlDim" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlDim_SelectedIndexChanged">
                            <asp:ListItem Value="1">大区</asp:ListItem>
                        </asp:DropDownList>
                      
                    </td>
                    <td align="right" width="50px" class="contrlFontSize_s" runat="server" id="td_qd1">
                        渠道：
                    </td>
                    <td align="left"  runat="server" id="td_qd2" >
                        <asp:DropDownList ID="ddlType2" CssClass="inputtext15" runat="server" Width="80px">
                        </asp:DropDownList>
                    </td>
                      <td align="left"  width="150px" >
                          &nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s" 
                            runat="server"  Text="查询" Width="60px" /> &nbsp;
                        <asp:Button runat="server"  OnClick="btnOutput_Click" CssClass="btn_s" Width="60px"   ID="btnOutput" Text="导出" /> </td>
              
                </tr>
              
            </tbody>
        </table>
    </fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">

    <div id="dataMian">
        <div id="content" runat="server" style="text-align:center">
            
        </div>
   
        <div style="display: none;" runat="server" id="viewstatesql">
        </div>
            <asp:HiddenField ID="hfMinDate" runat="server" />
            <asp:HiddenField ID="hfMaxDate" runat="server" />
            <asp:HiddenField ID="hfstorecode" runat="server" />
            <asp:HiddenField ID="hfdateid" runat="server" />
            <asp:HiddenField ID="hfKey" runat="server" />
            <asp:HiddenField ID="hfRound" runat="server" />
    </div>
</asp:Content>