﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using DBUtility;

namespace JSKWeb.Chart
{
    public partial class MapChart : System.Web.UI.Page
    {
        public String TopFiveJsonContent = "[]";
        public String RestResultJsonContent = "[]";



        protected void Page_Load(object sender, EventArgs e)
        {
            var sqlHelper = new SqlHelper();
            var sqlText = "SELECT [CityLocation].*, AVG([V_OTC_RESULT].[STPF]) AS [Average] " +
                          "FROM [CityLocation] " +
                          "     INNER JOIN [SurveyObj] ON [CityLocation].[City] = [SurveyObj].[city] " +
                          "     INNER JOIN [V_OTC_RESULT] ON [SurveyObj].[surveyObjId] = [V_OTC_RESULT].[shopid] " +
                          "GROUP BY [CityLocation].[City], [CityLocation].[Lng], [CityLocation].[Lat]";
            var resultDataset = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlText);
            if (resultDataset.Tables.Count > 0)
            {
                var resultList = resultDataset.Tables[0]
                    .AsEnumerable()
                    .Select(r => new
                    {
                        City = r.Field<String>("City"),
                        Average = (Double)r.Field<Decimal>("Average"),
                        Lng = r.Field<Double>("Lng"),
                        Lat = r.Field<Double>("Lat")
                    }).OrderByDescending(r => r.Average).ToArray();
                var topFiveList = resultList.Take(5).ToArray();
                var restList = resultList.Skip(5).ToArray();
                this.TopFiveJsonContent =
                    (new JavaScriptSerializer()).Serialize(
                        topFiveList.Select(l => new {name = l.City, value = new[] {l.Lng, l.Lat, l.Average}}));
                this.RestResultJsonContent =
                    (new JavaScriptSerializer()).Serialize(
                        restList.Select(l => new { name = l.City, value = new[] { l.Lng, l.Lat, l.Average } }));
            }
        }
    }
}