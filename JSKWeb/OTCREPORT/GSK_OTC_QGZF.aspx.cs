﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;

namespace JSKWeb.OTCREPORT
{
    public partial class GSK_OTC_QGZF : PageBase
    {

        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");
                var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
                var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
                ddlTicketType.DataSource = ddlTicketTypeDataSet;
                ddlTicketType.DataValueField = "paperId";
                ddlTicketType.DataTextField = "paperTitle";
                ddlTicketType.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void LoadData()
        {
            string className = string.Empty;  //css
            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }
                

                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb = new StringBuilder();

                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_sql.Append(" select CAST(avg((case when SYS_OTC_RESULT.t_score is null then 0.00 else SYS_OTC_RESULT.t_score end)) as decimal(10, 2)) '总分'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid = surveyobj.surveyobjid ");
                    sb_sql.Append(" where paperid  = '" + ddlTicketType.SelectedItem.Value.ToString() + "'  and surveyobj.projectId = 11 ");
                }

                if (ddlDim.SelectedIndex == 1)  //全国
                {
                    sb_sql.Append(" select CAST(avg((case when SYS_OTC_RESULT.t_score is null then 0.00 else SYS_OTC_RESULT.t_score end)) as decimal(10, 2)) '总分'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid = surveyobj.surveyobjid ");
                    sb_sql.Append(" where paperid  = '" + ddlTicketType.SelectedItem.Value.ToString() + "'  and surveyobj.projectId = 11 and surveyobj.col26 = '经销网' ");
                }


                if (ddlDim.SelectedIndex == 2)  //全国
                {
                    sb_sql.Append(" select CAST(avg((case when SYS_OTC_RESULT.t_score is null then 0.00 else SYS_OTC_RESULT.t_score end)) as decimal(10, 2)) '总分'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid = surveyobj.surveyobjid ");
                    sb_sql.Append(" where paperid  = '" + ddlTicketType.SelectedItem.Value.ToString() + "'  and surveyobj.projectId = 11 and surveyobj.col26 = '重点零售' ");
                }


                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
                
                //动态定义样式
                sb.Append("<table class='tablestyle' cellspacing='0' cellpadding='2' style='font-size: 9pt; width: 100%;border-collapse: collapse;'><tbody>");
                //sb.Append("<tr class='header'>");
                //string order_str = string.Empty;
                //for (int i = 0; i < dr.FieldCount; i++)
                //{
                //    order_str = dr.GetName(0);
                //    sb.Append("<th align='center' style='width:200px;' scope='col'>" + dr.GetName(i) + "</th>");
                //}
                //sb.Append("</tr>");

                sb.Append("<tr style='cursor:pointer'>");
                sb.Append("<td class='across'>问卷期数</td>" );
                sb.Append("<td class='across'>"+ddlTicketType.SelectedItem.Text.Trim()+"</td>");
                sb.Append("</tr>");


                sb.Append("<tr style='cursor:pointer'>");
                sb.Append("<td class='across'>渠道类型</td>");
                sb.Append("<td class='across'>" + ddlDim.SelectedItem.Text.Trim() + "</td>");
                sb.Append("</tr>");

                sb.Append("<tr style='cursor:pointer'>");
                sb.Append("<td class='across'>总分</td>");
                sb.Append("<td class='across'>" + dateData.Tables[0].Rows[0][0].ToString().Trim() + "</td>");
                sb.Append("</tr>");


                sb.Append("</tbody></table>");
                content.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }

        protected void btnOutput_Click(object sender, EventArgs e)
        {

            string className = string.Empty;  //css
            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }


                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb = new StringBuilder();

                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_sql.Append(" select CAST(avg((case when SYS_OTC_RESULT.t_score is null then 0.00 else SYS_OTC_RESULT.t_score end)) as decimal(10, 2)) '总分'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid = surveyobj.surveyobjid ");
                    sb_sql.Append(" where paperid  = '" + ddlTicketType.SelectedItem.Value.ToString() + "'  and surveyobj.projectId = 11 ");
                }

                if (ddlDim.SelectedIndex == 1)  //全国
                {
                    sb_sql.Append(" select CAST(avg((case when SYS_OTC_RESULT.t_score is null then 0.00 else SYS_OTC_RESULT.t_score end)) as decimal(10, 2)) '总分'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid = surveyobj.surveyobjid ");
                    sb_sql.Append(" where paperid  = '" + ddlTicketType.SelectedItem.Value.ToString() + "'  and surveyobj.projectId = 11 and surveyobj.col26 = '经销网' ");
                }


                if (ddlDim.SelectedIndex == 2)  //全国
                {
                    sb_sql.Append(" select CAST(avg((case when SYS_OTC_RESULT.t_score is null then 0.00 else SYS_OTC_RESULT.t_score end)) as decimal(10, 2)) '总分'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid = surveyobj.surveyobjid ");
                    sb_sql.Append(" where paperid  = '" + ddlTicketType.SelectedItem.Value.ToString() + "'  and surveyobj.projectId = 11 and surveyobj.col26 = '重点零售' ");
                }


                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());

            
                DataTable dt = new DataTable("AllVehSite");
                DataColumnCollection columns = dt.Columns;
                columns.Add("col1", typeof(System.String));
                columns.Add("col2", typeof(System.String));
                
                DataRow datarow = dt.NewRow();
                datarow["col1"] = "问卷期数";
                datarow["col2"] = ddlTicketType.SelectedItem.Text.Trim();
                dt.Rows.Add(datarow);


                DataRow datarow1 = dt.NewRow();
                datarow1["col1"] = "渠道类型";
                datarow1["col2"] = ddlDim.SelectedItem.Text.Trim();
                dt.Rows.Add(datarow1);

                DataRow datarow2 = dt.NewRow();
                datarow2["col1"] = "总分";
                datarow2["col2"] = dateData.Tables[0].Rows[0][0].ToString().Trim();
                dt.Rows.Add(datarow2);



                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcelWithNoHeader(dt, "");
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }
    }
}