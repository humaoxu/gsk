﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;

namespace JSKWeb.OTCREPORT
{
    public partial class OTC_SCORERPT : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();
        public static string ConnStrEagles = ConfigurationManager.ConnectionStrings["DTTEST"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ViewState["OTCUSERID"] = this._GetPowerID().ToUpper().Trim();

                TicketTypeDataBind();


                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }
        }

        public void TicketTypeDataBind()
        {

            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType2.Items.Clear();
            ddlType3.Items.Clear();
            ddlType4.Items.Clear();


            select_sql = " SELECT DISTINCT  col25 CODE,COL26 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col25 IS NOT NULL ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }

            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlType1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(ViewState["OTCUSERID"].ToString().ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  col27 CODE,COL28 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col27 IS NOT NULL ";

                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();
                    }
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.col27 CODE,SURVEYOBJ.COL28 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + ViewState["OTCUSERID"].ToString() + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.col27 IS NOT NULL ";

                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.col25 = '" + ddlType2.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType3.DataSource = select_ds;
                        ddlType3.DataValueField = "code";
                        ddlType3.DataTextField = "name";
                        ddlType3.DataBind();
                    }
                }
            }

            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;
            ddlType4.Items.Clear();
            DataSet select_ds = new DataSet();
            var select_sql = "";

            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(ViewState["OTCUSERID"].ToString().ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  col19 CODE,COL20 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col19 IS NOT NULL ";
                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' and col27 = '" + ddlType3.SelectedValue + "' ";

                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType4.DataSource = select_ds;
                        ddlType4.DataValueField = "code";
                        ddlType4.DataTextField = "name";
                        ddlType4.DataBind();

                    }
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.col19 CODE,SURVEYOBJ.COL20 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + ViewState["OTCUSERID"].ToString() + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL21 IS NOT NULL ";
                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "   and SURVEYOBJ.col25 = '" + ddlType2.SelectedValue + "'  and SURVEYOBJ.col27 = '" + ddlType3.SelectedValue + "' ";
                    select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                    if (select_ds != null)
                    {
                        ddlType4.DataSource = select_ds;
                        ddlType4.DataValueField = "code";
                        ddlType4.DataTextField = "name";
                        ddlType4.DataBind();

                    }
                }
            }

            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //LoadInfo();

            //author:dr 2016-4-26
            LoadData();

        }


        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadData();
            //LoadInfo();
        }


        /// <summary>
        ///  Author: Dr
        ///  修改数据绑定（权限）
        ///  2016-4-26
        /// </summary>
        protected void LoadData()
        {
            string className = string.Empty;  //css
            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }
                
                
                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb= new StringBuilder();

                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)' , ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");

                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col26 ");
                }
                else if (ddlDim.SelectedIndex == 1) //大区
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col31 as '大区经理',avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)' , ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");
                    if (ddlType2.SelectedIndex == 0)
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31 ");
                    else
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31 ");

                }
                else if (ddlDim.SelectedIndex == 2)  //团队
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',surveyobj.col34 as 'REPCODE',surveyobj.col33 as '团队负责人',");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)'  ,  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)'  from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   ");
                        }
                        else
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33  ");
                        }
                    }
                }
                else if (ddlDim.SelectedIndex == 3 )  //代表
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',");
                    sb_sql.Append(" surveyobj.col34 '上级岗位REPCODE',surveyobj.col33 '上级主管',surveyobj.col17 '本级岗位REPCODE',surveyobj.col18 as '销售代表', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)' ,  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)' from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");

                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col18  ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col18 ");
                        }
                        else
                        {
                            if (ddlType4.SelectedIndex == 0)
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col183 ");
                            }
                            else
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col20='" + ddlType4.SelectedItem.Text + "' and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col18 ");
                            }
                        }
                    }
                }
                else if(ddlDim.SelectedIndex == 4)  //城市
                {
                    sb_sql.Append(" select surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',surveyobj.col3 as '地级市', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)' ,  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)'  from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");

                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col28,surveyobj.col20,surveyobj.col3  ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'   group by surveyobj.col28,surveyobj.col20,surveyobj.col3  ");
                        }
                        else
                        {
                            if (ddlType4.SelectedIndex == 0)
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col28,surveyobj.col20,surveyobj.col3 ");
                            }
                            else
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col20='" + ddlType4.SelectedItem.Text + "' and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col28,surveyobj.col20,surveyobj.col3 ");
                            }
                        }
                    }
                }
                
                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_sql.ToString(), con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle' cellspacing='0' cellpadding='2' style='font-size: 9pt; width: 100%;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    order_str = dr.GetName(0);
                    sb.Append("<th align='center' style='width:200px;' scope='col'>" + dr.GetName(i) + "</th>");
                }
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer'>");
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        try
                        {
                            double dtemp = Double.Parse(dr[i].ToString());
                            sb.Append("<td class='across'>" + dtemp.ToString("0.###") + "</td>");
                        }
                        catch
                        {
                            sb.Append("<td class='across'>" + dr[i].ToString() + "</td>");
                        }
                        
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 计算Datat列合值
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        private double DataTableColumnSum(DataTable dt, string columnName)
        {
            double d = 0;
            foreach (DataRow dr in dt.Rows)
            {
                d += double.Parse(dr[columnName].ToString());
            }

            return d;
        }
        


        /// <summary>
        /// 导出数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnOutput_Click(object sender, EventArgs e)
        {

            ViewState["whereName"] = null;

            if (ViewState["whereName"] == null)
            {
                ViewState["whereName"] = "1=1 ";
            }

            if (ddlDim.SelectedIndex > 0)
            {
                ViewState["whereName"] += " and Ticketstaus ='" + ddlDim.SelectedItem.Text.Trim() + "' ";
            }


            StringBuilder sb_sql = new StringBuilder();
            StringBuilder sb = new StringBuilder();

            if (ddlDim.SelectedIndex == 0)  //全国
            {
                sb_sql.Append(" select surveyobj.col26 as '渠道',avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)'  , ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)', ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");

                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col26 ");
            }
            else if (ddlDim.SelectedIndex == 1) //大区
            {
                sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col31 as '大区经理',avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)' , ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)', ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");

                if (ddlType2.SelectedIndex == 0)
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31 ");
                else
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31 ");

            }
            else if (ddlDim.SelectedIndex == 2)  //团队
            {
                sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',surveyobj.col34 as 'REPCODE',surveyobj.col33 as '团队负责人',");
                sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)' ,  ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)'  from SYS_OTC_RESULT ");
                sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                if (ddlType2.SelectedIndex == 0)
                {
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   ");
                }
                else
                {
                    if (ddlType3.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   ");
                    }
                    else
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33  ");
                    }
                }
            }
            else if (ddlDim.SelectedIndex == 3)  //代表
            {
                sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',");
                sb_sql.Append(" surveyobj.col34 '上级岗位REPCODE',surveyobj.col33 '上级主管',surveyobj.col17 '本级岗位REPCODE',surveyobj.col18 as '销售代表', ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)'  ,  ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',  ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)'  from SYS_OTC_RESULT ");
                sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");

                if (ddlType2.SelectedIndex == 0)
                {
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col18  ");
                }
                else
                {
                    if (ddlType3.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col18 ");
                    }
                    else
                    {
                        if (ddlType4.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col183 ");
                        }
                        else
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col20='" + ddlType4.SelectedItem.Text + "' and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33,surveyobj.col17,surveyobj.col18 ");
                        }
                    }
                }
            }
            else if (ddlDim.SelectedIndex == 4)  //城市
            {
                sb_sql.Append(" select surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',surveyobj.col3 as '地级市', ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF)) as 'AVG(分销评分含分销附加分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(分销附加分)' ,  ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分含POSM附加分)',avg(SYS_OTC_RESULT.POSMFJ)  as 'AVG(POSM附加分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)'  from SYS_OTC_RESULT ");
                sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");

                if (ddlType2.SelectedIndex == 0)
                {
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col28,surveyobj.col20,surveyobj.col3  ");
                }
                else
                {
                    if (ddlType3.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'   group by surveyobj.col28,surveyobj.col20,surveyobj.col3  ");
                    }
                    else
                    {
                        if (ddlType4.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col28,surveyobj.col20,surveyobj.col3 ");
                        }
                        else
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col20='" + ddlType4.SelectedItem.Text + "' and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col28,surveyobj.col20,surveyobj.col3 ");
                        }
                    }
                }
            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }

        
        protected void ddlDim_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDim.SelectedValue.ToString().Trim() == "0" || ddlDim.SelectedValue.ToString().Trim() == "1" || ddlDim.SelectedValue.ToString().Trim() == "2")
            {
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }

            if (ddlDim.SelectedValue.ToString().Trim() == "3")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "4")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }

            if (ddlDim.SelectedValue.ToString().Trim() == "5" || ddlDim.SelectedValue.ToString().Trim() == "6")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = true;
                td_td2.Visible = true;
            }

            ddlType2.SelectedIndex = 0;
            ddlType3.SelectedIndex = 0;
            ddlType4.SelectedIndex = 0;

        }
    }
}