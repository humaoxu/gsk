﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;

namespace JSKWeb.OTCREPORT
{
    public partial class GSK_OTC_QGFX : PageBase
    {

        public string lengenddata = "";

        public string stpf = "";
        public string clpf = "";
        public string posmpf = "";
        public string jgpm = "";
        public string tjpm = "";
        public int chart_h = 0;

        
        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");
                //var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
                //var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
                //ddlTicketType.DataSource = ddlTicketTypeDataSet;
                //ddlTicketType.DataValueField = "paperId";
                //ddlTicketType.DataTextField = "paperTitle";
                //ddlTicketType.DataBind();

                TicketTypeDataBind();
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }
            //LoadChart();
        }

        public void TicketTypeDataBind()
        {

            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType2.Items.Clear();
            ddlType3.Items.Clear();
            ddlType4.Items.Clear();


            select_sql = " SELECT COL25 CODE,COL26 NAME FROM ( SELECT COL25,COL26, ROW_NUMBER () OVER (PARTITION BY COL26 ORDER BY COL26) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL25 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }

            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlType1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";

            select_sql = "SELECT DISTINCT  col27 CODE,COL28 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col27 IS NOT NULL ";

            if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
            {
                select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' ";

                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType3.DataSource = select_ds;
                    ddlType3.DataValueField = "code";
                    ddlType3.DataTextField = "name";
                    ddlType3.DataBind();
                }
            }
            


            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;
            ddlType4.Items.Clear();
            DataSet select_ds = new DataSet();
            var select_sql = "";


            select_sql = "SELECT DISTINCT  col19 CODE,COL20 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col19 IS NOT NULL ";
            if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
            {
                select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' and col27 = '" + ddlType3.SelectedValue + "' ";

                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType4.DataSource = select_ds;
                    ddlType4.DataValueField = "code";
                    ddlType4.DataTextField = "name";
                    ddlType4.DataBind();

                }
            }
            


            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //数据
            //this.chart.Visible = false;
            this.content.Visible = true;
            LoadData();
        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            //图表
            //this.chart.Visible = true;
            this.content.Visible = false;
            LoadChart();
        }

        protected void LoadData()
        {
            string className = string.Empty;  //css
            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }


                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb = new StringBuilder();

                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',CAST(avg(SYS_OTC_RESULT.t_score) as decimal(10, 2))  'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' , ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as   'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM) as    'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as  'AVG(价格评分)', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ) as  'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as  'AVG(渗透附加分)' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col26   order by 'AVG(总分)' desc  ");
                }
                else if (ddlDim.SelectedIndex == 1) //大区
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col31 as '大区经理',avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' , ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(渗透附加分)' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");

                    if (ddlType2.SelectedIndex == 0)
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31   order by 'AVG(总分)' desc  ");
                    else
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31   order by 'AVG(总分)' desc  ");
                }
                else if (ddlDim.SelectedIndex == 2)  //团队
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',surveyobj.col34 as 'REPCODE',surveyobj.col33 as '团队负责人',");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' ,  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(渗透附加分)' from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33    order by 'AVG(总分)' desc  ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   order by 'AVG(总分)' desc   ");
                        }
                        else
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   order by 'AVG(总分)' desc  ");
                        }
                    }
                }
                else if (ddlDim.SelectedIndex == 3)  //城市
                {
                    sb_sql.Append(" select surveyobj.col3 as '地级市', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' ,  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(渗透附加分)' from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'   group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                        }
                        else
                        {
                            if (ddlType4.SelectedIndex == 0)
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                            }
                            else
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col20='" + ddlType4.SelectedItem.Text + "' and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                            }
                        }
                    }
                }

                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_sql.ToString(), con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle' cellspacing='0' cellpadding='2' style='font-size: 9pt; width: 100%;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    order_str = dr.GetName(0);
                    sb.Append("<th align='center' style='width:200px;' scope='col'>" + dr.GetName(i) + "</th>");
                }
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer'>");
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        sb.Append("<td class='across'>" + dr[i].ToString() + "</td>");
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 生成图表
        /// 2016.6.2
        /// D.r
        /// </summary>
        protected void LoadChart()
        {
            string className = string.Empty;  //css
            try
            {
                var parameter = String.Empty;
                //var formatedSql = "";
                lengenddata = "";
                stpf = "";
                clpf = "";
                posmpf = "";
                jgpm = "";
                tjpm = "";

                //switch (this.ddlDim.SelectedIndex)
                //{
                //    case 0:
                //        parameter = " and surveyobj.col26 = '经销网' group by surveyobj.col28 ";
                        
                //        break;
                //    case 1:
                //        parameter = " and surveyobj.col26 = '重点零售'  group by surveyobj.col28 ";
                        
                //        break;
                //}



                


                //formatedSql = String.Format(
                //               " " +
                //               " select surveyobj.col28 as seldim , CAST(avg(SYS_OTC_RESULT.STPF + SYS_OTC_RESULT.STFJ) as decimal(10, 2)) '渗透评分',CAST(avg(SYS_OTC_RESULT.CLPF) as decimal(10, 2)) '陈列评分',CAST(avg(SYS_OTC_RESULT.POSM) as decimal(10, 2)) 'POSM评分',CAST(avg(SYS_OTC_RESULT.JG) as decimal(10, 2)) '价格评分',CAST(avg(SYS_OTC_RESULT.TJ) as decimal(10, 2)) '推荐评分',CAST(avg(SYS_OTC_RESULT.t_score) as decimal(10, 2)) '总分'  from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid = surveyobj.surveyobjid " +
                //               " where paperid = {0}  and surveyobj.projectId = 11 " +
                //               " {1}  ",
                //                ddlTicketType.SelectedItem.Value.ToString(), parameter);

                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb = new StringBuilder();

                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_sql.Append(" select surveyobj.col26 as seldim ,avg(SYS_OTC_RESULT.t_score) as '总分',CAST((avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as decimal(10, 2)) '渗透评分' , ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.CLPF) as decimal(10, 2)) '陈列评分',CAST(avg(SYS_OTC_RESULT.POSM) as decimal(10, 2)) 'POSM评分',CAST(avg(SYS_OTC_RESULT.JG) as decimal(10, 2)) '价格评分', ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.TJ) as decimal(10, 2)) '推荐评分' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col26  order by '总分'  ");
                }
                else if (ddlDim.SelectedIndex == 1) //大区
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as seldim,surveyobj.col31 as '大区经理',avg(SYS_OTC_RESULT.t_score) as '总分',CAST((avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as decimal(10, 2)) '渗透评分' , ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.CLPF) as decimal(10, 2)) '陈列评分',CAST(avg(SYS_OTC_RESULT.POSM)  as decimal(10, 2)) 'POSM评分',CAST(avg(SYS_OTC_RESULT.JG)  as decimal(10, 2)) '价格评分', ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.TJ)   as decimal(10, 2)) '推荐评分' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");

                    if (ddlType2.SelectedIndex == 0)
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31   order by '总分'  ");
                    else
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31   order by '总分'  ");
                }
                else if (ddlDim.SelectedIndex == 2)  //团队
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col20 as seldim,surveyobj.col34 as 'REPCODE',surveyobj.col33 as '团队负责人',");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as '总分',CAST((avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as decimal(10, 2)) '渗透评分' ,  ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.CLPF) as decimal(10, 2)) '陈列评分',CAST(avg(SYS_OTC_RESULT.POSM)  as decimal(10, 2)) 'POSM评分',  CAST(avg(SYS_OTC_RESULT.JG)  as decimal(10, 2)) '价格评分',  ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.TJ)   as decimal(10, 2)) '推荐评分' from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   order by '总分'    ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   order by '总分'    ");
                        }
                        else
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   order by '总分'   ");
                        }
                    }
                }
                else if (ddlDim.SelectedIndex == 3)  //城市
                {
                    sb_sql.Append(" select surveyobj.col3 as seldim, ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as '总分',CAST((avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as decimal(10, 2))  '渗透评分' ,  ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.CLPF) as decimal(10, 2))  '陈列评分',CAST(avg(SYS_OTC_RESULT.POSM)  as decimal(10, 2))  'POSM评分',CAST(avg(SYS_OTC_RESULT.JG) as decimal(10, 2))  '价格评分',  ");
                    sb_sql.Append(" CAST(avg(SYS_OTC_RESULT.TJ) as decimal(10, 2))  '推荐评分' from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col3   order by '总分'  ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'   group by surveyobj.col3   order by '总分'   ");
                        }
                        else
                        {
                            if (ddlType4.SelectedIndex == 0)
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col3   order by '总分'   ");
                            }
                            else
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col20='" + ddlType4.SelectedItem.Text + "' and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col3   order by '总分'   ");
                            }
                        }
                    }
                }

                //生成Dataset
                DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    //如果数据大于50
                    if (resultDataSet.Tables[0].Rows.Count > 50)
                    {
                        chart_h = 200;
                        for (int i = 0; i < 50; i++)
                        {
                            if (i == 0)
                            {
                                lengenddata = lengenddata + "'" + resultDataSet.Tables[0].Rows[i]["seldim"].ToString().Trim() + "'";
                                stpf = stpf + "" + resultDataSet.Tables[0].Rows[i]["渗透评分"].ToString().Trim() + "";
                                clpf = clpf + "" + resultDataSet.Tables[0].Rows[i]["陈列评分"].ToString().Trim() + "";
                                posmpf = posmpf + "" + resultDataSet.Tables[0].Rows[i]["POSM评分"].ToString().Trim() + "";
                                jgpm = jgpm + "" + resultDataSet.Tables[0].Rows[i]["价格评分"].ToString().Trim() + "";
                                tjpm = tjpm + "" + resultDataSet.Tables[0].Rows[i]["推荐评分"].ToString().Trim() + "";
                            }
                            else
                            {
                                lengenddata = lengenddata + ",'" + resultDataSet.Tables[0].Rows[i]["seldim"].ToString().Trim() + "'";
                                stpf = stpf + "," + resultDataSet.Tables[0].Rows[i]["渗透评分"].ToString().Trim() + "";
                                clpf = clpf + "," + resultDataSet.Tables[0].Rows[i]["陈列评分"].ToString().Trim() + "";
                                posmpf = posmpf + "," + resultDataSet.Tables[0].Rows[i]["POSM评分"].ToString().Trim() + "";
                                jgpm = jgpm + "," + resultDataSet.Tables[0].Rows[i]["价格评分"].ToString().Trim() + "";
                                tjpm = tjpm + "," + resultDataSet.Tables[0].Rows[i]["推荐评分"].ToString().Trim() + "";
                            }

                        }
                    }
                    else
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                lengenddata = lengenddata + "'" + resultDataSet.Tables[0].Rows[i]["seldim"].ToString().Trim() + "'";
                                stpf = stpf + "" + resultDataSet.Tables[0].Rows[i]["渗透评分"].ToString().Trim() + "";
                                clpf = clpf + "" + resultDataSet.Tables[0].Rows[i]["陈列评分"].ToString().Trim() + "";
                                posmpf = posmpf + "" + resultDataSet.Tables[0].Rows[i]["POSM评分"].ToString().Trim() + "";
                                jgpm = jgpm + "" + resultDataSet.Tables[0].Rows[i]["价格评分"].ToString().Trim() + "";
                                tjpm = tjpm + "" + resultDataSet.Tables[0].Rows[i]["推荐评分"].ToString().Trim() + "";
                            }
                            else
                            {
                                lengenddata = lengenddata + ",'" + resultDataSet.Tables[0].Rows[i]["seldim"].ToString().Trim() + "'";
                                stpf = stpf + "," + resultDataSet.Tables[0].Rows[i]["渗透评分"].ToString().Trim() + "";
                                clpf = clpf + "," + resultDataSet.Tables[0].Rows[i]["陈列评分"].ToString().Trim() + "";
                                posmpf = posmpf + "," + resultDataSet.Tables[0].Rows[i]["POSM评分"].ToString().Trim() + "";
                                jgpm = jgpm + "," + resultDataSet.Tables[0].Rows[i]["价格评分"].ToString().Trim() + "";
                                tjpm = tjpm + "," + resultDataSet.Tables[0].Rows[i]["推荐评分"].ToString().Trim() + "";
                            }

                        }
                    }

                    lengenddata = "[" + lengenddata + "]";
                    stpf = "[" + stpf + "]";
                    clpf = "[" + clpf + "]";
                    posmpf = "[" + posmpf + "]";
                    jgpm = "[" + jgpm + "]";
                    tjpm = "[" + tjpm + "]";



                }
                else
                {
                    lengenddata = "[0]";
                    stpf = "[0]";
                    clpf = "[0]";
                    posmpf = "[0]";
                    jgpm = "[0]";
                    tjpm = "[0]";
                }




            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }

        protected void btnOutput_Click(object sender, EventArgs e)
        {

            string className = string.Empty;  //css
            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }


                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb = new StringBuilder();

                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',CAST(avg(SYS_OTC_RESULT.t_score) as decimal(10, 2))  'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' , ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as   'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM) as    'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as  'AVG(价格评分)', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ) as  'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as  'AVG(渗透附加分)' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");
                    sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col26   order by 'AVG(总分)' desc  ");
                }
                else if (ddlDim.SelectedIndex == 1) //大区
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col31 as '大区经理',avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' , ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(渗透附加分)' from SYS_OTC_RESULT inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid  ");

                    if (ddlType2.SelectedIndex == 0)
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31   order by 'AVG(总分)' desc  ");
                    else
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col31   order by 'AVG(总分)' desc  ");
                }
                else if (ddlDim.SelectedIndex == 2)  //团队
                {
                    sb_sql.Append(" select surveyobj.col26 as '渠道',surveyobj.col28 as '大区',surveyobj.col20 as '所属团队',surveyobj.col34 as 'REPCODE',surveyobj.col33 as '团队负责人',");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' ,  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(渗透附加分)' from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33    order by 'AVG(总分)' desc  ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   order by 'AVG(总分)' desc   ");
                        }
                        else
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col26 ,surveyobj.col28,surveyobj.col20,surveyobj.col34,surveyobj.col33   order by 'AVG(总分)' desc  ");
                        }
                    }
                }
                else if (ddlDim.SelectedIndex == 3)  //城市
                {
                    sb_sql.Append(" select surveyobj.col3 as '地级市', ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.t_score) as 'AVG(总分)',(avg(SYS_OTC_RESULT.STPF) + avg(SYS_OTC_RESULT.stfj)) as 'AVG(渗透评分+渗透附加分)' ,  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.CLPF) as 'AVG(陈列评分)',avg(SYS_OTC_RESULT.POSM)  as 'AVG(POSM评分)',avg(SYS_OTC_RESULT.JG)   as 'AVG(价格评分)',  ");
                    sb_sql.Append(" avg(SYS_OTC_RESULT.TJ)   as 'AVG(推荐评分)',avg(SYS_OTC_RESULT.STFJ) as 'AVG(渗透附加分)' from SYS_OTC_RESULT ");
                    sb_sql.Append(" inner join surveyobj on SYS_OTC_RESULT.shopid =surveyobj.surveyobjid ");
                    if (ddlType2.SelectedIndex == 0)
                    {
                        sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11  group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                    }
                    else
                    {
                        if (ddlType3.SelectedIndex == 0)
                        {
                            sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'   group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                        }
                        else
                        {
                            if (ddlType4.SelectedIndex == 0)
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                            }
                            else
                            {
                                sb_sql.Append(" where paperid = '" + ddlTicketType.SelectedItem.Value.ToString() + "' and surveyobj.projectId = 11 and surveyobj.col20='" + ddlType4.SelectedItem.Text + "' and surveyobj.col28='" + ddlType3.SelectedItem.Text + "' and surveyobj.col26='" + ddlType2.SelectedItem.Text + "'  group by surveyobj.col3   order by 'AVG(总分)' desc  ");
                            }
                        }
                    }
                }


                //生成Dataset
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    ExcelOutPut excelOutPut = new ExcelOutPut();
                    System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                    try
                    {
                        byte[] bt = ms.ToArray();
                        //以字符流的形式下载文件  
                        Response.ContentType = "application/vnd.ms-excel";
                        //通知浏览器下载文件而不是打开
                        Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                        Response.BinaryWrite(bt);

                        Response.Flush();
                        Response.End();
                        bt = null;
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    finally
                    {
                        if (ms != null) ms.Dispose();
                    }
                }
                else
                {
                    JavaScript.Alert(this, "无符合查询条件的数据。");
                }
            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }

        protected void ddlDim_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDim.SelectedValue.ToString().Trim() == "0" || ddlDim.SelectedValue.ToString().Trim() == "1" || ddlDim.SelectedValue.ToString().Trim() == "2")
            {
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }

            if (ddlDim.SelectedValue.ToString().Trim() == "3")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "4")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }

            if (ddlDim.SelectedValue.ToString().Trim() == "5" || ddlDim.SelectedValue.ToString().Trim() == "6")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = true;
                td_td2.Visible = true;
            }

            ddlType2.SelectedIndex = 0;
            ddlType3.SelectedIndex = 0;
            ddlType4.SelectedIndex = 0;

        }
    }
}