﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="GSK_OTC_QGFX.aspx.cs" Inherits="JSKWeb.OTCREPORT.GSK_OTC_QGFX" %>



<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

    <style type="text/css">
        .aline {
            text-decoration: underline;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function change()
        {
            //alert(1);
            //$("#rightMain_chart").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">

    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td align="right" width="75px" class="contrlFontSize_s">问卷期数：
                    </td>
                    <td align="left" width="80px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="80px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s">分析维度：
                    </td>
                    <td align="left" width="100px">
                        <asp:DropDownList ID="ddlDim" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlDim_SelectedIndexChanged">
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="3">大区</asp:ListItem>
                            <asp:ListItem Value="4">团队</asp:ListItem>
                            <asp:ListItem Value="6">城市</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="50px" class="contrlFontSize_s" runat="server" id="td_qd1">渠道：
                    </td>
                    <td align="left" runat="server" id="td_qd2">
                        <asp:DropDownList ID="ddlType2" CssClass="inputtext15" runat="server" Width="80px" AutoPostBack="true" OnSelectedIndexChanged="ddlType2_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" runat="server" id="td_dq1" width="50px" class="contrlFontSize_s">大区：
                    </td>
                    <td align="left" runat="server" id="td_dq2">
                        <asp:DropDownList ID="ddlType3" CssClass="inputtext15" runat="server" Width="120px" AutoPostBack="true" OnSelectedIndexChanged="ddlType3_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="50px" class="contrlFontSize_s" runat="server" id="td_td1">团队：
                    </td>
                    <td align="left" width="120px" runat="server" id="td_td2">
                        <asp:DropDownList ID="ddlType4" CssClass="inputtext15" runat="server" Width="120px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="50px">&nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />
                    </td>
                    <td align="left" width="150px">&nbsp;
                        <asp:Button ID="btnChart" OnClick="btnChart_Click" CssClass="btn_s"
                            runat="server" Text="图表展示" Width="60px" />
                        <asp:Button runat="server"  OnClick="btnOutput_Click" CssClass="btn_s" Width="60px"   ID="btnOutput" Text="数据导出" />
                    </td>
                </tr>

            </tbody>
        </table>
    </fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align:center">
            
    </div>
    <div id="chart_100" style="width: 100%; height: 500px;"></div>
    <div id="chart_200" style="width: 100%; height: 1000px;"></div>
    <script src="echarts.min.js" type="text/javascript"></script>
    <script src="map/china.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        var s_legentdata = <%= this.lengenddata %>;
        var s_stpf = <%= this.stpf %>;
        var s_clpf = <%= this.clpf %>;
        var s_posmpf = <%= this.posmpf %>;
        var s_jgpm = <%= this.jgpm %>;
        var s_tjpm = <%= this.tjpm %>;

        var chart_h = <%= this.chart_h %>;
        option = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                    type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['渗透评分', '陈列评分', 'POSM评分', '价格评分', '推荐评分']
            },
            grid: {
                left: '10%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis:  {
                type: 'value',
                axisLable:{
                    interval: 0
                }
            },
            yAxis: {
                type: 'category',
                data: s_legentdata
            },
            series: [
                {
                    name: '渗透评分',
                    type: 'bar',
                    stack: '分数',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: s_stpf
                },
                {
                    name: '陈列评分',
                    type: 'bar',
                    stack: '分数',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: s_clpf
                },
                {
                    name: 'POSM评分',
                    type: 'bar',
                    stack: '分数',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: s_posmpf
                },
                {
                    name: '价格评分',
                    type: 'bar',
                    stack: '分数',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: s_jgpm
                },
                {
                    name: '推荐评分',
                    type: 'bar',
                    stack: '分数',
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight'
                        }
                    },
                    data: s_tjpm
                }
            ]
        };

        if(chart_h<=100)
        {
            $("#chart_200").hide();
            var chart = echarts.init(document.getElementById('chart_100'));
        }
        else
        {
            $("#chart_100").hide();
            var chart = echarts.init(document.getElementById('chart_200'));
        }
       
        chart.showLoading({
            text: '正在加载中..... ',
        });
        chart.hideLoading();

        chart.setOption(option);

        $("#rightMain_chart").resize(function(){
            $("#rightMain_chart").resize();
        })
    </script>

</asp:Content>


