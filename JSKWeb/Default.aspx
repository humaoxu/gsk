﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JSKWeb.Default" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>问卷系统</title>
 
   <link href="css/default_n.css" rel="stylesheet" type="text/css" />
     
</head>
<body>
    <form id="form1" runat="server">
    <div style="background-repeat: repeat-x; width: 100%; height: 300px; margin-top: 230px;">
        <div class="loginbg">
            <img alt="img" src="images/logologin.png">
            <dl>
                <dt style="text-align: right; padding-right: 3px;">用户编号:</dt>
                <dd>
                    <input type="text" class="input04" id="txtUsername" runat="server" value="" /></dd></dl>
            <dl>
                <dt style="text-align: right; padding-right: 3px;">密 码:</dt>
                <dd>
                    <input type="password" class="input05" id="txtPassword" runat="server" value="" /></dd></dl>
            <dl>
                <dt><span style="float: left;"></span><span class="input03">
                    <asp:LinkButton ID="btnLogin" runat="server" OnClick="btnLogin_Click">登陆</asp:LinkButton>
                </span></dt>
            </dl>
        </div>
    </div>
    </form>
</body>
</html>
