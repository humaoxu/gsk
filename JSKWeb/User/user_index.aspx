﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="user_index.aspx.cs" Inherits="JSKWeb.user.user_index"
    MasterPageFile="~/Master/Top.Master" %> 
    
<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                row.cells[0].innerHTML = "<input type=\"radio\" value=\"\" name=\"rd\" checked='checked' />";
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {
                var json = { "oo": [
                { "url": "user_add.aspx", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                { "url": "user_add.aspx?id=", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no"}]
                };

                if (index == 0) {//新增
                    window.location.href = json.oo[index].url;
                    //var result = window.open(json.oo[index].url, window, json.oo[index].parm);
                    return false;
                    //                    if (typeof (result) == "undefined" || result == "") {
                    //                        return false;
                    //                    }
                    //                    else {
                    //                        return true;
                    //                    }
                }
                else if (index == 1) {//修改
                    window.location.href = json.oo[index].url + key;
                    //var result = window.open(json.oo[index].url + key, window, json.oo[index].parm);
                    return false;
                    //                    if (typeof (result) == "undefined" || result == "") {
                    //                        return false;
                    //                    }
                    //                    else {
                    //                        return true;
                    //                    }
                }
                else if (index == 2) {//删除
                    return confirm("Confirm delete？");
                }
                else {
                    return false;
                }
            }
        }
        $(document).ready(function () {
            $("#sel_load").change(function () {
                var p = $(this).children('option:selected').val();
                window.location.href = "user_index.aspx?p=" + p;

            });
        })
        //alert($(window).width());                            //浏览器当前窗口可视区域宽度

        //alert($(document).width());                        //浏览器当前窗口文档对象宽度

        //  alert($(document.body).width());                //浏览器当前窗口文档body的宽度

 //  alert($(document.body).outerWidth(true));  //浏览器当前窗口文档body的总宽度 包括border padding margin
    </script>
    <style type="text/css">
        .style1
        {
            font-size: 15PX;
            width: 77px;
        }
        .style2
        {
            font-size: 15PX;
            width: 71px;
        }
       .style3
        {
            overflow:auto; width:; display:block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>Filters</legend>
        <table cellpadding="0" cellspacing="0" border="0"  class="formstyle">
            <tbody>
                <tr>
                    <td align="right" width="96px" class="contrlFontSize">
                        User Code
                    </td>
                    <td align="left" width="100px">
                        <asp:TextBox ID="txtUserCode" runat="server" Width="100px" CssClass="contrlFontSize"></asp:TextBox>
                    </td>
                    <td align="right" width="96px" class="contrlFontSize">
                        User Name
                    </td>
                    <td align="left" width="100px">
                        <asp:TextBox ID="txtKey" runat="server" Width="100px" CssClass="contrlFontSize"></asp:TextBox>
                    </td>
                    <td align="right" >
                        PageSize
                    </td>
                    <td align="left" width="100px">
                        <asp:TextBox ID="txtPageSise" runat="server" Width="104px">20</asp:TextBox>
                    </td>
                    <td align="right" class="style2">
                        Sorted By
                    </td>
                    <td align="left" width="100px">
                        <asp:DropDownList ID="ddlSort" runat="server" Width="100px">
                            <asp:ListItem Selected="True" Text="SEQ" Value="user_id"></asp:ListItem>
                            <asp:ListItem Text="USER NAME" Value="user_name"></asp:ListItem>
                            <asp:ListItem Text="USER TEL" Value="user_mobile"></asp:ListItem>
                            <asp:ListItem Text="USER ADDRESSS" Value="user_address"></asp:ListItem>
                            <asp:ListItem Text="COMMENTS" Value="USER_NOTE"></asp:ListItem>
                            <asp:ListItem Text="REGIST DATE" Value="user_regist_time"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    
                    <td align="left" width="100px">
                        <asp:RadioButton runat="server" ID="rbAsc" Text="Asc" GroupName="sc" Checked="true" />
                        <asp:RadioButton runat="server" ID="rbDesc" Text="Desc" GroupName="sc" />
                    </td>
                    <td align="left">
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn" runat="server"
                            Text="GO" Width="40px" />
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="dataMian"  >
        <div>
            <cc1:gridviewkit ID="gvMain" runat="server" AutoGenerateColumns="False" Width="100%"
                OnRowDataBound="gvMain_RowDataBound" Font-Size="9pt" 
                DataKeyNames="user_id" CssClass="tablestyle"
                EnableEmptyContentRender="True" CellPadding="2">
                <RowStyle />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Center" Width="30" />
                        <ItemTemplate>
                            <input type="radio" value="" name="rd" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="user_id" HeaderText="SEQ">
                        <HeaderStyle HorizontalAlign="Center" Width="160" />
                        <ItemStyle Width="25" HorizontalAlign="Center" />
                    </asp:BoundField>
                     <asp:BoundField DataField="user_CODE" HeaderText="USER CODE">
                        <HeaderStyle HorizontalAlign="Center" Width="160" />
                        <ItemStyle Width="25" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="user_name" HeaderText="USER NAME">
                        <HeaderStyle HorizontalAlign="Center" Width="250" />
                        <ItemStyle Width="250" HorizontalAlign="LEFT" />
                    </asp:BoundField>
                    <asp:BoundField DataField="user_mobile" HeaderText="USER TEL">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="LEFT" />
                    </asp:BoundField>
                    <asp:BoundField DataField="user_address" HeaderText="E-mail">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="LEFT" />
                    </asp:BoundField>
                    <asp:BoundField DataField="USER_NOTE" HeaderText="COMMENTS">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="LEFT" />
                    </asp:BoundField>
                    <asp:BoundField DataField="user_regist_time" HeaderText="REGIST DATE" DataFormatString="{0:yyyy-MM-dd}">
                        <HeaderStyle HorizontalAlign="Center" Width="100" />
                        <ItemStyle Width="100" HorizontalAlign="LEFT" />
                    </asp:BoundField>  
                </Columns>
                <HeaderStyle />
                <AlternatingRowStyle />
            </cc1:gridviewkit>
        </div>
        <div>
            <asp:HiddenField ID="hfKey" runat="server" />
            <asp:HiddenField ID="hfSQL" runat="server" />
            <asp:HiddenField ID="hfNam" runat="server" />
            <webdiyer:AspNetPager ID="Pager" runat="server" HorizontalAlign="Right" Width="100%"
                Style="font-size: 14px; color: Black;" AlwaysShow="true" FirstPageText="137"
                LastPageText="138" NextPageText="131" PrevPageText="132" SubmitButtonClass=""
                CustomInfoStyle="font-size:12px;text-align:Left;" TextBeforeInputBox="" TextAfterInputBox=""
                PageIndexBoxType="TextBox" ShowPageIndexBox="Always" TextAfterPageIndexBox="页"
                TextBeforePageIndexBox="转到" Font-Size="12px" ShowCustomInfoSection="Left" CustomInfoSectionWidth=""
                PagingButtonSpacing="3px" CustomInfoHTML="" CssClass="paginator" CurrentPageButtonClass="cpb"
                ShowInputBox="Never" CustomInfoClass="" OnPageChanged="pager_PageChanged" ButtonImageExtension=".png"
                ImagePath="~/images/" PagingButtonType="Image" NumericButtonType="Text" MoreButtonType="Text">
            </webdiyer:AspNetPager>
        </div>
    </div>
    <%--<div class="right_head">
        <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/images/addbtn.png" OnClientClick="operate(0)" onclick="btnAdd_Click"  />&nbsp;&nbsp;
        <asp:ImageButton ID="btnModify" runat="server" ImageUrl="~/images/modify.png" OnClientClick="operate(1)" onclick="btnModify_Click" />&nbsp;&nbsp;
        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/deletebtn.png" OnClientClick="return operate(2);" onclick="btnDelete_Click" />&nbsp;&nbsp;
    </div>--%>
    <div class="tabletitle">
        <asp:Button ID="btnAdd" Width="80px" runat="server" CssClass="btn" OnClientClick="return operate(0)"
            Text="CREATE" OnClick="btnAdd_Click" />&nbsp;&nbsp;
        <asp:Button ID="btnModify" Width="80px" runat="server" CssClass="btn" OnClientClick="return operate(1)"
            Text="EDIT" OnClick="btnModify_Click" />&nbsp;&nbsp;
        <asp:Button ID="btnDelete" Width="80px" runat="server" CssClass="btn" OnClientClick="return operate(2)"
            Text="DELETE" OnClick="btnDelete_Click" />&nbsp;&nbsp;
    </div>
</asp:Content>
