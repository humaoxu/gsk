﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserRoleEdit.aspx.cs" Inherits="JSKWeb.user.UserRoleEdit"   MasterPageFile="~/Master/Top.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <base target="_self" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <%--<table width="100%" class="BreadcrumbNavigation" style="border: none; text-align: left;"
        border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                >><a href="#">HOME</a> > <a href="#">LINX REPORT</a> > <a href="#">RPTCLIENT</a>
            </td>
        </tr>
    </table>--%>
     <%--<table cellpadding="0" cellspacing="0" border="0" width="100%" class="formstyle">
                	<tbody><tr><th colspan="9">Filters</th></tr>
                	<tr><td align="right" width="96px" class="contrlFontSize" >Client Name</td><td align="left" width="100px"><asp:TextBox ID="txtKey" runat="server"  width="100px"  CssClass="contrlFontSize"></asp:TextBox> </td>
                    <td align="right" width="120px" class="contrlFontSize" >Rows Per Page</td><td align="left" width="80px">
                        <asp:TextBox ID="txtRowsPerPage" Width="80px" runat="server" 
                            CssClass="contrlFontSize">20</asp:TextBox> </td>
                    <td align="left"><asp:Button ID="btnQuery"
                         OnClick="btnSearch_Click"   CssClass="btn"    runat="server" Text="Go" /></td> </tr>
                    
    </tbody>
    </table>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div style="width: 100%; vertical-align:left;overflow:visible" align="left">
        <table width="50%" style="text-align: left; font-size: 12px;" cellspacing="2px">
            
            <tr>
                <td>
                    <fieldset>
                        <legend>Required Information</legend>
                        <table width="95%" style="text-align: center">
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                       Role Name：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRoleName" runat="server" MaxLength="20" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <%--<tr runat="server" id="type">
                                <td width="30%" align="right">
                                    <label>
                                       User Role Type：
                                    </label>
                                </td>
                                <td>
                                    <asp:RadioButton runat="server" ID="rdTT" Text="TT" GroupName="T" Checked="true" />
                                    <asp:RadioButton runat="server" ID="rdMT" Text="MT" GroupName="T" Checked="false" />
                                </td>
                            </tr>--%>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                
                    <table width="100%" >
                        <tr>
                            <td colspan="2">
                                <font style="font-size: 12px"><asp:Label ID="lbltitle" runat="server" Text=""></asp:Label></font>
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSave" Width="80px" runat="server" CssClass="btn" Text="SAVE" OnClick="btnSave_Click"
                                    />
                                &nbsp;&nbsp;&nbsp;&nbsp
                                <input type="button" value="BACK" class="btn" onclick="javascript:document.location.href = 'UserRoleManage.aspx?menu_id=10';" style="width: 60px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
