﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="user_add.aspx.cs" Inherits="JSKWeb.user.user_add"  MasterPageFile="~/Master/Top.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <base target="_self" />
    <script type="text/javascript" src="../js/jquery-1.10.1.min.js"></script>
    <script type="text/javascript">
        function validate() {
            var message = "";

            var txtUserName = document.getElementById("<%=txtUserName.ClientID%>").value;
            var txtPwd = document.getElementById("<%=txtPwd.ClientID%>").value;
            var txtPwdConfirm = document.getElementById("<%=txtPwdConfirm.ClientID%>").value;

            $.each({ txtUserName: "UserName", txtPwd: "password", txtPwdConfirm: "confirm password" },
            function (i, n) {
                var text = $.trim($("#rightMain_" + i).val());
                if (text.length == 0) {
                    message = message + "[" + n + "]";
                }
            });

            if (message.length != 0) {
                alert(message + "not null！");
                return false;
            }

            var PW1 = document.getElementById("<%=txtPwd.ClientID%>").value;
            var PW2 = document.getElementById("<%=txtPwdConfirm.ClientID%>").value;
            if (PW1 != PW2) {
                alert("Password must be consistent, please re-enter！");
                $("#<%=txtPwd.ClientID %>").val("");
                $("#<%=txtPwdConfirm.ClientID %>").val("");
                return false;
            }

           

            return true;
        }       
    </script>
     <style type="text/css">
        .txtRemark
        {
            max-width:250px;
            max-height:150px
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <%--<table width="100%" class="BreadcrumbNavigation" style="border: none; text-align: left;"
        border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                >><a href="#">HOME</a> > <a href="#">LINX REPORT</a> > <a href="#">RPTCLIENT</a>
            </td>
        </tr>
    </table>--%>
     <%--<table cellpadding="0" cellspacing="0" border="0" width="100%" class="formstyle">
                	<tbody><tr><th colspan="9">Filters</th></tr>
                	<tr><td align="right" width="96px" class="contrlFontSize" >Client Name</td><td align="left" width="100px"><asp:TextBox ID="txtKey" runat="server"  width="100px"  CssClass="contrlFontSize"></asp:TextBox> </td>
                    <td align="right" width="120px" class="contrlFontSize" >Rows Per Page</td><td align="left" width="80px">
                        <asp:TextBox ID="txtRowsPerPage" Width="80px" runat="server" 
                            CssClass="contrlFontSize">20</asp:TextBox> </td>
                    <td align="left"><asp:Button ID="btnQuery"
                         OnClick="btnSearch_Click"   CssClass="btn"    runat="server" Text="Go" /></td> </tr>
                    
    </tbody>
    </table>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div style="width: 100%" align="left">
        <table width="50%" style="text-align: left; font-size: 12px;" cellspacing="2px">

            <tr>
                <td>
                    <fieldset>
                        <legend>Required Information</legend>
                        <table width="85%" style="text-align: center">
                           <tr>
                                <td width="30%" align="right">
                                    <label>
                                        UserCode：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserCode" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        UserName：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserName" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        Passwrod：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPwd" runat="server" MaxLength="50" Width="250px" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        Confirm Password：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPwdConfirm" runat="server" MaxLength="50" Width="250px" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        Tel:
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtTel" runat="server" MaxLength="20" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        E-mail：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right">
                                    Comments<label>：
                                    </label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMark" runat="server" MaxLength="200" Width="250px" Rows="5" 
                                        TextMode="MultiLine" CssClass="txtRemark" ></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend>Role Information</legend>
                        <table width="85%" style="text-align: center; height: 80px;">
                            <tr>
                                <td width="30%" align="right" valign="top">
                                    <label>
                                        User Role：
                                    </label>
                                </td>
                                <td align="center" valign="top">
                                    <div style="height: 80px; overflow: auto;">
                                        <asp:DropDownList ID="ddlRole" runat="server" Width="250px">
                                        </asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td>
                
                    <table width="100%" >
                        <tr>
                            <td colspan="2">
                                <font style="font-size: 12px"><asp:Label ID="lbltitle" runat="server" Text=""></asp:Label></font>
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn" Text="SAVE" Width="60px" OnClick="btnSave_Click"
                                    OnClientClick="return validate();" />
                                &nbsp;&nbsp;&nbsp;&nbsp
                                &nbsp;&nbsp;&nbsp;&nbsp
                                <input type="button" value="BACK" class="btn" onclick="javascript:document.location.href = 'user_index.aspx?menu_id=10';" style="width: 60px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField ID="hdUT" runat="server" />
</asp:Content>

