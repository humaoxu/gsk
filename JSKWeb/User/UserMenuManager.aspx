﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserMenuManager.aspx.cs" Inherits="JSKWeb.user.UserMenuManager"  MasterPageFile="~/Master/Top.Master"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/TreeViewSelect.js" type="text/javascript"></script>
    <script type="text/javascript">
        function operate(index) {

            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {

                var json = { "oo": [
                { "url": "UserMenu_Add.aspx?parent_id=" + key, "parm": "Height:500px;dialogWidth:600px;status:no;help:no;scroll:no" },
                { "url": "UserMenu_Add.aspx?parent_id=" + key + "&isAdd=1", "parm": "Height:500px;dialogWidth:600px;status:no;help:no;scroll:no"}]
                };

                if (index == 0) {//新增

                    var result = window.open(json.oo[index].url, window, json.oo[index].parm);
                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else if (index == 1) {//修改
                    var result = window.open(json.oo[index].url, window, json.oo[index].parm);
                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else if (index == 2) {//删除
                    return confirm("Confirm delete？");
                }
                else {
                    return false;
                }
            }
        }

        function addMenu(key) {
            var result = window.open("UserMenu_Add.aspx?parent_id=" + key, window, "Height:300px;Width:700px;status:no;help:no;scroll:no");
            if (typeof (result) == "undefined" || result == "") {
                return false;
            }
            else {
                return true;
            }
        }
        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
  <div class="toptoolsarea">
            <label style=" display:none;">Filename:</label>
            <input type="text" class="inputtext" style=" display:none;" />&nbsp;&nbsp;
            <label  class="lablekeyword" style=" display:none;">KeyWord:</label>
            <input  style=" display:none;" type="text" class="inputtext" />&nbsp;&nbsp;
            <a href="#" style=" display:none;"><img src="../images/searchbtn.png" class="inputbtn" /></a>
            </div>
           <div  class="toptoolUser">
             <label> user： <asp:Label ID="lblUserCode" runat="server"></asp:Label> </label>
             <img class="inputbtn_exit" src="../images/exit.png"  onclick="quitsystem()" />
           </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
<div class="right_head">
        <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/images/addbtn.png" 
             OnClientClick="operate(0)" onclick="btnAdd_Click1" />&nbsp;&nbsp;
        <asp:ImageButton ID="btnModify" runat="server" ImageUrl="~/images/modify.png" 
           OnClientClick="operate(1)"     onclick="btnModify_Click" />&nbsp;&nbsp;
        <asp:ImageButton ID="btnDelete" runat="server" 
            ImageUrl="~/images/deletebtn.png"  OnClientClick="return operate(2);"
            onclick="btnDelete_Click" />&nbsp;&nbsp;
             <asp:ImageButton ID="btnSave" runat="server" 
            ImageUrl="~/images/save.png" 
            onclick="btnSave_Click"  Visible="false" />&nbsp;&nbsp; 
    </div>
  <div style=" ">
      <table> 
        <tr>
            <td class="style3" valign="top">
                <asp:TreeView ID="tvUser" Visible="false" runat="server" ImageSet="Contacts" NodeIndent="10" 
                    onselectednodechanged="tvRole_SelectedNodeChanged">
                    <ParentNodeStyle Font-Bold="True" ForeColor="#5555DD" />
                    <HoverNodeStyle Font-Underline="False" />
                    <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" 
                        VerticalPadding="0px" ForeColor="#0000CC" />
                    <Nodes>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                    </Nodes>
                    <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" 
                        HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                </asp:TreeView>
            </td>
            <td class="style1" valign="top">
                <asp:TreeView ID="tvUserMenu" runat="server"
                    ontreenodecheckchanged="tvUserMenu_TreeNodeCheckChanged" 
                    onselectednodechanged="tvUserMenu_SelectedNodeChanged"  ExpanedDepth="1"
                    NodeStyle-ForeColor="Black" ontreenodeexpanded="tvUserMenu_TreeNodeExpanded">
                    <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" 
                        VerticalPadding="0px" ForeColor="Red" />
                    <Nodes>
                        <asp:TreeNode Text="New Node" Value="New Node">
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node">
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node">
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node">
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node">
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node">
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                    </Nodes>
                </asp:TreeView>
                  <asp:HiddenField ID="hfKey" runat="server" />
              
            </td>
        </tr> 
    </table>
      <script type="text/javascript">
//          SetTreeNodeAutoSelectParentNodeHandle("<%=tvUserMenu.ClientID%>");
                    
                </script>
  </div>

</asp:Content>
