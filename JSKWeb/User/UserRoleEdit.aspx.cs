﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using JSKWeb.Code;
//using DBBase;
using DBUtility;

namespace JSKWeb.user
{
    public partial class UserRoleEdit : PageBase
    {
        //DBUtil ora = new DBUtil();
        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    LoadInfo(Request.QueryString["id"]);
                    ViewState["id"] = Request.QueryString["id"];
                }
            }
        }

        private void LoadInfo(string id)
        {

            string sql = "select * from sys_user_role where user_role_id = '" + id + "'";
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                txtRoleName.Text = dr["user_role_name"].ToString();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["id"] != null)
                {
                    string sql = "update sys_user_role set user_role_name = '" + txtRoleName.Text.Trim() + "'  where user_role_id = '" + ViewState["id"] + "' ";
                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    this.lbltitle.Text = JsAlertMessage.message4;
                }
                else
                {
                    Guid guid = Guid.NewGuid();
                    string sql = "insert into sys_user_role(user_role_name) values('" + txtRoleName.Text.Trim() + "' ) ";
                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    this.lbltitle.Text = JsAlertMessage.message4;
                }
            }

            catch(Exception ex)
            {
                this.lbltitle.Text = ex.Message;
            }


        }
    }
}