﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserMenu_Add.aspx.cs" Inherits="JSKWeb.user.UserMenu_Add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <base target="_self" />
    <title></title>
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%" align="center">
        <table width="90%" style="text-align: left; font-size: 12px;" cellspacing="2px">
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <font style="font-size: 12pt"><b>Edit UserMenu</b> </font>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset>
                        <legend>Required Information</legend>
                        <table width="90%" style="text-align: center">
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        Menu Name：
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtMenuName" runat="server" MaxLength="200" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <%--<tr>
                                <td width="30%" align="right">
                                    <label>
                                        File Permissions：
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlFilePermissons" runat="server" 
                                        style="margin-left: 0px" Width="250px">
                                        <asp:ListItem Value="1">Public</asp:ListItem>
                                        <asp:ListItem Value="2">Private</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <td width="30%" align="right">
                                    <label>
                                        Menu Type：
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlMenuType" runat="server" style="margin-left: 0px" 
                                        Width="250px" AutoPostBack="true" 
                                        onselectedindexchanged="ddlMenuType_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>--%>
                            <tr>
                                <td width="30%" align="right">
                                    <label>
                                        Order ID：
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtOrder" runat="server" MaxLength="20" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="btime" style="display:none;" runat="server">
                                <td width="30%" align="right">
                                    <label>
                                        Stop Time:
                                    </label>
                                </td>
                                <td align="left">
                                    <input id="startDate" type="text" onclick="WdatePicker()" runat="server" class="BigInput" />-
                                    <input  id="endDate" type="text" onclick="WdatePicker()" runat="server" class="BigInput" />
                                </td>
                            </tr>
                            <tr id="trMenuParam" style="  display:none;">
                                <td width="30%" align="right">
                                    <label>
                                        Menu Param:
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtMenuParam" runat="server" MaxLength="20" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                            
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr id="tr" runat="server" visible="false">
                <td>
                    <fieldset>
                        <legend>Other Information</legend>
                        <table width="85%" style="text-align: center; height: 118px;">
                            <%--<tr>
                                <td width="30%" align="right" valign="top">
                                    <label>
                                        Script Params：
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtParms" runat="server" MaxLength="20" Width="250px"></asp:TextBox><span style="color:Red">*","号分隔</span>                           
                                </td>
                            </tr>--%>
                            <tr>
                                <td width="30%" align="right" valign="top">
                                    <label>
                                        SQL Script：
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtSQL" runat="server" MaxLength="2000" Width="250px" Rows="3" 
                                        TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="30%" align="right" valign="top">
                                    <label>
                                        Database：
                                    </label>
                                </td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlDB" runat="server" Width="250px"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSave" runat="server" Text="Save" Width="60px" OnClick="btnSave_Click"
                                   />
                                &nbsp;&nbsp;&nbsp;&nbsp
                                &nbsp;&nbsp;&nbsp;&nbsp
                                <input type="button" value="Close" onclick="javascript:window.close()" style="width: 60px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
