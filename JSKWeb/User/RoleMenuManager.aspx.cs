﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JSKWeb.Utility;
using JSKWeb.Code;
//using DBBase;
using System.Drawing;
using DBUtility;

namespace JSKWeb.user
{
    public partial class RoleMenuManager :  PageBase
    {
        //DBUtil ora = new DBUtil();
        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
               
                UserTreeBind();
                LoadInfo();
            }
        }
        private void LoadInfo()
        {
            if (tvUser.Nodes.Count > 0)
            {
                tvUser.Nodes[0].Select();
                this.SelectUser();
            }
        }

        private void SelectUser()
        {
             
            TreeNode tnUserRole = tvUser.SelectedNode;
            if (tnUserRole != null)
            {
                this.ViewState["MenuID"] = tnUserRole.Value;

                string strSQL = "select * from sys_menu  ";
                DataSet dsMenu = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

                string sqlUMenu = @"select * from  sys_role_menu  where user_role_id='" + ViewState["MenuID"].ToString() + "'  ";
                //DataTable dsUMenu = ora.ExecuteDataTable(sqlUMenu);
                DataSet dsUMenu = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlUMenu);

                if (dsMenu != null && dsMenu.Tables[0].Rows.Count > 0)
                {
                    LoadMenu(tvUserMenu.Nodes, "0", dsMenu.Tables[0], dsUMenu.Tables[0]);
                    tvUserMenu.ExpandAll();
                }
            }
        }

   
     

        private void LoadMenu(TreeNodeCollection treeNodeCollection, string p, DataTable ds, DataTable drs)
        {
            treeNodeCollection.Clear();
            DataRow[] drSub = ds.Select("parent_id='"+p+"'");
            foreach (DataRow dr in drSub)
            {
                TreeNode subNode = new TreeNode(dr["module_name"].ToString(), dr["menu_id"].ToString());
                treeNodeCollection.Add(subNode);

                subNode.Checked = drs.Select("menu_id='" + dr["menu_id"].ToString() + "'").Count() > 0;
                this.LoadMenu(subNode.ChildNodes, subNode.Value, ds, drs);
            }
        }



        public void UserTreeBind()
        {
            tvUser.Nodes.Clear();
            string strSQL = "select * from sys_user_role";
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);
            foreach (DataRow rowGroup in ds.Tables[0].Rows)
            {
                TreeNode tn = new TreeNode(rowGroup["user_role_name"].ToString(), rowGroup["user_role_id"].ToString());
                tvUser.Nodes.Add(tn);
            }
        }

        public void MenuTreeBind()
        {
 
        }

        protected void tvRole_SelectedNodeChanged(object sender, EventArgs e)
        {
            SelectUser();
            lblMessage.Text = "";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string roleID = InputText.GetStrByObj(this.ViewState["MenuID"]);
                if (string.IsNullOrEmpty(roleID) == false && tvUserMenu.Nodes.Count > 0)
                {
                    string sql = "delete  sys_role_menu where user_role_id='" + roleID + "' ";
                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    foreach (TreeNode tn in tvUserMenu.CheckedNodes )
                    {
                        if (tn.Checked)
                        {
                            sql = "insert into sys_role_menu  ( user_role_id,menu_id)values('" + roleID + "','" + tn.Value + "') ";
                            sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                        } 
                    }

                    lblMessage.Text = JsAlertMessage.message4;

                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        protected void tvUserMenu_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            selectParentNode(e.Node, e.Node.Checked);
            string menuid = "0";
            if (tvUserMenu.SelectedNode != null)
            {
                menuid = tvUserMenu.SelectedNode.Value;
            }
            hfKey.Value = menuid;
            lblMessage.Text = "";
        }

        private void selectParentNode(TreeNode node, bool check)
        {
            
        }

        protected void tvUserMenu_SelectedNodeChanged(object sender, EventArgs e)
        {
            string menuid = "0";
            if (tvUserMenu.SelectedNode != null)
            {
                menuid = tvUserMenu.SelectedNode.Value;
            }
            hfKey.Value = menuid;

            lblMessage.Text = "";
        }

    }
}