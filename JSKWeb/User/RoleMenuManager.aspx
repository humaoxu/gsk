﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="RoleMenuManager.aspx.cs"
    Inherits="JSKWeb.user.RoleMenuManager" MasterPageFile="~/Master/Top.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../js/TreeViewSelect.js" type="text/javascript"></script>
    <script type="text/javascript">
        function operate(index) {

            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {

                var json = { "oo": [
                { "url": "UserMenu_Add.aspx?parent_id=" + key, "parm": "dialogHeight:600px;dialogWidth:700px;status:no;help:no;scroll:no" },
                { "url": "UserMenu_Add.aspx?parent_id=" + key + "&isAdd=1", "parm": "dialogHeight:600px;dialogWidth:700px;status:no;help:no;scroll:no"}]
                };

                if (index == 0) {//新增

                    var result = window.open(json.oo[index].url, window, json.oo[index].parm);
                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else if (index == 1) {//修改
                    var result = window.open(json.oo[index].url, window, json.oo[index].parm);
                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
                else if (index == 2) {//删除
                    return confirm("Confirm delete？");
                }
                else {
                    return false;
                }
            }
        }

        function addMenu(key) {
            var result = window.open("UserMenu_Add.aspx?parent_id=" + key, window, "dialogHeight:300px;dialogWidth:700px;status:no;help:no;scroll:no");
            if (typeof (result) == "undefined" || result == "") {
                return false;
            }
            else {
                return true;
            }
        }
        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <%--<table width="100%" class="BreadcrumbNavigation" style="border: none; text-align: left;"
        border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                >><a href="#">HOME</a> > <a href="#">LINX REPORT</a> > <a href="#">RPTCLIENT</a>
            </td>
        </tr>
    </table>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div class="right_head" align="left">
        <asp:ImageButton ID="btnSave" runat="server" ImageUrl="~/images/save.png" OnClick="btnSave_Click" />
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
    <div align="left">
        <table>
            <tr>
                <td class="style3" valign="top">
                    <asp:TreeView ID="tvUser" runat="server" ImageSet="Contacts" NodeIndent="10" 
                        OnSelectedNodeChanged="tvRole_SelectedNodeChanged" ForeColor="Black">
                        <ParentNodeStyle Font-Bold="True"  />
                        <HoverNodeStyle Font-Underline="False" />
                        <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px"
                           />
                        <Nodes>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </Nodes>
                        <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px"
                            NodeSpacing="0px" VerticalPadding="0px" />
                    </asp:TreeView>
                </td>
                <td class="style1" valign="top">
                    <asp:TreeView ID="tvUserMenu" runat="server" ShowCheckBoxes="All" OnTreeNodeCheckChanged="tvUserMenu_TreeNodeCheckChanged"
                        OnSelectedNodeChanged="tvUserMenu_SelectedNodeChanged" 
                        NodeStyle-ForeColor="Black" ForeColor="Black">
                        <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" VerticalPadding="0px"
                            />
                        <Nodes>
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node">
                                    <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                </asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node">
                                    <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                </asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node">
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                                <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            </asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                            <asp:TreeNode Text="New Node" Value="New Node"></asp:TreeNode>
                        </Nodes>
                    </asp:TreeView>
                    <asp:HiddenField ID="hfKey" runat="server" />
                </td>
            </tr>
        </table>
        <script type="text/javascript">
            SetTreeNodeAutoSelectParentNodeHandle("<%=tvUserMenu.ClientID%>");
                    
        </script>
    </div>
</asp:Content>
