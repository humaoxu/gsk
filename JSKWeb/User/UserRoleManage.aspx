﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="UserRoleManage.aspx.cs"
    Inherits="JSKWeb.user.UserRoleManage" MasterPageFile="~/Master/Top.Master" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                row.cells[0].innerHTML = "<input type=\"radio\" value=\"\" name=\"rd\" checked='checked' />";
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {
                var json = { "oo": [
                { "url": "UserRoleEdit.aspx", "parm": "dialogHeight:300px;dialogWidth:500px;status:no;help:no;scroll:no" },
                { "url": "UserRoleEdit.aspx?id=", "parm": "dialogHeight:300px;dialogWidth:500px;status:no;help:no;scroll:no"}]
                };

                if (index == 0) {//新增
                    window.location.href = json.oo[index].url;
                    return false;
                }
                else if (index == 1) {//修改
                    window.location.href = json.oo[index].url + key;
                    return false;
                }
                else if (index == 2) {//删除
                    return confirm("Confirm delete？");
                }
                else {
                    return false;
                }
            }
        }
        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        $(document).ready(function () {
            $("#sel_load").change(function () {
                var p = $(this).children('option:selected').val();
                window.location.href = "UserRoleManage.aspx?p=" + p;

            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <%--<table width="100%" class="BreadcrumbNavigation" style="border: none; text-align: left;"
        border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                >><a href="#">HOME</a> > <a href="#">SYSTEM MANAGER</a> > <a href="#">USER MANAGER</a>
            </td>
        </tr>
    </table>--%>
    <fieldset>
    <legend>Filters</legend>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="formstyle">
        <tbody>
            <tr>
                <td align="right" width="96px" class="contrlFontSize">
                    Role Name
                </td>
                <td align="left" width="100px">
                    <asp:TextBox ID="txtKey" runat="server" Width="100px" CssClass="contrlFontSize"></asp:TextBox>
                </td>
                <%--<td align="right" width="120px" class="contrlFontSize">
                    Rows Per Page
                </td>
                <td align="left" width="80px">
                    <asp:TextBox ID="txtRowsPerPage" Width="80px" runat="server" CssClass="contrlFontSize">20</asp:TextBox>
                </td>--%>
                  <td align="right" width="96px" class="contrlFontSize">
                    PageSize
                </td>
                  <td align="left" width="100px">
                        <asp:TextBox ID="txtPageSise" runat="server" Width="104px">20</asp:TextBox>
                    </td>
                <td align="right" width="96px" class="contrlFontSize">
                    Sorted By
                </td>
                <td align="left" width="100px">
                    <asp:DropDownList ID="ddlSort" runat="server" Width="100px">
                        <asp:ListItem Selected="True" Text="SEQ" Value="user_role_id"></asp:ListItem>
                        <asp:ListItem Text="ROLE NAME" Value="user_role_name"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="left" width="100px">
                    <asp:RadioButton runat="server" ID="rbAsc" Text="Asc" GroupName="sc" Checked="true" />
                    <asp:RadioButton runat="server" ID="rbDesc" Text="Desc" GroupName="sc" />
                </td>
                <td align="left">
                    <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn" runat="server"
                        Text="GO" Width="40px" />
                </td>
            </tr>
        </tbody>
    </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="dataMian">
        <div>
            <cc1:GridViewKit ID="gvMain" runat="server" AutoGenerateColumns="False" Width="100%"
                OnRowDataBound="gvMain_RowDataBound" Font-Size="9pt" DataKeyNames="user_role_id"
                CssClass="tablestyle" EnableEmptyContentRender="True" CellPadding="2">
                <RowStyle />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Center" Width="30" />
                        <ItemTemplate >
                   
                            <input type="radio" value="" name="rd" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="user_role_id" HeaderText="ROLE SEQ">
                        <HeaderStyle HorizontalAlign="CENTER" />
                        <ItemStyle HorizontalAlign="Center" Width="60px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="user_role_name" HeaderText="ROLE NAME">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="left" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle />
                <AlternatingRowStyle />
            </cc1:GridViewKit>
        </div>
        <div>
            <asp:HiddenField ID="hfKey" runat="server" />
            <asp:HiddenField ID="hfSQL" runat="server" />
            <asp:HiddenField ID="hfNam" runat="server" />
            <webdiyer:AspNetPager ID="Pager" runat="server"  HorizontalAlign="Right"
            Width="100%" Style="font-size: 14px; color: Black;" AlwaysShow="true" FirstPageText="137"
            LastPageText="138" NextPageText="131" PrevPageText="132" SubmitButtonClass=""
            CustomInfoStyle="font-size:12px;text-align:Left;"
            TextBeforeInputBox="" TextAfterInputBox="" PageIndexBoxType="TextBox" ShowPageIndexBox="Always"
            TextAfterPageIndexBox="页" TextBeforePageIndexBox="转到" Font-Size="12px" ShowCustomInfoSection="Left"
            CustomInfoSectionWidth="" PagingButtonSpacing="3px" CustomInfoHTML="" CssClass="paginator"
            CurrentPageButtonClass="cpb" ShowInputBox="Never" CustomInfoClass="" 
            OnPageChanged="pager_PageChanged" ButtonImageExtension=".png" 
            ImagePath="~/images/" PagingButtonType="Image" NumericButtonType="Text">
        </webdiyer:AspNetPager>
        </div>
    </div>
    <%--<div class="right_head">
        <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/images/addbtn.png" OnClientClick="operate(0)"
            OnClick="btnAdd_Click" />&nbsp;&nbsp;
        <asp:ImageButton ID="btnModify" runat="server" ImageUrl="~/images/modify.png" OnClientClick="operate(1)"
            OnClick="btnModify_Click" />&nbsp;&nbsp;
        <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/deletebtn.png"
            OnClientClick="operate(2)" OnClick="btnDelete_Click" />&nbsp;&nbsp;
    </div>--%>
    <div class="tabletitle">
        <asp:Button ID="btnAdd" Width="80px" runat="server" CssClass="btn" OnClientClick="return operate(0)"
            Text="CREATE" OnClick="btnAdd_Click" />&nbsp;&nbsp;
        <asp:Button ID="btnModify" Width="80px"  runat="server" CssClass="btn" OnClientClick="return operate(1)"
            Text="EDIT" OnClick="btnModify_Click" />&nbsp;&nbsp;
        <asp:Button ID="btnDelete" Width="80px"  runat="server" CssClass="btn" OnClientClick="return operate(2)"
            Text="DELETE" OnClick="btnDelete_Click" />&nbsp;&nbsp;
    </div>
</asp:Content>
