﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using JSKWeb.Code;
//using DBBase;
using JSKWeb.user;
using DBUtility;
namespace JSKWeb.user
{
    public partial class UserRoleManage : PageBase
    {
        private int iPageSize = 20;

        //DBUtil ora = new DBUtil();
        SqlHelper sqlHelper = new SqlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iPageSize = Int32.Parse(ConfigurationManager.AppSettings["DefaultPageSize"].ToString().Trim());
            }
            catch
            {
            }
            /*if (Request.QueryString["p"] != null)
            {
                iPageSize = Int32.Parse(Request.QueryString["p"].ToString());
            }*/
            if (!IsPostBack)
            {
                LoadInfo();
            }
        }

        private void LoadInfo()
        {
            if (txtPageSise.Text.Trim().Length > 0)
            {
                iPageSize = Int32.Parse(txtPageSise.Text); ;
            }
            Pager.PageSize = iPageSize;

            int recCnt;
            try
            {
                ViewState["whereName"] = "1=1";
                if (!txtKey.Text.Trim().Equals(""))
                {
                    ViewState["whereName"] += " and sys_role_name like '%" + txtKey.Text.Trim() + "%'";
                }


                //OracleCommon oc = new OracleCommon();
                DataTable dt = sqlHelper.GetPagingRecord("sys_user_role", ddlSort.SelectedValue + " " + (rbAsc.Checked ? rbAsc.Text : rbDesc.Text), Pager.PageSize, Pager.CurrentPageIndex, ViewState["whereName"].ToString(), "*", out recCnt);
  

                //SqlExecutive se = new SqlExecutive(ConfigFactory.FileDBSysConStr);
                //DataTable dt = se.GetPagingRecord("meta_user_role", "user_role_id", Pager.PageSize, Pager.CurrentPageIndex, ViewState["whereName"].ToString(), "*", out recCnt);
                Pager.RecordCount = recCnt;
                Pager.Visible = recCnt > 0;
                CommonStirng cm = new CommonStirng();
             //   string stmp = cm.GetSelectIdHtml(iPageSize);

                //this.Pager.CustomInfoHTML = string.Format(CommonStirng.StrPagerCustomerInfo, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount, this.Pager.RecordCount, Pager.PageSize });
                this.Pager.CustomInfoHTML = string.Format(CommonStirng.StrPagerCustomerInfo_N, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount,
                    this.Pager.RecordCount, "",(Pager.CurrentPageIndex - 1) * Pager.PageSize + 1,Pager.CurrentPageIndex * Pager.PageSize >= Pager.RecordCount ? Pager.RecordCount : Pager.CurrentPageIndex * Pager.PageSize });

                gvMain.DataSource = dt;
                gvMain.DataBind();
                txtPageSise.Text = iPageSize.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["user_role_id"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");
            }
        }

        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            LoadInfo();
            hfKey.Value = string.Empty;
        }

        protected void btnModify_Click(object sender, EventArgs e)
        {
            LoadInfo();
            hfKey.Value = string.Empty;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                string delSql1 = "delete from sys_user_role where user_role_id = '" + hfKey.Value + "'";
                sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, delSql1);
            }
            catch
            {
                //JavaScript.Alert(this, "Error!");
            }
            LoadInfo();
            hfKey.Value = string.Empty;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Pager.CurrentPageIndex = 1;
            LoadInfo();
        }
    }
}