﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using JSKWeb.Code;
using System.Text;
//using DBBase;
using System.Data.SqlClient;
using DBUtility;

namespace JSKWeb.user
{
    public partial class user_index : PageBase
    {
        private int iPageSize = 20;
        SqlHelper sqlHelper = new SqlHelper();
     
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iPageSize = Int32.Parse(ConfigurationManager.AppSettings["DefaultPageSize"].ToString().Trim());
            }
            catch
            {
                //eat it
            }
            CommonStirng cm = new CommonStirng();

           /* if (Request.QueryString["p"] != null)
            {
                iPageSize = Int32.Parse(txtPageSise.Text);
            }*/
            if (!IsPostBack)
            {
                LoadInfo();
            }
        }

        private void LoadInfo()
        {
            if (txtPageSise.Text.Trim().Length >0 )
            {
                iPageSize =  Int32.Parse(txtPageSise.Text); ;
            }
            Pager.PageSize = iPageSize;
            int recCnt;
            try
            {
                CommonStirng cm = new CommonStirng();

                ViewState["whereName"] = null;
                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 and is_admin!='1'";
                }
                if (txtKey.Text.ToString() != "")
                {
                    ViewState["whereName"]  += " and user_name like '%" + txtKey.Text.Trim() + "%'";
                }
                if (txtUserCode.Text.Trim().Length>0)
                {
                    ViewState["whereName"] += " and USER_Code = '" +txtUserCode.Text.Trim() + "'";
                }

                DataTable dt = sqlHelper.GetPagingRecord("sys_user_info", ddlSort.SelectedValue + " " + (rbAsc.Checked ? rbAsc.Text : rbDesc.Text), Pager.PageSize, Pager.CurrentPageIndex, ViewState["whereName"].ToString(), "*", out recCnt);
                Pager.RecordCount = recCnt;
                Pager.Visible = recCnt > 0;
                // string stmp = cm.GetSelectIdHtml(iPageSize);
                //下拉列表跳转页面
                string shtml    = string.Format(CommonStirng.StrPagerCustomerInfo_N, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount,
                    this.Pager.RecordCount, "",(Pager.CurrentPageIndex - 1) * Pager.PageSize + 1,Pager.CurrentPageIndex * Pager.PageSize >= Pager.RecordCount ? Pager.RecordCount : Pager.CurrentPageIndex * Pager.PageSize });

                this.Pager.CustomInfoHTML = shtml ;
                
                gvMain.DataSource = dt;
                gvMain.DataBind();
                txtPageSise.Text = iPageSize.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["user_id"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");
            }
        }

        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            LoadInfo();
            hfKey.Value = string.Empty;
        }

        protected void btnModify_Click(object sender, EventArgs e)
        {
            LoadInfo();
            hfKey.Value = string.Empty;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                string delSql1 = "delete from sys_user_info where user_id = " + hfKey.Value + "";

                sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, delSql1);
            }
            catch
            {
                throw;
            }
            LoadInfo();
            hfKey.Value = string.Empty;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Pager.CurrentPageIndex = 1;
            LoadInfo();
        }

    }
}