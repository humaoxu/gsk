﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using JSKWeb.Code;
//using System.Data.OracleClient;
//using DBBase;
using DBUtility;

namespace JSKWeb.User
{
    public partial class user_set : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            Label lbltitle = (Label)this.Master.FindControl("lblTitle");
            lbltitle.Text = "----个人信息管理";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string s_user = "";
            if (Request.Cookies["linxsanmpleuser"] != null && Request.Cookies["linxsanmpleuser"].Value != null)
            {
                s_user = Request.Cookies["linxsanmpleuser"].Value.ToString();
            }
            #region 营业所5位编码处理
            string s_role = "";
            if (Request.Cookies["_UserRole"] != null && Request.Cookies["_UserRole"].Value != null)
            {
                s_role = Request.Cookies["_UserRole"].Value.ToString();
            }

            //if (s_role == "9")
            //{
            //    s_user = s_user.PadLeft(5, '0');
            //}
            #endregion
            try
            {
                if (ViewState["id"] != null)
                {

                    string sql = " update sys_user_set set user_code=@user_code, user_question = @user_question,user_answer =@user_answer ";

                    SqlParameter[] pars = {
                                   
                                  new SqlParameter("@user_code",SqlDbType.VarChar),
                                  new SqlParameter ("@user_question",SqlDbType.VarChar),
                                  new SqlParameter ("@user_answer",SqlDbType.VarChar),

                                  };
                    pars[0].Value = s_user;
                    pars[1].Value = this.ddlQuestion.SelectedValue;
                    pars[2].Value = this.txtAnswer.Text; 
                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    this.lbltitle.Text = JsAlertMessage.message4;
                }
                else
                { 

                    string sql1 = string.Empty;

                    SqlParameter[] pars = {
                                   
                                  new SqlParameter("@user_code",SqlDbType.VarChar),
                                  new SqlParameter ("@user_question",SqlDbType.VarChar),
                                  new SqlParameter ("@user_answer",SqlDbType.VarChar),

                                  };
                    pars[0].Value = s_user;                    
                    pars[1].Value = this.ddlQuestion.SelectedValue;
                    pars[2].Value = this.txtAnswer.Text;



                    string sql = "insert into sys_user_set(user_code,user_question,user_answer) ";
                    sql += " values(@user_code,@user_question,@user_answer)";

                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql, pars);

                    this.lbltitle.Text = JsAlertMessage.message4;
                }
            }

            catch (Exception ex)
            {
                //throw ex;
                this.lbltitle.Text = JsAlertMessage.message5;
            }
        }
    }
}