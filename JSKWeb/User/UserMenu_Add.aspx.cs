﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
//using DBBase;

namespace JSKWeb.user
{
    public partial class UserMenu_Add : System.Web.UI.Page
    {
        string parent_id = "";
        SqlHelper sqlHelper = new SqlHelper();
        // DBUtil ora = new DBUtil();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["parent_id"] != "")
            {
                parent_id = Request.QueryString["parent_id"].ToString();
                if (Request.QueryString["parent_id"] == "e7834e24-5556-428d-90ff-dda335f4afb7")
                {
                    this.btime.Attributes.Add("style", "");
                }
                else
                {

                }
            }

            if (true)
            {

            }
            if (!IsPostBack)
            {

                DDLBind();
                LoadInfo();
            }
        }

        private void BindDDL()
        {
            string sql = "select * from DataConnect ";
            DataSet dt = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

            if (dt.Tables[0].Rows.Count > 0)
            {
                ddlDB.DataSource = dt;
                ddlDB.DataTextField = "DB_Name";
                ddlDB.DataValueField = "DB_Code";
                ddlDB.DataBind();
            }
            //ddlDB.Items.Insert(0, new ListItem("Choose A Database","00000"));
        }

        private void DDLBind()
        {
            //string sql = "SELECT MenuTypeCode,MenuName FROM  User_MenuType where MenuTypeCode <> '1' order by  MenuTypeCode ";
            //DataSet ds = SqlHelper.ExecuteDataset(ConfigFactory.FileDBSysConStr, CommandType.Text, sql);
            //ddlMenuType.DataSource = ds;
            //ddlMenuType.DataTextField = "MenuName";
            //ddlMenuType.DataValueField = "MenuTypeCode";
            //ddlMenuType.DataBind();
        }

        private void LoadInfo()
        {
            if (Request.QueryString["isAdd"] != null)
            {
                parent_id = Request.QueryString["parent_id"].ToString();
                string sql = " select * from meta_sys_menu where menu_id='" + parent_id + "'";

                //DataTable dt = ora.ExecuteDataTable(sql);
                DataSet dt = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                if (dt.Tables[0].Rows.Count > 0)
                {
                    txtMenuParam.Text = dt.Tables[0].Rows[0]["MenuParam"].ToString();
                    txtMenuName.Text = dt.Tables[0].Rows[0]["module_name"].ToString();
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                parent_id = Request.QueryString["parent_id"];
                if (Request.QueryString["isAdd"] != null)
                {
                    string sql = "update  meta_sys_menu set order_id = '" + txtOrder.Text.Trim() + "', module_name='" + txtMenuName.Text.Trim() + "',MenuParam='" + txtMenuParam.Text.Trim() + "',linkurl='../index.aspx?menu_id=" + parent_id + "'  where menu_id='" + parent_id + "'";


                    sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                    JavaScript.ClosePage(this, "ok", "Menu save successfully！");
                }
                else
                {
                    if (parent_id.Equals(""))
                    {
                        parent_id = "0";
                    }
                    //string sql = @" insert into user_menu  select '" + Guid.NewGuid().ToString() + "','" + txtMenuName.Text.Trim() + "','" + parent_id + "',  MAX(order_id)+1,'../index.aspx?menu_id=" + Guid.NewGuid().ToString() + "&menuType=" + ddlMenuType.SelectedItem.Value + "','',1,''," + ddlFilePermissons.SelectedItem.Value + "," + ddlMenuType.SelectedItem.Value + ",'" + txtMenuParam.Text + "' from user_menu where parent_id='" + parent_id + "'   ";

                    Guid guid = Guid.NewGuid();
                    string sql = @" insert into meta_sys_menu ([menu_id],[module_name],[parent_id],[order_id],[linkurl],[note],[keyName],[conString]
                                ,[MenuParam]) values ( '" + guid.ToString().ToUpper() + "','" + txtMenuName.Text.Trim() + "','" + parent_id + "', '" + txtOrder.Text.Trim() + "','../index.aspx?menu_id=" + guid.ToString().ToUpper() + "','',1,'','" + txtMenuParam.Text + "' )";

                    sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                    JavaScript.ClosePage(this, "ok", "Menu add successfully！");
                }

            }

            catch (Exception ex)
            {
                if (Request.QueryString["isAdd"] != null)
                {
                    JavaScript.Alert(this, "Menu save failed！");
                }
                else
                    JavaScript.Alert(this, "Menu add failed！");
            }


        }
    }
}
    
        