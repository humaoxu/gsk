﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using JSKWeb.Code;
//using System.Data.OracleClient;
//using DBBase;
using DBUtility;
namespace JSKWeb.user
{
    public partial class user_add : PageBase
    {
        //DBUtil ora = new DBUtil();
        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Reset();
                DDLBind();
                if (Request.QueryString["id"] != null)
                {
                    LoadInfo(Request.QueryString["id"]);
                    ViewState["id"] = Request.QueryString["id"];
                    txtUserCode.ReadOnly = true; 
                }
            }
        }

        private void Reset()
        {
            txtUserName.Text = string.Empty;
            txtPwd.Text = string.Empty;
            txtPwdConfirm.Text = string.Empty;
            txtTel.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtMark.Text = string.Empty;
            txtUserCode.Text = string.Empty;
        }

        private void DDLBind()
        {

            string strSQL = "select * from sys_user_role";
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);
            foreach (DataRow rowGroup in ds.Tables[0].Rows)
            {
                ListItem GroupItem = new ListItem(rowGroup["user_role_name"].ToString(), rowGroup["user_role_id"].ToString());

                ddlRole.Items.Add(GroupItem);
            }
        }

        private void LoadInfo(string id)
        {

            string sql = "select * from sys_user_info where user_id = '" + id + "'";
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            //this.lbltitle.Text = "Edit User";
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                txtUserName.Text = dr["user_name"].ToString(); 
                txtPwd.Attributes.Add("value", dr["user_pwd"].ToString().Trim()); 
                txtPwdConfirm.Attributes.Add("value", dr["user_pwd"].ToString().Trim());
                txtTel.Text = dr["user_mobile"].ToString();
                txtAddress.Text = dr["user_address"].ToString();
                txtMark.Text = dr["user_note"].ToString(); 
                txtUserCode.Text = dr["user_code"].ToString();
                ddlRole.SelectedValue = dr["user_role"].ToString();
            }            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["id"] != null)
                {

                    string sql = "update sys_user_info set user_code='" + txtUserCode.Text.Trim() +"', user_name = '" + txtUserName.Text.Trim() + "',user_pwd='" + txtPwd.Text.Trim() + "'"
                           + ",user_mobile='" + txtTel.Text.Trim() + "',user_role='" + this.ddlRole.SelectedValue + "',user_address='" + txtAddress.Text.Trim() + "', is_admin='0',user_note='" + txtMark.Text.Trim() + "' where user_id = '" + ViewState["id"] + "' ";
                    //ora.ExecuteNonQuery(CommandType.Text, sql,null);
                    sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    this.lbltitle.Text = JsAlertMessage.message4;
                }
                else
                {

                    //OracleCommon cm = new OracleCommon();
                    string sql = "select * from sys_user_info where user_code='" +txtUserCode.Text.Trim() + "'";
                    //DataSet dtUser = sql.SystemCheckUserName(this.txtUserName.Text.Trim());
                    DataSet dtUser = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    if (dtUser.Tables[0].Rows.Count > 0)
                    {
                        JavaScript.Alert(this, "User name can not be repeated , Please input again. ");
                        return;
                    }
                    
                    string sql1 = string.Empty;
                     
                    SqlParameter[] pars = {
                                   
                                  new SqlParameter("@user_name",SqlDbType.VarChar),
                                  new SqlParameter ("@user_pwd",SqlDbType.VarChar),
                                  new SqlParameter ("@user_role",SqlDbType.VarChar),
                                  new SqlParameter ("@user_moblie",SqlDbType.VarChar),
                                  new SqlParameter ("@user_address",SqlDbType.VarChar),
                                  new SqlParameter ("@user_note",SqlDbType.VarChar),
                                  new SqlParameter ("@is_admin",SqlDbType.VarChar),
                                  new SqlParameter ("@user_regist_time",SqlDbType.DateTime),
                                  new SqlParameter("@user_code",SqlDbType.VarChar)
                                  };
                    pars[0].Value = this.txtUserName.Text;
                    pars[1].Value = this.txtPwd.Text;
                    pars[2].Value = this.ddlRole.SelectedValue;
                    pars[3].Value = this.txtTel.Text;
                    pars[4].Value = this.txtAddress.Text;
                    pars[5].Value = this.txtMark.Text;
                    pars[6].Value = "0";
                    pars[7].Value = DateTime.Now.ToString();
                    pars[8].Value = txtUserCode.Text.Trim();


                    sql = "insert into SYS_USER_INFO(user_name,user_pwd,user_role,user_mobile,user_address,user_note,is_admin,user_regist_time,user_code) ";
                    sql += " values(@user_name,@user_pwd,@user_role,@user_moblie,@user_address,@user_note,@is_admin,@user_regist_time,@user_code)";
                
                       sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql,pars);
                      
                    this.lbltitle.Text = JsAlertMessage.message4;
                }
            }

            catch (Exception ex)
            {
                //throw ex;
                this.lbltitle.Text = JsAlertMessage.message5;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }
    }
}