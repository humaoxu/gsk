﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using JSKWeb.Code;
using JSKWeb.Utility;
using DBUtility;


namespace JSKWeb.user
{
    public partial class UserMenuManager :  PageBase
    {
        SqlHelper sqlHelper = new SqlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            CommonStirng cm = new CommonStirng();
            string s_user = "";
            if (Request.Cookies["linxsanmpleuser"] != null && Request.Cookies["linxsanmpleuser"].Value != null)
            {
                s_user = Request.Cookies["linxsanmpleuser"].Value.ToString();
            }
            lblUserCode.Text = cm.GetUserName(s_user.Trim());
            if (!IsPostBack)
            {
                UserTreeBind();
                LoadInfo();
                tvUserMenu.Nodes[0].Expanded = true;
            }
        }

        private void LoadInfo()
        {
            if (tvUser.Nodes.Count > 0)
            {
                tvUser.Nodes[0].Select();
                this.SelectUser();
            }
        }

        private void SelectUser()
        {
            string sql = @"select um.menu_id,um.module_name,um.parent_id,um.order_id from sys_menu um   order by parent_id, order_id";
            DataSet dsMenu = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);


            string sqlUMenu = @"select * from msys_menu um  ";

            DataSet dsUMenu = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sqlUMenu);

            if (dsMenu != null && dsMenu.Tables[0].Rows.Count > 0)
            {
                LoadMenu(tvUserMenu.Nodes, "0", dsMenu.Tables[0], dsUMenu.Tables[0]);
                tvUserMenu.ExpandAll();
            }
        }

        private void LoadMenu(TreeNodeCollection treeNodeCollection, string p, DataTable ds, DataTable drs)
        {
            treeNodeCollection.Clear();
            DataRow[] drSub = ds.Select("parent_id='" + p + "'");
            foreach (DataRow dr in drSub)
            {
                TreeNode subNode = new TreeNode(dr["module_name"].ToString(), dr["menu_id"].ToString());
                treeNodeCollection.Add(subNode);

                subNode.Checked = drs.Select("menu_id='" + dr["menu_id"].ToString() + "'").Count() > 0;
                this.LoadMenu(subNode.ChildNodes, subNode.Value, ds, drs);
            }
        }

        public void UserTreeBind()
        {
            string sql = "SELECT  user_role_id  , user_role_name  FROM  sys_user_role order by  user_role_name ";
            DataSet ds= sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            tvUser.Nodes.Clear();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                TreeNode tn = new TreeNode(dr["user_role_name"].ToString(), dr["user_role_id"].ToString());
                tvUser.Nodes.Add(tn);
            }
        }

        public void MenuTreeBind()
        {
        }

        protected void tvRole_SelectedNodeChanged(object sender, EventArgs e)
        {
            SelectUser();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string roleID = InputText.GetStrByObj(this.ViewState["MenuID"]);
                if (string.IsNullOrEmpty(roleID) == false && tvUserMenu.Nodes.Count > 0)
                {
                    string sql = "delete  sys_role_menu where user_role_id='" + roleID + "' ";

                    sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    foreach (TreeNode tn in tvUserMenu.CheckedNodes)
                    {
                        if (tn.Checked)
                        {
                            sql = "insert into   sys_role_menu  ( user_role_id,menu_id)values('" + roleID + "','" + tn.Value + "') ";
                            sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                        }
                    }
                    JavaScript.Alert(this, "Menu save successfully!");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void tvUserMenu_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            selectParentNode(e.Node, e.Node.Checked);
            string menuid = "0";
            if (tvUserMenu.SelectedNode != null)
            {
                menuid = tvUserMenu.SelectedNode.Value;
            }
            hfKey.Value = menuid;
        }

        private void selectParentNode(TreeNode node, bool check)
        {

        }

        protected void btnAdd_Click(object sender, ImageClickEventArgs e)
        {
            string menuid = "0";
            if (tvUserMenu.SelectedNode != null)
            {
                menuid = tvUserMenu.SelectedNode.Value;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "js", "addMenu(" + menuid + ")", true);
            tvUserMenu.SelectedNode.Selected = false;
        }

        protected void btnModify_Click(object sender, ImageClickEventArgs e)
        {
            SelectUser();
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                string sql = "delete  sys_menu where menu_id='" + tvUserMenu.SelectedNode.Value + "' ";

                sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                SelectUser();
                JavaScript.Alert(this, "Delete successfully!");
            }
            catch (Exception)
            {

                JavaScript.Alert(this, "Delete failed!");
            }

            SelectUser();
            hfKey.Value = null;
        }

        protected void btnAdd_Click1(object sender, ImageClickEventArgs e)
        {
            SelectUser();
        }

        protected void tvUserMenu_SelectedNodeChanged(object sender, EventArgs e)
        {
            string menuid = "0";
            if (tvUserMenu.SelectedNode != null)
            {
                menuid = tvUserMenu.SelectedNode.Value;
            }
            hfKey.Value = menuid;
        }

        protected void tvUserMenu_TreeNodeExpanded(object sender, TreeNodeEventArgs e)
        {
            TreeNodeCollection ts = null;
            if (e.Node.Parent == null)
            {
                ts = ((TreeView)sender).Nodes;
            }
            else
            {
                ts = e.Node.Parent.ChildNodes;
            }
            foreach (TreeNode node in ts)
            {
                if (node != e.Node)
                {
                    node.Collapse();
                }
            }
        }

    }
}