﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestChart.aspx.cs" Inherits="JSKWeb.Chart.TestChart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="../js/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        
    </script>
</head>
<body>
    <form id="form2" runat="server">
    <input id="btn_chart" type="button" value="生成Chart" />
    <div id="main" style="height: 400px">
    </div>

    <!-- ECharts单文件引入 -->
    <%--<script src="http://echarts.baidu.com/build/dist/echarts.js"></script>--%>
    <script src="../js/echarts.js"></script>
    <script type="text/javascript">
        // 路径配置
        require.config({
            paths: {
                echarts: '../Chart/dist'
            }
        });

        
        $("#btn_chart").click(function () {
            $.ajax({
                type: "get",
                url: "../Chart/chart.ashx?id=1",
                data: null,
                success: function (msg) {
                    if (msg != "") {
                        var col_title = "";     //标题的列名
                        var col_data = [];      //显示值
                        var col_data_name = []; //显示标题

                        var json_data = null;   //初始变量
                        var myChart = null;     //初始变量
                        var option = null;      //初始变量

                        var chart_title = new Array(); //标题数组
                        var chart_data = new Array(); //值数组

                        json_data = eval(msg);


                        //列标题,列字段名取值
                        var col = 0;
                        for (var key in json_data[0]) {
                            if (col == 0)
                                col_title = key;
                            else {
                                col_data.push(key);
                                col_data_name.push(key);
                            }
                            col++;
                        }

                        //给值字段赋值
                        for (var i = 0; i < col_data.length; i++) {
                            chart_data[i] = {
                                "name": json_data[i],
                                "type": "bar",
                                "data": [] //[5, 20, 40, 10, 10, 20]
                            };
                        }

                        //填入标题及各值的数据
                        for (var i = 0; i < json_data.length; i++) {
                            chart_title.push(json_data[i]["otc_type"]);
                            for (var j = 0; j < col_data.length; j++) {
                                var col_name = col_data[j];
                                chart_data[j].data.push(json_data[i][col_name]);
                                //alert(chart_data[j])
                            };
                        };

                        // 使用
                        require(
                            [
                                'echarts',
                                'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
                            ],
                            function (ec) {
                                // 基于准备好的dom，初始化echarts图表
                                myChart = ec.init(document.getElementById('main'));

                                option = {
                                    tooltip: {
                                        show: true
                                    },
                                    legend: {
                                        padding: 1,
                                        itemGap : 10,
                                        data: col_data_name
                                    },
                                    xAxis: [
                                        {
                                            type: 'category',
                                            data: chart_title
                                        }
                                    ],
                                    yAxis: [
                                        {
                                            type: 'value'
                                        }
                                    ],
                                    series: chart_data
                                };

                                // 为echarts对象加载数据 
                                myChart.setOption(option);
                            }

                            
                        );

                    }
                }
            });
        });
       
    </script>
    </form>
</body>