﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="Fmcg_Index.aspx.cs" Inherits="JSKWeb.Fmcg_Index" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div style="margin: 0px; width: 100%; height: 100%">
        <table width="100%">
            <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>分数报告</span><a href="REPORTFMCG/RPTFMCGSCORE.aspx"><img width="140px" height="140px" src="otcimg/icon01.png" /></a></li>
                            <li class="smallbutton"><span>指标说明</span><a href="http://106.14.254.140:8080/fmcg_help.pptx"><img  width="140px" height="140px" src="otcimg/help.png" /></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="tablebody">
                        <ul class="buttonicon">
                            <li class="smallbutton"><span>分销率</span><a href="REPORTFMCG/RPTFMCG_FXlv.aspx"><img  width="140px" height="140px"  src="otcimg/icon03.png" /></a></li>
                            <li class="smallbutton"><span>全分销达标率 </span><a href="../REPORTFMCG/RPTFMCG_FXDBL.aspx"><img  width="140px" height="140px"  src="otcimg/ddt01.png" /></a></li>
                            <li class="smallbutton"><span>排面份额</span><a href="REPORTFMCG/RPTFMCG_PMFE.aspx"><img  width="140px" height="140px"  src="otcimg/icon05.png" /></a></li>
                            <li class="smallbutton"><span>有效排面数</span><a href="../REPORTFMCG/RPTFMCG_PMS.aspx"><img  width="140px" height="140px"  src="otcimg/ddt01.png" /></a></li>
                            <li class="smallbutton"><span>排面达标率</span><a href="../REPORTFMCG/RPTFMCG_PMDBL.aspx"><img  width="140px" height="145px"  src="otcimg/icon06.png" /></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
