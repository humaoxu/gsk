﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JSKWeb.Enum
{
    public enum PrincipalEnum
    {
        UserId = 0,
        UserCode = 1, 
        IsAdm=2,
        User_Mobile = 3,
        UserType = 4,
        user_address = 5
    }
}
