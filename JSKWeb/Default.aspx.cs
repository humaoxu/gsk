﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JSKWeb.Code;
using System.Data;
using System.Data.SqlClient;
using DBUtility;
using JSKWeb.Utility;
using System.Runtime.InteropServices;
using System.Text;

namespace JSKWeb
{
    public partial class Default : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();   
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {

                string sql = "select * from sys_user_info where user_code=@user_code and user_pwd=@user_pwd ";
                SqlParameter[] pars = {
                                  new SqlParameter("@user_code",SqlDbType.VarChar),
                                  new SqlParameter ("@user_pwd",SqlDbType.VarChar),
                                  };

                pars[0].Value = txtUsername.Value.Trim();
                pars[1].Value = txtPassword.Value.Trim();
                DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql, pars);

                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    //var userId = (string)ds.Tables[0].Rows[0]["user_code"];
                    //LoginCounter.UserLogin(userId.ToString());
                    //Session["UserCode"] = userId;
                    string so = txtUsername.Value.Trim();
                    Cookie.Delete("linxsanmpleuser");
                    HttpCookie cookie_user = new HttpCookie("linxsanmpleuser");
                    cookie_user.Value = so;
                    cookie_user.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie_user);

                    Cookie.Delete("_UserRole");
                    HttpCookie cookie_userrole = new HttpCookie("_UserRole");
                    cookie_userrole.Value = ds.Tables[0].Rows[0]["USER_ROLE"].ToString();
                    cookie_userrole.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie_userrole);

                    Cookie.Delete("_UserName");
                    HttpCookie cookie_username = new HttpCookie("_UserName");
                    cookie_username.Value = HttpUtility.UrlEncode(ds.Tables[0].Rows[0]["USER_NAME"].ToString());
                    cookie_username.Expires = DateTime.Now.AddDays(1);
                    Response.Cookies.Add(cookie_username);
                    this.Response.Redirect("Welcome.aspx", false);
                    InputText.InsertLog(txtUsername.Value, "登录成功", "");

                }
                else
                {
                    this.txtPassword.Value = "";
                    InputText.InsertLog(txtUsername.Value, "登录失败", "用户名与密码不匹配");
                    JavaScript.Alert(this, "用户名与密码不匹配，请重试");
                }
            }
            catch (Exception ex)
            {
                InputText.InsertLog(txtUsername.Value, "登录失败", ex.Message);
                JavaScript.Alert(this, ex.Message);
            }
        }

    }
}