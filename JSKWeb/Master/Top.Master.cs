﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.Security;
using JSKWeb.Code;
using DBUtility;
namespace JSKWeb.Master
{
    public partial class Top : System.Web.UI.MasterPage
    {
        SysMenuManager syMenuManager = new SysMenuManager();
        private string parent_id = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            string s_user = "";
            if (Request.Cookies["linxsanmpleuser"] != null && Request.Cookies["linxsanmpleuser"].Value != null)
            {
                s_user = Request.Cookies["linxsanmpleuser"].Value.ToString();
            }
            string s_role = "";
            if (Request.Cookies["_UserRole"] != null && Request.Cookies["_UserRole"].Value != null)
            {
                s_role = Request.Cookies["_UserRole"].Value.ToString();
            }

            CommonTitleBar cmT = new CommonTitleBar();
            GetUser();
            nav.InnerHtml = cmT.WriteLeft("", s_user);
            if (nav.InnerHtml.Trim().Length == 0)
            {
                Response.Redirect("Default.aspx");
            }
        }

        public void GetUser()
        { 
            string s_user = "";
            if (Request.Cookies["linxsanmpleuser"] != null && Request.Cookies["linxsanmpleuser"].Value != null)
            {
                s_user = Request.Cookies["linxsanmpleuser"].Value.ToString();
            }
            

            string s_username = "";
            if (Request.Cookies["_UserName"] != null && Request.Cookies["_UserName"].Value != null)
            {
                s_username =HttpUtility.UrlDecode( Request.Cookies["_UserName"].Value.ToString() );
            } 

            lblUserCode.Text = s_user+"_"+s_username;
        }
 
    }
}