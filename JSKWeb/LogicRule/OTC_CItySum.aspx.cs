﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;

namespace JSKWeb.Report
{
    public partial class OTC_CItySum : WebForm2
    {
        SqlHelper sqlhelper = new SqlHelper();
        int iPageSize = 20;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // TicketTypeDataBind();
                //this.txtEdate.Text = DateTime.Now.ToString("yyyyMMdd");
                //this.txtSdate.Text = DateTime.Now.AddDays(-InputText.GetInt(InputText.GetConfig("gsk_selectday"))).ToString("yyyyMMdd");
            }
        }

        public void TicketTypeDataBind()
        {
        }

        private bool checkDateTimeFormat(string txtTime)
        {
            try
            {
                string sDate = txtTime.Substring(0, 4) + "-" + txtTime.Substring(4, 2) + "-" + txtTime.Substring(6, 2);
                DateTime.Parse(sDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }


        protected void LoadInfo()
        {
          
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["resultId"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");

                HyperLink btnsm = (HyperLink)e.Row.FindControl("lbtnMingxi");
                StringBuilder sb = new StringBuilder();
                sb.Append(InputText.GetConfig("gsk_url"));
                sb.Append("resultId=");
                sb.Append(drv["resultId"].ToString());
                sb.Append("&paperId=");
                sb.Append(drv["paperId"].ToString());
                btnsm.NavigateUrl = @"javascript:void window.open('" + sb.ToString() + "')";


                DateTime dtS = InputText.GetDateTime(drv["scdate"].ToString());

                ///屏蔽申诉 如果 超时则屏蔽 或 已申诉
                if (drv["Ticketstaus"].ToString().Trim() != "未申诉" || (InputText.GetInt(InputText.GetConfig("outDay")) < DateTime.Compare(DateTime.Now, dtS)))
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                    btn.Visible = false;
                }
                else
                {
                    ///申诉url
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShensu");
                    btn.Visible = true;
                    string strUrlsu = "SysTicketDet.aspx?resultId=" + drv["resultId"] + "&projectId=" + drv["projectId"];
                    btn.NavigateUrl = @"javascript:void window.open('" + strUrlsu + "')";
                }

                if (drv["Ticketstaus"].ToString().Trim() == "已处理")
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = true;
                    btn.NavigateUrl = @"javascript:void window.open('SysTicketReBackDet.aspx?resultId=" + drv["resultId"].ToString() + "')";
                }
                else
                {
                    ///屏蔽申诉
                    HyperLink btn = (HyperLink)e.Row.FindControl("lbtnShuoming");
                    btn.Visible = false;
                }
            }
        }
    }
}