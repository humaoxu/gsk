﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="OTC_CItySum.aspx.cs" Inherits="JSKWeb.Report.OTC_CItySum" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    
    <script src="../js/echarts.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                document.getElementById("<%=hfstorecode.ClientID %>").value = akey;
                document.getElementById("<%=hfdateid.ClientID %>").value = $(row.cells[2]).text();
                document.getElementById("<%=hfRound.ClientID %>").value = $(row.cells[1]).text();
                // row.cells[0].innerHTML = "<input type=\"radio\" value=\"\" name=\"rd\" checked='checked' />";
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {
                var json = { "oo": [
                { "url": "Application_Add.aspx", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                { "url": "Application_Edit.aspx?ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                {},
                { "url": "Logic_Rule.aspx?TYPE=Default&ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" }
                ]

                };

                if (index == 0) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 1) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 2) {
                    return confirm("Confirm delete？");
                } else if (index == 3) {
                    return confirm("Confirm Run？");
                } else if (index == 5) {//Set out
                    var result = window.open(json.oo[4].url + key, window, json.oo[4].parm);

                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                else {
                    return false;
                }
            }
        }
    
    </script>
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        .auto-style1 {
            width: 170px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
      <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="">
            <tbody>
                <tr>
                    <td align="right" width="80px" class="contrlFontSize">
                        问卷批次：
                    </td>
                    <td align="left" width="180px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        零售大区：
                    </td>
                    <td align="left" class="auto-style1">
                        <asp:DropDownList ID="DropDownList1" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        城市：
                    </td>
                    <td align="left" width="0px" >
                        <asp:DropDownList ID="DropDownList2" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>&nbsp;&nbsp;
                        <asp:Button ID="Button1"  CssClass="btn" 
                            runat="server"  Text="查询" Width="105px" /> 
                    </td>
                    <td align="left"  >
                    </td>
                </tr>
                
            </tbody>
        </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="dataMian" style="display: none;" >
        <div>
        
            <cc1:GridViewKit ID="gvMain" runat="server" AutoGenerateColumns="False" Width="100%"
                OnRowDataBound="gvMain_RowDataBound" Font-Size="9pt"  CssClass="tablestyle" EnableEmptyContentRender="True"
                CellPadding="2">
                <RowStyle />
                <Columns>
                    <asp:BoundField DataField="shopcode" HeaderText="店铺编码">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="10px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="shopname" HeaderText="店铺名称">
                        <HeaderStyle HorizontalAlign="Center" Width="250px"  />
                        <ItemStyle HorizontalAlign="left" Width="250px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="t_score" HeaderText="总分">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="right" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SSDFX" HeaderText="舒适达分销">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="right" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BLJFX" HeaderText="保丽净分销">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="right" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="YZSFX" HeaderText="益周适分销">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="right" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SSDPM" HeaderText="舒适达排面">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="right" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="BLJPM" HeaderText="保丽净排面">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="right" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="YZSPM" HeaderText="益周适排面">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="right" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="scdate" HeaderText="上传时间">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="left" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="csdate" HeaderText="申诉时间">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="left" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="godate" HeaderText="反馈时间">
                        <HeaderStyle HorizontalAlign="Center" Width="80px" />
                        <ItemStyle HorizontalAlign="left" Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Ticketstaus" HeaderText="状态">
                        <HeaderStyle HorizontalAlign="Center" Width="100px" />
                        <ItemStyle HorizontalAlign="left" Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="操作">
                        <HeaderStyle HorizontalAlign="Center" Width="150" />
                        <ItemStyle HorizontalAlign="left" Width="150" />
                        <ItemTemplate> 
                            <asp:HyperLink ID="lbtnMingxi" runat="server">明细</asp:HyperLink></u>
                            <asp:HyperLink ID="lbtnShensu"   runat="server">申诉</asp:HyperLink>
                            <asp:HyperLink ID="lbtnShuoming" runat="server">反馈结果</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle />
                <AlternatingRowStyle />
            </cc1:GridViewKit>


        </div>
        <div>
            <asp:HiddenField ID="hfMinDate" runat="server" />
            <asp:HiddenField ID="hfMaxDate" runat="server" />
            <asp:HiddenField ID="hfstorecode" runat="server" />
            <asp:HiddenField ID="hfdateid" runat="server" />
            <asp:HiddenField ID="hfKey" runat="server" />
            <asp:HiddenField ID="hfRound" runat="server" />
            <webdiyer:AspNetPager ID="Pager" runat="server" HorizontalAlign="Right" Width="100%"
                Style="font-size: 14px; color: Black;" AlwaysShow="true" FirstPageText="137"
                LastPageText="138" NextPageText="131" PrevPageText="132" SubmitButtonClass=""
                CustomInfoStyle="font-size:12px;text-align:Left;" TextBeforeInputBox="" TextAfterInputBox=""
                PageIndexBoxType="TextBox" ShowPageIndexBox="Always" TextAfterPageIndexBox="页"
                TextBeforePageIndexBox="转到" Font-Size="12px" ShowCustomInfoSection="Left" CustomInfoSectionWidth=""
                PagingButtonSpacing="3px" CustomInfoHTML="" CssClass="paginator" CurrentPageButtonClass="cpb"
                ShowInputBox="Never" CustomInfoClass="" OnPageChanged="pager_PageChanged" ButtonImageExtension=".png"
                ImagePath="~/images/" PagingButtonType="Image" NumericButtonType="Text" MoreButtonType="Text">
            </webdiyer:AspNetPager>
        </div>
        <div style="display: none;" runat="server" id="viewstatesql">
        </div>
    </div>
    <div id="main" style="width: 100%;height:400px;">
       
    </div>

      <script type="text/javascript">

        option = {
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    legend: {
        data: ['渗透', '陈列', '价格', '推荐', 'POSM', '渗透附加']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'value'
        }
    ],
    yAxis : [
        {
            type : 'category',
            data : ['东区','南区','西区','北区']
        }
    ],
    series : [
        {
            name:'渗透',
            type:'bar',
            stack: '分数',
            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
            data:[2.61, 2.43, 2.79, 2.66]
        },
        {
            name:'陈列',
            type:'bar',
            stack: '分数',
            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
            data:[0.4, 0.34, 0.39, 0.32]
        },
        {
            name:'价格',
            type:'bar',
            stack: '分数',
            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
            data:[0.13, 0.06, 0, 0.03]
        },
        {
            name: '推荐',
            type:'bar',
            stack: '分数',
            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
            data:[0.16, 0.21, 0.22, 0.14]
        },
        {
            name:'POSM',
            type:'bar',
            stack: '分数',
            itemStyle : { normal: {label : {show: true, position: 'insideRight'}}},
            data:[0.13, 0.45, 0.23, 0.27]
        },
        {
            name: '渗透附加',
            type: 'bar',
            stack: '分数',
            itemStyle: { normal: { label: { show: true, position: 'insideRight' } } },
            data: [0.06, 0.04, 0.05, 0.07]
        }
    ]
        };
        var myChart = echarts.init(document.getElementById('main'));
        myChart.setOption(option);
  </script>
</asp:Content>

