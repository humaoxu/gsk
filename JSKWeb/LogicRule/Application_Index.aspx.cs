﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JSKWeb.Code;
using System.Configuration;
//using DBBase;
using DBUtility;

namespace JSKWeb.LogicRule
{
    public partial class Application_Index : PageBase
    {
        private int iPageSize = 20;
        SqlHelper sqlhelper=new SqlHelper() ;
       // DBUtil ora = new DBUtil();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                iPageSize = Int32.Parse(ConfigurationManager.AppSettings["DefaultPageSize"].ToString().Trim());
            }
            catch
            {
                //eat it
            }
            CommonStirng cm = new CommonStirng();

            if (Request.QueryString["p"] != null)
            {
                iPageSize = Int32.Parse(Request.QueryString["p"].ToString());
            }

            if (!IsPostBack)
            {
                LoadInfo();
            }
        }

        protected void LoadInfo()
        {
            Pager.PageSize = iPageSize;
            int recCnt;

            try
            {
                ViewState["whereName"] = null;

                if (ViewState["whereName"] == null)
                {
                    ViewState["whereName"] = "1=1 ";
                }

                DataTable dt = sqlhelper.GetPagingRecord("Ap_view", "seqid", Pager.PageSize, Pager.CurrentPageIndex, ViewState["whereName"].ToString(), "*", out recCnt);
                Pager.RecordCount = recCnt;
                Pager.Visible = recCnt > 0;

                this.Pager.CustomInfoHTML = string.Format(CommonStirng.StrPagerCustomerInfo, new object[] { this.Pager.CurrentPageIndex, this.Pager.PageCount, this.Pager.RecordCount, iPageSize });
                gvMain.DataSource = dt;
                gvMain.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                DataRowView drv = e.Row.DataItem as DataRowView;
                e.Row.Attributes.Add("onclick", string.Format("gv_selectRow(this,'{0}')", drv["SEQID"].ToString()));
                e.Row.Attributes.Add("onmouseover", "gv_mouseHover(this)");
            }
        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            Pager.CurrentPageIndex = 1;
            LoadInfo();
        }

        protected void pager_PageChanged(object sender, EventArgs e)
        {
            LoadInfo();
        }

        protected void btnDel_Click(object sender, EventArgs e)
        {
            try
            {

                string sql = "DELETE FROM SYS_APPLICATION WHERE SEQID = " + this.hfKey.Value;

                sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                LoadInfo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}