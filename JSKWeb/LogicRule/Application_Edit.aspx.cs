﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JSKWeb.Code;
//using DBBase;
using System.Data;
using DBUtility;

namespace JSKWeb.LogicRule
{
    public partial class Application_Edit : PageBase
    {
       // DBUtil ora = new DBUtil();
        SqlHelper sqlHelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.hfKey.Value = Request["ID"];
                TypeBind();
                LoadInfo();
            }
        }
        public void TypeBind()
        {
            string sql = " select * from SYS_APPLICATION_TYPE where 1=1 order by type_code ";
            DataSet ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ddlType.DataSource = ds;
                ddlType.DataTextField = "type_name";
                ddlType.DataValueField = "type_code";
                ddlType.DataBind();
            }
        }

        protected void LoadInfo()
        {
            try
            {
                string sql = "SELECT SEQID,DESCRIPTION,VALUE FROM SYS_APPLICATION WHERE SEQID = " + this.hfKey.Value;

                DataSet ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    this.txt_seqid.Text = ds.Tables[0].Rows[0][0].ToString();
                    //this.txt_description.Text = dt.Rows[0][1].ToString();
                    this.ddlType.SelectedValue = ds.Tables[0].Rows[0][1].ToString();
                    this.txt_value.Text = ds.Tables[0].Rows[0][2].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = "UPDATE SYS_APPLICATION SET DESCRIPTION = '" + this.ddlType.SelectedItem.Text + "',TYPE_CODE='" + this.ddlType.SelectedValue + "',VALUE = '" + this.txt_value.Text + "' WHERE SEQID = " + this.hfKey.Value;

                sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql);


                this.lbl_res.Text = JsAlertMessage.message4;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            //this.txt_description.Text = string.Empty;
            this.txt_value.Text = string.Empty;
            this.lbl_res.Text = string.Empty;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Application_Index.aspx"); 
        }
    }
}