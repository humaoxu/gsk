﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true"
    CodeBehind="Application_Edit.aspx.cs" Inherits="JSKWeb.LogicRule.Application_Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script type="text/javascript">
        function validate() {
            if (document.getElementById("<%=txt_value.ClientID %>").value == "") {
                alert("Value not null!");
                return false;
            }

            return true;

        }  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <asp:HiddenField ID="hfKey" runat="server" />
    <div style="width: 100%" align="left">
        <table width="400px" style="text-align: left; font-size: 12px;" cellspacing="2px">
            <tr>
                <td>
                    <table width="90%" style="text-align: center">
                        <tr>
                            <td width="50%" align="right">
                                <label>
                                    SEQUENCE：
                                </label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_seqid" runat="server" MaxLength="20" Width="200px" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <label>
                                    DESCRIPTION：
                                </label>
                            </td>
                            <td>
                                <%--<asp:TextBox ID="txt_description" runat="server" MaxLength="20" Width="200px"></asp:TextBox>--%>
                                <asp:DropDownList ID="ddlType" runat="server" Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="50%" align="right">
                                <label>
                                    VALUE：
                                </label>
                            </td>
                            <td>
                                <asp:TextBox ID="txt_value" runat="server" MaxLength="200" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lbl_res" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn" Text="EDIT" Width="60px" 
                                    OnClientClick="return validate();" onclick="btnSave_Click" />
                                &nbsp;&nbsp;&nbsp;&nbsp
                                <asp:Button ID="btnBack" runat="server" CssClass="btn" Text="BACK" Width="60px" 
                                    onclick="btnBack_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
