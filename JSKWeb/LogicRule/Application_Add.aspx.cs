﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JSKWeb.Code;
using System.Data;
//using DBBase;
//using System.Data.OracleClient;
using System.Data.SqlClient;
using DBUtility;

namespace JSKWeb.LogicRule
{
    public partial class Application_Add : PageBase
    {
        //DBUtil ora = new DBUtil();
        SqlHelper sqlHelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadInfo();
                TypeBind();
            }
        }

        public void TypeBind()
        {
            string sql = " select * from SYS_APPLICATION_TYPE where 1=1 order by type_code ";
            DataSet ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ddlType.DataSource = ds;
                ddlType.DataTextField = "type_name";
                ddlType.DataValueField = "type_code";
                ddlType.DataBind();
            }
        }

        protected void LoadInfo()
        {
            try
            {
                string sql = "SELECT MAX(SEQID) AS SEQID FROM SYS_APPLICATION";
                DataSet ds = sqlHelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0][0].ToString()))
                {
                    Int32 logic_code = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                    logic_code++;
                    this.txt_seqid.Text = logic_code.ToString();
                }
                else
                {
                    this.txt_seqid.Text = "1";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string s_user = "";
            if (Request.Cookies["linxsanmpleuser"] != null && Request.Cookies["linxsanmpleuser"].Value != null)
            {
                s_user = Request.Cookies["linxsanmpleuser"].Value.ToString();
            }
            try
            {
                SqlParameter[] parameters = new SqlParameter[6];

                string sql = "INSERT INTO SYS_APPLICATION(SEQID,DESCRIPTION,VALUE,CREATE_DT,CREATE_BY,TYPE_CODE) VALUES (@SEQID,@DESCRIPTION,@VALUE,@CREATE_DT,@CREATE_BY,@TYPE_CODE)";

                parameters[0] = new SqlParameter("@SEQID",SqlDbType.Int);
                parameters[1] = new SqlParameter("@DESCRIPTION", SqlDbType.VarChar);
                parameters[2] = new SqlParameter("@VALUE", SqlDbType.VarChar);
                parameters[3] = new SqlParameter("@CREATE_DT", SqlDbType.DateTime);
                parameters[4] = new SqlParameter("@CREATE_BY", SqlDbType.VarChar);
                parameters[5] = new SqlParameter("@TYPE_CODE", SqlDbType.Int);

                parameters[0].Value = Convert.ToInt32(this.txt_seqid.Text);
                parameters[1].Value = this.ddlType.SelectedItem.Text;
                parameters[2].Value = this.txt_value.Text;
                parameters[3].Value = DateTime.Now;
                parameters[4].Value = s_user.Trim();
                parameters[5].Value = this.ddlType.SelectedValue;

                sqlHelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql,parameters);

                this.lbl_res.Text = JsAlertMessage.message4;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            this.txt_value.Text = string.Empty;
            this.lbl_res.Text = string.Empty;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Application_Index.aspx"); 
        }
    }
}