﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true"
    CodeBehind="Application_Index.aspx.cs" Inherits="JSKWeb.LogicRule.Application_Index" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
            document.getElementById("<%=hfKey.ClientID %>").value = akey;

            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                row.cells[0].innerHTML = "<input type=\"radio\" value=\"\" name=\"rd\" checked='checked' />";
            }
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
            var key = document.getElementById("<%=hfKey.ClientID %>").value;
            if ((typeof (key) == "undefined" || key == "") && index != 0) {
                alert('First select a row to operate！');
                return false;
            }
            else {
                var json = { "oo": [
                { "url": "Application_Add.aspx", "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                { "url": "Application_Edit.aspx?ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" },
                {},
                { "url": "Logic_Rule.aspx?TYPE=Default&ID=" + key, "parm": "Height:600px;Width:700px;status:no;help:no;scroll:no" }
                ]

                };

                if (index == 0) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 1) {
                    window.location.href = json.oo[index].url;

                    return false;
                }
                else if (index == 2) {
                    return confirm("Confirm delete？");
                } else if (index == 3) {
                    return confirm("Confirm Run？");
                } else if (index == 5) {//Set out
                    var result = window.open(json.oo[4].url + key, window, json.oo[4].parm);

                    if (typeof (result) == "undefined" || result == "") {
                        return false;
                    }
                    else {
                        return true;
                    }
                }

                else {
                    return false;
                }
            }
        }
        $(document).ready(function () {
            $("#sel_load").change(function () {
                var p = $(this).children('option:selected').val();
                window.location.href = "Report_Store.aspx?p=" + p;

            });
        })
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>Filters</legend>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="formstyle">
        </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="dataMian">
        <div>
            <cc1:GridViewKit ID="gvMain" runat="server" AutoGenerateColumns="False" Width="100%"
                OnRowDataBound="gvMain_RowDataBound" Font-Size="9pt" DataKeyNames="SEQID" CssClass="tablestyle"
                EnableEmptyContentRender="True" CellPadding="2">
                <RowStyle />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle HorizontalAlign="Center" Width="30" />
                        <ItemTemplate>
                            <input type="radio" value="" name="rd" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SEQID" HeaderText="SEQUENCE">
                        <HeaderStyle HorizontalAlign="Center" Width="80" />
                        <ItemStyle HorizontalAlign="left" Width="80" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DESCRIPTION" HeaderText="DESCRIPTION">
                        <HeaderStyle HorizontalAlign="Center" Width="80" />
                        <ItemStyle HorizontalAlign="left" Width="80" />
                    </asp:BoundField>
                    <asp:BoundField DataField="VALUE" HeaderText="VALUE">
                        <HeaderStyle HorizontalAlign="Center" Width="80" />
                        <ItemStyle HorizontalAlign="left" Width="80" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle />
                <AlternatingRowStyle />
            </cc1:GridViewKit>
        </div>
        <div>
            <asp:HiddenField ID="hfKey" runat="server" />
            <webdiyer:AspNetPager ID="Pager" runat="server" HorizontalAlign="Right" Width="100%"
                Style="font-size: 14px; color: Black;" AlwaysShow="true" FirstPageText="137"
                LastPageText="138" NextPageText="131" PrevPageText="132" SubmitButtonClass=""
                CustomInfoStyle="font-size:12px;text-align:Left;" TextBeforeInputBox="" TextAfterInputBox=""
                PageIndexBoxType="TextBox" ShowPageIndexBox="Always" TextAfterPageIndexBox="页"
                TextBeforePageIndexBox="转到" Font-Size="12px" ShowCustomInfoSection="Left" CustomInfoSectionWidth=""
                PagingButtonSpacing="3px" CustomInfoHTML="" CssClass="paginator" CurrentPageButtonClass="cpb"
                ShowInputBox="Never" CustomInfoClass="" OnPageChanged="pager_PageChanged" ButtonImageExtension=".png"
                ImagePath="~/images/" PagingButtonType="Image" NumericButtonType="Text" MoreButtonType="Text">
            </webdiyer:AspNetPager>
        </div>
    </div>
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tabletitle">
        <tbody>
            <tr>
                <td align="left">
                    <asp:Button ID="btnAdd" CssClass="btn" runat="server" Text="CREATE"  Visible="false"
                        OnClientClick="return operate(0)"  />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnEdit" CssClass="btn" runat="server" Text="EDIT"  Visible="false"
                        OnClientClick="return operate(1)"  />
                    &nbsp;&nbsp;
                    <asp:Button ID="btnDel" CssClass="btn" runat="server" Text="DELETE"  Visible="false" OnClientClick="return operate(2)"
                        OnClick="btnDel_Click" />
                </td>
            </tr>
        </tbody>
    </table>
</asp:Content>
