﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;

namespace JSKWeb.Report
{
    public partial class GSK_OTC_ITEM_JG : System.Web.UI.Page
    {
        public string itemName = "";
        public string itemValue = "";
        public string itemTitle = "";

        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");

                TicketTypeDataBind();
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;

                /*zpl_td1.Visible = false;
                zpl_td2.Visible = false;

                pp_td1.Visible = false;
                pp_td2.Visible = false;

                nm_td1.Visible = false;
                nm_td2.Visible = false;*/
            }
        }

        public void TicketTypeDataBind()
        {

            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();


            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            //----------2016.6.21---------
            ddlPL.DataSource = null;
            ddlZPL.DataSource = null;
            dllPP.DataSource = null;
            ddlCPMC.DataSource = null;

            ddlType2.Items.Clear();
            ddlType3.Items.Clear();
            ddlType4.Items.Clear();
            //----------2016.6.21---------
            ddlPL.Items.Clear();
            ddlZPL.Items.Clear();
            dllPP.Items.Clear();
            ddlCPMC.Items.Clear();

            select_sql = " SELECT COL25 CODE,COL26 NAME FROM ( SELECT COL25,COL26, ROW_NUMBER () OVER (PARTITION BY COL26 ORDER BY COL26) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL25 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }

            select_sql = " SELECT COL27 CODE,COL28 NAME FROM ( SELECT COL27,COL28, ROW_NUMBER () OVER (PARTITION BY COL27 ORDER BY COL27) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL27 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType3.DataSource = select_ds;
                ddlType3.DataValueField = "code";
                ddlType3.DataTextField = "name";
                ddlType3.DataBind();
            }

            select_sql = " SELECT COL19 CODE,COL20 NAME FROM ( SELECT COL19,COL20, ROW_NUMBER () OVER (PARTITION BY COL19 ORDER BY COL19) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL19 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType4.DataSource = select_ds;
                ddlType4.DataValueField = "code";
                ddlType4.DataTextField = "name";
                ddlType4.DataBind();
            }

            var ddlItmeTypeSelectSql = String.Format("SELECT DISTINCT LV1_CODE ,LV1_NAME  FROM SYS_OTC_ITEM WHERE PAPERID ={0} ORDER BY LV1_CODE ", ddlTicketType.SelectedValue);
            var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
            ddlPL.DataSource = ddlItmeTypeDataSet;
            ddlPL.DataValueField = "LV1_CODE";
            ddlPL.DataTextField = "LV1_NAME";
            ddlPL.DataBind();

            //----------2016.6.21---------
            ddlPL.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlZPL.Items.Insert(0, new ListItem("全部", string.Empty));
            dllPP.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlCPMC.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPL.DataSource = null;
            ddlZPL.DataSource = null;
            dllPP.DataSource = null;
            ddlCPMC.DataSource = null;

            ddlPL.Items.Clear();
            ddlZPL.Items.Clear();
            dllPP.Items.Clear();
            ddlCPMC.Items.Clear();

            var ddlItmeTypeSelectSql = String.Format("SELECT DISTINCT LV1_CODE ,LV1_NAME  FROM SYS_OTC_ITEM WHERE PAPERID ={0} ORDER BY LV1_CODE ", ddlTicketType.SelectedValue);
            var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
            ddlPL.DataSource = ddlItmeTypeDataSet;
            ddlPL.DataValueField = "LV1_CODE";
            ddlPL.DataTextField = "LV1_NAME";
            ddlPL.DataBind();

            ddlPL.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlZPL.Items.Insert(0, new ListItem("全部", string.Empty));
            dllPP.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlCPMC.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlPL_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlZPL.DataSource = null;
            dllPP.DataSource = null;
            ddlCPMC.DataSource = null;

            ddlZPL.Items.Clear();
            dllPP.Items.Clear();
            ddlCPMC.Items.Clear();

            if (!ddlPL.SelectedItem.Text.Trim().Equals("全部"))
            {
                var ddlItmeTypeSelectSql = String.Format("SELECT DISTINCT LV2_CODE ,LV2_NAME  FROM SYS_OTC_ITEM WHERE LV1_CODE ={0} ORDER BY LV2_CODE ", ddlPL.SelectedValue);
                var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
                ddlZPL.DataSource = ddlItmeTypeDataSet;
                ddlZPL.DataValueField = "LV2_CODE";
                ddlZPL.DataTextField = "LV2_NAME";
                ddlZPL.DataBind();
            }

            ddlZPL.Items.Insert(0, new ListItem("全部", string.Empty));
            dllPP.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlCPMC.Items.Insert(0, new ListItem("全部", string.Empty));


        }


        protected void ddlZPL_SelectedIndexChanged(object sender, EventArgs e)
        {
            dllPP.DataSource = null;
            ddlCPMC.DataSource = null;

            dllPP.Items.Clear();
            ddlCPMC.Items.Clear();


            if (!ddlZPL.SelectedItem.Text.Trim().Equals("全部"))
            {
                var ddlItmeTypeSelectSql = String.Format("SELECT DISTINCT LV3_CODE ,LV3_NAME  FROM SYS_OTC_ITEM WHERE LV2_CODE ={0} ORDER BY LV3_CODE ", ddlZPL.SelectedValue);
                var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
                dllPP.DataSource = ddlItmeTypeDataSet;
                dllPP.DataValueField = "LV3_CODE";
                dllPP.DataTextField = "LV3_NAME";
                dllPP.DataBind();
            }

            dllPP.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlCPMC.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void dllPP_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCPMC.DataSource = null;
            ddlCPMC.Items.Clear();

            if (!dllPP.SelectedItem.Text.Trim().Equals("全部"))
            {
                var ddlItmeTypeSelectSql = String.Format("SELECT DISTINCT LV4_CODE ,LV4_NAME  FROM SYS_OTC_ITEM WHERE LV3_CODE ={0} ORDER BY LV4_CODE ", dllPP.SelectedValue);
                var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
                ddlCPMC.DataSource = ddlItmeTypeDataSet;
                ddlCPMC.DataValueField = "LV4_CODE";
                ddlCPMC.DataTextField = "LV4_NAME";
                ddlCPMC.DataBind();
            }

            ddlCPMC.Items.Insert(0, new ListItem("全部", string.Empty));
        }


        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";

            select_sql = "SELECT DISTINCT  col27 CODE,COL28 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col27 IS NOT NULL ";
            select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType3.DataSource = select_ds;
                ddlType3.DataValueField = "code";
                ddlType3.DataTextField = "name";
                ddlType3.DataBind();
            }
            /*if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
            {
                select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' ";

                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType3.DataSource = select_ds;
                    ddlType3.DataValueField = "code";
                    ddlType3.DataTextField = "name";
                    ddlType3.DataBind();
                }
            }



            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));*/

        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;
            ddlType4.Items.Clear();
            DataSet select_ds = new DataSet();
            var select_sql = "";


            select_sql = "SELECT DISTINCT  col19 CODE,COL20 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col19 IS NOT NULL ";
            select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' and col27 = '" + ddlType3.SelectedValue + "' ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType4.DataSource = select_ds;
                ddlType4.DataValueField = "code";
                ddlType4.DataTextField = "name";
                ddlType4.DataBind();

            }
            /*if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
            {
                select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' and col27 = '" + ddlType3.SelectedValue + "' ";

                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType4.DataSource = select_ds;
                    ddlType4.DataValueField = "code";
                    ddlType4.DataTextField = "name";
                    ddlType4.DataBind();

                }
            }



            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));*/

        }

        protected void ddlDim_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDim.SelectedValue.ToString().Trim() == "0")
            {
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;


            }

            if (ddlDim.SelectedValue.ToString().Trim() == "1")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "2")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "3")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = true;
                td_td2.Visible = true;

            }

            //ddlType2.SelectedIndex = 0;
            //ddlType3.SelectedIndex = 0;
            //ddlType4.SelectedIndex = 0;
        }

        //加载数据
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.content.Visible = true;
            string className = string.Empty;  //css
            try
            {
                StringBuilder sb = new StringBuilder();
                //获取MBD
                string sql = "";//"select ItemName,DataValue from Sys_Otc_ItemValue where datatype = '分销'";
                StringBuilder sb_mbd_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();
                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_mbd_sql.Append(" and TypeCode = 'GSKOTC001'");
                }
                else if (ddlDim.SelectedIndex == 1) //渠道
                {
                    if (ddlType2.SelectedIndex == 0)
                        sb_mbd_sql.Append(" and TypeCode = 'GSKN02RTNM001'");
                    else
                        sb_mbd_sql.Append(" and TypeCode = 'GSKN02RTNM002'");
                }
                else if (ddlDim.SelectedIndex == 2) //渠道
                {
                    sb_mbd_sql.Append(" and TypeCode = '" + this.ddlType3.SelectedValue + "'");
                }
                else if (ddlDim.SelectedIndex == 3) //团队
                {
                    sb_mbd_sql.Append(" and TypeCode = '" + this.ddlType4.SelectedValue + "'");
                }


                if (ddlType.SelectedValue.ToString().Trim() == "0")
                {
                    //获取ITEM CODE
                    string item_code = GetSku();
                    if (item_code.Equals(""))
                    {
                        sql = "select ItemName 产品名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' " + sb_mbd_sql + " order by 产品名称 ";
                    }
                    else
                    {
                        sql = "select ItemName 产品名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格'  " + sb_mbd_sql + " and itemCode in (" + item_code + ")  order by 产品名称 ";
                    }
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "1")  //品类
                {
                    sql = "select ItemName 品类名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '品类'" + sb_mbd_sql + " order by 品类名称 ";
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "2")  //子品类
                {
                    sql = "select ItemName 子品类名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '子品类'" + sb_mbd_sql + " order by 子品类名称 ";
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "3")  //品牌
                {
                    sql = "select ItemName 品牌名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '品牌'" + sb_mbd_sql + " order by 品牌名称 ";
                }

                

                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sql, con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    order_str = dr.GetName(0);
                    sb.Append("<th align='left' style='width:25%;' scope='col'>" + dr.GetName(i) + "</th>");
                }
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer;' >");
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        if (i == 1)
                        {
                            sb.Append("<td class='across' style='width:75%;'>" + GetDouble(dr[i].ToString()) + "</td>");
                        }
                        else
                        {
                            sb.Append("<td class='across' style='width:25%;'>" + dr[i].ToString() + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //获取SKUCODE
        private string GetSku()
        {
            string sku_items = string.Empty;
            List<string> items = new List<string>();
            string sql = string.Empty;
            try
            {
                if (ddlPL.SelectedIndex == 0)
                {
                    sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE 1=1 ";

                }
                else
                {
                    if (ddlZPL.SelectedIndex == 0)
                    {
                        sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE lv1_name = '" + ddlPL.SelectedItem.Text + "'";
                    }
                    else
                    {
                        if (dllPP.SelectedIndex == 0)
                        {
                            sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE  lv1_name = '" + ddlPL.SelectedItem.Text + "' and  lv2_name = '" + ddlZPL.SelectedItem.Text + "'";
                        }
                        else
                        {
                            if (ddlCPMC.SelectedIndex == 0)
                            {
                                sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE lv1_name = '" + ddlPL.SelectedItem.Text + "' and  lv2_name = '" + ddlZPL.SelectedItem.Text + "' and  lv3_name = '" + dllPP.SelectedItem.Text + "'";
                            }
                            else
                            {
                                sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE lv1_name = '" + ddlPL.SelectedItem.Text + "' and  lv2_name = '" + ddlZPL.SelectedItem.Text + "' and  lv3_name = '" + dllPP.SelectedItem.Text + "' and  lv4_name = '" + ddlCPMC.SelectedItem.Text + "'";
                            }
                        }
                    }
                }

                DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                    {
                        items.Add(resultDataSet.Tables[0].Rows[i]["Lv4_Code"].ToString().Trim());
                        //sku_items += resultDataSet.Tables[0].Rows[i]["Lv4_Code"].ToString().Trim() + ",";
                    }

                    string s = "";
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (s == "")
                        {
                            s += "'" + items[i] + "'";
                        }
                        else
                        {
                            s += ",'" + items[i] + "'";
                        }
                    }

                    //return sku_items.Substring(0, sku_items.Length - 1);
                    return s;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return "";
        }


        protected void btnChart_Click(object sender, EventArgs e)
        {
            this.content.Visible = false;
            LoadChart();
        }


        protected void LoadChart()
        {
            string className = string.Empty;  //css
            try
            {
                var parameter = String.Empty;
                itemName = "";
                itemValue = "";
                itemTitle = "";

                StringBuilder sb = new StringBuilder();
                //获取MBD
                string sql = "";//"select ItemName,DataValue from Sys_Otc_ItemValue where datatype = '分销'";
                StringBuilder sb_mbd_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();
                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_mbd_sql.Append(" and TypeCode = 'GSKOTC001'");
                    itemTitle = "'品类价格 - 全国'"; 
                }
                else if (ddlDim.SelectedIndex == 1) //渠道
                {
                    if (ddlType2.SelectedIndex == 0)
                        sb_mbd_sql.Append(" and TypeCode = 'GSKN02RTNM001'");
                    else
                        sb_mbd_sql.Append(" and TypeCode = 'GSKN02RTNM002'");
                    itemTitle = "'品类价格 - " + ddlType2.SelectedItem.Text + "'";
                }
                else if (ddlDim.SelectedIndex == 2) //渠道
                {
                    sb_mbd_sql.Append(" and TypeCode = '" + this.ddlType3.SelectedValue + "'");
                    itemTitle = "'品类价格 - " + ddlType3.SelectedItem.Text + "'";
                }
                else if (ddlDim.SelectedIndex == 3) //团队
                {
                    sb_mbd_sql.Append(" and TypeCode = '" + this.ddlType4.SelectedValue + "'");
                    itemTitle = "'品类价格 - " + ddlType4.SelectedItem.Text + "'";
                }


                if (ddlType.SelectedValue.ToString().Trim() == "0")
                {
                    //获取ITEM CODE
                    string item_code = GetSku();
                    if (item_code.Equals(""))
                    {
                        sql = "select ItemName 产品名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' " + sb_mbd_sql + " order by 产品名称 ";
                    }
                    else
                    {
                        sql = "select ItemName 产品名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格'  " + sb_mbd_sql + " and itemCode in (" + item_code + ")  order by 产品名称 ";
                    }
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "1")  //品类
                {
                    sql = "select ItemName 品类名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '品类'" + sb_mbd_sql + " order by 品类名称 ";
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "2")  //子品类
                {
                    sql = "select ItemName 子品类名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '子品类'" + sb_mbd_sql + " order by 子品类名称 ";
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "3")  //品牌
                {
                    sql = "select ItemName 品牌名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '品牌'" + sb_mbd_sql + " order by 品牌名称 ";
                }



                //生成Dataset
                DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                    {
                        if (i == 0)
                        {
                            itemName = itemName + "'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim().Split(' ')[0] + "'";
                            itemValue = itemValue + "" + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                        }
                        else
                        {
                            itemName = itemName + ",'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim().Split(' ')[0] + "'";
                            itemValue = itemValue + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                        }

                    }

                    itemName = "[" + itemName + "]";
                    itemValue = "[" + itemValue + "]";
                    itemTitle = "[" + itemTitle + "]";
                }
                else
                {
                    itemName = "[0]";
                    itemValue = "[0]";
                    itemTitle = "[0]";
                }
            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }

        /// <summary>
        /// 数据导出
        /// </summary>
        public void btnOutput_Click(object sender, EventArgs e)
        {
            //获取MBD
            string sql = "";//"select ItemName,DataValue from Sys_Otc_ItemValue where datatype = '分销'";
            StringBuilder sb_mbd_sql = new StringBuilder();
            StringBuilder sb_itme_sql = new StringBuilder();
            if (ddlDim.SelectedIndex == 0)  //全国
            {
                sb_mbd_sql.Append(" and TypeCode = 'GSKOTC001'");
            }
            else if (ddlDim.SelectedIndex == 1) //渠道
            {
                if (ddlType2.SelectedIndex == 0)
                    sb_mbd_sql.Append(" and TypeCode = 'GSKN02RTNM001'");
                else
                    sb_mbd_sql.Append(" and TypeCode = 'GSKN02RTNM002'");
            }
            else if (ddlDim.SelectedIndex == 2) //渠道
            {
                sb_mbd_sql.Append(" and TypeCode = '" + this.ddlType3.SelectedValue + "'");
            }
            else if (ddlDim.SelectedIndex == 3) //团队
            {
                sb_mbd_sql.Append(" and TypeCode = '" + this.ddlType4.SelectedValue + "'");
            }


            if (ddlType.SelectedValue.ToString().Trim() == "0")
            {
                //获取ITEM CODE
                string item_code = GetSku();
                if (item_code.Equals(""))
                {
                    sql = "select ItemName 产品名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' " + sb_mbd_sql + " order by 产品名称 ";
                }
                else
                {
                    sql = "select ItemName 产品名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格'  " + sb_mbd_sql + " and itemCode in (" + item_code + ")  order by 产品名称 ";
                }
            }
            else if (ddlType.SelectedValue.ToString().Trim() == "1")  //品类
            {
                sql = "select ItemName 品类名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '品类'" + sb_mbd_sql + " order by 品类名称 ";
            }
            else if (ddlType.SelectedValue.ToString().Trim() == "2")  //子品类
            {
                sql = "select ItemName 子品类名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '子品类'" + sb_mbd_sql + " order by 子品类名称 ";
            }
            else if (ddlType.SelectedValue.ToString().Trim() == "3")  //品牌
            {
                sql = "select ItemName 品牌名称 ,DataValue 价格 from Sys_Otc_ItemValue where datatype = '价格' and ItemCode = '品牌'" + sb_mbd_sql + " order by 品牌名称 ";
            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }

        private decimal GetDouble(string date)
        {
            try
            {
                return decimal.Round(decimal.Parse(date), 2);
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue.ToString().Trim() == "0")
            {
                pl_td1.Visible = true;
                pl_td2.Visible = true;

                zpl_td1.Visible = true;
                zpl_td2.Visible = true;

                pp_td1.Visible = true;
                pp_td2.Visible = true;

                nm_td1.Visible = true;
                nm_td2.Visible = true;
            }

            if (ddlType.SelectedValue.ToString().Trim() == "1" || ddlType.SelectedValue.ToString().Trim() == "2" || ddlType.SelectedValue.ToString().Trim() == "3")
            {
                pl_td1.Visible = false;
                pl_td2.Visible = false;

                zpl_td1.Visible = false;
                zpl_td2.Visible = false;

                pp_td1.Visible = false;
                pp_td2.Visible = false;

                nm_td1.Visible = false;
                nm_td2.Visible = false;
            }
        }
    }
}