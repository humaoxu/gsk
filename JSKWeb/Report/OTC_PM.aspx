﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="OTC_PM.aspx.cs" Inherits="JSKWeb.Report.OTC_PM" %>
<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    
    <script src="../js/echarts.min.js" type="text/javascript"></script>
    
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        .auto-style1 {
            width: 170px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
      <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="">
            <tbody>
                <tr>
                    <td align="right" width="80px" class="contrlFontSize">
                        问卷批次：
                    </td>
                    <td align="left" width="180px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        零售大区：
                    </td>
                    <td align="left" class="auto-style1">
                        <asp:DropDownList ID="DropDownList1" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        城市：
                    </td>
                    <td align="left" width="0px" >
                        <asp:DropDownList ID="DropDownList2" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>&nbsp;&nbsp;
                        <asp:Button ID="Button1"  CssClass="btn" 
                            runat="server"  Text="查询" Width="105px" /> 
                    </td>
                    <td align="left"  >
                    </td>
                </tr>
                
            </tbody>
        </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    
    <div id="main" style="width: 100%;height:400px;">
       
    </div>

      <script type="text/javascript">

          option = {
              title: {
                  text: '分数占比分析',
                  subtext: '',
                  x: 'center'
              },
              tooltip: {
                  trigger: 'item',
                  formatter: "{a} <br/>{b} : {c} ({d}%)"
              },
              legend: {
                  x: 'center',
                  y: 'bottom',
                  data: ['北京市', '大连市', '哈尔滨市', '沈阳市', '石家庄市', '太原市', '天津市', '上海市']
              },
              toolbox: {
                  show: true,
                  feature: {
                      mark: { show: true },
                      dataView: { show: true, readOnly: false },
                      magicType: {
                          show: true,
                          type: ['pie', 'funnel']
                      },
                      restore: { show: true },
                      saveAsImage: { show: true }
                  }
              },
              calculable: true,
              series: [
                  {
                      name: '渗透占比',
                      type: 'pie',
                      radius: [20, 110],
                      center: ['25%', 200],
                      roseType: 'radius',
                      label: {
                          normal: {
                              show: false
                          },
                          emphasis: {
                              show: true
                          }
                      },
                      lableLine: {
                          normal: {
                              show: false
                          },
                          emphasis: {
                              show: true
                          }
                      },

                      data: [
                          { value: 10, name: '北京市' },
                          { value: 5, name: '大连市' },
                          { value: 15, name: '哈尔滨市' },
                          { value: 25, name: '沈阳市' },
                          { value: 20, name: '石家庄市' },
                          { value: 35, name: '太原市' },
                          { value: 30, name: '天津市' },
                          { value: 40, name: '上海市' }
                      ]
                  },
                  {
                      name: '推荐占比',
                      type: 'pie',
                      radius: [30, 110],
                      center: ['75%', 200],
                      roseType: 'area',
                      data: [
                          { value: 10, name: '北京市' },
                          { value: 5, name: '大连市' },
                          { value: 15, name: '哈尔滨市' },
                          { value: 25, name: '沈阳市' },
                          { value: 20, name: '石家庄市' },
                          { value: 35, name: '太原市' },
                          { value: 30, name: '天津市' },
                          { value: 40, name: '上海市' }
                      ]
                  }
              ]
          };



        var myChart = echarts.init(document.getElementById('main'));
        myChart.setOption(option);
  </script>
</asp:Content>
