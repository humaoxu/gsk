﻿<%@ Page Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="GSK_OTC_ITEM_POSM.aspx.cs" Inherits="JSKWeb.Report.GSK_OTC_ITEM_POSM" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }

        .auto-style1
        {
            width: 170px;
        }

        select {
            border-color: #cccccc;
            border-width: 1px;
            border-style: solid;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td align="right" width="75px" class="contrlFontSize_s">问卷期数：
                    </td>
                    <td align="left" width="80px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="dd_chk_select" runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="75px" class="contrlFontSize_s">类型：
                    </td>
                    <td align="left" width="100px">
                        <asp:DropDownList ID="ddlType" CssClass="dd_chk_select" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                            <asp:ListItem Value="0" Selected="True">sku</asp:ListItem>
                            <asp:ListItem Value="1">品牌</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="fx_qd1">分析维度：
                    </td>
                    <td align="left" width="100px" runat="server" id="fx_qd2">
                        <asp:DropDownList ID="ddlDim" CssClass="dd_chk_select" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlDim_SelectedIndexChanged">
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="1">渠道</asp:ListItem>
                            <asp:ListItem Value="2">大区</asp:ListItem>
                            <asp:ListItem Value="3">团队</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="td_qd1">渠道：
                    </td>
                    <td align="left" runat="server" id="td_qd2">
                        <asp:DropDownList ID="ddlType2" CssClass="dd_chk_select" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlType2_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" runat="server" id="td_dq1" width="80px" class="contrlFontSize_s">大区：
                    </td>
                    <td align="left" runat="server" id="td_dq2">
                        <asp:DropDownList ID="ddlType3" CssClass="dd_chk_select" runat="server" Width="140px" AutoPostBack="true" OnSelectedIndexChanged="ddlType3_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="td_td1">团队：
                    </td>
                    <td align="left" runat="server" id="td_td2">
                        <asp:DropDownList ID="ddlType4" CssClass="dd_chk_select" runat="server" Width="140px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="250px">&nbsp;&nbsp;<asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />
                        &nbsp;
                        <asp:Button ID="btnChart" OnClick="btnChart_Click" CssClass="btn_s"
                            runat="server" Text="图表展示" Width="60px" />&nbsp;
                        <asp:Button runat="server" OnClick="btnOutput_Click" CssClass="btn_s" Width="60px" ID="btnOutput" Text="数据导出" />
                    </td>
                    <td align="left" width="150px">
                    </td>
                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>
    </fieldset>
    <fieldset>
        <legend>&nbsp;POSM品类筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td style="padding: 10px 0px 10px 10px;">
                        <asp:DropDownList ID="plList" CssClass="dd_chk_select" runat="server" Width="150px">
                        </asp:DropDownList>

                    </td>
                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>

    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center">
    </div>
    <div id="main" style="width: 100%; height: 500px; overflow: hidden;"></div>
    <script src="echarts.min.js" type="text/javascript"></script>
    <script src="map/china.js" type="text/javascript"></script>
    <script type="text/javascript">
        var s_itemName = <%= this.itemName %>;
        var s_itemValue = <%= this.itemValue %>;
        var s_itemTitle = <%= this.itemTitle %>;
        var s_legend = [];


        s_legend[0] = $('#topTool_ddlTicketType option:selected').text() + '期';


        option = {
            title: {
                x: 'left',
                text: s_itemTitle
            },
            tooltip : {
                trigger: 'axis'
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: true, readOnly: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {
                data : s_legend
            },
            xAxis : [
                 {
                     type :'category',
                     axisLabel:{
                         interval:0,
                         rotate:45,
                         margin:2,
                         textStyle:{
                             color:"#222"
                         }
                     },
                     data : s_itemName
                 }
            ],
            grid: {
                x: 40,
                x2: 20,
                y2: 100,
            },
            yAxis: [
                  {
                      type: 'value',
                      //min: 0,
                      //max: 25,
                      //interval: 10,
                      axisLabel: {
                          formatter: '{value} %'
                      }
                  }
            ],
            series: [
                {
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: function(params) {
                                //设置背景色
                                var colorList = ['#FFCA02' ];
                                return colorList[0]
                            },
                            label: {
                                show: true,
                                position: 'top',
                                //formatter: '{c} %',
                                textStyle: {
                                    color: 'black'
                                }
                            }
                        }
                    },
                    data: s_itemValue
                }]
        };

        var chart = echarts.init(document.getElementById('main'));
       
        chart.setOption(option);

        $("#main").resize(function(){
            $("#main").resize();
        })
    </script>
</asp:Content>
