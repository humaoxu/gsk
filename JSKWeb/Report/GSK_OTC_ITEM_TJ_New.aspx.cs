﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.Collections;

namespace JSKWeb.Report
{
    public partial class GSK_OTC_ITEM_TJ_New : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();
        ArrayList qdList = new ArrayList();
        ArrayList qyList = new ArrayList();

        public string lengenddata = "";
        public string xkh_q1 = "";
        public string xkl_q1 = "";
        public string f300_q2 = "";
        public string f400_q2 = "";
        public string fmk_q2 = "";
        public string ftl_q2 = "";
        public string bdb_q3 = "";
        public string fsl_q3 = "";
        //------q4
        public string f300_q4 = "";
        public string f400_q4 = "";
        public string fbdjjp_q4 = "";
        public string xkh_q4 = "";
        public string xkl_q4 = "";
        //----2017q2
        public string ftlrg_2017q2 = "";

        public int s_sum = 0;
        public int chart_h = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");
                DataBinds();

                qdDownCheckBoxes.Visible = true;
                qyDownCheckBoxes.Visible = false;
                tdDownCheckBoxes.Visible = false;

                ddlTicketType_SelectedIndexChanged(null, null);
            }
        }

        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            qdDownCheckBoxes.Visible = true;

            qdDownCheckBoxes.DataSource = null;
            qyDownCheckBoxes.DataSource = null;
            tdDownCheckBoxes.DataSource = null;

            qdDownCheckBoxes.Items.Clear();
            qyDownCheckBoxes.Items.Clear();
            tdDownCheckBoxes.Items.Clear();

            //获取渠道
            var ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level in (0,1) ";
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qdDownCheckBoxes.DataSource = ddlTypeDataSet;
            qdDownCheckBoxes.DataValueField = "dimcode";
            qdDownCheckBoxes.DataTextField = "dimname";
            qdDownCheckBoxes.DataBind();
        }

        //加载数据
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (qdDownCheckBoxes.SelectedValue.Equals("")
                  && qyDownCheckBoxes.SelectedValue.Equals("")
                  && tdDownCheckBoxes.SelectedValue.Equals(""))
            {
                JavaScript.Alert(this, "请选择MBD筛选");
                return;
            }

            this.content.Visible = true;
            string className = string.Empty;  //css
            try
            {
                ArrayList plsQD = new ArrayList();  //渠道
                ArrayList plsQY = new ArrayList();  //区域
                ArrayList plsTD = new ArrayList();  //团队

                //获取渠道选择值
                foreach (ListItem item in qdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQD.Add(item.Value);
                    }
                }

                //获取区域选择值
                foreach (ListItem item in qyDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQY.Add(item.Value);
                    }
                }

                //获取团对选择值
                foreach (ListItem item in tdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsTD.Add(item.Value);
                    }
                }

                StringBuilder sb = new StringBuilder();
                StringBuilder sb_2 = new StringBuilder();
                StringBuilder sb_3 = new StringBuilder();
                //获取MBD
                StringBuilder sb_mbd_sql = new StringBuilder();
                StringBuilder sb_mbd_sql_2 = new StringBuilder();
                StringBuilder sb_mbd_sql_3 = new StringBuilder();
                StringBuilder sb_sql_where = new StringBuilder();
                StringBuilder sb_sql_where_2 = new StringBuilder();
                StringBuilder sb_sql_where_3 = new StringBuilder();
                StringBuilder sb_sql_All = new StringBuilder();
                StringBuilder sb_sql_All_2 = new StringBuilder();
                StringBuilder sb_sql_All_3 = new StringBuilder();


                if (ddlTicketType.SelectedValue.Equals("27"))
                {
                    content2.Visible = false;
                    content3.Visible = false;
                    sb_mbd_sql.Append(" select b.* from ( SELECT TYPENAME, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 新康红装1, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '首推'  then FirstItem_Vaule else 0 end) 新康蓝装1, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 新康红装2, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 新康蓝装2 ");
                    sb_mbd_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" ) ");
                    }
                    sb_mbd_sql.Append(sb_sql_where.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql.ToString());
                    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql.ToString(), con);
                    System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                    //动态定义样式
                    sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb.Append("<tr class='header'>");
                    string order_str = string.Empty;
                    s_sum = dateData.Tables[0].Rows.Count;
                    sb.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 7; i++)
                        {
                            decimal s1 = Convert.ToDecimal(dr[1].ToString());
                            decimal s2 = Convert.ToDecimal(dr[2].ToString());
                            decimal s3 = Convert.ToDecimal(dr[3].ToString());
                            decimal s4 = Convert.ToDecimal(dr[4].ToString());
                            decimal sum_1 = s1 + s2;
                            decimal sum_2 = s3 + s4;
                            if (i == 3)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_1 * 100, 2) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 2)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 4)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[3].ToString()) + "%" + "</td>");
                            }
                            else if (i == 5)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[4].ToString()) + "%" + "</td>");
                            }
                            else if (i == 6)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_2 * 100, 2) + "%" + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");
                    con.Close();
                    dr.Close();
                }
                //二期数据
                else if (ddlTicketType.SelectedValue.Equals("51"))
                {
                    content3.Visible = false;
                    sb_mbd_sql.Append(" select b.* from ( SELECT TYPENAME, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '芬必得300' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得3001, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '芬必得400' and FirstItem = '首推'  then FirstItem_Vaule else 0 end) 芬必得4001, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '芬必得酚咖片' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得酚咖片1, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '芬必得300' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 芬必得3002, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '芬必得400' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 芬必得4002, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '芬必得酚咖片' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 芬必得酚咖片2 ");
                    sb_mbd_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" ) ");
                    }
                    sb_mbd_sql.Append(sb_sql_where.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql.ToString());
                    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql.ToString(), con);
                    System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                    //动态定义样式
                    sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb.Append("<tr class='header'>");
                    string order_str = string.Empty;
                    s_sum = dateData.Tables[0].Rows.Count;
                    sb.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>芬必得300</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>芬必得400</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>芬必得酚咖片</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>芬必得300</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>芬必得400</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>芬必得酚咖片</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 9; i++)
                        {
                            decimal s1 = Convert.ToDecimal(dr[1].ToString());
                            decimal s2 = Convert.ToDecimal(dr[2].ToString());
                            decimal s3 = Convert.ToDecimal(dr[3].ToString());
                            decimal s4 = Convert.ToDecimal(dr[4].ToString());
                            decimal s5 = Convert.ToDecimal(dr[5].ToString());
                            decimal s6 = Convert.ToDecimal(dr[6].ToString());
                            decimal sum_1 = s1 + s2 + s3;
                            decimal sum_2 = s4 + s5 + s6;
                            if (i == 4)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_1 * 100, 2) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 2)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 3)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[3].ToString()) + "%" + "</td>");
                            }
                            else if (i == 5)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[4].ToString()) + "%" + "</td>");
                            }
                            else if (i == 6)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[5].ToString()) + "%" + "</td>");
                            }
                            else if (i == 7)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[6].ToString()) + "%" + "</td>");
                            }
                            else if (i == 8)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_2 * 100, 2) + "%" + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");
                    con.Close();
                    dr.Close();

                    //第二个table
                    sb_mbd_sql_2.Append(" SELECT B.* FROM ( SELECT TYPENAME, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '扶他林乳膏' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 扶他林乳膏1,");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '扶他林乳膏' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 扶他林乳膏2 ");
                    sb_mbd_sql_2.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where_2.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where_2.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" ) ");
                    }
                    sb_mbd_sql_2.Append(sb_sql_where_2.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql_2.ToString());
                    con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql_2.ToString(), con);
                    dr = com.ExecuteReader();
                    //动态定义样式
                    sb_2.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb_2.Append("<tr class='header'>");
                    order_str = string.Empty;
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("</tr>");
                    sb_2.Append("<tr>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>扶他林乳膏</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>扶他林乳膏</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("</tr>");
                    while (dr.Read())
                    {
                        sb_2.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 5; i++)
                        {
                            if (i == 2)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 3)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 4)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                        }
                        sb_2.Append("</tr>");
                    }
                    sb_2.Append("</tbody></table>");
                    con.Close();
                    dr.Close();
                    content2.Visible = true;
                    content2.InnerHtml = sb_2.ToString();

                }
                else if (ddlTicketType.SelectedValue.Equals("60"))
                {
                    content2.Visible = false;

                    //第一个table
                    sb_mbd_sql.Append(" SELECT B.* FROM ( SELECT TYPENAME, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '百多邦乳膏' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 百多邦乳膏1,");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '百多邦乳膏' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 百多邦乳膏2 ");
                    sb_mbd_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" ) ");
                    }
                    sb_mbd_sql.Append(sb_sql_where.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql.ToString());
                    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql.ToString(), con);
                    System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                    //动态定义样式
                    sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb.Append("<tr class='header'>");
                    string order_str = string.Empty;
                    sb.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>百多邦乳膏</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>百多邦乳膏</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 5; i++)
                        {
                            if (i == 2)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 3)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 4)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");
                    con.Close();
                    dr.Close();

                    //第二个table
                    sb_mbd_sql_2.Append(" SELECT B.* FROM ( SELECT TYPENAME, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '辅舒良' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 辅舒良1,");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '辅舒良' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 辅舒良2 ");
                    sb_mbd_sql_2.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where_2.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where_2.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" ) ");
                    }
                    sb_mbd_sql_2.Append(sb_sql_where_2.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql_2.ToString());
                    con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql_2.ToString(), con);
                    dr = com.ExecuteReader();
                    //动态定义样式
                    sb_2.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb_2.Append("<tr class='header'>");
                    order_str = string.Empty;
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("</tr>");
                    sb_2.Append("<tr>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>辅舒良</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>辅舒良</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("</tr>");
                    while (dr.Read())
                    {
                        sb_2.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 5; i++)
                        {
                            if (i == 2)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 3)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 4)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                        }
                        sb_2.Append("</tr>");
                    }
                    sb_2.Append("</tbody></table>");
                    con.Close();
                    dr.Close();
                    content3.Visible = true;
                    content3.InnerHtml = sb_2.ToString();
                }
                else if (ddlTicketType.SelectedValue.Equals("66"))
                {
                    content2.Visible = false;
                    content3.Visible = false;

                    //第一个table
                    sb_mbd_sql.Append(" SELECT B.* FROM ( SELECT TYPENAME, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 新康红装1,");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 新康蓝装1, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 新康红装2, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 新康蓝装2  ");
                    sb_mbd_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" ) ");
                    }
                    sb_mbd_sql.Append(sb_sql_where.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql.ToString());
                    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql.ToString(), con);
                    System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                    //动态定义样式
                    sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb.Append("<tr class='header'>");
                    string order_str = string.Empty;
                    sb.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 7; i++)
                        {
                            decimal s1 = Convert.ToDecimal(dr[1].ToString());
                            decimal s2 = Convert.ToDecimal(dr[2].ToString());
                            decimal s3 = Convert.ToDecimal(dr[3].ToString());
                            decimal s4 = Convert.ToDecimal(dr[4].ToString());
                            decimal sum_1 = s1 + s2;
                            decimal sum_2 = s3 + s4;
                            if (i == 3)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_1 * 100, 2) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 2)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 4)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[3].ToString()) + "%" + "</td>");
                            }
                            else if (i == 5)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[4].ToString()) + "%" + "</td>");
                            }
                            else if (i == 6)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_2 * 100, 2) + "%" + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");
                    con.Close();
                    dr.Close();

                    //第二个table
                    sb_mbd_sql_2.Append(" SELECT B.* FROM ( SELECT TYPENAME, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得300' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得3001, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得400' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得4001, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得咀嚼片' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得咀嚼片1, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得300' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 芬必得3002 ,");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得400' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 芬必得4002 ,");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得咀嚼片' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 芬必得咀嚼片2  ");
                    sb_mbd_sql_2.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where_2.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where_2.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" ) ");
                    }
                    sb_mbd_sql_2.Append(sb_sql_where_2.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql_2.ToString());
                    con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql_2.ToString(), con);
                    dr = com.ExecuteReader();
                    //动态定义样式
                    sb_2.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb_2.Append("<tr class='header'>");
                    order_str = string.Empty;
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("</tr>");
                    sb_2.Append("<tr>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得300</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得400</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得酚咖片</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得300</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得400</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得酚咖片</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 9; i++)
                        {
                            decimal s1 = Convert.ToDecimal(dr[1].ToString());
                            decimal s2 = Convert.ToDecimal(dr[2].ToString());
                            decimal s3 = Convert.ToDecimal(dr[3].ToString());
                            decimal s4 = Convert.ToDecimal(dr[4].ToString());
                            decimal s5 = Convert.ToDecimal(dr[5].ToString());
                            decimal s6 = Convert.ToDecimal(dr[6].ToString());
                            decimal sum_1 = s1 + s2 + s3;
                            decimal sum_2 = s4 + s5 + s6;
                            if (i == 4)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_1 * 100, 2) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 2)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 3)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[3].ToString()) + "%" + "</td>");
                            }
                            else if (i == 5)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[4].ToString()) + "%" + "</td>");
                            }
                            else if (i == 6)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[5].ToString()) + "%" + "</td>");
                            }
                            else if (i == 7)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[6].ToString()) + "%" + "</td>");
                            }
                            else if (i == 8)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_2 * 100, 2) + "%" + "</td>");
                            }
                        }
                        sb_2.Append("</tr>");
                    }
                    sb_2.Append("</tbody></table>");
                    con.Close();
                    dr.Close();
                    content4.Visible = true;
                    content4.InnerHtml = sb_2.ToString();
                } else if (ddlTicketType.SelectedValue.Equals("74"))
                {
                    content4.Visible = false;

                    sb_mbd_sql.Append(" select b.* from ( SELECT TYPENAME, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 新康红装1, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '首推'  then FirstItem_Vaule else 0 end) 新康蓝装1, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 新康红装2, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 新康蓝装2 ");
                    sb_mbd_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" ) ");
                    }
                    sb_mbd_sql.Append(sb_sql_where.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql.ToString());
                    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql.ToString(), con);
                    System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                    //动态定义样式
                    sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb.Append("<tr class='header'>");
                    string order_str = string.Empty;
                    s_sum = dateData.Tables[0].Rows.Count;
                    sb.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 7; i++)
                        {
                            decimal s1 = Convert.ToDecimal(dr[1].ToString());
                            decimal s2 = Convert.ToDecimal(dr[2].ToString());
                            decimal s3 = Convert.ToDecimal(dr[3].ToString());
                            decimal s4 = Convert.ToDecimal(dr[4].ToString());
                            decimal sum_1 = s1 + s2;
                            decimal sum_2 = s3 + s4;
                            if (i == 3)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_1 * 100, 2) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 2)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 4)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[3].ToString()) + "%" + "</td>");
                            }
                            else if (i == 5)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[4].ToString()) + "%" + "</td>");
                            }
                            else if (i == 6)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_2 * 100, 2) + "%" + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");
                    con.Close();
                    dr.Close();
                }
                else if (ddlTicketType.SelectedValue.Equals("82"))
                {
                    content2.Visible = false;
                    content3.Visible = false;

                    //第一个table
                    sb_mbd_sql.Append(" SELECT B.* FROM ( SELECT TYPENAME, ");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '扶他林乳膏' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 扶他林乳膏1,");
                    sb_mbd_sql.Append(" MAX(case when Itemname = '扶他林乳膏' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 扶他林乳膏2 ");
                    sb_mbd_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where.Append("'" + item + "',");
                        }
                        sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                        sb_sql_where.Append(" ) ");
                    }
                    sb_mbd_sql.Append(sb_sql_where.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql.ToString());
                    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql.ToString(), con);
                    System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                    //动态定义样式
                    sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb.Append("<tr class='header'>");
                    string order_str = string.Empty;
                    sb.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("</tr>");
                    sb.Append("<tr>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>扶他林乳膏</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>扶他林乳膏</th>");
                    sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 5; i++)
                        {
                            if (i == 2)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 3)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 4)
                            {
                                sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table>");
                    con.Close();
                    dr.Close();

                    //第二个table
                    sb_mbd_sql_2.Append(" SELECT B.* FROM ( SELECT TYPENAME, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得300' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得3001, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得400' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得4001, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得咀嚼片' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 芬必得咀嚼片1, ");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得300' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 芬必得3002 ,");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得400' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 芬必得4002 ,");
                    sb_mbd_sql_2.Append(" MAX(case when Itemname = '芬必得咀嚼片' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 芬必得咀嚼片2  ");
                    sb_mbd_sql_2.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");

                    if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
                    {
                        sb_sql_where_2.Append(" WHERE TYPECODE in(");
                        foreach (var item in plsQD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        foreach (var item in plsQY)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" )");
                    }

                    if (plsTD.Count > 0)
                    {
                        sb_sql_where_2.Append(" or TypeName in(");
                        foreach (var item in plsTD)
                        {
                            sb_sql_where_2.Append("'" + item + "',");
                        }
                        sb_sql_where_2.Remove(sb_sql_where_2.Length - 1, 1);
                        sb_sql_where_2.Append(" ) ");
                    }
                    sb_mbd_sql_2.Append(sb_sql_where_2.ToString().TrimEnd(',')).Append(" and perperid = " + ddlTicketType.SelectedValue + " GROUP BY TYPENAME )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

                    //生成Dataset
                    dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_mbd_sql_2.ToString());
                    con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                    con.Open();
                    com = new System.Data.SqlClient.SqlCommand(sb_mbd_sql_2.ToString(), con);
                    dr = com.ExecuteReader();
                    //动态定义样式
                    sb_2.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                    sb_2.Append("<tr class='header'>");
                    order_str = string.Empty;
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("</tr>");
                    sb_2.Append("<tr>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'></th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得300</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得400</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得咀嚼片</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得300</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得400</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>芬必得咀嚼片</th>");
                    sb_2.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                    sb_2.Append("</tr>");
                    while (dr.Read())
                    {
                        sb.Append("<tr style='cursor:pointer;' >");
                        for (int i = 0; i < 9; i++)
                        {
                            decimal s1 = Convert.ToDecimal(dr[1].ToString());
                            decimal s2 = Convert.ToDecimal(dr[2].ToString());
                            decimal s3 = Convert.ToDecimal(dr[3].ToString());
                            decimal s4 = Convert.ToDecimal(dr[4].ToString());
                            decimal s5 = Convert.ToDecimal(dr[5].ToString());
                            decimal s6 = Convert.ToDecimal(dr[6].ToString());
                            decimal sum_1 = s1 + s2 + s3;
                            decimal sum_2 = s4 + s5 + s6;
                            if (i == 4)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_1 * 100, 2) + "%" + "</td>");
                            }
                            else if (i == 0)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                            }
                            else if (i == 1)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                            }
                            else if (i == 2)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                            }
                            else if (i == 3)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[3].ToString()) + "%" + "</td>");
                            }
                            else if (i == 5)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[4].ToString()) + "%" + "</td>");
                            }
                            else if (i == 6)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[5].ToString()) + "%" + "</td>");
                            }
                            else if (i == 7)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[6].ToString()) + "%" + "</td>");
                            }
                            else if (i == 8)
                            {
                                sb_2.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_2 * 100, 2) + "%" + "</td>");
                            }
                        }
                        sb_2.Append("</tr>");
                    }
                    sb_2.Append("</tbody></table>");
                    con.Close();
                    dr.Close();
                    content4.Visible = true;
                    content4.InnerHtml = sb_2.ToString();
                }

                content.InnerHtml = sb.ToString();
                LoadChart(sb_mbd_sql.ToString(), sb_mbd_sql_2.ToString());

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadChart(string sql,string sql2)
        {
            string className = string.Empty;  //css
            try
            {
                var parameter = String.Empty;
                lengenddata = "";
                xkh_q1 = "";
                xkl_q1 = "";

                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb = new StringBuilder();

                if (ddlTicketType.SelectedValue.Equals("27"))
                {
                    //生成Dataset
                    DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                    if (resultDataSet.Tables[0].Rows.Count > 0)
                    {

                        chart_h = 100;
                        for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                lengenddata = lengenddata + "'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                xkh_q1 = xkh_q1 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                xkl_q1 = xkl_q1 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";

                            }
                            else
                            {
                                lengenddata = lengenddata + ",'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                xkh_q1 = xkh_q1 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                xkl_q1 = xkl_q1 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";

                            }
                        }

                        lengenddata = "[" + lengenddata + "]";
                        xkh_q1 = "[" + xkh_q1 + "]";
                        xkl_q1 = "[" + xkl_q1 + "]";
                    }
                    else
                    {
                        lengenddata = "[0]";
                        xkh_q1 = "[0]";
                        xkl_q1 = "[0]";
                        ftl_q2 = "[0]";
                    }

                    f300_q2 = "[0]";
                    f400_q2 = "[0]";
                    fmk_q2 = "[0]";
                    ftl_q2 = "[0]";
                    bdb_q3 = "[0]";
                    fsl_q3 = "[0]";
                    xkh_q4 = "[0]";
                    xkl_q4 = "[0]";
                    f300_q4 = "[0]";
                    f400_q4 = "[0]";
                    fbdjjp_q4 = "[0]";
                    ftlrg_2017q2 = "[0]";
                }
                else if (ddlTicketType.SelectedValue.Equals("51"))  //二期数据
                {
                    //生成Dataset
                    DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                    if (resultDataSet.Tables[0].Rows.Count > 0)
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                lengenddata = lengenddata + "'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                f300_q2 = f300_q2 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                f400_q2 = f400_q2 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";
                                fmk_q2 = fmk_q2 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][3].ToString().Trim()) + "";

                            }
                            else
                            {
                                lengenddata = lengenddata + ",'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                f300_q2 = f300_q2 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                f400_q2 = f400_q2 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";
                                fmk_q2 = fmk_q2 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][3].ToString().Trim()) + "";
                            }
                        }

                        lengenddata = "[" + lengenddata + "]";
                        f300_q2 = "[" + f300_q2 + "]";
                        f400_q2 = "[" + f400_q2 + "]";
                        fmk_q2 = "[" + fmk_q2 + "]";
                    }
                    else
                    {
                        lengenddata = "[0]";
                        f300_q2 = "[0]";
                        f400_q2 = "[0]";
                        fmk_q2 = "[0]";
                    }

                    //第二个table
                    //生成Dataset
                    DataSet resultDataSet_q2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql2);

                    if (resultDataSet_q2.Tables[0].Rows.Count > 0)
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet_q2.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                //lengenddata = lengenddata + "'" + resultDataSet_q2.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                ftl_q2 = ftl_q2 + "" + GetDouble(resultDataSet_q2.Tables[0].Rows[i][1].ToString().Trim()) + "";
                            }
                            else
                            {
                                ftl_q2 = ftl_q2 + "," + GetDouble(resultDataSet_q2.Tables[0].Rows[i][1].ToString().Trim()) + "";

                            }
                        }

                        ftl_q2 = "[" + ftl_q2 + "]";
                    }
                    else
                    {
                        ftl_q2 = "[0]";
                    }

                    xkh_q1 = "[0]";
                    xkl_q1 = "[0]";
                    bdb_q3 = "[0]";
                    fsl_q3 = "[0]";
                    xkh_q4 = "[0]";
                    xkl_q4 = "[0]";
                    f300_q4 = "[0]";
                    f400_q4 = "[0]";
                    fbdjjp_q4 = "[0]";
                    ftlrg_2017q2 = "[0]";
                }
                else if (ddlTicketType.SelectedValue.Equals("60"))  //三期数据
                {
                    //生成Dataset
                    DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                    if (resultDataSet.Tables[0].Rows.Count > 0)
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                fsl_q3 = fsl_q3 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                            }
                            else
                            {
                                fsl_q3 = fsl_q3 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                            }
                        }

                        lengenddata = "[" + lengenddata + "]";
                        fsl_q3 = "[" + fsl_q3 + "]";
                    }
                    else
                    {
                        lengenddata = "[0]";
                        fsl_q3 = "[0]";
                    }

                    //第二个table
                    //生成Dataset
                    DataSet resultDataSet_q2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql2);

                    if (resultDataSet_q2.Tables[0].Rows.Count > 0)
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet_q2.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                //lengenddata = lengenddata + "'" + resultDataSet_q2.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                bdb_q3 = bdb_q3 + "" + GetDouble(resultDataSet_q2.Tables[0].Rows[i][1].ToString().Trim()) + "";
                            }
                            else
                            {
                                bdb_q3 = bdb_q3 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";

                            }
                        }

                        bdb_q3 = "[" + bdb_q3 + "]";
                    }
                    else
                    {
                        bdb_q3 = "[0]";
                    }

                    xkh_q1 = "[0]";
                    xkl_q1 = "[0]";
                    f300_q2 = "[0]";
                    f400_q2 = "[0]";
                    fmk_q2 = "[0]";
                    ftl_q2 = "[0]";
                    xkh_q4 = "[0]";
                    xkl_q4 = "[0]";
                    f300_q4 = "[0]";
                    f400_q4 = "[0]";
                    fbdjjp_q4 = "[0]";
                    ftlrg_2017q2 = "[0]";
                }
                else if (ddlTicketType.SelectedValue.Equals("66"))  //四期数据
                {
                    //生成Dataset
                    DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                    //第一个table
                    //生成Dataset
                    if (resultDataSet.Tables[0].Rows.Count > 0)
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                lengenddata = lengenddata + "'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                f300_q4 = f300_q4 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                f400_q4 = f400_q4 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";
                                fbdjjp_q4 = fbdjjp_q4 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][3].ToString().Trim()) + "";

                            }
                            else
                            {
                                lengenddata = lengenddata + ",'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                f300_q4 = f300_q4 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                f400_q4 = f400_q4 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";
                                fbdjjp_q4 = fbdjjp_q4 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][3].ToString().Trim()) + "";
                            }
                        }

                        lengenddata = "[" + lengenddata + "]";
                        f300_q4 = "[" + f300_q4 + "]";
                        f400_q4 = "[" + f400_q4 + "]";
                        fbdjjp_q4 = "[" + fbdjjp_q4 + "]";
                    }
                    else
                    {
                        lengenddata = "[0]";
                        f300_q4 = "[0]";
                        f400_q4 = "[0]";
                        fbdjjp_q4 = "[0]";
                    }

                    //第二个table
                    //生成Dataset
                    DataSet resultDataSet_q2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql2);

                    if (resultDataSet_q2.Tables[0].Rows.Count > 0)
                    {

                        chart_h = 100;
                        for (int i = 0; i < resultDataSet_q2.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                xkh_q4 = xkh_q4 + "" + GetDouble(resultDataSet_q2.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                xkl_q4 = xkl_q4 + "" + GetDouble(resultDataSet_q2.Tables[0].Rows[i][2].ToString().Trim()) + "";

                            }
                            else
                            {
                                xkh_q4 = xkh_q4 + "," + GetDouble(resultDataSet_q2.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                xkl_q4 = xkl_q4 + "," + GetDouble(resultDataSet_q2.Tables[0].Rows[i][2].ToString().Trim()) + "";

                            }
                        }

                        xkh_q4 = "[" + xkh_q4 + "]";
                        xkl_q4 = "[" + xkl_q4 + "]";
                    }
                    else
                    {
                        xkh_q4 = "[0]";
                        xkl_q4 = "[0]";
                    }

                    xkh_q1 = "[0]";
                    xkl_q1 = "[0]";
                    f300_q2 = "[0]";
                    f400_q2 = "[0]";
                    fmk_q2 = "[0]";
                    ftl_q2 = "[0]";
                    bdb_q3 = "[0]";
                    fsl_q3 = "[0]";
                    ftlrg_2017q2 = "[0]";
                }
                else if (ddlTicketType.SelectedValue.Equals("74"))
                {
                    //生成Dataset
                    DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                    if (resultDataSet.Tables[0].Rows.Count > 0)
                    {

                        chart_h = 100;
                        for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                lengenddata = lengenddata + "'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                xkh_q1 = xkh_q1 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                xkl_q1 = xkl_q1 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";

                            }
                            else
                            {
                                lengenddata = lengenddata + ",'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                xkh_q1 = xkh_q1 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                xkl_q1 = xkl_q1 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";

                            }
                        }

                        lengenddata = "[" + lengenddata + "]";
                        xkh_q1 = "[" + xkh_q1 + "]";
                        xkl_q1 = "[" + xkl_q1 + "]";
                    }
                    else
                    {
                        lengenddata = "[0]";
                        xkh_q1 = "[0]";
                        xkl_q1 = "[0]";
                        ftl_q2 = "[0]";
                    }

                    f300_q2 = "[0]";
                    f400_q2 = "[0]";
                    fmk_q2 = "[0]";
                    ftl_q2 = "[0]";
                    bdb_q3 = "[0]";
                    fsl_q3 = "[0]";
                    xkh_q4 = "[0]";
                    xkl_q4 = "[0]";
                    f300_q4 = "[0]";
                    f400_q4 = "[0]";
                    fbdjjp_q4 = "[0]";
                    ftlrg_2017q2 = "[0]";
                }
                else if (ddlTicketType.SelectedValue.Equals("82"))  //2017Q2
                {
                    //生成Dataset
                    DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                    if (resultDataSet.Tables[0].Rows.Count > 0)
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                ftlrg_2017q2 = ftlrg_2017q2 + "" + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                            }
                            else
                            {
                                ftlrg_2017q2 = ftlrg_2017q2 + "," + GetDouble(resultDataSet.Tables[0].Rows[i][1].ToString().Trim()) + "";
                            }
                        }
                        
                        ftlrg_2017q2 = "[" + ftlrg_2017q2 + "]";
                    }
                    else
                    {
                        ftlrg_2017q2 = "[0]";
                    }

                    //第二个table
                    //生成Dataset
                    DataSet resultDataSet_q2 = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql2);

                    if (resultDataSet_q2.Tables[0].Rows.Count > 0)
                    {
                        chart_h = 100;
                        for (int i = 0; i < resultDataSet_q2.Tables[0].Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                lengenddata = lengenddata + "'" + resultDataSet_q2.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                f300_q4 = f300_q4 + "" + GetDouble(resultDataSet_q2.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                f400_q4 = f400_q4 + "" + GetDouble(resultDataSet_q2.Tables[0].Rows[i][2].ToString().Trim()) + "";
                                fbdjjp_q4 = fbdjjp_q4 + "" + GetDouble(resultDataSet_q2.Tables[0].Rows[i][3].ToString().Trim()) + "";

                            }
                            else
                            {
                                lengenddata = lengenddata + ",'" + resultDataSet_q2.Tables[0].Rows[i][0].ToString().Trim() + "'";
                                f300_q4 = f300_q4 + "," + GetDouble(resultDataSet_q2.Tables[0].Rows[i][1].ToString().Trim()) + "";
                                f400_q4 = f400_q4 + "," + GetDouble(resultDataSet_q2.Tables[0].Rows[i][2].ToString().Trim()) + "";
                                fbdjjp_q4 = fbdjjp_q4 + "," + GetDouble(resultDataSet_q2.Tables[0].Rows[i][3].ToString().Trim()) + "";
                            }
                        }

                        lengenddata = "[" + lengenddata + "]";
                        f300_q4 = "[" + f300_q4 + "]";
                        f400_q4 = "[" + f400_q4 + "]";
                        fbdjjp_q4 = "[" + fbdjjp_q4 + "]";
                    }
                    else
                    {
                        lengenddata = "[0]";
                        f300_q4 = "[0]";
                        f400_q4 = "[0]";
                        fbdjjp_q4 = "[0]";
                    }

                    xkh_q1 = "[0]";
                    xkl_q1 = "[0]";
                    f300_q2 = "[0]";
                    f400_q2 = "[0]";
                    fmk_q2 = "[0]";
                    ftl_q2 = "[0]";
                    bdb_q3 = "[0]";
                    fsl_q3 = "[0]";
                    xkh_q4 = "[0]";
                    xkl_q4 = "[0]";
                }

            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }

        /// <summary>
        /// 数据导出
        /// </summary>
        public void btnOutput_Click(object sender, EventArgs e)
        {

        }

        private decimal GetDouble(string date)
        {
            try
            {
                return decimal.Round(decimal.Parse(date) * 100, 2);
            }
            catch (Exception)
            {
                return 0;
                throw;
            }


        }

        public void DataBinds()
        {
            var ddlProjectId = InputText.GetConfig("gsk_otc");
            //var ddlTicketTypeSelectSql = String.Format("select paperid,paperTitle from Paper where paperid in (27,51,60) order by paperid desc ");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);

            //var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE paperId in (27,51,60,66,74,82) ", ddlProjectId);

            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";

            ddlTicketType.DataBind();

        }

        protected void qdDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qdDownCheckBoxes.Visible = true;
            qyDownCheckBoxes.Visible = true;
            qy_td.Style.Add("padding-left", "25px");


            qyDownCheckBoxes.DataSource = null;
            tdDownCheckBoxes.DataSource = null;


            qyDownCheckBoxes.Items.Clear();
            tdDownCheckBoxes.Items.Clear();

            qdList = new ArrayList();
            //如果全选
            int select_sum = Int32.Parse(qdDownCheckBoxes.Items.Capacity.ToString());

            //根据渠道筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    qdList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            if (qdList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - qdList.Count == 1)
                {
                    str_code = "'GSKN02KANM002','GSKN02RTNM001'";
                }
                else
                {
                    for (int i = 0; i < qdList.Count; i++)
                    {
                        str_code += "'" + qdList[i].ToString() + "',";
                    }
                }
            }
            //获取区域
            var ddlTypeSelectSql = "";
            if (str_code.Trim().Length == 0 || str_code.Trim().Equals("'GSKOTC001',"))
            {
                ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level = 2 order by dimname";
            }
            else
            {
                if (select_sum - qdList.Count == 1)
                {
                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code + ")  and level = 2 order by dimname";
                }
                else
                {

                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code.TrimEnd(',').ToString() + ")  and level = 2 order by dimname ";
                }
            }

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "dimcode";
            qyDownCheckBoxes.DataTextField = "dimname";
            qyDownCheckBoxes.DataBind();
        }

        protected void qyDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qdDownCheckBoxes.Visible = true;
            qyDownCheckBoxes.Visible = true;
            tdDownCheckBoxes.Visible = true;
            qy_td.Style.Add("padding-left", "25px");
            td_td.Style.Add("padding-left", "25px");

            tdDownCheckBoxes.DataSource = null;


            tdDownCheckBoxes.Items.Clear();

            //如果全选
            int select_sum = Int32.Parse(qyDownCheckBoxes.Items.Capacity.ToString());
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    qyList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            if (qyList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - qyList.Count == 1)
                {
                    str_code = "(select dimcode from sys_otc_dim  where level in (2))";
                }
                else
                {
                    for (int i = 0; i < qyList.Count; i++)
                    {
                        str_code += "'" + qyList[i].ToString() + "',";
                    }
                }
            }
            //获取团队
            var ddlTypeSelectSql = "";
            if (str_code.Trim().Length == 0)
            {
                ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level =3 order by dimname ";
            }
            else
            {

                if (select_sum - qyList.Count == 1)
                {
                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code + ")  order by dimname";
                }
                else
                {

                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code.TrimEnd(',').ToString() + ")  order by dimname ";
                }
            }

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            tdDownCheckBoxes.DataSource = ddlTypeDataSet;
            tdDownCheckBoxes.DataValueField = "dimcode";
            tdDownCheckBoxes.DataTextField = "dimname";
            tdDownCheckBoxes.DataBind();
        }
    }
}