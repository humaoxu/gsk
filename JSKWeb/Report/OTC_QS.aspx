﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="OTC_QS.aspx.cs" Inherits="JSKWeb.Report.OTC_QS" %>
<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    
    <script src="../js/echarts.min.js" type="text/javascript"></script>
    
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        .auto-style1 {
            width: 170px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
      <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="">
            <tbody>
                <tr>
                    <td align="right" width="80px" class="contrlFontSize">
                        问卷批次：
                    </td>
                    <td align="left" width="180px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        零售大区：
                    </td>
                    <td align="left" class="auto-style1">
                        <asp:DropDownList ID="DropDownList1" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        城市：
                    </td>
                    <td align="left" width="0px" >
                        <asp:DropDownList ID="DropDownList2" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>&nbsp;&nbsp;
                        <asp:Button ID="Button1"  CssClass="btn" 
                            runat="server"  Text="查询" Width="105px" /> 
                    </td>
                    <td align="left"  >
                    </td>
                </tr>
                
            </tbody>
        </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    
    <div id="main" style="width: 100%;height:400px;">
       
    </div>

      <script type="text/javascript">

          option = {
              title: {
                  text: '趋势分析'
              },
              tooltip: {
                  trigger: 'axis'
              },
              legend: {
                  data: ['渗透', '陈列面', '价格', '推荐', 'POSM']
              },
              grid: {
                  left: '3%',
                  right: '4%',
                  bottom: '3%',
                  containLabel: true
              },
              toolbox: {
                  feature: {
                      saveAsImage: {}
                  }
              },
              xAxis: [
                  {
                      type: 'category',
                      boundaryGap: false,
                      data: ['201601', '201602', '201603', '201604', '201605', '201606']
                  }
              ],
              yAxis: [
                  {
                      type: 'value'
                  }
              ],
              series: [
                  {
                      name: '渗透',
                      type: 'line',
                      stack: '分数',
                      data: [120, 132, 101, 134, 90, 230, 210]
                  },
                  {
                      name: '陈列面',
                      type: 'line',
                      stack: '分数',
                      data: [220, 182, 191, 234, 290, 330, 310]
                  },
                  {
                      name: '价格',
                      type: 'line',
                      stack: '分数',
                      data: [150, 232, 201, 154, 190, 330, 410]
                  },
                  {
                      name: '推荐',
                      type: 'line',
                      stack: '分数',
                      data: [320, 332, 301, 334, 390, 330, 320]
                  },
                  {
                      name: 'POSM',
                      type: 'line',
                      stack: '分数',
                      data: [820, 932, 901, 934, 1290, 1330, 1320]
                  }
              ]
          };


        var myChart = echarts.init(document.getElementById('main'));
        myChart.setOption(option);
  </script>
</asp:Content>

