﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="OTC_SCORE.aspx.cs" Inherits="JSKWeb.Report.OTC_SCORE" %>


<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
        <script src="../js/jquery-1.10.1.min.js" type="text/javascript"></script>
        <script src="../js/echarts.min.js" type="text/javascript"></script>
    
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        .auto-style1 {
            width: 177px;
        }
        .auto-style3 {
            width: 96px;
        }
        .auto-style4 {
            width: 180px;
        }
        .auto-style5 {
            font-size: 15px;
            width: 52px;
        }
        .auto-style6 {
            width: 52px;
        }
        .auto-style7 {
            width: 443px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0"  class="">
            <tbody>
                <tr>
                    <td align="right" width="80px" class="contrlFontSize">
                        问卷期数：
                    </td>
                    <td align="left" class="auto-style3">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        OTC总部：
                    </td>
                    <td align="left" class="auto-style1" >
                        <asp:DropDownList ID="ddlType1" CssClass="inputtext15" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="ddlType1_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        大区总监：
                    </td>
                    <td align="left" class="auto-style4" >
                        <asp:DropDownList ID="ddlType2" CssClass="inputtext15" runat="server" Width="180px" AutoPostBack="true" OnSelectedIndexChanged="ddlType2_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right"  class="auto-style5">
                        大区：
                    </td>
                    <td align="left" class="auto-style7" >
                        <asp:DropDownList ID="ddlType3"  CssClass="inputtext15" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="ddlType3_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    
                    <td align="right" width="80px" class="contrlFontSize">
                        所属团队：
                    </td>
                    <td align="left" class="auto-style3" >
                        <asp:DropDownList ID="ddlType4" CssClass="inputtext15" runat="server" Width="160px" AutoPostBack="true" OnSelectedIndexChanged="ddlType4_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize">
                        &nbsp;销售代表：
                    </td>
                    <td align="left" class="auto-style1" >
                        <asp:DropDownList ID="ddlType5" CssClass="inputtext15" runat="server" Width="160px">
                        </asp:DropDownList>
                    </td>
                    
              
                    <td align="left" class="auto-style7" colspan="4" >
                                       <asp:RadioButton ID="rad1" Width="80px" Text="数值展示" Checked="true" GroupName="sel"  runat="server" class="contrlFontSize"  />&nbsp;&nbsp;
                        <asp:RadioButton ID="rad2"  Width="80px" Text="图表展示"  runat="server"  GroupName="sel" class="contrlFontSize"  />
                       &nbsp;
                        <input id="btn_chart" type="button" value="查询" style="width:80px" class="btn"  />
                   
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server"  >

         
    </div>    

    <div id="dvmain" style="width: 100%;height:400px;"  >
       
    </div>

      <script type="text/javascript">
          $("#btn_chart").click(function () {

              var radType = '1';

              if (document.getElementById('<%= rad1.ClientID %>').checked == true) {
                  radType = '1';
              }
              else
              {
                  radType = '2';
              }

              var dd1 = document.getElementById('<%= ddlType1.ClientID %>');
              var indd1 = dd1.selectedIndex;             
              var var_dd1 = dd1.options[indd1].value;

              var dd2 = document.getElementById('<%= ddlType2.ClientID %>');
              var indd2 = dd2.selectedIndex;             
              var var_dd2 = dd2.options[indd2].value;

              var dd3 = document.getElementById('<%= ddlType3.ClientID %>');
              var indd3 = dd3.selectedIndex;             
              var var_dd3 = dd3.options[indd3].value;

              var dd4 = document.getElementById('<%= ddlType4.ClientID %>');
              var indd4 = dd4.selectedIndex;             
              var var_dd4 = dd4.options[indd4].value;

              var dd5 = document.getElementById('<%= ddlType5.ClientID %>');
              var indd5 = dd5.selectedIndex;             
              var var_dd5 = dd5.options[indd5].value;

              var ddpaper = document.getElementById('<%= ddlTicketType.ClientID %>');
              var inddpaper = ddpaper.selectedIndex;
              var var_ddpaper = ddpaper.options[inddpaper].value;

              var strUser = "";
              if (var_dd5 != "")
              {
                  strUser = var_dd5;
              }
              else if (var_dd4 != "") {
                  strUser = var_dd4;
              }
              else if (var_dd3 != "") {
                  strUser = var_dd3;
              }
              else if (var_dd2 != "") {
                  strUser = var_dd2;
              }
              else  {
                  strUser = var_dd1;
              }

              alert(strUser)

              var linkUrl = "OTC_SCORE_CAL.ashx?id=2&pid=" + var_ddpaper + "&radType=" + radType + "&cmUser=" + strUser;
              alert(linkUrl)

              $.ajax({
                  type: "get",
                  url: linkUrl,
                  data: null,
                  success: function (msg) {
                      if (msg != "") {
                          
                          var col_title = "";     //标题的列名
                          var col_data = [];      //显示值
                          var col_data_name = []; //显示标题

                          var json_data = null;   //初始变量
                          var myChart = null;     //初始变量
                          var option = null;      //初始变量

                          var chart_title = new Array(); //标题数组
                          var chart_data = new Array(); //值数组

                          json_data = eval(msg);


                          //列标题,列字段名取值
                          var col = 0;
                          for (var key in json_data[0]) {
                              col_data.push(key);
                              col++;
                          }
                          var str = '';
                          for (var i = 0; i < json_data.length; i++) {
                              for (var j = 0; j < col_data.length; j++) {
                                  var col_name = col_data[j];

                                  str = str + ' ' + col_name + ' ' + json_data[i][col_name];
                              };
                          };
                          alert(str);
                      }
                  }
              });
              
          }
       )
  </script>
      

</asp:Content>
