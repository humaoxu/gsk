﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="GSK_OTC_DEMO_PL.aspx.cs" Inherits="JSKWeb.Report.GSK_OTC_DEMO_PL" %>
<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    
    <script src="../js/echarts.min.js" type="text/javascript"></script>
    
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
        .auto-style1 {
            width: 170px;
        }

    </style>
    <script language="javascript" type="text/javascript">
        function change()
        {
            alert(1);
            document.getElementById('main').hide();
            //$("#main").hide();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td align="right" width="75px" class="contrlFontSize_s">问卷期数：
                    </td>
                    <td align="left" width="80px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s">分析维度：
                    </td>
                    <td align="left" width="100px">
                        <asp:DropDownList ID="ddlDim" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlDim_SelectedIndexChanged">
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="1">渠道</asp:ListItem>
                            <asp:ListItem Value="2">大区</asp:ListItem>
                            <asp:ListItem Value="3">团队</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="td_qd1">渠道：
                    </td>
                    <td align="left" runat="server" id="td_qd2">
                        <asp:DropDownList ID="ddlType2" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlType2_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" runat="server" id="td_dq1" width="80px" class="contrlFontSize_s">大区：
                    </td>
                    <td align="left" runat="server" id="td_dq2">
                        <asp:DropDownList ID="ddlType3" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlType3_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="td_td1">团队：
                    </td>
                    <td align="left" runat="server" id="td_td2">
                        <asp:DropDownList ID="ddlType4" CssClass="inputtext15" runat="server" Width="100px">
                        </asp:DropDownList>
                    </td>
                </tr>

            </tbody>
        </table>
        <div style="height:5px"></div>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tr>
                <td align="right" width="75px" class="contrlFontSize_s">品类：
                </td>
                <td align="left" width="80px">
                    <asp:DropDownList ID="ddlPL" CssClass="inputtext15" runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="ddlPL_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="zpl_td1">子品类：
                </td>
                <td align="left" width="100px" runat="server" id="zpl_td2">
                    <asp:DropDownList ID="ddlZPL" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlZPL_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="pp_td1">品牌：
                </td>
                <td align="left" width="100px" runat="server" id="pp_td2">
                    <asp:DropDownList ID="dllPP" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="dllPP_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="nm_td1">产品名称：
                </td>
                <td align="left" width="100px" runat="server" id="nm_td2">
                    <asp:DropDownList ID="ddlCPMC" CssClass="inputtext15" runat="server" Width="100px">
                    </asp:DropDownList>
                </td>
                <%--<td align="right" width="80px" class="contrlFontSize_s" runat="server" id="Td1">类型：
                </td>
                <td align="left" width="100px">
                    <asp:DropDownList ID="ddlItemType" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlDim_SelectedIndexChanged">
                        <asp:ListItem Value="0">分销</asp:ListItem>
                        <asp:ListItem Value="1">陈列面</asp:ListItem>
                        <asp:ListItem Value="2">好位置</asp:ListItem>
                        <asp:ListItem Value="3">POSM</asp:ListItem>
                        <asp:ListItem Value="4">价格</asp:ListItem>
                        <asp:ListItem Value="5">推荐</asp:ListItem>
                        <asp:ListItem Value="6">竞品</asp:ListItem>
                    </asp:DropDownList>
                </td>--%>
                <td align="right" width="80px">&nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />
                </td>
                <td align="left" width="150px">&nbsp;
                        <asp:Button ID="btnChart" OnClick="btnChart_Click" CssClass="btn_s"
                            runat="server" Text="图表展示" Width="60px" />
                </td>
            </tr>
        </table>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align:center">
            
    </div>
    <div id="main" style="width: 100%; height: 500px; overflow:hidden;"></div>
    <script src="echarts.min.js" type="text/javascript"></script>
    <script src="map/china.js" type="text/javascript"></script>
    <script type="text/javascript">
        var s_itemName = <%= this.itemName %>;
        var s_itemValue = <%= this.itemValue %>;

        option = {
            title: {
                x: 'center',
                text: '单品分效率分析',
                subtext: 'Rainbow bar'
            },
            tooltip : {
                trigger: 'axis',
                axisPointer : {            
                    type : 'shadow'
                }
            },
            toolbox: {
                feature: {
                    dataView: { show: true, readOnly: false },
                    magicType: { show: true, type: ['line', 'bar'] },
                    restore: { show: true },
                    saveAsImage: { show: true }
                }
            },
            legend: {
                data: ['单品分效率分析']
            },
            xAxis: [
                  {
                      type: 'category',
                      data: s_itemName
                  }
            ],
            yAxis: [
                  {
                      type: 'value',
                      name: '分销率',
                      //min: 0,
                      //max: 100,
                      //interval: 50,
                      axisLabel: {
                          formatter: '{value} %'
                      }
                  }
            ],
            series: [
                {
                    name: '分销率',
                    type: 'bar',
                    data: s_itemValue
                }
            ]
        };

        var chart = echarts.init(document.getElementById('main'));
       
        chart.showLoading({
            text: '正在加载中..... ',
        });
        chart.hideLoading();

        chart.setOption(option);

        $("#main").resize(function(){
            $("#main").resize();
        })
    </script>
      <%--<script type="text/javascript">
          option = {
              tooltip: {
                  trigger: 'axis'
              },
              toolbox: {
                  feature: {
                      dataView: { show: true, readOnly: false },
                      magicType: { show: true, type: ['line', 'bar'] },
                      restore: { show: true },
                      saveAsImage: { show: true }
                  }
              },
              legend: {
                  data: ['品类分销率']
              },
              xAxis: [
                  {
                      type: 'category',
                      data: ['呼吸类（感冒)', '呼吸类（鼻贴)', '呼吸类（鼻炎)', '疼痛类（口服）', '疼痛类（外用）', '皮肤类（抗炎）', '皮肤类（真菌）', '驱虫类', '牙膏类', '假牙清洁类']
                  }
              ],
              yAxis: [
                  {
                      type: 'value',
                      name: '分销率',
                      min: 0,
                      max: 100,
                      interval: 50,
                      axisLabel: {
                          formatter: '{value} %'
                      }
                  }
              ],
              series: [
                  {
                      name: '分销率',
                      type: 'bar',
                      data: [49.3, 51.9, 35.2,69.9, 37.6, 28.3, 12.8, 30.7,56.3,77.7]
                  }
              ]
          };


          var myChart = echarts.init(document.getElementById('main'));
          myChart.setOption(option);
  </script>--%>
</asp:Content>

