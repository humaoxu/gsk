﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;

namespace JSKWeb.Report
{
    /// <summary>
    /// OTC_SCORE_CAL 的摘要说明
    /// </summary>
    public class OTC_SCORE_CAL : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            SqlHelper sqlhelper = new SqlHelper();
            context.Response.ContentType = "text/plain";
            try
            {
                ComAshx cm = new ComAshx();
                string linxUser = context.Request.Cookies["linxsanmpleuser"].Value.ToString().Trim().ToUpper();


                string strRadType = context.Request.QueryString["radType"].ToString().ToLower();
                string strUsercode = context.Request.QueryString["cmUser"].ToString().ToLower();
                string strPid = context.Request.QueryString["pid"].ToString().ToLower();
                string strcol = "";
                string strgroupby = "";
                string strType = "0";

                if (context.Request.QueryString["id"] != null)
                {
                    strType = context.Request.QueryString["id"].ToString().Trim();
                }

                if (strType == "0")
                {
                    strcol = "col21 as OTC总部编码,col22 as OTC总部名称,";
                    strgroupby = "col21,col22";
                }
                else if (strType == "1")
                {
                    strcol = "col25 as 大区总监编码,col26 as 大区总监名称,";
                    strgroupby = "col25,col26";
                }
                else if (strType == "2")
                {
                    strcol = "col27 as 大区编码,col28 as 大区名称,";
                    strgroupby = "col27,col28";
                }
                else if (strType == "3")
                {
                    strcol = "col19 as 所属团队编码,col20 as 所属团队名称,";
                    strgroupby = "col19,col20";
                }
                else if (strType == "4")
                {
                    strcol = "col17 as 销售代表编码,col18 as 销售代表名称,";
                    strgroupby = "col17,col18";
                }
                else
                {
                    strcol = "col21 as OTC总部编码,col22 as OTC总部名称,";
                    strgroupby = "col21,col22";
                }

                StringBuilder sb = new StringBuilder();
                if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb.Append(" select " + strcol + " ");
                    sb.Append("  CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(t_score) / COUNT(1) end) ");
                    sb.Append("   as 总分, CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STPF) / COUNT(1) end) as 渗透评分 ");
                    sb.Append(" , CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(CLPF) / COUNT(1) end) as 陈列评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(POSM) / COUNT(1) end) as POSM, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(JG) / COUNT(1) end) as 价格评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(TJ) / COUNT(1) end) as 推荐评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STFJ) / COUNT(1) end) as 渗透附加分  ");
                    sb.Append("  from sys_otc_result where paperid='" + strPid + "' group by " + strgroupby + " ");

                    sb.Append(@" order by  总分 desc ");
                }
                else
                {
                    sb.Append(" select " + strcol + " ");
                    sb.Append("  CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(t_score) / COUNT(1) end) ");
                    sb.Append("   as 总分, CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STPF) / COUNT(1) end) as 渗透评分 ");
                    sb.Append(" , CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(CLPF) / COUNT(1) end) as 陈列评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(POSM) / COUNT(1) end) as POSM, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(JG) / COUNT(1) end) as 价格评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(TJ) / COUNT(1) end) as 推荐评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STFJ) / COUNT(1) end) as 渗透附加分  ");
                    sb.Append(" from sys_otc_result   where paperid='" + strPid + "' ");
                    if (linxUser.ToUpper() != InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                    {
                        sb.Append(" and sys_otc_result.shopcode in (select shopcode from V_OTC_USER_SHOP where usercode = '" + linxUser + "'  ) ");
                    }

                    sb.Append(" group by " + strgroupby + " ");

                    sb.Append(@" order by  总分 desc ");
                }


                DataSet ds = sqlhelper.GetDataTableFromSq(sb.ToString());

                string temp = string.Empty;
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    temp = cm.GetJsonByDataset(ds);
                }
                context.Response.Write(temp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        

    }
}