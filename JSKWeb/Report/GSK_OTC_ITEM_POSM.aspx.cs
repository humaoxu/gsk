﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;

namespace JSKWeb.Report
{
    public partial class GSK_OTC_ITEM_POSM : System.Web.UI.Page
    {
        public string itemName = "";
        public string itemValue = "";
        public string itemTitle = "";

        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");

                TicketTypeDataBind();
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();


            // 根据期数 选择POSM分类数值
            var plListSelectSql = String.Format("select distinct itempl from Sys_Otc_ItemValue_POSM where perperid={0} order by  itempl ", ddlTicketType.SelectedValue);
            var plListDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, plListSelectSql);
            plList.DataSource = plListDataSet;
            plList.DataValueField = "itempl";
            plList.DataTextField = "itempl";
            plList.DataBind();
            plList.Items.Insert(0, new ListItem("全部", "-1")); //添加默认

            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;


            ddlType2.Items.Clear();
            ddlType3.Items.Clear();
            ddlType4.Items.Clear();


            select_sql = " SELECT COL25 CODE,COL26 NAME FROM ( SELECT COL25,COL26, ROW_NUMBER () OVER (PARTITION BY COL26 ORDER BY COL26) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL25 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }

            select_sql = " SELECT COL27 CODE,COL28 NAME FROM ( SELECT COL27,COL28, ROW_NUMBER () OVER (PARTITION BY COL27 ORDER BY COL27) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL27 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType3.DataSource = select_ds;
                ddlType3.DataValueField = "code";
                ddlType3.DataTextField = "name";
                ddlType3.DataBind();
            }

            select_sql = " SELECT COL19 CODE,COL20 NAME FROM ( SELECT COL19,COL20, ROW_NUMBER () OVER (PARTITION BY COL19 ORDER BY COL19) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL19 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType4.DataSource = select_ds;
                ddlType4.DataValueField = "code";
                ddlType4.DataTextField = "name";
                ddlType4.DataBind();
            }


        }

        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 根据期数 选择POSM分类数值
            var plListSelectSql = String.Format("select distinct itempl from Sys_Otc_ItemValue_POSM where perperid={0} order by  itempl ", ddlTicketType.SelectedValue);
            var plListDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, plListSelectSql);
            plList.DataSource = plListDataSet;
            plList.DataValueField = "itempl";
            plList.DataTextField = "itempl";
            plList.DataBind();

            plList.Items.Insert(0, new ListItem("全部", "-1")); //添加默认
        }


        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";

            select_sql = "SELECT DISTINCT  col27 CODE,COL28 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col27 IS NOT NULL ";
            select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType3.DataSource = select_ds;
                ddlType3.DataValueField = "code";
                ddlType3.DataTextField = "name";
                ddlType3.DataBind();
            }
        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;
            ddlType4.Items.Clear();
            DataSet select_ds = new DataSet();
            var select_sql = "";


            select_sql = "SELECT DISTINCT  col19 CODE,COL20 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col19 IS NOT NULL ";
            select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' and col27 = '" + ddlType3.SelectedValue + "' ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType4.DataSource = select_ds;
                ddlType4.DataValueField = "code";
                ddlType4.DataTextField = "name";
                ddlType4.DataBind();

            }

        }

        protected void ddlDim_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDim.SelectedValue.ToString().Trim() == "0")
            {
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;


            }

            if (ddlDim.SelectedValue.ToString().Trim() == "1")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "2")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "3")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = true;
                td_td2.Visible = true;

            }
        }

        //加载数据
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.content.Visible = true;
            string className = string.Empty;  //css
            try
            {


                StringBuilder sb = new StringBuilder();
                //获取MBD
                string sql = "";
                StringBuilder sb_mbd_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();
                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_mbd_sql.Append(" AND TypeCode = 'GSKOTC001'");
                }
                else if (ddlDim.SelectedIndex == 1) //渠道
                {
                    sb_mbd_sql.Append(" AND TypeCode = '"+ this.ddlType2.SelectedValue + "'");
                }
                else if (ddlDim.SelectedIndex == 2) //渠道
                {
                    sb_mbd_sql.Append(" AND TypeCode = '" + this.ddlType3.SelectedValue + "'");
                }
                else if (ddlDim.SelectedIndex == 3) //团队
                {
                    sb_mbd_sql.Append(" AND TypeCode = '" + this.ddlType4.SelectedValue + "'");
                }

                //获取posm分类
                if (plList.SelectedValue.Equals("-1"))
                {
                    sb_mbd_sql.Append(" AND 1=1 ");
                }
                else
                {
                    sb_mbd_sql.Append(" AND itempl ='" + this.plList.SelectedValue + "'");
                }

                //获取ITEM CODE
                if (ddlType.SelectedValue.ToString().Trim() == "0")
                {
                    sql = "select ItemPL POSM品类 ,ItemName POSM单品, DataCnt 分销数, DataValue POSM分销率 from Sys_Otc_ItemValue_POSM where perperid = '" + ddlTicketType.SelectedValue + "' and itemcode = 'sku'  " + sb_mbd_sql + "  order by POSM品类 ";
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "1")  //品牌
                {
                    sql = "select ItemName 品牌, DataCnt 分销数, DataValue POSM分销率 from Sys_Otc_ItemValue_POSM where perperid = '" + ddlTicketType.SelectedValue + "' and itemcode = '品牌' " + sb_mbd_sql + " order by 品牌 ";
                }
                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sql, con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    order_str = dr.GetName(0);
                    sb.Append("<th align='left' style='width:25%;' scope='col'>" + dr.GetName(i) + "</th>");
                }
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer;' >");
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        if (ddlType.SelectedValue.ToString().Trim() == "0")
                        {
                            if (i == 3)
                            {
                                sb.Append("<td class='across' style='width:50%;'>" + GetDouble(dr[i].ToString()) + "%" + "</td>");
                            }
                            else
                            {
                                sb.Append("<td class='across' style='width:25%;'>" + dr[i].ToString() + "</td>");
                            }
                        }
                        else
                        {
                            if (i == 2)
                            {
                                sb.Append("<td class='across' style='width:50%;'>" + GetDouble(dr[i].ToString()) + "%" + "</td>");
                            }
                            else
                            {
                                sb.Append("<td class='across' style='width:25%;'>" + dr[i].ToString() + "</td>");
                            }
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            this.content.Visible = false;
            LoadChart();
        }


        protected void LoadChart()
        {
            string className = string.Empty;  //css
            try
            {
                var parameter = String.Empty;
                itemName = "";
                itemValue = "";
                itemTitle = "";

                StringBuilder sb = new StringBuilder();
                //获取MBD
                string sql = "";
                StringBuilder sb_mbd_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();
                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_mbd_sql.Append(" AND TypeCode = 'GSKOTC001'");
                    itemTitle = "'POSM分销率 - 全国'";
                }
                else if (ddlDim.SelectedIndex == 1) //渠道
                {
                    if (ddlType2.SelectedIndex == 0)
                        sb_mbd_sql.Append(" AND TypeCode = 'GSKOTC002'");
                    else
                        sb_mbd_sql.Append(" AND TypeCode = 'GSKOTC003'");
                    itemTitle = "'POSM分销率 - " + ddlType2.SelectedItem.Text + "'";
                }
                else if (ddlDim.SelectedIndex == 2) //渠道
                {
                    sb_mbd_sql.Append(" AND TypeCode = '" + this.ddlType3.SelectedValue + "'");
                    itemTitle = "'POSM分销率 - " + ddlType3.SelectedItem.Text + "'";
                }
                else if (ddlDim.SelectedIndex == 3) //团队
                {
                    sb_mbd_sql.Append(" AND TypeCode = '" + this.ddlType4.SelectedValue + "'");
                    itemTitle = "'POSM分销率 - " + ddlType4.SelectedItem.Text + "'";
                }

                //获取posm分类
                if (plList.SelectedValue.Equals("-1"))
                {
                    sb_mbd_sql.Append(" AND 1=1 ");
                }
                else
                {
                    sb_mbd_sql.Append(" AND itempl ='" + this.plList.SelectedValue + "'");
                }

                //获取ITEM CODE
                if (ddlType.SelectedValue.ToString().Trim() == "0")
                {
                    sql = "select ItemPL POSM品类 ,ItemName POSM单品, DataCnt 分销数, DataValue POSM分销率 from Sys_Otc_ItemValue_POSM where perperid = '" + ddlTicketType.SelectedValue + "' and itemcode = 'sku'  " + sb_mbd_sql + "  order by POSM品类 ";
                }
                else if (ddlType.SelectedValue.ToString().Trim() == "1")  //品牌
                {
                    sql = "select ItemName 品牌, DataCnt 分销数, DataValue POSM分销率 from Sys_Otc_ItemValue_POSM where perperid = '" + ddlTicketType.SelectedValue + "' and itemcode = '品牌' " + sb_mbd_sql + " order by 品牌 ";
                }
                //生成Dataset
                DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                    {
                        if (ddlType.SelectedValue.ToString().Trim() == "0")
                        {
                            if (i == 0)
                            {
                                itemName = itemName + "'" + resultDataSet.Tables[0].Rows[i][1].ToString().Trim() + "'";
                                itemValue = itemValue + "" + GetDouble(resultDataSet.Tables[0].Rows[i][3].ToString().Trim()) + "";
                            }
                            else
                            {
                                itemName = itemName + ",'" + resultDataSet.Tables[0].Rows[i][1].ToString().Trim() + "'";
                                itemValue = itemValue + "," + GetDouble(resultDataSet.Tables[0].Rows[i][3].ToString().Trim()) + "";
                            }
                        }
                        else
                        {
                            if (i == 0)
                            {
                                itemName = itemName + "'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim().Split(' ')[0] + "'";
                                itemValue = itemValue + "" + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";
                            }
                            else
                            {
                                itemName = itemName + ",'" + resultDataSet.Tables[0].Rows[i][0].ToString().Trim().Split(' ')[0] + "'";
                                itemValue = itemValue + "," + GetDouble(resultDataSet.Tables[0].Rows[i][2].ToString().Trim()) + "";
                            }
                        }
                    }

                    itemName = "[" + itemName + "]";
                    itemValue = "[" + itemValue + "]";
                    itemTitle = "[" + itemTitle + "]";

                }
                else
                {
                    itemName = "[0]";
                    itemValue = "[0]";
                    itemTitle = "[0]";
                }
            }
            catch (Exception ex)
            {
                JavaScript.Alert(this, ex.Message);
            }
        }

        /// <summary>
        /// 数据导出
        /// </summary>
        public void btnOutput_Click(object sender, EventArgs e)
        {
            //获取MBD
            string sql = "";
            StringBuilder sb_mbd_sql = new StringBuilder();
            StringBuilder sb_itme_sql = new StringBuilder();
            if (ddlDim.SelectedIndex == 0)  //全国
            {
                sb_mbd_sql.Append(" AND TypeCode = 'GSKOTC001'");
            }
            else if (ddlDim.SelectedIndex == 1) //渠道
            {
                if (ddlType2.SelectedIndex == 0)
                    sb_mbd_sql.Append(" AND TypeCode = 'GSKOTC002'");
                else
                    sb_mbd_sql.Append(" AND TypeCode = 'GSKOTC003'");
            }
            else if (ddlDim.SelectedIndex == 2) //渠道
            {
                sb_mbd_sql.Append(" AND TypeCode = '" + this.ddlType3.SelectedValue + "'");
            }
            else if (ddlDim.SelectedIndex == 3) //团队
            {
                sb_mbd_sql.Append(" AND TypeCode = '" + this.ddlType4.SelectedValue + "'");
            }

            //获取ITEM CODE
            if (ddlType.SelectedValue.ToString().Trim() == "0")
            {
                sql = "select ItemPL POSM品类 ,ItemName POSM单品, DataCnt 分销数, DataValue POSM分销率 from Sys_Otc_ItemValue_POSM where itemcode = 'sku'  " + sb_mbd_sql + "  order by POSM品类 ";
            }
            else if (ddlType.SelectedValue.ToString().Trim() == "1")  //品牌
            {
                sql = "select ItemName 品牌, DataCnt 分销数, DataValue POSM分销率 from Sys_Otc_ItemValue_POSM where itemcode = '品牌' " + sb_mbd_sql + " order by 品牌 ";
            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }


        private decimal GetDouble(string date)
        {
            try
            {
                return decimal.Round(decimal.Parse(date) * 100, 2);
            }
            catch (Exception)
            {
                return 0;
                throw;
            }


        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (ddlType.SelectedValue.ToString().Trim() == "0")
            //{
            //    fx_qd1.Visible = true;
            //    fx_qd2.Visible = true;
            //}

            //if (ddlType.SelectedValue.ToString().Trim() == "1")
            //{
            //    fx_qd1.Visible = false;
            //    fx_qd2.Visible = false;

            //    td_qd1.Visible = false;
            //    td_qd2.Visible = false;

            //    td_dq1.Visible = false;
            //    td_dq2.Visible = false;

            //    td_td1.Visible = false;
            //    td_td2.Visible = false;
            //}
        }
    }
}