﻿<%@ Page Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="GSK_OTC_ITEM_TJ.aspx.cs" Inherits="JSKWeb.Report.GSK_OTC_ITEM_TJ" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <style type="text/css">
        .aline {
            text-decoration: underline;
        }

        .auto-style1 {
            width: 170px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td align="right" width="75px" class="contrlFontSize_s">问卷期数：
                    </td>
                    <td align="left" width="80px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="inputtext15" runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s">分析维度：
                    </td>
                    <td align="left" width="100px">
                        <asp:DropDownList ID="ddlDim" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlDim_SelectedIndexChanged">
                            <asp:ListItem Value="0">全国</asp:ListItem>
                            <asp:ListItem Value="1">渠道</asp:ListItem>
                            <asp:ListItem Value="2">大区</asp:ListItem>
                            <asp:ListItem Value="3">团队</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="td_qd1">渠道：
                    </td>
                    <td align="left" runat="server" id="td_qd2">
                        <asp:DropDownList ID="ddlType2" CssClass="inputtext15" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlType2_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" runat="server" id="td_dq1" width="80px" class="contrlFontSize_s">大区：
                    </td>
                    <td align="left" runat="server" id="td_dq2">
                        <asp:DropDownList ID="ddlType3" CssClass="inputtext15" runat="server" Width="140px" AutoPostBack="true" OnSelectedIndexChanged="ddlType3_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s" runat="server" id="td_td1">团队：
                    </td>
                    <td align="left" runat="server" id="td_td2">
                        <asp:DropDownList ID="ddlType4" CssClass="inputtext15" runat="server" Width="140px">
                        </asp:DropDownList>
                    </td>
                    <td align="left" width="150px">&nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />
                        <asp:Button runat="server"  OnClick="btnOutput_Click" CssClass="btn_s" Width="60px"   ID="btnOutput" Text="数据导出" />
                    </td>
                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center">
    </div>
</asp:Content>