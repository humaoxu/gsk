﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using JSKWeb.Controls;

namespace JSKWeb.Report
{
    public partial class OTC_SCORE : PageBase
    {
        SqlHelper sqlhelper = new SqlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            
            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();
            ddlType1.DataSource = null;
            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;
            ddlType5.DataSource = null;
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                //Loading...
                select_sql = "SELECT DISTINCT COL21 CODE,COL22 NAME  FROM  SURVEYOBJ WHERE PROJECTID=11 AND COL21 IS NOT NULL ";
                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType1.DataSource = select_ds;
                    ddlType1.DataValueField = "code";
                    ddlType1.DataTextField = "name";
                    ddlType1.DataBind();
                }
            }
            else
            {
                //Loading...
                select_sql = "SELECT DISTINCT SURVEYOBJ.COL21 CODE,SURVEYOBJ.COL22 NAME  FROM  SURVEYOBJ inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL21 IS NOT NULL ";
                select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
                if (select_ds != null)
                {
                    ddlType1.DataSource = select_ds;
                    ddlType1.DataValueField = "code";
                    ddlType1.DataTextField = "name";
                    ddlType1.DataBind();
                }
            }
            ddlType1.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));
        }

        protected void ddlType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;
            ddlType5.DataSource = null;

            ddlType2.Items.Clear();
            ddlType3.Items.Clear();
            ddlType4.Items.Clear();
            ddlType5.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = " SELECT DISTINCT  COL25 CODE,COL26 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND COL25 IS NOT NULL ";
                if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and COL21 = '" + ddlType1.SelectedValue + "' ";
                }
            }
            else
            {
                select_sql = " SELECT DISTINCT  SURVEYOBJ.COL25 CODE,SURVEYOBJ.COL26 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL25 IS NOT NULL ";
                if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.COL21 = '" + ddlType1.SelectedValue + "' ";
                }
            }

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }

            ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;
            ddlType5.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();
            ddlType5.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  COL27 CODE,COL28 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND COL27 IS NOT NULL ";

                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and COL21 = '" + ddlType1.SelectedValue + "'  and COL25 = '" + ddlType2.SelectedValue + "' ";
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL27 CODE,SURVEYOBJ.COL28 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL27 IS NOT NULL ";

                if (!ddlType2.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.COL21 = '" + ddlType1.SelectedValue + "' and SURVEYOBJ.COL25 = '" + ddlType2.SelectedValue + "' ";
                }
            }

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType3.DataSource = select_ds;
                ddlType3.DataValueField = "code";
                ddlType3.DataTextField = "name";
                ddlType3.DataBind();

            }

            ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;
            ddlType5.DataSource = null;

            ddlType4.Items.Clear();
            ddlType5.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  COL19 CODE,COL20 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND COL19 IS NOT NULL ";
                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and COL21 = '" + ddlType1.SelectedValue + "'  and COL25 = '" + ddlType2.SelectedValue + "' and COL27 = '" + ddlType3.SelectedValue + "' ";
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL19 CODE,SURVEYOBJ.COL20 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL21 IS NOT NULL ";
                if (!ddlType3.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.COL21 = '" + ddlType1.SelectedValue + "' and SURVEYOBJ.COL25 = '" + ddlType2.SelectedValue + "'  and SURVEYOBJ.COL27 = '" + ddlType3.SelectedValue + "' ";
                }
            }

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType4.DataSource = select_ds;
                ddlType4.DataValueField = "code";
                ddlType4.DataTextField = "name";
                ddlType4.DataBind();

            }

            ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType4_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlType5.DataSource = null;
            ddlType5.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";
            if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            {
                select_sql = "SELECT DISTINCT  COL17 CODE,COL18 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND COL17 IS NOT NULL ";
                if (!ddlType4.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + " and COL21 = '" + ddlType1.SelectedValue + "'  and COL25 = '" + ddlType2.SelectedValue + "' and COL27 = '" + ddlType3.SelectedValue + "' and COL19 = '" + ddlType4.SelectedValue + "' ";
                }
            }
            else
            {
                select_sql = "SELECT DISTINCT  SURVEYOBJ.COL17 CODE,SURVEYOBJ.COL18 NAME  FROM SURVEYOBJ   inner join (select * from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=11 AND SURVEYOBJ.COL17 IS NOT NULL ";
                if (!ddlType4.SelectedItem.Text.Trim().Equals("全部"))
                {
                    select_sql = select_sql + "  and SURVEYOBJ.COL21 = '" + ddlType1.SelectedValue + "' and SURVEYOBJ.COL25 = '" + ddlType2.SelectedValue + "'  and SURVEYOBJ.COL27 = '" + ddlType3.SelectedValue + "'  and SURVEYOBJ.COL19 = '" + ddlType4.SelectedValue + "' ";
                }
            }

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType5.DataSource = select_ds;
                ddlType5.DataValueField = "code";
                ddlType5.DataTextField = "name";
                ddlType5.DataBind();
            }
            ddlType5.Items.Insert(0, new ListItem("全部", string.Empty));
        }



        private bool checkDateTimeFormat(string txtTime)
        {
            try
            {
                string sDate = txtTime.Substring(0, 4) + "-" + txtTime.Substring(4, 2) + "-" + txtTime.Substring(6, 2);
                DateTime.Parse(sDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();

        }


        protected void pager_PageChanged(object sender, EventArgs e)
        {

        }


        protected void LoadInfo()
        {
            try
            {
                string strUsercode = this._LinxSanmpleUserCode;

                //dr 添加筛选
                if (!ddlType5.SelectedItem.Text.Equals("全部"))
                {
                    strUsercode = ddlType5.SelectedValue;
                }
                else if (!ddlType4.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType4.SelectedValue;
                }

                else if (!ddlType3.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType3.SelectedValue;
                }

                else if (!ddlType2.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType2.SelectedValue;
                }

                else if (!ddlType1.SelectedItem.Text.Equals("全部"))
                {

                    strUsercode = ddlType1.SelectedValue;
                }

                string strcol = "";
                string strgroupby = "";

                string strType = "0";
                if (Request.QueryString["id"] != null)
                {
                    strType = Request.QueryString["id"].ToString().Trim();
                }

                if (strType == "0")
                {
                    strcol = "col21 as OTC总部编码,col22 as OTC总部名称,";
                    strgroupby = "col21,col22";
                }
                else if (strType == "1")
                {
                    strcol = "col25 as 大区总监编码,col26 as 大区总监名称,";
                    strgroupby = "col25,col26";
                }
                else if (strType == "2")
                {
                    strcol = "col27 as 大区编码,col28 as 大区名称,";
                    strgroupby = "col27,col28";
                }
                else if (strType == "3")
                {
                    strcol = "col19 as 所属团队编码,col20 as 所属团队名称,";
                    strgroupby = "col19,col20";
                }
                else if (strType == "4")
                {
                    strcol = "col17 as 销售代表编码,col18 as 销售代表名称,";
                    strgroupby = "col17,col18";
                }
                else
                {
                    strcol = "col21 as OTC总部编码,col22 as OTC总部名称,";
                    strgroupby = "col21,col22";
                }

                StringBuilder sb = new StringBuilder();
                
                if (strUsercode.ToUpper() == InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                {
                    sb.Append(" select "+ strcol + " ");
                    sb.Append("  CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(t_score) / COUNT(1) end) ");
                    sb.Append("   as 总分, CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STPF) / COUNT(1) end) as 渗透评分 ");
                    sb.Append(" , CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(CLPF) / COUNT(1) end) as 陈列评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(POSM) / COUNT(1) end) as POSM, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(JG) / COUNT(1) end) as 价格评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(TJ) / COUNT(1) end) as 推荐评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STFJ) / COUNT(1) end) as 渗透附加分  ");
                    sb.Append("  from sys_otc_result where paperid='" + ddlTicketType.SelectedItem.Value.ToString() + "' group by " + strgroupby + " ");

                    sb.Append(@" order by  总分 desc ");
                }
                else
                {
                    sb.Append(" select "+ strcol + " ");
                    sb.Append("  CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(t_score) / COUNT(1) end) ");
                    sb.Append("   as 总分, CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STPF) / COUNT(1) end) as 渗透评分 ");
                    sb.Append(" , CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(CLPF) / COUNT(1) end) as 陈列评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(POSM) / COUNT(1) end) as POSM, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(JG) / COUNT(1) end) as 价格评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(TJ) / COUNT(1) end) as 推荐评分, ");
                    sb.Append(" CONVERT(numeric(8, 2), case when count(1) = 0 then 0 else SUM(STFJ) / COUNT(1) end) as 渗透附加分  ");
                    sb.Append(" from sys_otc_result  inner join (select * from V_OTC_USER_SHOP where usercode = '" + strUsercode + "' ) VUSER on sys_otc_result.shopcode = VUSER.shopcode  where paperid='" + ddlTicketType.SelectedItem.Value.ToString() + "' ");
                    if (this._LinxSanmpleUserCode.ToUpper() != InputText.GetConfig("gsk_user").ToString().Trim().ToUpper())
                    {
                        sb.Append(" and sys_otc_result.shopcode in (select shopcode from V_OTC_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "'  ) ");
                    }

                    sb.Append(" group by "+ strgroupby + " ");

                    sb.Append(@" order by  总分 desc ");
                }
                

                DataTable dt =  sqlhelper.GetDataTableFromSq(sb.ToString()).Tables[0];

                StringBuilder table_sb = new StringBuilder();
                if (dt != null && dt.Rows.Count>0)
                {
                    table_sb.Append("<table class ='tablestyle'  cellspacing = '0' cellpadding='2' style = 'font-size:10pt;'><tbody>");
                    table_sb.Append("<tr class='header'>");
                    table_sb.Append("<td align = 'center' style='width:60px;' scope='col'> No.</td>");
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        table_sb.Append("<td align = 'center' style='width:200px;' scope='col'> " + dt.Columns[i].ColumnName + "</td>");
                    }
                    table_sb.Append("</tr>");

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        table_sb.Append("<tr style='cursor:pointer'>");
                        table_sb.Append("<td class='across' align='center'>" + (i+1).ToString() + "</td>");
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            string strTmp = " align='left' ";
                     
                            table_sb.Append("<td class='across' "+ strTmp + ">" + dt.Rows[i][j].ToString().Trim() + "</td>");
                        }
                        table_sb.Append("</tr>");
                    }
                    
                    table_sb.Append("</tbody></table>");
                    content.InnerHtml = table_sb.ToString();
                }
                else
                {
                    content.InnerHtml = "No Data!";
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 计算Datat列合值
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        private double DataTableColumnSum(DataTable dt, string columnName)
        {
            double d = 0;
            foreach (DataRow dr in dt.Rows)
            {
                d += double.Parse(dr[columnName].ToString());
            }

            return d;
        }

        protected void gvMain_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow && e.Row.DataItem != null)
            {
                
            }
        }


    }
}