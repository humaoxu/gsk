﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.Collections;

namespace JSKWeb.Report
{
    public partial class GSK_OTC_ITEM_JG_New : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();
        ArrayList qsList = new ArrayList();
        ArrayList qdList = new ArrayList();
        ArrayList qyList = new ArrayList();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");
                DataBinds();

                qdDownCheckBoxes.Visible = true;
                qyDownCheckBoxes.Visible = false;
                tdDownCheckBoxes.Visible = false;

                plDownCheckBoxes.Visible = true;
                zplDownCheckBoxes.Visible = true;
                ppDownCheckBoxes.Visible = true;
                mcDownCheckBoxes.Visible = true;



                pl_td.Visible = true;
                zpl_td.Visible = true;
                pp_td.Visible = true;
                mc_td.Visible = true;

                lxDropDownCheckBoxes_SelectedIndexChanged(null, null);
                qsDropDownCheckBoxes_SelcetedIndexChanged(null, null);
            }
        }

        public void DataBinds()
        {
            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc  ", ddlProjectId);
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            qsDropDownCheckBoxes.DataSource = ddlTypeDataSet;
            qsDropDownCheckBoxes.DataValueField = "paperId";
            qsDropDownCheckBoxes.DataTextField = "paperTitle";
            qsDropDownCheckBoxes.DataBind();
			for (int i = 0; i < (qsDropDownCheckBoxes.Items.Count); i++)
            {
                ///期数超过1期
                if (qsDropDownCheckBoxes.Items.Count > 1)
                {
                    if (i == 0 || i == 1)
                    {
                        qsDropDownCheckBoxes.Items[i].Selected = true;
                    }
                }
                else
                {
                    qsDropDownCheckBoxes.Items[i].Selected = true;
                }
            }

        }

        protected void qsDropDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qdDownCheckBoxes.Visible = true;

            qdDownCheckBoxes.DataSource = null;
            qyDownCheckBoxes.DataSource = null;
            tdDownCheckBoxes.DataSource = null;

            qdDownCheckBoxes.Items.Clear();
            qyDownCheckBoxes.Items.Clear();
            tdDownCheckBoxes.Items.Clear();

            //selectedItemsPanel.Controls.Clear();
            //获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                    qsList.Add(item.Value);
            }

            //获取渠道
            var ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level in (0,1) ";
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qdDownCheckBoxes.DataSource = ddlTypeDataSet;
            qdDownCheckBoxes.DataValueField = "dimcode";
            qdDownCheckBoxes.DataTextField = "dimname";
            qdDownCheckBoxes.DataBind();


        }

        protected void qdDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qdDownCheckBoxes.Visible = true;
            qyDownCheckBoxes.Visible = true;
            qy_td.Style.Add("padding-left", "25px");
            qyDownCheckBoxes.DataSource = null;
            tdDownCheckBoxes.DataSource = null;


            qyDownCheckBoxes.Items.Clear();
            tdDownCheckBoxes.Items.Clear();

            qdList = new ArrayList();
            //如果全选
            int select_sum = Int32.Parse(qdDownCheckBoxes.Items.Capacity.ToString());

            //根据渠道筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    qdList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            if (qdList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - qdList.Count == 1)
                {
                    str_code = "'GSKN02KANM002','GSKN02RTNM001'";
                }
                else
                {
                    for (int i = 0; i < qdList.Count; i++)
                    {
                        str_code += "'" + qdList[i].ToString() + "',";
                    }
                }
            }
            //获取区域
            var ddlTypeSelectSql = "";
            if (str_code.Trim().Length == 0 || str_code.Trim().Equals("'GSKOTC001',"))
            {
                ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level = 2 order by dimname";
            }
            else
            {
                if (select_sum - qdList.Count == 1)
                {
                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code + ")  and level = 2 order by dimname";
                }
                else
                {

                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code.TrimEnd(',').ToString() + ")  and level = 2 order by dimname ";
                }
            }

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "dimcode";
            qyDownCheckBoxes.DataTextField = "dimname";
            qyDownCheckBoxes.DataBind();
        }

        protected void qyDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qdDownCheckBoxes.Visible = true;
            qyDownCheckBoxes.Visible = true;
            tdDownCheckBoxes.Visible = true;
            qy_td.Style.Add("padding-left", "25px");
            td_td.Style.Add("padding-left", "25px");
            tdDownCheckBoxes.DataSource = null;


            tdDownCheckBoxes.Items.Clear();

            //如果全选
            int select_sum = Int32.Parse(qyDownCheckBoxes.Items.Capacity.ToString());
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    qyList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            if (qyList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - qyList.Count == 1)
                {
                    str_code = "(select dimcode from sys_otc_dim  where level in (2))";
                }
                else
                {
                    for (int i = 0; i < qyList.Count; i++)
                    {
                        str_code += "'" + qyList[i].ToString() + "',";
                    }
                }
            }
            //获取团队
            var ddlTypeSelectSql = "";
            if (str_code.Trim().Length == 0)
            {
                ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where level =3 order by dimname ";
            }
            else
            {

                if (select_sum - qyList.Count == 1)
                {
                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code + ")  order by dimname";
                }
                else
                {

                    ddlTypeSelectSql = "select dimcode,dimname from sys_otc_dim where parentcode in (" + str_code.TrimEnd(',').ToString() + ")  order by dimname ";
                }
            }

            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            tdDownCheckBoxes.DataSource = ddlTypeDataSet;
            tdDownCheckBoxes.DataValueField = "dimcode";
            tdDownCheckBoxes.DataTextField = "dimname";
            tdDownCheckBoxes.DataBind();
        }


        protected void lxDropDownCheckBoxes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "0")
            {
                plDownCheckBoxes.Visible = true;
                zplDownCheckBoxes.Visible = true;
                ppDownCheckBoxes.Visible = true;
                mcDownCheckBoxes.Visible = true;
                pl_td.Visible = true;
                zpl_td.Visible = true;
                pp_td.Visible = true;
                mc_td.Visible = true;

                var ddlItmeTypeSelectSql = "SELECT DISTINCT LV1_CODE ,LV1_NAME  FROM SYS_OTC_ITEM ORDER BY LV1_CODE ";
                var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
                plDownCheckBoxes.DataSource = ddlItmeTypeDataSet;
                plDownCheckBoxes.DataValueField = "LV1_CODE";
                plDownCheckBoxes.DataTextField = "LV1_NAME";
                plDownCheckBoxes.DataBind();

                //for (int i = 0; i < plDownCheckBoxes.Items.Count; i++)
                //{
                //    plDownCheckBoxes.Items[i].Selected = true;

                //}

            }

            if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "-1" || lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "1" || lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "2" || lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "3")
            {
                plDownCheckBoxes.Visible = false;
                zplDownCheckBoxes.Visible = false;
                ppDownCheckBoxes.Visible = false;
                mcDownCheckBoxes.Visible = false;
                pl_td.Visible = false;
                zpl_td.Visible = false;
                pp_td.Visible = false;
                mc_td.Visible = false;

            }
        }

        protected void plDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            ArrayList plList = new ArrayList();
            //根据品类筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    plList.Add(item.Value);
                }
            }
            string str_code = string.Empty;
            if (plList.Count > 0)
            {
                for (int i = 0; i < plList.Count; i++)
                {
                    str_code += "'" + plList[i].ToString() + "',";
                }

            }
            else
            {
                str_code = "-1";
            }

            var ddlItmeTypeSelectSql = "SELECT DISTINCT LV2_CODE ,LV2_NAME  FROM SYS_OTC_ITEM WHERE LV1_CODE IN (" + str_code.TrimEnd(',').ToString() + ")   ORDER BY LV2_CODE ";
            var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
            zplDownCheckBoxes.DataSource = ddlItmeTypeDataSet;
            zplDownCheckBoxes.DataValueField = "LV2_CODE";
            zplDownCheckBoxes.DataTextField = "LV2_NAME";
            zplDownCheckBoxes.DataBind();

        }

        protected void zplDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            ArrayList ppList = new ArrayList();
            //根据子品类筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    ppList.Add(item.Value);
                }
            }
            string str_code = string.Empty;
            if (ppList.Count > 0)
            {
                for (int i = 0; i < ppList.Count; i++)
                {
                    str_code += "'" + ppList[i].ToString() + "',";
                }

            }
            else
            {
                str_code = "-1";
            }

            var ddlItmeTypeSelectSql = "SELECT DISTINCT LV3_CODE ,LV3_NAME  FROM SYS_OTC_ITEM WHERE LV2_CODE IN (" + str_code.TrimEnd(',').ToString() + ")   ORDER BY LV3_CODE ";
            var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
            ppDownCheckBoxes.DataSource = ddlItmeTypeDataSet;
            ppDownCheckBoxes.DataValueField = "LV3_CODE";
            ppDownCheckBoxes.DataTextField = "LV3_NAME";
            ppDownCheckBoxes.DataBind();

            ppDownCheckBoxes_SelcetedIndexChanged(null, null);
        }

        protected void ppDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            ArrayList mcList = new ArrayList();
            //根据子品类筛选where
            foreach (ListItem item in ppDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    mcList.Add(item.Value);
                }
            }
            string str_code = string.Empty;
            if (mcList.Count > 0)
            {
                for (int i = 0; i < mcList.Count; i++)
                {
                    str_code += "'" + mcList[i].ToString() + "',";
                }

            }
            else
            {
                str_code = "-1";
            }

            var ddlItmeTypeSelectSql = "SELECT DISTINCT LV4_CODE ,LV4_NAME  FROM SYS_OTC_ITEM WHERE LV3_CODE  IN (" + str_code.TrimEnd(',').ToString() + ") ORDER BY LV4_CODE ";
            var ddlItmeTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlItmeTypeSelectSql);
            mcDownCheckBoxes.DataSource = ddlItmeTypeDataSet;
            mcDownCheckBoxes.DataValueField = "LV4_CODE";
            mcDownCheckBoxes.DataTextField = "LV4_NAME";
            mcDownCheckBoxes.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (qsDropDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择问卷期数");
                    return;
                }

                if (qdDownCheckBoxes.SelectedValue.Equals("")
                  && qyDownCheckBoxes.SelectedValue.Equals("")
                  && tdDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择MBD筛选");
                    return;
                }

                //获取问卷期数选择值
                if (plDownCheckBoxes.SelectedValue.Equals("") && lxDropDownCheckBoxes.SelectedIndex.Equals(0))
                {
                    JavaScript.Alert(this, "请选择产品筛选");
                    return;
                }



                ArrayList plsQS = new ArrayList();  //期数
                ArrayList plsQD = new ArrayList();  //渠道
                ArrayList plsQY = new ArrayList();  //区域
                ArrayList plsTD = new ArrayList();  //团队
                ArrayList plsLX = new ArrayList();  //SKU

                StringBuilder sb = new StringBuilder();
                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();
                //获取问卷期数选择值
                if (qsDropDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择期数");
                    return;
                }
                else
                {
                    foreach (ListItem item in qsDropDownCheckBoxes.Items)
                    {
                        if (item.Selected)
                        {
                            plsQS.Add(item.Value);
                        }
                    }
                }

                //获取渠道选择值
                foreach (ListItem item in qdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQD.Add(item.Value);
                    }
                }

                //获取区域选择值
                foreach (ListItem item in qyDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQY.Add(item.Value);
                    }
                }

                //获取团对选择值
                foreach (ListItem item in tdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsTD.Add(item.Value);
                    }
                }

                //获取品类选择值
                foreach (ListItem item in plDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //获取子品类选择值
                foreach (ListItem item in zplDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //获取品牌选择值
                foreach (ListItem item in ppDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //获取产品名称选择值
                foreach (ListItem item in mcDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //SKU
                if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "0")
                {
                    //获取ITEM CODE
                    ArrayList item_code = GetSku();
                    if (item_code.Count > 0)
                    {
                        OTCTableShow(plsQS, plsQD, plsQY, plsTD, item_code, "SKU");
                    }
                }
                else if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "1")  //品类
                {
                    OTCTableShow(plsQS, plsQD, plsQY, plsTD, plsLX, "品类");
                }
                else if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "2")  //子品类
                {
                    OTCTableShow(plsQS, plsQD, plsQY, plsTD, plsLX, "子品类");
                }
                else if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "3")  //品牌
                {
                    OTCTableShow(plsQS, plsQD, plsQY, plsTD, plsLX, "品牌");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void OTCTableShow(ArrayList plsQS, ArrayList plsQD, ArrayList plsQY, ArrayList plsTD, ArrayList plsLX, string leixing)
        {
            var ddlProjectId = "";
            string sql = "";
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    ddlProjectId += "'" + item.Value + "',";
                }
            }
            if (ddlProjectId.Length > 0)
            {
                ddlProjectId = ddlProjectId.TrimEnd(',').ToString();
            }
            //var ddlTypeSelectSql = String.Format("select paperid,paperTitle from Paper where paperid in ( {0} ) and isRPTReleased =1 order by paperid desc ", ddlProjectId);
            var ddlTypeSelectSql = String.Format("select paperid,paperTitle from Paper where paperid in ( {0} )  order by paperid desc ", ddlProjectId);

            var qsDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);

            DataSet ds_qs = new DataSet();
            DataTable dt_qs = new DataTable();
            DataSet ds_lx = new DataSet();
            DataTable dt_lx = new DataTable();
            DataSet ds_data = new DataSet();

            StringBuilder sb_html = new StringBuilder();
            StringBuilder sb_header1 = new StringBuilder();
            StringBuilder sb_header2 = new StringBuilder();
            StringBuilder sb_body = new StringBuilder();
            sb_html.Append(@"<table class='tablestyle_otc' cellspacing='0' cellpadding='2' rules='all' enableemptycontentrender='True' 
border='1' style='width: 100 %; border - collapse:collapse; ' ><tbody >");

            sb_header1.Append("<tr class='header'><td></td>");
            sb_header2.Append("<tr class='header'><td></td>");

            StringBuilder sb_sql_All = new StringBuilder();
            StringBuilder sb_sql_base = new StringBuilder();
            StringBuilder sb_sql_where = new StringBuilder();
            if (leixing == "SKU")
            {
                sql = "select distinct lv4_code typecode,lv4_name typename from Sys_Otc_Item ";

            }
            else if (leixing == "品类")
            {
                sql = " select distinct ItemName,  ItemCode from Sys_Otc_ItemValue where ItemCode ='品类' and DataType ='价格' order by ItemName ";
            }
            else if (leixing == "子品类")
            {
                sql = " select distinct ItemName,  ItemCode from Sys_Otc_ItemValue where ItemCode ='子品类' and DataType ='价格' order by ItemName ";
            }
            else if (leixing == "品牌")
            {
                sql = " select distinct ItemName,  ItemCode from Sys_Otc_ItemValue where ItemCode ='品牌' and DataType ='价格' order by ItemName ";
            }
            ds_lx = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            dt_lx = ds_lx.Tables[0];

            sb_sql_All.Append(" select b.* from ( select TypeName,");

            if (leixing == "SKU")
            {
                foreach (var item in plsLX)
                {
                    var lx = dt_lx.Select("typecode='" + item + "'");
                    if (lx != null && lx.Count() > 0)
                    {
                        sb_header1.Append("<td colspan='" + plsQS.Count + "'>" + InputText.GetStrByObj(lx[0][1]) + "</td>");
                    }
                    else
                    {
                        sb_header1.Append("<td colspan='" + plsQS.Count + "'>&nbsp;</td>");
                    }
                    foreach (var qsItem in plsQS)
                    {
                        var qsItemName = qsDataSet.Tables[0].Select("paperid=" + qsItem);
                        if (qsItemName.Count() > 0)
                        {
                            sb_header2.Append("<td >" + InputText.GetString(qsItemName[0][1]) + "</td>");
                        }
                        else
                        {
                            sb_header2.Append("<td >" + qsItem + "</td>");
                        }
                        sb_sql_base.Append("MAX(case when ItemCode ='" + item + "' and PerperID = " + qsItem + " then DataValue else 0 end)  'SKU_" + item + "_" + qsItem + "',");
                    }
                }
            }
            else
            {
                foreach (DataRow lxItem in dt_lx.Rows)
                {

                    sb_header1.Append("<td colspan='" + plsQS.Count + "'>" + InputText.GetStrByObj(lxItem[0]) + "</td>");
                    foreach (var qsItem in plsQS)
                    {
                        var qsItemName = qsDataSet.Tables[0].Select("paperid=" + qsItem);
                        if (qsItemName.Count() > 0)
                        {
                            sb_header2.Append("<td >" + InputText.GetString(qsItemName[0][1]) + "</td>");
                        }
                        else
                        {
                            sb_header2.Append("<td >" + qsItem + "</td>");
                        }
                        sb_sql_base.Append("MAX(case when ItemName ='" + lxItem[0] + "' and PerperID = " + qsItem + " then DataValue else 0 end)  'SKU_" + lxItem[0] + "_" + qsItem + "',");
                    }
                }
            }




            sb_sql_All.Append(sb_sql_base.ToString().TrimEnd(',').ToString());


            sb_sql_All.Append(" from Sys_Otc_ItemValue where datatype = '价格'  and  (");

            sb_sql_where.Append(" ( 1=2 ");
            if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
            {
                sb_sql_where.Append(" or TypeCode in(");
                foreach (var item in plsQD)
                {
                    sb_sql_where.Append("'" + item + "',");
                }
                foreach (var item in plsQY)
                {
                    sb_sql_where.Append("'" + item + "',");
                }
                sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                sb_sql_where.Append(" )");
            }

            if (plsTD.Count > 0)
            {
                sb_sql_where.Append(" or TypeName in(");
                foreach (var item in plsTD)
                {
                    sb_sql_where.Append("'" + item + "',");
                }
                sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                sb_sql_where.Append(" ) ");
            }
            sb_sql_where.Append(" ) ");
            sb_sql_All.Append(sb_sql_where.ToString().TrimEnd(',')).Append(") group by TypeName )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

            try
            {
                ds_data = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql_All.ToString());
            }
            catch (Exception)
            {

                content.InnerHtml = "没有数据或条件有误";
                return;
            }


            div_sql.InnerText = sb_sql_All.ToString();
            if (ds_data != null && ds_data.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds_data.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    //if (i%2 ==0 )
                    //{
                    sb_body.Append("<tr  onclick='gv_selectRow(this)' onmouseover='gv_mouseHover(this)' style='cursor: pointer; '>");
                    //}
                    //else
                    //{
                    //    sb_body.Append("<tr>");
                    //}
                    ///查询结果包含 TypeCode 因此从1开始
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j == 0)
                        {
                            sb_body.Append("<td>" + InputText.GetStrByObj(dt.Rows[i][j]) + "</td>");
                        }
                        else
                        {
                            sb_body.Append("<td>" + GetDouble(dt.Rows[i][j].ToString()) + "</td>");
                        }
                    }
                    sb_body.Append("</tr>");
                }
                sb_header1.Append("</tr>");
                sb_header2.Append("</tr>");
                sb_body.Append("</tbody></table> ");
                content.InnerHtml = sb_html.ToString() + sb_header1.ToString() + sb_header2.ToString() + sb_body.ToString();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="plsQS">期数list</param>
        /// <param name="plsQD">渠道list</param>
        /// <param name="plsQY">区域list</param>
        /// <param name="plsTD">团队list</param>
        /// <param name="plsLX">类型list</param>
        /// <param name="leixing">类型（应传入SKU/品类/子品类/品牌）</param>



        /// <summary>
        /// 数据导出
        /// </summary>
        public void btnOutput_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string htmlOutPut = "";
            string[] header_Col = new string[2];
            try
            {
                if (qsDropDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择问卷期数");
                    return;
                }

                if (qdDownCheckBoxes.SelectedValue.Equals("")
                  && qyDownCheckBoxes.SelectedValue.Equals("")
                  && tdDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择MBD筛选");
                    return;
                }

                //获取问卷期数选择值
                if (lxDropDownCheckBoxes.SelectedValue.Equals("-1"))
                {
                    JavaScript.Alert(this, "请选择产品筛选");
                    return;
                }



                ArrayList plsQS = new ArrayList();  //期数
                ArrayList plsQD = new ArrayList();  //渠道
                ArrayList plsQY = new ArrayList();  //区域
                ArrayList plsTD = new ArrayList();  //团队
                ArrayList plsLX = new ArrayList();  //SKU

                StringBuilder sb = new StringBuilder();
                StringBuilder sb_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();
                //获取问卷期数选择值
                if (qsDropDownCheckBoxes.SelectedValue.Equals(""))
                {
                    JavaScript.Alert(this, "请选择期数");
                    return;
                }
                else
                {
                    foreach (ListItem item in qsDropDownCheckBoxes.Items)
                    {
                        if (item.Selected)
                        {
                            plsQS.Add(item.Value);
                        }
                    }
                }

                //获取渠道选择值
                foreach (ListItem item in qdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQD.Add(item.Value);
                    }
                }

                //获取区域选择值
                foreach (ListItem item in qyDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsQY.Add(item.Value);
                    }
                }

                //获取团对选择值
                foreach (ListItem item in tdDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsTD.Add(item.Value);
                    }
                }

                //获取品类选择值
                foreach (ListItem item in plDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //获取子品类选择值
                foreach (ListItem item in zplDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //获取品牌选择值
                foreach (ListItem item in ppDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //获取产品名称选择值
                foreach (ListItem item in mcDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        plsLX.Add(item.Value);
                    }
                }

                //SKU
                if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "0")
                {
                    //获取ITEM CODE
                    ArrayList item_code = GetSku();
                    if (item_code.Count > 0)
                    {
                        dt = OTCTableOutPut(plsQS, plsQD, plsQY, plsTD, item_code, "SKU",out header_Col);
                    }
  
                }
                else if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "1")  //品类
                {
                    dt =  OTCTableOutPut(plsQS, plsQD, plsQY, plsTD, plsLX, "品类", out header_Col);
                }
                else if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "2")  //子品类
                {
                    dt = OTCTableOutPut(plsQS, plsQD, plsQY, plsTD, plsLX, "子品类", out header_Col);
                }
                else if (lxDropDownCheckBoxes.SelectedValue.ToString().Trim() == "3")  //品牌
                {
                    dt =  OTCTableOutPut(plsQS, plsQD, plsQY, plsTD, plsLX, "品牌", out header_Col);
                }
  
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //if (htmlOutPut.Length == 0 || htmlOutPut.Contains("Erro"))
            //{
            //    JavaScript.Alert(this,"没有查到有效的数据");
            //    return;
            //}
            ExcelOutPut excelOutPut = new ExcelOutPut();
            System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(dt,  header_Col  );
            try
            {
                byte[] bt = ms.ToArray();
                //以字符流的形式下载文件  
                Response.ContentType = "application/vnd.ms-excel";
                //通知浏览器下载文件而不是打开
                Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                Response.BinaryWrite(bt);

                Response.Flush();
                Response.End();
                bt = null;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (ms != null) ms.Dispose();
            }
        }

        public DataTable OTCTableOutPut(ArrayList plsQS, ArrayList plsQD, ArrayList plsQY, ArrayList plsTD, ArrayList plsLX, string leixing, out string[] header)
        {
            header = new string[2];
            string header1 = ",";
            string header2 = ",";

            var ddlProjectId = "";
            string sql = "";
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    ddlProjectId += "'" + item.Value + "',";
                }
            }
            if (ddlProjectId.Length > 0)
            {
                ddlProjectId = ddlProjectId.TrimEnd(',').ToString();
            }
            //var ddlTypeSelectSql = String.Format("select paperid,paperTitle from Paper where paperid in ( {0} ) and isRPTReleased =1 order by paperid desc ", ddlProjectId);
            var ddlTypeSelectSql = String.Format("select paperid,paperTitle from Paper where paperid in ( {0} )  order by paperid desc ", ddlProjectId);

            var qsDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);

            DataSet ds_qs = new DataSet();
            DataTable dt_qs = new DataTable();
            DataSet ds_lx = new DataSet();
            DataTable dt_lx = new DataTable();
            DataSet ds_data = new DataSet();

            StringBuilder sb_html = new StringBuilder();
            StringBuilder sb_header1 = new StringBuilder();
            StringBuilder sb_header2 = new StringBuilder();
            StringBuilder sb_body = new StringBuilder();
            sb_html.Append(@"<table class='tablestyle_otc' cellspacing='0' cellpadding='2' rules='all' enableemptycontentrender='True' 
border='1' style='width: 100 %; border - collapse:collapse; ' ><tbody >");

            sb_header1.Append("<tr class='header'><td></td>");
            sb_header2.Append("<tr class='header'><td></td>");

            StringBuilder sb_sql_All = new StringBuilder();
            StringBuilder sb_sql_base = new StringBuilder();
            StringBuilder sb_sql_where = new StringBuilder();
            if (leixing == "SKU")
            {
                sql = "select distinct lv4_code typecode,lv4_name typename from Sys_Otc_Item ";

            }
            else if (leixing == "品类")
            {
                sql = " select distinct ItemName,  ItemCode from Sys_Otc_ItemValue where ItemCode ='品类' and DataType ='价格' order by ItemName ";
            }
            else if (leixing == "子品类")
            {
                sql = " select distinct ItemName,  ItemCode from Sys_Otc_ItemValue where ItemCode ='子品类' and DataType ='价格' order by ItemName ";
            }
            else if (leixing == "品牌")
            {
                sql = " select distinct ItemName,  ItemCode from Sys_Otc_ItemValue where ItemCode ='品牌' and DataType ='价格' order by ItemName ";
            }
            ds_lx = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
            dt_lx = ds_lx.Tables[0];

            sb_sql_All.Append(" select b.* from ( select TypeName,");

            if (leixing == "SKU")
            {
                foreach (var item in plsLX)
                {
                    var lx = dt_lx.Select("typecode='" + item + "'");
                    if (lx != null && lx.Count() > 0)
                    {
                        sb_header1.Append("<td colspan='" + plsQS.Count + "'>" + InputText.GetStrByObj(lx[0][1]) + "</td>");
                    }
                    else
                    {
                        sb_header1.Append("<td colspan='" + plsQS.Count + "'>&nbsp;</td>");
                    }
                    foreach (var qsItem in plsQS)
                    {
                        var qsItemName = qsDataSet.Tables[0].Select("paperid=" + qsItem);
                        if (qsItemName.Count() > 0)
                        {
                            sb_header2.Append("<td >" + InputText.GetString(qsItemName[0][1]) + "</td>");
                        }
                        else
                        {
                            sb_header2.Append("<td >" + qsItem + "</td>");
                        }
                        header1 += InputText.GetStrByObj(lx[0][1]) + ",";
                        header2 += InputText.GetStrByObj(qsItemName[0][1]) + ",";

                        sb_sql_base.Append("MAX(case when ItemCode ='" + item + "' and PerperID = " + qsItem + " then DataValue else 0 end)  'SKU_" + item + "_" + qsItem + "',");
                    }
                }
            }
            else
            {
                foreach (DataRow lxItem in dt_lx.Rows)
                {

                    sb_header1.Append("<td colspan='" + plsQS.Count + "'>" + InputText.GetStrByObj(lxItem[0]) + "</td>");
                    foreach (var qsItem in plsQS)
                    {
                        var qsItemName = qsDataSet.Tables[0].Select("paperid=" + qsItem);
                        if (qsItemName.Count() > 0)
                        {
                            sb_header2.Append("<td >" + InputText.GetString(qsItemName[0][1]) + "</td>");
                        }
                        else
                        {
                            sb_header2.Append("<td >" + qsItem + "</td>");
                        }
                        header1 += InputText.GetStrByObj(lxItem[0]) + ",";
                        header2 += InputText.GetStrByObj(qsItemName[0][1]) + ",";

                        sb_sql_base.Append("MAX(case when ItemName ='" + lxItem[0] + "' and PerperID = " + qsItem + " then DataValue else 0 end)  'SKU_" + lxItem[0] + "_" + qsItem + "',");
                    }
                }
            }


            header[0] = header1.TrimEnd(',');
            header[1] = header2.TrimEnd(',');

            sb_sql_All.Append(sb_sql_base.ToString().TrimEnd(',').ToString());


            sb_sql_All.Append(" from Sys_Otc_ItemValue where datatype = '价格'  and  (");

            sb_sql_where.Append(" ( 1=2 ");
            if (plsQD.Count > 0 || plsQY.Count > 0 || plsQD.Count > 0)
            {
                sb_sql_where.Append(" or TypeCode in(");
                foreach (var item in plsQD)
                {
                    sb_sql_where.Append("'" + item + "',");
                }
                foreach (var item in plsQY)
                {
                    sb_sql_where.Append("'" + item + "',");
                }
                sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                sb_sql_where.Append(" )");
            }

            if (plsTD.Count > 0)
            {
                sb_sql_where.Append(" or TypeName in(");
                foreach (var item in plsTD)
                {
                    sb_sql_where.Append("'" + item + "',");
                }
                sb_sql_where.Remove(sb_sql_where.Length - 1, 1);
                sb_sql_where.Append(" ) ");
            }
            sb_sql_where.Append(" ) ");
            sb_sql_All.Append(sb_sql_where.ToString().TrimEnd(',')).Append(") group by TypeName )b  left join (select distinct dimname,level from  sys_otc_dim) o on o.dimname = b.TypeName order by o.level ");

            try
            {
               return  sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql_All.ToString()).Tables[0];
            }
            catch (Exception)
            {

                return null;
            } 
           
        }


        private string listTostring(ArrayList list)
        {
            string s = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (s == "")
                {
                    s += "'" + list[i] + "'";
                }
                else
                {
                    s += ",'" + list[i] + "'";
                }
            }
            return s;
        }

        //获取SKUCODE
        private ArrayList GetSku()
        {
            string sku_items = string.Empty;
            ArrayList items = new ArrayList();
            ArrayList pls1 = new ArrayList();
            ArrayList pls2 = new ArrayList();
            ArrayList pls3 = new ArrayList();
            ArrayList pls4 = new ArrayList();
            string sql = string.Empty;
            try
            {
                //获取品类选择值
                foreach (ListItem item in plDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        pls1.Add(item.Value);
                    }

                    if (pls1.Count > 0)
                    {
                        //获取子品类选择值
                        foreach (ListItem item1 in zplDownCheckBoxes.Items)
                        {
                            if (item1.Selected)
                            {
                                pls2.Add(item1.Value);
                            }
                        }

                        if (pls2.Count > 0)
                        {
                            //获取品牌选择值
                            foreach (ListItem item2 in ppDownCheckBoxes.Items)
                            {
                                if (item2.Selected)
                                {
                                    pls3.Add(item2.Value);
                                }
                            }

                            if (pls3.Count > 0)
                            {
                                //获取产品名称选择值
                                foreach (ListItem item3 in mcDownCheckBoxes.Items)
                                {
                                    if (item3.Selected)
                                    {
                                        pls4.Add(item3.Value);
                                    }
                                }

                                if (pls4.Count > 0)
                                {

                                    sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE lv1_Code in (" + listTostring(pls1) + ")  and  lv2_Code in (" + listTostring(pls2) + ")  and  lv3_Code in (" + listTostring(pls3) + ") and  lv4_Code in (" + listTostring(pls4) + ") ";
                                }
                                else
                                {
                                    sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE lv1_Code in (" + listTostring(pls1) + ")  and  lv2_Code in (" + listTostring(pls2) + ")  and  lv3_Code in (" + listTostring(pls3) + ")";
                                }
                            }
                            else
                            {
                                sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE  lv1_Code in (" + listTostring(pls1) + ")  and  lv2_Code in (" + listTostring(pls2) + ") ";
                            }

                        }
                        else
                        {
                            sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE lv1_Code in (" + listTostring(pls1) + ")";
                        }
                    }
                    else
                    {
                        sql = "select Lv4_Code,Lv4_Name from Sys_Otc_Item WHERE 1=1 ";
                    }
                }

                DataSet resultDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql);
                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                    {
                        items.Add(resultDataSet.Tables[0].Rows[i]["Lv4_Code"].ToString().Trim());
                    }

                    //string s = "";
                    //for (int i = 0; i < items.Count; i++)
                    //{
                    //    if (s == "")
                    //    {
                    //        s += "'" + items[i] + "'";
                    //    }
                    //    else
                    //    {
                    //        s += ",'" + items[i] + "'";
                    //    }
                    //}
                    return items;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private decimal GetDouble(string date)
        {
            try
            {
                return decimal.Round(decimal.Parse(date), 2);
            }
            catch (Exception)
            {
                return 0;
                throw;
            }


        }

    }
}