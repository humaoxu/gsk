﻿<%@ Page Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="GSK_OTC_ITEM_TJ_New.aspx.cs" Inherits="JSKWeb.Report.GSK_OTC_ITEM_TJ_New" %>


<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <link rel="Stylesheet" type="text/css" href="../css/CustomDDStyles.css" />
    <style type="text/css">
        select {
            border-color: #cccccc;
            border-width: 1px;
            border-style: solid;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td align="right" width="80px" class="contrlFontSize_s">问卷期数：
                    </td>
                    <td align="left" width="80px">
                        <asp:DropDownList ID="ddlTicketType" CssClass="dd_chk_select" runat="server" Width="80px" AutoPostBack="True" OnSelectedIndexChanged="ddlTicketType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="right" width="80px" class="contrlFontSize_s">分析维度：
                    </td>
                    <td style="padding-left: 25px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qdDownCheckBoxes" runat="server" OnSelectedIndexChanged="qdDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td id="qy_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qyDownCheckBoxes" runat="server" OnSelectedIndexChanged="qyDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td id="td_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="tdDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="团队" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td align="left" width="150px">&nbsp;
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />
                        <asp:Button runat="server" OnClick="btnOutput_Click" CssClass="btn_s" Width="60px" ID="btnOutput" Text="数据导出" />
                    </td>
                </tr>
            </tbody>
        </table>
        <div style="height: 5px"></div>
    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center">
    </div>
    <div style="margin-top: 20px;"></div>
    <div id="content2" runat="server" style="text-align: center;">
    </div>
    <div style="margin-top: 20px;"></div>
    <div id="content3" runat="server" style="text-align: center;">
    </div>
    <div id="content4" runat="server" style="text-align: center;">
    </div>
    <div id="content5" runat="server" style="text-align: center;">
    </div>
    <div id="chart_100" style="width: 100%; height: 500px;"></div>
    <div id="chart_200" style="width: 100%; height: 500px;"></div>
    <script type="text/javascript">
        var s_legentdata = <%= this.lengenddata %>;
        var s_xkh_q1 = <%= this.xkh_q1 %>;
        var s_xkl_q1 = <%= this.xkl_q1 %>;

        var s_sum = <%= this.s_sum %>;
        var colorList = ['#0076BB', '#F9BB00','#D77569']; 

        var chart_h = <%= this.chart_h %>;

        if(s_sum>10)
            s_sum = 45;
        else 
            s_sum = 0;
        if ($("#topTool_ddlTicketType option[selected=selected]").val() == '27' || $("#topTool_ddlTicketType option[selected=selected]").val() == '74') {
            var title = '';
            if($("#topTool_ddlTicketType option[selected=selected]").val() == '27')
            {
                title = '2016.Q1推荐';
            }
            else
            {
                title = '2017.Q1推荐';
            }
            option = {
                title : {
                    x:'center',
                    //subtext: '2016.Q1推荐'
                    subtext: title
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['新康红装', '新康蓝装']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        axisLabel: {
                            interval: 0,
                            rotate: s_sum,
                            margin: 2,
                            textStyle: {
                                color: "#222"
                            }
                        },
                        data: s_legentdata
                    }
                ],
                yAxis:  {
                    type: 'value' 
                },
                series: [
                    {
                        name: '新康红装',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_xkh_q1, 
                        itemStyle: {
                            normal: {
                                color: colorList[0] 
                            }
                        }
                    },
                    {
                        name: '新康蓝装',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_xkl_q1,
                        itemStyle: {
                            normal: {
                                color: colorList[1] 
                            }
                        }
                    }
                ]
            };

            var chart = echarts.init(document.getElementById('chart_100'));

            chart.setOption(option);
        }
        else if ($("#topTool_ddlTicketType option[selected=selected]").val() == '60'){
            var s_bdb_q3 = <%= this.bdb_q3 %>;
            var s_fsl_q3 = <%= this.fsl_q3 %>;
            option1 = {
                title : {
                    x:'center',
                    subtext: '2016.Q3推荐'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['百多邦乳膏']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        axisLabel: {
                            interval: 0,
                            rotate: s_sum,
                            margin: 2,
                            textStyle: {
                                color: "#222"
                            }
                        },
                        data: s_legentdata
                    }
                ],
                yAxis:  {
                    type: 'value'
                },
                series: [
                    {
                        name: '百多邦乳膏',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_bdb_q3,
                        itemStyle: {
                            normal: {
                                color: colorList[2] 
                            }
                        }
                    }
                ]
            };

            //辅舒良
            option2 = {
                title : {
                    x:'center',
                    subtext: '2016.Q3推荐'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['辅舒良']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        axisLabel: {
                            interval: 0,
                            rotate: s_sum,
                            margin: 2,
                            textStyle: {
                                color: "#222"
                            }
                        },
                        data: s_legentdata
                    }
                ],
                yAxis:  {
                    type: 'value'
                },
                series: [
                    {
                        name: '辅舒良',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_fsl_q3, 
                        itemStyle: {
                            normal: {
                                color: colorList[0] 
                            }
                        }
                    }
                ]
            };

            

            var chart21 = echarts.init(document.getElementById('chart_100'));
            var chart22 = echarts.init(document.getElementById('chart_200'));


            chart21.setOption(option1);
            chart22.setOption(option2);

        }else if ($("#topTool_ddlTicketType option[selected=selected]").val() == '66') {
            var s_f300_q4 = <%= this.f300_q4 %>;
            var s_f400_q4 = <%= this.f400_q4 %>;
            var s_fbdjjp_q4 = <%= this.fbdjjp_q4 %>;
            var s_xkh_q4 = <%= this.xkh_q4 %>;
            var s_xkl_q4 = <%= this.xkl_q4 %>;

            title = '2016.Q4推荐';
            option1 = {
                title : {
                    x:'center',
                    subtext: title
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['新康红装', '新康蓝装']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        axisLabel: {
                            interval: 0,
                            rotate: s_sum,
                            margin: 2,
                            textStyle: {
                                color: "#222"
                            }
                        },
                        data: s_legentdata
                    }
                ],
                yAxis:  {
                    type: 'value' 
                },
                series: [
                    {
                        name: '新康红装',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_xkh_q4, 
                        itemStyle: {
                            normal: {
                                color: colorList[0] 
                            }
                        }
                    },
                    {
                        name: '新康蓝装',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_xkl_q4,
                        itemStyle: {
                            normal: {
                                color: colorList[1] 
                            }
                        }
                    }
                ]
            };

            option2 = {
                title : {
                    x:'center',
                    subtext: title
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['芬必得300', '芬必得400','芬必得咀嚼片']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                        {
                            type: 'category',
                            axisLabel: {
                                interval: 0,
                                rotate: s_sum,
                                margin: 2,
                                textStyle: {
                                    color: "#222"
                                }
                            },
                            data: s_legentdata
                        }
                ],
                yAxis:  {
                    type: 'value'
                },
                series: [
                        {
                            name: '芬必得300',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_f300_q4, 
                            itemStyle: {
                                normal: {
                                    color: colorList[0] 
                                }
                            }
                        },
                        {
                            name: '芬必得400',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_f400_q4,
                            itemStyle: {
                                normal: {
                                    color: colorList[1] 
                                }
                            }
                        },
                        {
                            name: '芬必得咀嚼片',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_fbdjjp_q4,
                            itemStyle: {
                                normal: {
                                    color: colorList[2] 
                                }
                            }
                        }
                ]
            };

            var chart21 = echarts.init(document.getElementById('chart_100'));
            var chart22 = echarts.init(document.getElementById('chart_200'));

            chart21.setOption(option1);
            chart22.setOption(option2);
        }
        else if ($("#topTool_ddlTicketType option[selected=selected]").val() == '82') {
            var s_f300_q4 = <%= this.f300_q4 %>;
            var s_f400_q4 = <%= this.f400_q4 %>;
            var s_fbdjjp_q4 = <%= this.fbdjjp_q4 %>;
            var s_ftlrg_2017q2 = <%= this.ftlrg_2017q2 %>;

            title = '2017.Q2推荐';
                
                
            option1 = {
                title : {
                    x:'center',
                    subtext: title
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['扶他林乳膏']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        axisLabel: {
                            interval: 0,
                            rotate: s_sum,
                            margin: 2,
                            textStyle: {
                                color: "#222"
                            }
                        },
                        data: s_legentdata
                    }
                ],
                yAxis:  {
                    type: 'value' 
                },
                series: [
                    {
                        name: '扶他林乳膏',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_ftlrg_2017q2, 
                        itemStyle: {
                            normal: {
                                color: colorList[0] 
                            }
                        }
                    }
                ]
            };

            option2 = {
                title : {
                    x:'center',
                    subtext: title
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['芬必得300', '芬必得400','芬必得咀嚼片']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                        {
                            type: 'category',
                            axisLabel: {
                                interval: 0,
                                rotate: s_sum,
                                margin: 2,
                                textStyle: {
                                    color: "#222"
                                }
                            },
                            data: s_legentdata
                        }
                ],
                yAxis:  {
                    type: 'value'
                },
                series: [
                        {
                            name: '芬必得300',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_f300_q4, 
                            itemStyle: {
                                normal: {
                                    color: colorList[0] 
                                }
                            }
                        },
                        {
                            name: '芬必得400',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_f400_q4,
                            itemStyle: {
                                normal: {
                                    color: colorList[1] 
                                }
                            }
                        },
                        {
                            name: '芬必得咀嚼片',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_fbdjjp_q4,
                            itemStyle: {
                                normal: {
                                    color: colorList[2] 
                                }
                            }
                        }
                ]
            };
      

            var chart21 = echarts.init(document.getElementById('chart_100'));
            var chart22 = echarts.init(document.getElementById('chart_200'));

            chart21.setOption(option1);
            chart22.setOption(option2);
        }
        else{
            var s_f300_q2 = <%= this.f300_q2 %>;
            var s_f400_q2 = <%= this.f400_q2 %>;
            var s_fmk_q2 = <%= this.fmk_q2 %>;
            var s_ftl_q2 = <%= this.ftl_q2 %>;
            option1 = {
                title : {
                    x:'center',
                    subtext: '2016.Q2推荐'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['芬必得300', '芬必得400','芬必得酚咖片']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                        {
                            type: 'category',
                            axisLabel: {
                                interval: 0,
                                rotate: s_sum,
                                margin: 2,
                                textStyle: {
                                    color: "#222"
                                }
                            },
                            data: s_legentdata
                        }
                ],
                yAxis:  {
                    type: 'value'
                },
                series: [
                        {
                            name: '芬必得300',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_f300_q2, 
                            itemStyle: {
                                normal: {
                                    color: colorList[0] 
                                }
                            }
                        },
                        {
                            name: '芬必得400',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_f400_q2,
                            itemStyle: {
                                normal: {
                                    color: colorList[1] 
                                }
                            }
                        },
                        {
                            name: '芬必得酚咖片',
                            type: 'bar',
                            stack: '推荐率',
                            label: {
                                normal: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            data: s_fmk_q2,
                            itemStyle: {
                                normal: {
                                    color: colorList[2] 
                                }
                            }
                        }
                ]
            };

            //扶他林乳膏
            option2 = {
                title : {
                    x:'center',
                    subtext: '2016.Q2推荐'
                },
                tooltip : {
                    trigger: 'axis',
                    axisPointer : {            
                        type : 'shadow'        
                    }
                },
                calculable : true,
                legend: {
                    data: ['扶他林乳膏']
                },
                grid: {
                    x: 110,
                    y: 80,
                    y2: 150,
                },
                toolbox: {
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        axisLabel: {
                            interval: 0,
                            rotate: s_sum,
                            margin: 2,
                            textStyle: {
                                color: "#222"
                            }
                        },
                        data: s_legentdata
                    }
                ],
                yAxis:  {
                    type: 'value'
                },
                series: [
                    {
                        name: '扶他林乳膏',
                        type: 'bar',
                        stack: '推荐率',
                        label: {
                            normal: {
                                show: true,
                                position: 'inside'
                            }
                        },
                        data: s_ftl_q2, 
                        itemStyle: {
                            normal: {
                                color: colorList[0] 
                            }
                        }
                    }
                ]
            };

            

            var chart21 = echarts.init(document.getElementById('chart_100'));
            var chart22 = echarts.init(document.getElementById('chart_200'));


            chart21.setOption(option1);
            chart22.setOption(option2);

        }


        
    </script>
</asp:Content>
