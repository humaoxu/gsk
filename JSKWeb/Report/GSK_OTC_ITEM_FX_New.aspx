﻿<%@ Page Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="GSK_OTC_ITEM_FX_New.aspx.cs" Inherits="JSKWeb.Report.GSK_OTC_ITEM_FX_New" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <link rel="Stylesheet" type="text/css" href="../css/CustomDDStyles.css" />

    <style>
        select {
            border-color: #cccccc;
            border-width: 1px;
            border-style: solid;
        }

    </style>

    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        var s_sum = <%= this.s_sum %>;
        var s_itemName = [];
        var s_itemValue = []; 
        var s_itemTitle = [];
        var series_tmp = [];
        var s_xAxis_data = [];
        function gv_selectRow(row ){  
            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;

                if ($("#topTool_lxDropDownCheckBoxes").get(0).selectedIndex == 3  ) {
                    bindChart();
                }
                else {
                    baindEchar2();
                }
             
                $('html,body').animate({ scrollTop: $("#main_echar").offset().top }, 1200);
                $("#main_echar").width($(document.body).width());
            }
        }

        function bindChart()
        {
            s_itemValue = [];
            s_itemName = [];
            var colorList = [
             '#009DD9', '#00D050', '#ff0000', '#ffc000', '#be0059', '#ff6600', '#83f3ff',
             '#7f7f7f', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0'
            ];
            var i = 0;
            var r = 0;
            var step = $(".tablestyle_otc tr:eq(0) td:gt(0)").length;
            $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").each(
                function(){
                    s_xAxis_data[i] =  $(this).next().text();
                    i++;
                })
            i= 0;
            if($("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").length == 1)
            {
                step =1;
                i=0;
                r =0;
                var barname = "";
                $(".select td:gt(0)").each(
                    function()
                    {
                        s_itemValue = [];
                        s_itemValue[0] = parseFloat($(this).text());
                        s_itemName[i] = "" + $(this).text() + "";
                        barname =  $(".tablestyle_otc tr:eq(0) td:eq("+(i+1)+")").text();
                        s_itemName[i] = barname;   
                        series_tmp[i] = {
                            name: "" +barname+ "", data: s_itemValue, type: 'bar', itemStyle: {
                                normal: {
                                    color: colorList[i],
                                    label : {
                                        show: true,
                                        position: 'top',
                                        textStyle: {
                                            color: 'black'
                                        }
                                    }
                                }
                            }
                        }
                        i++;
                    }
                     
         )



            }else
            {
       
                var z = 0;
                var n = 2;
                var y = 1;
                $(".tablestyle_otc tr:eq(0) td:gt(0)").each(
                    function () {
                        s_itemName[i] = "" + $(this).text() + "";
                        s_itemValue = [];
                        r = 0;
                        $(".select td:gt(0)").each(
                        function () {

                            if (r == (y * 2 - n)) {
                                s_itemValue[z] = parseFloat($(this).text());
                                z++;
                                n--;
                            }
                            r++;
                            if (n == 0) {
                                n = 2;
                            }
                            if (z > 1) {
                                z = 0;
                            }
                        })

                        series_tmp[i] = {
                            name: "" + $(this).text() + "", data: s_itemValue, type: 'bar', itemStyle: {
                                normal: {
                                    color: colorList[i],
                                    label : {
                                        show: true,
                                        position: 'top',
                                        textStyle: {
                                            color: 'black'
                                        }
                                    }
                                }
                            }
                        }
                        y++;
                        i++;
                    })
            }
           
 
            option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    data: s_itemName
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: { show: true },
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'stack'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        data: s_xAxis_data
                    }
                ],
                grid: {
                    x: 40,
                    x2: 20,
                    y2: 100,
                },
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: series_tmp
            };

            var chart = echarts.init(document.getElementById('main_echar'));

           
            chart.setOption(option);

            $("#main_echar").resize(function () {
                $("#main_echar").resize();
            })
        }
      
        function baindEchar2()
        {
            if(s_sum>10)
                s_sum = 45;
            else 
                s_sum = 0;
            var colorList = [
              '#FFC000', '#009DFF', '#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#6495ed',
              '#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0'
            ]; 
 
            series_tmp = [];
            s_itemName = [];
            s_itemValue = [];
            s_legend = [];
            s_itemTitle = ["分销率—" + $(".select td:eq(0)").text()];
            var i = 0;
            var r = 0;
            var qs = 0;
            var step = $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").length;

            $(".tablestyle_otc tr:eq(0) td:gt(0)").each(
               function () {
                   s_itemName[i] = "" + $(this).text() + "";
                   i++;
               })

            var gridWidth = $(document.body).width() ;
           

            $(".tablestyle_otc tr:eq(1) td:gt(1)").each(
               function () {
                   s_itemName[i] = "" + $(this).text() + "";
                   i++;
               })

            $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").each(
             function () {
                 r = 0;
                 i = 0;
                 s_itemValue = [];
                 $(".select td:gt(0)").each(
                  function () {
                      if (i == (qs + r * step)) {
                          s_itemValue[r] = parseFloat($(this).text().replace("%", ""));
                          r++;
                      }
                      i++;
                  })

                 series_tmp[qs] = {
                     data: s_itemValue,
                     type: 'bar',
                     name: $(".tablestyle_otc tr:eq(1) td:eq(" + (qs + 1) + ")").text() + '期',
                     itemStyle: {
                         normal: {
                             color: colorList[qs],
                             label : {
                                 show: true,
                                 position: 'top',
                                 textStyle: {
                                     color: 'black'
                                 }
                             }
                         }
                     }
                 }

                 
                 s_legend[qs] = $(".tablestyle_otc tr:eq(1) td:eq(" + (qs +1) + ")").text() + '期';

                 qs++;
             })
           

            option = {          
                title: {
                    x:'left',
                    text:s_itemTitle
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: s_legend
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis: [
                     {
                         type: 'category',
                         axisLabel: {
                             interval: 0,
                             rotate: s_sum,
                             margin: 2,
                             textStyle: {
                                 color: "#222"
                             }
                         },
                         data: s_itemName
                     }
                ],
                grid: {
                    x: 60,
                    y: 60,
                    x2: 40,
                    y2: 150,
                },
                yAxis: [
                      {
                          type: 'value',
                          //min: 0,
                          //max: 100,
                          //interval: 20,
                          axisLabel: {
                              formatter: '{value} %'
                          }
                      }
                ],
                series: series_tmp    
            };

            var chart = echarts.init(document.getElementById('main_echar'));

            chart.setOption(option);

        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;MBD筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td style="padding: 10px 0px 10px 10px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qsDropDownCheckBoxes" runat="server" OnSelectedIndexChanged="qsDropDownCheckBoxes_SelcetedIndexChanged" 
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="问卷期数" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td style="padding-left: 25px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qdDownCheckBoxes" runat="server" OnSelectedIndexChanged="qdDownCheckBoxes_SelcetedIndexChanged" 
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td id="qy_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qyDownCheckBoxes" runat="server" OnSelectedIndexChanged="qyDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td id="td_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="tdDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="团队" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 20px;" align="left" width="150px">&nbsp;
                        <asp:Button ID="Button1" OnClick="btnSearch_Click" CssClass="btn_s"
                            runat="server" Text="数据查询" Width="60px" />
                        <asp:Button runat="server" OnClick="btnOutput_Click" CssClass="btn_s" Width="60px" ID="Button2" Text="数据导出" />
                        <button type="button" value="查询条件"></button>
                    </td>

                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>
    </fieldset>
    <fieldset>
        <legend>&nbsp;产品筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                    <td style="padding: 10px 0px 10px 10px;">
                        <asp:DropDownList ID="lxDropDownCheckBoxes" CssClass="dd_chk_select" runat="server" Width="160px" AutoPostBack="True" OnSelectedIndexChanged="lxDropDownCheckBoxes_SelectedIndexChanged">
                            <asp:ListItem Value="0">Sku</asp:ListItem>
                            <asp:ListItem Value="1">品类</asp:ListItem>
                            <asp:ListItem Value="2">子品类</asp:ListItem>
                           <%-- <asp:ListItem Value="3">品牌</asp:ListItem>--%>
                        </asp:DropDownList>

                    </td>
                    <td class="paddingleft25" id="pl_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="plDownCheckBoxes" runat="server" OnSelectedIndexChanged="plDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="品类" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                        <asp:DropDownList ID="plddl" CssClass="dropdownlist" Width="160" AutoPostBack="true" Visible="false" runat="server" OnSelectedIndexChanged="plddl_SelcetedIndexChanged"></asp:DropDownList>
                    </td>

                    <td class="paddingleft25" id="zpl_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="zplDownCheckBoxes" runat="server" OnSelectedIndexChanged="zplDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="子品类" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                        <asp:DropDownList ID="zplddl" CssClass="dropdownlist" Width="160" AutoPostBack="true" Visible="false" runat="server" OnSelectedIndexChanged="zplddl_SelcetedIndexChanged"></asp:DropDownList>
                    </td>

                    <td class="paddingleft25" id="pp_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="ppDownCheckBoxes" runat="server" OnSelectedIndexChanged="ppDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="品牌" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>

                    </td>

                    <td class="paddingleft25" id="mc_td" runat="server">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="mcDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="280" DropDownBoxBoxWidth="280" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="产品名称" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                </tr>

            </tbody>
        </table>
        <div style="height: 5px"></div>

    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center;">
    </div>

    <div id="Div1" runat="server" style="text-align: center">
    </div>
    <div runat="server" id="div_sql" style="display: none;"></div>
    <div id="main_echar" style="height: 600px;"></div>
    <p id="echarBotton" style="margin-top: 20px;">&nbsp;</p>
    <script type="text/javascript">

        $("#main_echar").width($(document.body).width());
    </script>
</asp:Content>
