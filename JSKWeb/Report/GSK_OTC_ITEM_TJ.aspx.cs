﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;

namespace JSKWeb.Report
{
    public partial class GSK_OTC_ITEM_TJ : System.Web.UI.Page
    {
        public string itemName = "";
        public string itemValue = "";

        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");

                TicketTypeDataBind();
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;
            }
        }

        public void TicketTypeDataBind()
        {

            var ddlProjectId = InputText.GetConfig("gsk_otc");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();


            //加载关联关系（如果为GSKGLOBAL）
            var select_sql = string.Empty;
            DataSet select_ds = new DataSet();

            ddlType2.DataSource = null;
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;


            ddlType2.Items.Clear();
            ddlType3.Items.Clear();
            ddlType4.Items.Clear();


            select_sql = " SELECT COL25 CODE,COL26 NAME FROM ( SELECT COL25,COL26, ROW_NUMBER () OVER (PARTITION BY COL26 ORDER BY COL26) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL25 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType2.DataSource = select_ds;
                ddlType2.DataValueField = "code";
                ddlType2.DataTextField = "name";
                ddlType2.DataBind();
            }

            select_sql = " SELECT COL27 CODE,COL28 NAME FROM ( SELECT COL27,COL28, ROW_NUMBER () OVER (PARTITION BY COL27 ORDER BY COL27) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL27 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType3.DataSource = select_ds;
                ddlType3.DataValueField = "code";
                ddlType3.DataTextField = "name";
                ddlType3.DataBind();
            }

            select_sql = " SELECT COL19 CODE,COL20 NAME FROM ( SELECT COL19,COL20, ROW_NUMBER () OVER (PARTITION BY COL19 ORDER BY COL19) NUM FROM SURVEYOBJ WHERE PROJECTID=11 AND COL19 IS NOT NULL ) A WHERE NUM = 1";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType4.DataSource = select_ds;
                ddlType4.DataValueField = "code";
                ddlType4.DataTextField = "name";
                ddlType4.DataBind();
            }


        }

        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }


        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType3.DataSource = null;
            ddlType4.DataSource = null;

            ddlType3.Items.Clear();
            ddlType4.Items.Clear();

            DataSet select_ds = new DataSet();
            var select_sql = "";

            select_sql = "SELECT DISTINCT  col27 CODE,COL28 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col27 IS NOT NULL ";
            select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType3.DataSource = select_ds;
                ddlType3.DataValueField = "code";
                ddlType3.DataTextField = "name";
                ddlType3.DataBind();
            }
        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlType4.DataSource = null;
            ddlType4.Items.Clear();
            DataSet select_ds = new DataSet();
            var select_sql = "";


            select_sql = "SELECT DISTINCT  col19 CODE,COL20 NAME  FROM SURVEYOBJ  WHERE PROJECTID=11 AND col19 IS NOT NULL ";
            select_sql = select_sql + "   and col25 = '" + ddlType2.SelectedValue + "' and col27 = '" + ddlType3.SelectedValue + "' ";

            select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            if (select_ds != null)
            {
                ddlType4.DataSource = select_ds;
                ddlType4.DataValueField = "code";
                ddlType4.DataTextField = "name";
                ddlType4.DataBind();

            }

        }

        protected void ddlDim_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDim.SelectedValue.ToString().Trim() == "0")
            {
                td_qd1.Visible = false;
                td_qd2.Visible = false;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;


            }

            if (ddlDim.SelectedValue.ToString().Trim() == "1")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = false;
                td_dq2.Visible = false;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "2")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = false;
                td_td2.Visible = false;

            }

            if (ddlDim.SelectedValue.ToString().Trim() == "3")
            {
                td_qd1.Visible = true;
                td_qd2.Visible = true;

                td_dq1.Visible = true;
                td_dq2.Visible = true;

                td_td1.Visible = true;
                td_td2.Visible = true;

            }
        }

        //加载数据
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            this.content.Visible = true;
            string className = string.Empty;  //css
            try
            {
                StringBuilder sb = new StringBuilder();
                //获取MBD
                StringBuilder sb_mbd_sql = new StringBuilder();
                StringBuilder sb_itme_sql = new StringBuilder();

                sb_itme_sql.Append(" SELECT TYPENAME, ");
                sb_itme_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 新康红装, ");
                sb_itme_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '首推'  then FirstItem_Vaule else 0 end) 新康蓝装, ");
                sb_itme_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 新康红装, ");
                sb_itme_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 新康蓝装 ");
                sb_itme_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");
                if (ddlDim.SelectedIndex == 0)  //全国
                {
                    sb_itme_sql.Append(" WHERE TYPECODE = 'GSKOTC001' GROUP BY TYPENAME ");
                }
                else if (ddlDim.SelectedIndex == 1) //渠道
                {
                    sb_itme_sql.Append(" WHERE TYPECODE = '" + this.ddlType2.SelectedValue+ "' GROUP BY TYPENAME ");
                }
                else if (ddlDim.SelectedIndex == 2) //大区
                {
                    sb_itme_sql.Append(" WHERE TYPECODE = '" + this.ddlType3.SelectedValue + "' GROUP BY TYPENAME ");
                }
                else if (ddlDim.SelectedIndex == 3) //团队
                {
                    sb_itme_sql.Append(" WHERE TYPECODE = '" + this.ddlType4.SelectedValue + "' GROUP BY TYPENAME ");
                }

                //生成Dataset
                DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_itme_sql.ToString());
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(sb_itme_sql.ToString(), con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                sb.Append("<th align='left' style='width:10%;' scope='col'>渠道</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>首推</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<th align='left' style='width:10%;' scope='col'></th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>第一推荐</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>新康红装</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>新康蓝装</th>");
                sb.Append("<th align='left' style='width:10%;' scope='col'>累计推荐</th>");
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer;' >");
                    for (int i = 0; i < 7; i++)
                    {
                        decimal s1 = Convert.ToDecimal(dr[1].ToString());
                        decimal s2 = Convert.ToDecimal(dr[2].ToString());
                        decimal s3 = Convert.ToDecimal(dr[3].ToString());
                        decimal s4 = Convert.ToDecimal(dr[4].ToString());
                        decimal sum_1 = s1 + s2;
                        decimal sum_2 = s3 + s4;
                        if (i == 3)
                        {
                            sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_1 * 100, 2) + "%" + "</td>");
                        }
                        else if (i == 0)
                        {
                            sb.Append("<td class='across' style='width:10%;'>" + dr[0].ToString() + "</td>");
                        }
                        else if (i == 1)
                        {
                            sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[1].ToString()) + "%" + "</td>");
                        }
                        else if (i == 2)
                        {
                            sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[2].ToString()) + "%" + "</td>");
                        }
                        else if (i == 4)
                        {
                            sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[3].ToString()) + "</td>");
                        }
                        else if (i == 5)
                        {
                            sb.Append("<td class='across' style='width:10%;'>" + GetDouble(dr[4].ToString()) + "%" + "</td>");
                        }
                        else if (i == 6)
                        {
                            sb.Append("<td class='across' style='width:10%;'>" + Math.Round(sum_2 * 100, 2) + "%" + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 数据导出
        /// </summary>
        public void btnOutput_Click(object sender, EventArgs e)
        {
            //获取MBD
            StringBuilder sb_mbd_sql = new StringBuilder();
            StringBuilder sb_itme_sql = new StringBuilder();

            sb_itme_sql.Append(" SELECT TYPENAME, ");
            sb_itme_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '首推' then FirstItem_Vaule else 0 end) 新康红装, ");
            sb_itme_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '首推'  then FirstItem_Vaule else 0 end) 新康蓝装, ");
            sb_itme_sql.Append(" MAX(case when Itemname = '新康红装' and FirstItem = '累计' then FirstItem_Vaule else 0 end) 新康红装, ");
            sb_itme_sql.Append(" MAX(case when Itemname = '新康蓝装' and FirstItem = '累计'  then FirstItem_Vaule else 0 end) 新康蓝装 ");
            sb_itme_sql.Append(" FROM SYS_OTC_ITEMVALUE_TJ ");
            if (ddlDim.SelectedIndex == 0)  //全国
            {
                sb_itme_sql.Append(" WHERE TYPECODE = 'GSKOTC001' GROUP BY TYPENAME ");
            }
            else if (ddlDim.SelectedIndex == 1) //渠道
            {
                sb_itme_sql.Append(" WHERE TYPECODE = '" + this.ddlType2.SelectedValue + "' GROUP BY TYPENAME ");
            }
            else if (ddlDim.SelectedIndex == 2) //大区
            {
                sb_itme_sql.Append(" WHERE TYPECODE = '" + this.ddlType3.SelectedValue + "' GROUP BY TYPENAME ");
            }
            else if (ddlDim.SelectedIndex == 3) //团队
            {
                sb_itme_sql.Append(" WHERE TYPECODE = '" + this.ddlType4.SelectedValue + "' GROUP BY TYPENAME ");
            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_itme_sql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }



        private decimal GetDouble(string date)
        {
            try
            {
                return decimal.Round(decimal.Parse(date) * 100, 2);
            }
            catch (Exception)
            {
                return 0;
                throw;
            }


        }
    }
}