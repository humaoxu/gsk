﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="DDTQG_Hylv.aspx.cs" Inherits="JSKWeb.REPORTDDT.DDTQG_Hylv" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <style type="text/css">
        .aline {
            text-decoration: underline;
        }

        select {
            border-color: #cccccc;
            border-width: 1px;
            border-style: solid;
        }

    </style>
    <script language="javascript" type="text/javascript">
        function hiderightMain_content()
        {
            document.getElementById('rightMain_content').style.display='none'
        }
    </script>
        <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        var s_sum = 0;
        var s_itemName = [];
        var s_itemValue = []; 
        var s_itemTitle = [];
        var series_tmp = [];
        var s_xAxis_data = [];
        function gv_selectRow(row ){  
            if (lastRowSelected != row) {
                if (lastRowSelected != null) {
                    lastRowSelected.className = lastRowClassName;
                }
                lastRowClassName = row.className;
                row.className = 'select';
                lastRowSelected = row;
                s_sum = 45;

                {
                    baindEchar2();
                }
             
                $('html,body').animate({ scrollTop: $("#main_echar").offset().top }, 1200);
                $("#main_echar").width($(document.body).width());
            }
        }

        function bindChart()
        {
            s_itemValue = [];
            s_itemName = [];
            var colorList = [
             '#009DD9', '#00D050', '#ff0000', '#ffc000', '#be0059', '#ff6600', '#83f3ff',
             '#7f7f7f', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0'
            ];
            var i = 0;
            var r = 0;
            var step = $(".tablestyle_otc tr:eq(0) td:gt(0)").length;
            $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").each(
                function(){
                    s_xAxis_data[i] =  $(this).next().text();
                    i++;
                })
            i= 0;
            if($("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").length == 1)
            {
                step =1;
                i=0;
                r =0;
                var barname = "";
                $(".select td:gt(0)").each(
                    function()
                    {
                        s_itemValue = [];
                        s_itemValue[0] = parseFloat($(this).text());
                        s_itemName[i] = "" + $(this).text() + "";
                        barname =  $(".tablestyle_otc tr:eq(0) td:eq("+(i+1)+")").text();
                        s_itemName[i] = barname;   
                        series_tmp[i] = {
                            name: "" +barname+ "", data: s_itemValue, type: 'bar', itemStyle: {
                                normal: {
                                    color: colorList[i],
                                    label : {
                                        show: true,
                                        position: 'top',
                                        textStyle: {
                                            color: 'black'
                                        }
                                    }
                                }
                            }
                        }
                        i++;
                    }
                     
         )



            }else
            {
       
                var z = 0;
                var n = 2;
                var y = 1;
                $(".tablestyle_otc tr:eq(0) td:gt(0)").each(
                    function () {
                        s_itemName[i] = "" + $(this).text() + "";
                        s_itemValue = [];
                        r = 0;
                        $(".select td:gt(0)").each(
                        function () {

                            if (r == (y * 2 - n)) {
                                s_itemValue[z] = parseFloat($(this).text());
                                z++;
                                n--;
                            }
                            r++;
                            if (n == 0) {
                                n = 2;
                            }
                            if (z > 1) {
                                z = 0;
                            }
                        })

                        series_tmp[i] = {
                            name: "" + $(this).text() + "", data: s_itemValue, type: 'bar', itemStyle: {
                                normal: {
                                    color: colorList[i],
                                    label : {
                                        show: true,
                                        position: 'top',
                                        textStyle: {
                                            color: 'black'
                                        }
                                    }
                                }
                            }
                        }
                        y++;
                        i++;
                    })
            }
           
 
            option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    data: s_itemName
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: { show: true },
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'stack'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        data: s_xAxis_data
                    }
                ],
                grid: {
                    x: 40,
                    x2: 20,
                    y2: 100,
                },
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: series_tmp
            };

            var chart = echarts.init(document.getElementById('main_echar'));


            chart.setOption(option);

            $("#main_echar").resize(function () {
                $("#main_echar").resize();
            })
        }
      
        function baindEchar2()
        {
            if(s_sum>10)
                s_sum = 45;
            else 
                s_sum = 0;
            var colorList = [
              '#FFC000', '#009DFF', '#ff7f50', '#87cefa', '#da70d6', '#32cd32', '#6495ed',
              '#ff69b4', '#ba55d3', '#cd5c5c', '#ffa500', '#40e0d0'
            ]; 
 
            series_tmp = [];
            s_itemName = [];
            s_itemValue = [];
            s_legend = [];
            s_itemTitle = [ "活跃率-" +$(".select td:eq(0)").text()];
            var i = 0;
            var r = 0;
            var qs = 0;
            var step = $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").length;

            $(".tablestyle_otc tr:eq(0) td:gt(0)").each(
               function () {
                   s_itemName[i] = "" + $(this).text() + "";
                   i++;
               })

            var gridWidth = $(document.body).width() ;
           

            $(".tablestyle_otc tr:eq(1) td:gt(1)").each(
               function () {
                   s_itemName[i] = "" + $(this).text() + "";
                   i++;
               })

            $("#topTool_qsDropDownCheckBoxes input[type=checkbox]:checked").each(
             function () {
                 r = 0;
                 i = 0;
                 s_itemValue = [];
                 $(".select td:gt(0)").each(
                  function () {
                      if (i == (qs + r * step)) {
                          s_itemValue[r] = parseFloat($(this).text().replace("%", ""));
                          r++;
                      }
                      i++;
                  })

                 series_tmp[qs] = {
                     data: s_itemValue,
                     type: 'bar',
                     name: $(".tablestyle_otc tr:eq(1) td:eq(" + (qs + 1) + ")").text() + '期',
                     itemStyle: {
                         normal: {
                             color: colorList[qs],
                             label : {
                                 show: true,
                                 position: 'top',
                                 textStyle: {
                                     color: 'black'
                                 }
                             }
                         }
                     }
                 }

                 
                 s_legend[qs] = $(".tablestyle_otc tr:eq(1) td:eq(" + (qs +1) + ")").text() + '期';

                 qs++;
             })
           

            option = {          
                title: {
                    x:'left',
                    text:s_itemTitle
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: s_legend
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis: [
                     {
                         type: 'category',
                         axisLabel: {
                             interval: 0,
                             rotate: s_sum,
                             margin: 2,
                             textStyle: {
                                 color: "#222"
                             }
                         },
                         data: s_itemName
                     }
                ],
                grid: {
                    x: 40,
                    x2: 20,
                    y2: 100,
                },
                yAxis: [
                      {
                          type: 'value',
                          //min: 0,
                          //max: 100,
                          //interval: 20,
                          axisLabel: {
                              formatter: '{value} %'
                          }
                      }
                ],
                series: series_tmp    
            };

            var chart = echarts.init(document.getElementById('main_echar'));

            chart.setOption(option);

        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function QueryChecked()
        {
            var cgLen =  $("#topTool_SkuDownCheckBoxes_sl input:[type=checkbox]:checked").length
            if (cgLen == 0) { 
                alert("请选择产品！");
                return false;
            }
            var daquLen = $("#topTool_daquDownCheckBoxes_sl input:[type=checkbox]:checked").length;
            var quyuLen = $("#topTool_quYuDownCheckBoxes_sl input:[type=checkbox]:checked").length;
            var chengshiLen = $("#topTool_chengShiDownCheckBoxes_sl input:[type=checkbox]:checked").length; 
            var xiaodaiLen = $("#topTool_xiaoShouDaiBiaoDownCheckBoxes_sl input:[type=checkbox]:checked").length;

            if ((daquLen+quyuLen+chengshiLen+xiaodaiLen)  == 0) {
                alert("请选择MBD！");
                return false;
            }
        }
    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;MBD筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                  
                    <td style="padding: 10px 0px 10px 10px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qsDropDownCheckBoxes" runat="server" OnSelectedIndexChanged="qsDropDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="120" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="问卷期数" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td> 
                   
                    <td style="padding-left: 5px;">
                     
                        <asp:DropDownList ID="ddlWeiDu"  CssClass="ddl_select"   AutoPostBack="true" Width="90px" runat="server" OnSelectedIndexChanged="ddlWeiDu_SelcetedIndexChanged" >
                             <asp:ListItem Text="市场划分" Value="市场划分"></asp:ListItem>
                             <asp:ListItem Text="渠道" Value="渠道"></asp:ListItem>
                        </asp:DropDownList>
                    </td>


                    <td style="padding-left: 5px;">
                     <%--   <DropDownCheckBoxes:DropDownCheckBoxes ID="quDaoDownCheckBoxes" runat="server"  OnSelectedIndexChanged="quDaoDownCheckBoxes_SelcetedIndexChanged"
                          Visible="false"   AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>--%>
                        <asp:DropDownList ID="ddlquDao" Visible="false"  CssClass="ddl_select"   Width="90px" runat="server" ></asp:DropDownList>
                    </td>

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="daquDownCheckBoxes" runat="server" OnSelectedIndexChanged="daquDownCheckBoxes_SelcetedIndexChanged"
                           Visible="false"    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="120" />
                            <Texts SelectBoxCaption="大区" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="quYuDownCheckBoxes" runat="server" OnSelectedIndexChanged="quYuDownCheckBoxes_SelcetedIndexChanged"
                           Visible="false"    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="chengShiDownCheckBoxes" runat="server"  OnSelectedIndexChanged="chengShiDownCheckBoxes_SelcetedIndexChanged"
                           Visible="false"    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="城市" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="xiaoShouDaiBiaoDownCheckBoxes" runat="server"  
                             Visible="false"  AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                           
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="销售代表" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td> 
                   
                </tr>
            </tbody>
        </table>
        <div style="height: 5px"></div>
     <fieldset>
        <legend>&nbsp;产品筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                 <tr> 
                    <td style="padding: 10px 0px 10px 10px;">
                        <asp:DropDownList ID="ddlPP"  CssClass="ddl_select"  runat="server" Width="160px" OnSelectedIndexChanged="ddlPP_SelcetedIndexChanged" AutoPostBack="True">
                          <asp:ListItem Value="0">舒适达</asp:ListItem>
                            <asp:ListItem Value="1">保丽净</asp:ListItem>
                            <asp:ListItem Value="2">保丽净稳固剂</asp:ListItem>
                            <asp:ListItem Value="3">保丽净清洁片</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding: 10px 0px 10px 10px;"> 
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="SkuDownCheckBoxes" runat="server" 
                              AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="120" /> 
                            <Texts SelectBoxCaption="请选择SKU" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>

                    </td>
                     
                    <td align="left"  >&nbsp;<asp:Button ID="btnQuery"  CssClass="btn_s" OnClick="btnSearch_Click"  OnClientClick="return QueryChecked()"
                        runat="server" Text="数据查询" Width="60px" />
                        &nbsp;
                        <asp:Button ID="btnOutPut"   CssClass="btn_s"
                        runat="server" Text="数据导出" Width="60px" OnClick="btnOutput_Click" OnClientClick="return QueryChecked()"  />
                    </td> 
                 </tr>
            </tbody>
        </table>
        <div style="height: 5px"></div>

    </fieldset>

    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center">
    </div>
    <div id="main_echar" style="width: 100%; height: 500px; overflow: hidden;"></div>
   
    <script type="text/javascript">
        var s_sum = 10;
        var s_itemName  = <%= this.item_Name %>; 
        s_itemName='';
        if (s_itemName != '[0]' && s_itemName !='') {
            var series_tmp = <%= this.item_series %>;
            var s_legend = <%= this.item_legend %>;  
            var s_itemTitle =[ $("#topTool_ddlPP option:selected").text() + ' 产品分销率'];
            var s_xAxis_data =[];
            if(s_itemName.length>10)
                s_sum = 45;
            else 
                s_sum = 0;
            option = {          
                title: {
                    x:'left',
                    text:s_itemTitle
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: s_legend
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis: [
                     {
                         type: 'category',
                         axisLabel: {
                             interval: 0,
                             rotate: s_sum,
                             margin: 2,
                             textStyle: {
                                 color: "#222"
                             }
                         },
                         data: s_itemName
                     }
                ],
                grid: {
                    x: 40,
                    x2: 20,
                    y2: 100,
                },
                yAxis: [
                      {
                          type: 'value',
                          //min: 0,
                          //max: 12,
                          //interval: 3,
                          axisLabel: {
                              formatter: '{value}'
                          }
                      }
                ],
                series: series_tmp    
            };



            var chart = echarts.init(document.getElementById('main_echar'));
       
            chart.showLoading({
                text: '正在加载中..... ',
            });
            chart.hideLoading();

            chart.setOption(option);

            $("#main").resize(function(){
                $("#main").resize();
            })
        }
        
    </script>
    <div id="sqlHtml" runat="server">
    </div>
</asp:Content>
