﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;
using System.Collections;

namespace JSKWeb.REPORTDDT
{
    public partial class DDTQG_Hy : System.Web.UI.Page
    {
        public string item_Name = "[]";
        public string item_series = "[]";
        public string item_legend = "[]";

        SqlHelper sqlhelper = new SqlHelper();


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                var ddlProjectId = InputText.GetConfig("gsk_otc");
                DataBinds();

                qsDropDownCheckBoxes_SelcetedIndexChanged(null, null);
            }
        }

        /// <summary>
        /// 报告期数绑定
        /// </summary>
        private void DataBinds()
        {

            var ddlProjectId = InputText.GetConfig("gsk_ddt");
            var ddlTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId ", ddlProjectId);
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qsDropDownCheckBoxes.DataSource = ddlTypeDataSet;
            qsDropDownCheckBoxes.DataValueField = "paperId";
            qsDropDownCheckBoxes.DataTextField = "paperTitle";
            qsDropDownCheckBoxes.DataBind();


            for (int i = 0; i < (qsDropDownCheckBoxes.Items.Count); i++)
            {
                ///期数超过1期
                if (qsDropDownCheckBoxes.Items.Count > 1)
                {
                    if (i >= (qsDropDownCheckBoxes.Items.Count - 2))
                    {
                        qsDropDownCheckBoxes.Items[i].Selected = true;
                    }
                }
                else
                {
                    qsDropDownCheckBoxes.Items[i].Selected = true;
                }
            }
        }

        #region DropDownCheckBoxes选择触发器
        /// <summary>
        /// 期数选择触发器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void qsDropDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {

            #region 渠道 城市 销售  清空数据 
            ///显示渠道
            //quDaoDownCheckBoxes.Visible = true;
            //ddlquDao.Visible = true;

            ///显示大区
            daquDownCheckBoxes.Visible = true;
            ///显示区域
            quYuDownCheckBoxes.Visible = true;
            ///显示城市
            chengShiDownCheckBoxes.Visible = true;
            ///显示销售代表
            xiaoShouDaiBiaoDownCheckBoxes.Visible = true;

            ddlquDao.DataSource = null;
            //quDaoDownCheckBoxes.DataSource = null;
            chengShiDownCheckBoxes.DataSource = null;
            daquDownCheckBoxes.DataSource = null;
            quYuDownCheckBoxes.DataSource = null;
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = null;

            ddlquDao.Items.Clear();
            //quDaoDownCheckBoxes.Items.Clear();
            chengShiDownCheckBoxes.Items.Clear();
            daquDownCheckBoxes.Items.Clear();
            quYuDownCheckBoxes.Items.Clear();
            xiaoShouDaiBiaoDownCheckBoxes.Items.Clear();
            #endregion

            #region 筛选条件 和 分析维度

            var strWhere = "";
            ///获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    strWhere += "'" + item.Value + "',";
                }
            }
            if (strWhere.Length > 0)
            {
                strWhere = " and paperid in (" + strWhere.TrimEnd(',') + ")";
            }
            else
            {
                return;
            }

            #endregion 

            var ddlsql = "";
            DataSet ddlDataSet;

            ////绑定渠道
            //var ddlsql = String.Format(@" select distinct shoptype qudao from RPT_DDT t where 1=1{0} ", strWhere);
            //var ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            ////quDaoDownCheckBoxes.DataSource = ddlDataSet;
            ////quDaoDownCheckBoxes.DataValueField = "qudao";
            ////quDaoDownCheckBoxes.DataTextField = "qudao";
            ////quDaoDownCheckBoxes.DataBind();
            //ddlquDao.DataSource = ddlDataSet;
            //ddlquDao.DataTextField = "qudao";
            //ddlquDao.DataValueField = "qudao";
            //ddlquDao.DataBind();
            //ddlquDao.Items.Insert(0,new ListItem("市场划分", "-1"));



            ///绑定大区
            ddlsql = String.Format(@"select distinct dq , 1 orderid from RPT_DDT where 1=1 {0}  union
                                        select '全国' ,0 orderid order by orderid ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            daquDownCheckBoxes.DataSource = ddlDataSet;
            daquDownCheckBoxes.DataValueField = "dq";
            daquDownCheckBoxes.DataTextField = "dq";
            daquDownCheckBoxes.DataBind();


            ///绑定区域
            ddlsql = String.Format(@"select distinct xq from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            quYuDownCheckBoxes.DataSource = ddlDataSet;
            quYuDownCheckBoxes.DataValueField = "xq";
            quYuDownCheckBoxes.DataTextField = "xq";
            quYuDownCheckBoxes.DataBind();


            ///绑定城市
            ddlsql = String.Format(@"select distinct city from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            chengShiDownCheckBoxes.DataSource = ddlDataSet;
            chengShiDownCheckBoxes.DataValueField = "city";
            chengShiDownCheckBoxes.DataTextField = "city";
            chengShiDownCheckBoxes.DataBind();

            ///绑定销售代表
            ddlsql = String.Format(@"select distinct ddt_name from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = ddlDataSet;
            xiaoShouDaiBiaoDownCheckBoxes.DataValueField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataTextField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataBind();


        }


        /// <summary>
        /// 维度划分
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlWeiDu_SelcetedIndexChanged(object sender, EventArgs e)
        {
            if (ddlWeiDu.SelectedItem.Text.Equals("渠道"))
            {
                ddlquDao.Visible = true;

                var strWhere = "";
                ///获取问卷期数
                foreach (ListItem item in qsDropDownCheckBoxes.Items)
                {
                    if (item.Selected)
                    {
                        strWhere += "'" + item.Value + "',";
                    }
                }
                if (strWhere.Length > 0)
                {
                    strWhere = " and paperid in (" + strWhere.TrimEnd(',') + ")";
                }
                else
                {
                    return;
                }


                var ddlsql = String.Format(@" select distinct shoptype qudao from RPT_DDT t where 1=1{0} ", strWhere);
                var ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
                //quDaoDownCheckBoxes.DataSource = ddlDataSet;
                //quDaoDownCheckBoxes.DataValueField = "qudao";
                //quDaoDownCheckBoxes.DataTextField = "qudao";
                //quDaoDownCheckBoxes.DataBind();
                ddlquDao.DataSource = ddlDataSet;
                ddlquDao.DataTextField = "qudao";
                ddlquDao.DataValueField = "qudao";
                ddlquDao.DataBind(); 
            }
            else
            {
                ddlquDao.Visible = false;
            }
        }

        /// <summary>
        /// 渠道选择触发器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void quDaoDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {

            #region  清空数据 
            ///显示大区 
            daquDownCheckBoxes.Visible = true;
            ///显示区域
            quYuDownCheckBoxes.Visible = true;


            chengShiDownCheckBoxes.DataSource = null;
            daquDownCheckBoxes.DataSource = null;
            quYuDownCheckBoxes.DataSource = null;
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = null;

            chengShiDownCheckBoxes.Items.Clear();
            daquDownCheckBoxes.Items.Clear();
            quYuDownCheckBoxes.Items.Clear();
            xiaoShouDaiBiaoDownCheckBoxes.Items.Clear();
            #endregion 

            var strWhere = "";

            #region 筛选条件 和 分析维度
            var sqlTmp = "";

            ///获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    strWhere += "'" + item.Value + "',";
                }
            }
            if (strWhere.Length > 0)
            {
                strWhere = " and paperid in (" + strWhere.TrimEnd(',') + ")";
            }
            else
            {
                return;
            }

            ///渠道筛选
            sqlTmp = "";
            //foreach (ListItem item in quDaoDownCheckBoxes.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (!item.Value.Equals("全国"))
            //        {
            //            sqlTmp += "'" + item.Value + "',";
            //        }
            //    }
            //}
            //if (sqlTmp.Trim().Length > 0)
            //{
            //    strWhere += " and shoptype in (" + sqlTmp.TrimEnd(',') + ")";
            //}
            if (ddlquDao.Visible)
            {
                strWhere += " and shoptype ='"+ddlquDao.SelectedItem.Text+"'";
            }


            #endregion 

            var ddlsql = "";
            DataSet ddlDataSet = null;
            ///绑定大区
            ddlsql = String.Format(@"select distinct dq , 1 orderid from RPT_DDT where 1=1 {0}  union
                                        select '全国' ,0 orderid order by orderid ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            daquDownCheckBoxes.DataSource = ddlDataSet;
            daquDownCheckBoxes.DataValueField = "dq";
            daquDownCheckBoxes.DataTextField = "dq";
            daquDownCheckBoxes.DataBind();


            ///绑定区域
            ddlsql = String.Format(@"select distinct xq from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            quYuDownCheckBoxes.DataSource = ddlDataSet;
            quYuDownCheckBoxes.DataValueField = "xq";
            quYuDownCheckBoxes.DataTextField = "xq";
            quYuDownCheckBoxes.DataBind();


            ///绑定城市
            ddlsql = String.Format(@"select distinct city from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            chengShiDownCheckBoxes.DataSource = ddlDataSet;
            chengShiDownCheckBoxes.DataValueField = "city";
            chengShiDownCheckBoxes.DataTextField = "city";
            chengShiDownCheckBoxes.DataBind();


            ///绑定销售代表
            ddlsql = String.Format(@"select distinct ddt_name from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = ddlDataSet;
            xiaoShouDaiBiaoDownCheckBoxes.DataValueField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataTextField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataBind();
        }
        /// <summary>
        /// 大区选择触发器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void daquDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {

            #region  清空数据 
            ///显示大区 
            daquDownCheckBoxes.Visible = true;
            ///显示区域
            quYuDownCheckBoxes.Visible = true;


            chengShiDownCheckBoxes.DataSource = null;
            quYuDownCheckBoxes.DataSource = null;
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = null;

            chengShiDownCheckBoxes.Items.Clear();
            quYuDownCheckBoxes.Items.Clear();
            xiaoShouDaiBiaoDownCheckBoxes.Items.Clear();
            #endregion 

            var strWhere = "";

            #region 筛选条件 和 分析维度
            var sqlTmp = "";

            ///获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    strWhere += "'" + item.Value + "',";
                }
            }
            if (strWhere.Length > 0)
            {
                strWhere = " and paperid in (" + strWhere.TrimEnd(',') + ")";
            }
            else
            {
                return;
            }

            ///渠道筛选
            sqlTmp = "";
            //foreach (ListItem item in quDaoDownCheckBoxes.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (!item.Value.Equals("全国"))
            //        {
            //            sqlTmp += "'" + item.Value + "',";
            //        }
            //    }
            //}
            //if (sqlTmp.Trim().Length > 0)
            //{
            //    strWhere += " and shoptype in (" + sqlTmp.TrimEnd(',') + ")";
            //}
            if (ddlquDao.Visible)
            {
                strWhere += " and shoptype ='" + ddlquDao.SelectedItem.Text + "'";
            }


            ///大区筛选
            sqlTmp = "";
            foreach (ListItem item in daquDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.Equals("全国"))
                    {

                    }else
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
            }
            if (sqlTmp.Trim().Length > 0)
            {
                strWhere += " and dq in (" + sqlTmp.TrimEnd(',') + ")";
            }

            #endregion 

            var ddlsql = "";
            DataSet ddlDataSet = null;

            ///绑定区域
            ddlsql = String.Format(@"select distinct xq from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            quYuDownCheckBoxes.DataSource = ddlDataSet;
            quYuDownCheckBoxes.DataValueField = "xq";
            quYuDownCheckBoxes.DataTextField = "xq";
            quYuDownCheckBoxes.DataBind();


            ///绑定城市
            ddlsql = String.Format(@"select distinct city from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            chengShiDownCheckBoxes.DataSource = ddlDataSet;
            chengShiDownCheckBoxes.DataValueField = "city";
            chengShiDownCheckBoxes.DataTextField = "city";
            chengShiDownCheckBoxes.DataBind();


            ///绑定销售代表
            ddlsql = String.Format(@"select distinct ddt_name from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = ddlDataSet;
            xiaoShouDaiBiaoDownCheckBoxes.DataValueField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataTextField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataBind();

        }
        /// <summary>
        /// 区域选择触发器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void quYuDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            #region  清空数据 
            ///显示大区 
            daquDownCheckBoxes.Visible = true;
            ///显示区域
            quYuDownCheckBoxes.Visible = true;


            chengShiDownCheckBoxes.DataSource = null;
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = null;

            chengShiDownCheckBoxes.Items.Clear();
            xiaoShouDaiBiaoDownCheckBoxes.Items.Clear();
            #endregion 

            var strWhere = "";

            #region 筛选条件 和 分析维度
            var sqlTmp = "";

            ///获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    strWhere += "'" + item.Value + "',";
                }
            }
            if (strWhere.Length > 0)
            {
                strWhere = " and paperid in (" + strWhere.TrimEnd(',') + ")";
            }
            else
            {
                return;
            }

            ///渠道筛选
            sqlTmp = "";
            //foreach (ListItem item in quDaoDownCheckBoxes.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (!item.Value.Equals("全国"))
            //        {
            //            sqlTmp += "'" + item.Value + "',";
            //        }
            //    }
            //}
            //if (sqlTmp.Trim().Length > 0)
            //{
            //    strWhere += " and shoptype in (" + sqlTmp.TrimEnd(',') + ")";
            //}
            if (ddlquDao.Visible)
            {
                strWhere += " and shoptype ='" + ddlquDao.SelectedItem.Text + "'";
            }


            ///大区筛选
            sqlTmp = "";
            foreach (ListItem item in daquDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.Equals("全国"))
                    {

                    }
                    else
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
            }
            if (sqlTmp.Trim().Length > 0)
            {
                strWhere += " and dq in (" + sqlTmp.TrimEnd(',') + ")";
            }


            ///区域筛选
            sqlTmp = "";
            foreach (ListItem item in quYuDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
            }
            if (sqlTmp.Trim().Length > 0)
            {
                strWhere += " and xq in (" + sqlTmp.TrimEnd(',') + ")";
            }

            #endregion 

            var ddlsql = "";
            DataSet ddlDataSet = null;

            ///绑定城市
            ddlsql = String.Format(@"select distinct city from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            chengShiDownCheckBoxes.DataSource = ddlDataSet;
            chengShiDownCheckBoxes.DataValueField = "city";
            chengShiDownCheckBoxes.DataTextField = "city";
            chengShiDownCheckBoxes.DataBind();


            ///绑定销售代表
            ddlsql = String.Format(@"select distinct ddt_name from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = ddlDataSet;
            xiaoShouDaiBiaoDownCheckBoxes.DataValueField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataTextField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataBind();


        }
        /// <summary>
        /// 城市选择触发器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chengShiDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            #region  清空数据 
            ///显示大区 
            daquDownCheckBoxes.Visible = true;
            ///显示区域
            quYuDownCheckBoxes.Visible = true;



            xiaoShouDaiBiaoDownCheckBoxes.DataSource = null;


            xiaoShouDaiBiaoDownCheckBoxes.Items.Clear();
            #endregion 

            var strWhere = "";

            #region 筛选条件 和 分析维度
            var sqlTmp = "";

            ///获取问卷期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    strWhere += "'" + item.Value + "',";
                }
            }
            if (strWhere.Length > 0)
            {
                strWhere = " and paperid in (" + strWhere.TrimEnd(',') + ")";
            }
            else
            {
                return;
            }

            ///渠道筛选
            sqlTmp = "";
            //foreach (ListItem item in quDaoDownCheckBoxes.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (!item.Value.Equals("全国"))
            //        {
            //            sqlTmp += "'" + item.Value + "',";
            //        }
            //    }
            //}
            //if (sqlTmp.Trim().Length > 0)
            //{
            //    strWhere += " and shoptype in (" + sqlTmp.TrimEnd(',') + ")";
            //}
            if (ddlquDao.Visible)
            {
                strWhere += " and shoptype ='" + ddlquDao.SelectedItem.Text + "'";
            }

            ///大区筛选
            sqlTmp = "";
            foreach (ListItem item in daquDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.Equals("全国"))
                    {

                    }
                    else
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
            }
            if (sqlTmp.Trim().Length > 0)
            {
                strWhere += " and dq in (" + sqlTmp.TrimEnd(',') + ")";
            }


            ///区域筛选
            sqlTmp = "";
            foreach (ListItem item in quYuDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
            }
            if (sqlTmp.Trim().Length > 0)
            {
                strWhere += " and xq in (" + sqlTmp.TrimEnd(',') + ")";
            }

            ///城市筛选
            sqlTmp = "";
            foreach (ListItem item in chengShiDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                }
            }
            if (sqlTmp.Trim().Length > 0)
            {
                strWhere += " and city in (" + sqlTmp.TrimEnd(',') + ")";
            }

            #endregion 

            var ddlsql = "";
            DataSet ddlDataSet = null;


            ///绑定销售代表
            ddlsql = String.Format(@"select distinct ddt_name from RPT_DDT where 1=1 {0} ", strWhere);
            ddlDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlsql);
            xiaoShouDaiBiaoDownCheckBoxes.DataSource = ddlDataSet;
            xiaoShouDaiBiaoDownCheckBoxes.DataValueField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataTextField = "ddt_name";
            xiaoShouDaiBiaoDownCheckBoxes.DataBind();



        }
        #endregion

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        { 

            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = DataLoad();
                if (dt.Rows.Count == 0)
                {
                    item_Name = "[]";
                    item_series = "[]";
                    item_legend = "[]";
                    content.InnerHtml = "请检查MBD筛选";
                    return;
                }
                else
                {
                    item_Name = "";
                    item_series = "";
                    item_legend = "";
                }
                //动态定义样式
                sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                int w = 75 / (dt.Columns.Count - 1);

                
                //echart 值
                string[] item_series_value_tmp = new string[dt.Columns.Count - 2];
                //echart 名称
                string[] item_series_name_tmp = new string[dt.Columns.Count - 2];


                for (int i = 0; i < dt.Columns.Count - 1; i++)
                {

                    sb.Append("<th align='left' style='width:25%;' scope='col'>" + dt.Columns[i].ToString() + "</th>");
                    if (i > 0)
                    {
                        item_legend += "'" + dt.Columns[i].ToString() + "',";
                    }
                }



                sb.Append("</tr>");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    sb.Append("<tr style='cursor:pointer;' >");
                    for (int j = 0; j < dt.Columns.Count - 1; j++)
                    {
                        if (j >= 1)
                        {
                            sb.Append("<td class='across' style='width:" + w + "%;'>" + Math.Round(InputText.GetDouble(dr[j].ToString()), 2).ToString("##0.00") + "</td>");

                            item_series_value_tmp[j-1] += InputText.GetDouble(dr[j].ToString()).ToString("##0.00") + ",";
                            item_series_name_tmp[j-1] = "'" + dt.Columns[j].ToString() + "'";
                        }
                        else
                        {
                            sb.Append("<td class='across' style='width:25%;'>" + dr[j].ToString() + "</td>");

                            item_Name += "'" + dr[j].ToString() + "',";
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");



                content.InnerHtml = sb.ToString();



                ///echart  legend
                item_legend = "[" + item_legend.TrimEnd(',') + "]";
                #region echart series
                //{
                //    name: "" + $(this).text() + "", data: s_itemValue, type: 'bar', itemStyle:
                //    {
                //        normal:
                //        {
                //            color: colorList[i],
                //                        label:
                //            {
                //                show: true,
                //                            position: 'top',
                //                            textStyle:
                //                {
                //                    color: 'black'
                //                            }
                //            }
                //        }
                //    }
                //}
                ///echart series

                for (int i = 0; i < item_series_name_tmp.Length; i++)
                {
                    item_series += "{name:" + item_series_name_tmp[i]
                        + ",data:[" + item_series_value_tmp[i].TrimEnd(',')
                        + "],type:'bar',itemStyle:{normal:{color: '" + InputText.GetEchartColors()[i] + "',label:{show: true,position: 'top',textStyle:{color:'black'}}}}},";

                }
                item_series = "[" + item_series.TrimEnd(',') + "]";
                #endregion

                ///echart item_Name
                item_Name = "[" + item_Name.TrimEnd(',') + "]";


            }
            catch 
            {

                
            }



        }


        /// <summary>
        /// 数据查询
        /// </summary>
        /// <returns></returns>
        private DataTable DataLoad()
        {
            DataTable dt = new DataTable();

            string sql = "";



            string fenshuCol = "ssdhr_scroe";
            string pinPaiConfig = "ssdhr_cfg";


            if (ddlPP.SelectedIndex == 0)
            {
                fenshuCol = "ssdhr_score";
                pinPaiConfig = "ssdhr_cfg";
            }
            else if (ddlPP.SelectedIndex == 1)
            {
                fenshuCol = "bljhr_score";
                pinPaiConfig = "bljhr_cfg";
            }
            else if (ddlPP.SelectedIndex == 2)
            {
                fenshuCol = "bljwgjhr_score";
                pinPaiConfig = "bljwgjhr_cfg";
            }
            else if (ddlPP.SelectedIndex == 3)
            {
                fenshuCol = "bljqjphr_score";
                pinPaiConfig = "bljqjphr_cfg";
            }







            //string fenshuCol = "ssdhr_score";
            //string pinPaiConfig = "bljhr_cfg";

            //fenshuCol = ddlPP.SelectedItem.Value.Equals("0") ? "ssdhr_score" : "bljhr_score";

            //pinPaiConfig = ddlPP.SelectedItem.Value.Equals("0") ? "ssdhr_cfg" : "bljhr_cfg";

            ///
            ArrayList sqlList = new ArrayList();
            ///分析维度
            ArrayList fenXiWeiDu = new ArrayList();

            string sqlWhere_quDao = "";
            string sqlWhere_daQu = "";
            string sqlWhere_quYu = "";
            string sqlWhere_chengShi = "";
            string sqlWhere_xiaoShouDaiBiao = "";


            var sqlTmp = "";

            ///渠道
            sqlTmp = "";
            //foreach (ListItem item in quDaoDownCheckBoxes.Items)
            //{
            //    if (item.Selected)
            //    {

            //        {
            //            sqlTmp += "'" + item.Value + "',";
            //        }
            //    }
            //}
            //if (sqlTmp.Length > 0)
            //{
            //    sqlWhere_quDao += " and shoptype in (" + sqlTmp.TrimEnd(',') + ")";
            //}
            if (ddlquDao.Visible)
            {
                sqlWhere_quDao += " and shoptype ='" + ddlquDao.SelectedItem.Text + "'";
            }


            ///大区
            sqlTmp = "";
            foreach (ListItem item in daquDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    if (item.Value.Equals("全国"))
                    {
                        sqlList.Add(string.Format( GetSqlQuanGuoByCol(fenshuCol, pinPaiConfig, 0), sqlWhere_quDao.Trim().Length > 0 ? sqlWhere_quDao : ""));
                        //sqlList.Add(string.Format(GetSqlQuanGuoByCol(fenshuCol, pinPaiConfig, 0), ""));
                    }
                    else
                    {
                        sqlTmp += "'" + item.Value + "',";
                    }
                      
                }
            }
            if (sqlTmp.Length > 0)
            {
                sqlWhere_daQu += " and dq in (" + sqlTmp.TrimEnd(',') + ")" +  (sqlWhere_quDao.Trim().Length > 0 ? sqlWhere_quDao : "");
            }


            ///区域
            sqlTmp = "";
            foreach (ListItem item in quYuDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    sqlTmp += "'" + item.Value + "',";
                }
            }
            if (sqlTmp.Length > 0)
            {
                sqlWhere_quYu += " and xq in (" + sqlTmp.TrimEnd(',') + ") "
                    + (sqlWhere_quDao.Trim().Length > 0 ? sqlWhere_quDao : "")
                    + (sqlWhere_daQu.Trim().Length > 0 ? sqlWhere_daQu : "");
            }


            ///城市
            sqlTmp = "";
            foreach (ListItem item in chengShiDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    sqlTmp += "'" + item.Value + "',";
                }
            }
            if (sqlTmp.Length > 0)
            {
                sqlWhere_chengShi += " and city in (" + sqlTmp.TrimEnd(',') + ")"
                    + (sqlWhere_quDao.Trim().Length > 0 ? sqlWhere_quDao : "")
                    + (sqlWhere_daQu.Trim().Length > 0 ? sqlWhere_daQu : "")
                    + (sqlWhere_quYu.Trim().Length > 0 ? sqlWhere_quYu : "");
            }



            ///销售代表
            foreach (ListItem item in xiaoShouDaiBiaoDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    sqlTmp += "'" + item.Value + "',";
                }
            }
            if (sqlTmp.Length > 0)
            {
                sqlWhere_xiaoShouDaiBiao += " and ddt_name in (" + sqlTmp.TrimEnd(',') + ")"
                    + (sqlWhere_quDao.Trim().Length > 0 ? sqlWhere_quDao : "")
                    + (sqlWhere_daQu.Trim().Length > 0 ? sqlWhere_daQu : "")
                    + (sqlWhere_quYu.Trim().Length > 0 ? sqlWhere_quYu : "")
                    + (sqlWhere_chengShi.Trim().Length > 0 ? sqlWhere_chengShi : "");
            }








            var sqlWhere = "";
            ///期数
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    sqlWhere += "'" + item.Value + "'";
                }
            }
            if (sqlWhere.Length > 0)
            {
                sqlWhere += " and paperid in(" + sqlWhere + ")";
            }
            else
            {
                return null;
            }



            if (sqlWhere_xiaoShouDaiBiao.Trim().Length > 0)
            {
                sqlList.Add(string.Format(GetSqlByCol(fenshuCol, pinPaiConfig, "ddt_name", 5), sqlWhere_xiaoShouDaiBiao));
            }
            if (sqlWhere_chengShi.Trim().Length > 0)
            {
                sqlList.Add(string.Format(GetSqlByCol(fenshuCol, pinPaiConfig, "city", 4), sqlWhere_chengShi));
            }
            if (sqlWhere_quYu.Trim().Length > 0)
            {
                sqlList.Add(string.Format(GetSqlByCol(fenshuCol, pinPaiConfig, "xq", 3), sqlWhere_quYu));
            }
            if (sqlWhere_daQu.Trim().Length > 0)
            {
                sqlList.Add(string.Format(GetSqlByCol(fenshuCol, pinPaiConfig, "dq", 2), sqlWhere_daQu));
            }
            //if (sqlWhere_quDao.Trim().Length > 0)
            //{
            //    sqlList.Add(string.Format(GetSqlByCol(fenshuCol, pinPaiConfig, "shoptype", 1), sqlWhere_quDao));
            //}

            if (sqlList.Count > 0)
            {
                sql = "select * from (";
                for (int i = 0; i < sqlList.Count; i++)
                {
                    if (i > 0)
                    {
                        sql += " union ";
                    }
                    sql += sqlList[i];
                }

                sql += " ) b order by orderid ";
            }

            if (sql.Length>0)
            {
                dt = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql).Tables[0];
            }
           

            return dt;
        }

        /// <summary>
        /// 全国SQL
        /// </summary>
        /// <param name="fenshuCol"></param>
        /// <param name="pinPaiConfig"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        private string GetSqlQuanGuoByCol(string fenshuCol, string pinPaiConfig, int v)
        {
            var sql = @"select '全国' 销售区域, ";

            int i = 1;
            var paperidSql = "";
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    paperidSql += "'" + item.Value + "',";

                    sql += @"case when
(sum(case when paperid = " + item.Value + @" then " + fenshuCol + @" else 0 end) = 0)
then 0
else (sum(case when paperid = " + item.Value + @" then " + fenshuCol + " else 0 end) / sum(case when paperid = " + item.Value + " then 1 else 0 end)) end [" + item.Text + "],";
                }
                i++;
            }
            sql += @"0 'orderid' from RPT_DDT where " + pinPaiConfig + " = 1 and paperid in (" + paperidSql.TrimEnd(',') + ") {0}";

            return sql;
        }

        /// <summary>
        /// 明细SQL
        /// </summary>
        /// <param name="fenshuCol"></param>
        /// <param name="pinPaiConfig"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        private string GetSqlByCol(string fenshuCol, string pinPaiConfig, string weiDuCol, int v)
        {

            var sql = @"select " + weiDuCol + " 销售区域, ";

            int i = 1;
            var paperidSql = "";
            foreach (ListItem item in qsDropDownCheckBoxes.Items)
            {
                if (item.Selected)
                {
                    paperidSql += "'" + item.Value + "',";

                    sql += @"case when
(sum(case when paperid = " + item.Value + @" then " + fenshuCol + @" else 0 end) = 0)
then 0
else (sum(case when paperid = " + item.Value + @" then " + fenshuCol + " else 0 end) / sum(case when paperid = " + item.Value + " then 1 else 0 end)) end [" + item.Text + "],";
                }
                i++;
            }
            sql += v + @" 'orderid' from RPT_DDT where " + pinPaiConfig + " = 1 and paperid in (" + paperidSql.TrimEnd(',') + ") {0} group by " + weiDuCol;

            return sql;
        }

        public void btnOutput_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string htmlOutPut = "";
            string[] header_Col = new string[2];

            dt = DataLoad();
            dt.Columns.Remove(dt.Columns[dt.Columns.Count-1]);
            ExcelOutPut excelOutPut = new ExcelOutPut();
            System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(dt);
            try
            {
                byte[] bt = ms.ToArray();
                //以字符流的形式下载文件  
                Response.ContentType = "application/vnd.ms-excel";
                //通知浏览器下载文件而不是打开
                Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                Response.BinaryWrite(bt);

                Response.Flush();
                Response.End();
                bt = null;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (ms != null) ms.Dispose();
            }
        }


    }
}