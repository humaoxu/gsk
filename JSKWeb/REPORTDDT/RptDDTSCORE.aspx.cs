﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Collections;

namespace JSKWeb.REPORTDDT
{
    public partial class RptDDTSCORE : System.Web.UI.Page
    {
        SqlHelper sqlhelper = new SqlHelper();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TicketTypeDataBind();
            }
        }

        public void TicketTypeDataBind()
        {
            var ddlProjectId = InputText.GetConfig("gsk_ddt");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0} and isRPTReleased = 1 order by paperId desc ", ddlProjectId);
            //var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE projectId={0}  order by paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();

            var ddlTypeSelectSql = " SELECT distinct col13  FROM surveyobj where projectid = 13 and col13 is not null  ";
            var ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qdDownCheckBoxes.DataSource = ddlTypeDataSet;
            qdDownCheckBoxes.DataValueField = "col13";
            qdDownCheckBoxes.DataTextField = "col13";
            qdDownCheckBoxes.DataBind();

            //ddlTypeSelectSql = " SELECT distinct area FROM surveyobj where projectid = 13 and area is not null ";
            ddlTypeSelectSql = " select distinct dq from RPT_DDT  ";
            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            dqDownCheckBoxes.DataSource = ddlTypeDataSet;
            dqDownCheckBoxes.DataValueField = "dq";
            dqDownCheckBoxes.DataTextField = "dq";
            dqDownCheckBoxes.DataBind();

            ddlTypeSelectSql = " SELECT distinct col21 FROM surveyobj where projectid = 13 and col21 is not null ";
            //ddlTypeSelectSql = " select distinct xq from RPT_DDT";

            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            qyDownCheckBoxes.DataSource = ddlTypeDataSet;
            qyDownCheckBoxes.DataValueField = "col21";
            qyDownCheckBoxes.DataTextField = "col21";
            qyDownCheckBoxes.DataBind();

            ddlTypeSelectSql = " SELECT distinct city FROM surveyobj where projectid = 13 and city is not null ";
            ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
            cityDownCheckBoxes.DataSource = ddlTypeDataSet;
            cityDownCheckBoxes.DataValueField = "city";
            cityDownCheckBoxes.DataTextField = "city";
            cityDownCheckBoxes.DataBind();
            
        }

        ArrayList dqList = new ArrayList();  //大区
        protected void dqDownCheckBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            qyDownCheckBoxes.Items.Clear();
            qyDownCheckBoxes.DataSource = null;

            cityDownCheckBoxes.Items.Clear();
            cityDownCheckBoxes.DataSource = null;

            //如果大区全选
            int select_sum = Int32.Parse(dqDownCheckBoxes.Items.Capacity.ToString());
            //根据区域筛选where
            foreach (ListItem item in (sender as ListControl).Items)
            {
                if (item.Selected)
                {
                    dqList.Add(item.Value);
                }
            }

            string str_code = string.Empty;
            bool bSelectAll = false;
            if (dqList.Count > 0)
            {
                //如果是全选状态下
                if (select_sum - dqList.Count == 1)
                {
                    bSelectAll = true; 
                }
                else
                {
                    for (int i = 0; i < dqList.Count; i++)
                    {
                        if (i == 0)
                        {

                            str_code += " '" + dqList[i].ToString() + "' ";
                        }
                        else
                        {

                            str_code += " ,'" + dqList[i].ToString() + "' ";
                        }
                    }
                }
            }
            var ddlTypeSelectSql = "";
            DataSet ddlTypeDataSet;
            if (bSelectAll || dqList.Count == 0)
            {
                ddlTypeSelectSql = " SELECT distinct col21 FROM surveyobj where projectid = 13 and col21 is not null ";
                ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
                qyDownCheckBoxes.DataSource = ddlTypeDataSet;
                qyDownCheckBoxes.DataValueField = "col21";
                qyDownCheckBoxes.DataTextField = "col21";
                qyDownCheckBoxes.DataBind();

                ddlTypeSelectSql = " SELECT distinct city FROM surveyobj where projectid = 13 and city is not null ";
                ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
                cityDownCheckBoxes.DataSource = ddlTypeDataSet;
                cityDownCheckBoxes.DataValueField = "city";
                cityDownCheckBoxes.DataTextField = "city";
                cityDownCheckBoxes.DataBind();
            }
            else
            {
                ddlTypeSelectSql = " SELECT distinct col21 FROM surveyobj where projectid = 13 and col21 is not null and area in ("+ str_code + ") ";
                ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
                qyDownCheckBoxes.DataSource = ddlTypeDataSet;
                qyDownCheckBoxes.DataValueField = "col21";
                qyDownCheckBoxes.DataTextField = "col21";
                qyDownCheckBoxes.DataBind();

                ddlTypeSelectSql = " SELECT distinct city FROM surveyobj where projectid = 13 and city is not null and area in (" + str_code + ")  ";
                ddlTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTypeSelectSql);
                cityDownCheckBoxes.DataSource = ddlTypeDataSet;
                cityDownCheckBoxes.DataValueField = "city";
                cityDownCheckBoxes.DataTextField = "city";
                cityDownCheckBoxes.DataBind();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadInfo();
        }
        
    
        private bool checkDateTimeFormate(string txtTime)
        {
            try
            {
                DateTime.Parse(txtTime);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool checkDateTimeFormat(string txtTime)
        {
            try
            {
                string sDate = txtTime.Substring(0, 4) + "-" + txtTime.Substring(4, 2) + "-" + txtTime.Substring(6, 2);
                DateTime.Parse(sDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        protected void LoadInfo()
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                string strSql = "";

                if (ddldim.SelectedIndex == 0)
                {
                    strSql = strSql + "select * from (";
                    strSql = strSql + GetSql("全渠道");

                    strSql = strSql + " Union  ";

                    strSql = strSql + GetSql("col13");

                    strSql = strSql + " ) b order by orderid ";
                }
                else if (ddldim.SelectedIndex == 1)
                {
                    strSql = strSql + "select * from (";
                    strSql = strSql + GetSql("全国");

                    strSql = strSql + " Union  ";

                    strSql = strSql + GetSql("area");
                    strSql = strSql + " ) b order by orderid ";
                }

                else if (ddldim.SelectedIndex == 2)
                {
                    strSql = strSql + "select * from (";
                    strSql = strSql + GetSql("全国");

                    strSql = strSql + " Union  ";

                    strSql = strSql + GetSql("col21");
                    strSql = strSql + " ) b order by orderid ";
                }

                else if (ddldim.SelectedIndex == 3)
                {
                    strSql = strSql + "select * from (";
                    strSql = strSql + GetSql("全国");

                    strSql = strSql + " Union  ";
                    strSql = strSql + GetSql("city");
                    strSql = strSql + " ) b order by orderid ";
                }
               
                else if (ddldim.SelectedIndex == 4)
                {
                    strSql = strSql + GetSql("col27");
                }

                else if (ddldim.SelectedIndex == 5)
                {
                    strSql = strSql + GetSql("col25");
                }

                else if (ddldim.SelectedIndex == 6)
                {
                    strSql = strSql + GetSql("col23");
                }

                ////生成Dataset
                //DataSet dateData = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sb_sql.ToString());
                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(SqlHelper.ConnStrEagles);
                con.Open();
                System.Data.SqlClient.SqlCommand com = new System.Data.SqlClient.SqlCommand(strSql.ToString(), con);
                System.Data.SqlClient.SqlDataReader dr = com.ExecuteReader();
                //动态定义样式
                sb.Append("<table class='tablestyle' cellspacing='0' cellpadding='2' style='font-size: 9pt; width: 100%;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                string order_str = string.Empty;
                for (int i = 0; i < dr.FieldCount - 1; i++)
                {
                    order_str = dr.GetName(0);

                    sb.Append("<th align='center' style='width:200px;' scope='col'>" + dr.GetName(i) + "</th>");
                }
                sb.Append("</tr>");
                while (dr.Read())
                {
                    sb.Append("<tr style='cursor:pointer'>");
                    for (int i = 0; i < dr.FieldCount - 1; i++)
                    {
                        try
                        {
                            double dtemp = Double.Parse(dr[i].ToString());
                            sb.Append("<td class='across'>" + dtemp.ToString("0.###") + "</td>");
                        }
                        catch
                        {
                            sb.Append("<td class='across'>" + dr[i].ToString() + "</td>");
                        }


                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
                con.Close();
                dr.Close();
                content.InnerHtml = sb.ToString();

            }
            catch
            {
                content.InnerHtml = "";
            }
        }

        private string GetSql(string strWd)
        {
            string str = " select ";
            if (strWd == "全国" || strWd == "全渠道")
            {
                str = str + " '" + strWd + "'  '名称',";
                str = str + " avg(SYS_DDT_RESULT.t_score) 'AVG(总分)',avg(SSDPOSM) 'AVG(舒适达POSM)',avg(BLJPOSM) 'AVG(保丽净POSM)',avg(ssdfx) 'AVG(舒适达分销)',avg(BLJFX)  'AVG(保丽净分销)',avg(BLJWGJFX)  'AVG(保丽净稳固剂分销)',avg(BLJQJPFX)  'AVG(保丽净清洁片分销)',avg(SSDHR)  'AVG(舒适达活跃)',avg(BLJHR)  'AVG(保丽净活跃)',0 orderid from SYS_DDT_RESULT inner join SurveyObj on SYS_DDT_RESULT.shopid = surveyobj.surveyobjid  ";

            }
            else
            {
                str = str + "surveyobj." + strWd + " '名称',";
                str = str + " avg(SYS_DDT_RESULT.t_score) 'AVG(总分)',avg(SSDPOSM) 'AVG(舒适达POSM)',avg(BLJPOSM) 'AVG(保丽净POSM)',avg(ssdfx) 'AVG(舒适达分销)',avg(BLJFX)  'AVG(保丽净分销)',avg(BLJWGJFX)  'AVG(保丽净稳固剂分销)',avg(BLJQJPFX)  'AVG(保丽净清洁片分销)',avg(SSDHR)  'AVG(舒适达活跃)',avg(BLJHR)  'AVG(保丽净活跃)',1 orderid  from SYS_DDT_RESULT inner join SurveyObj on SYS_DDT_RESULT.shopid = surveyobj.surveyobjid  ";
            }
            str = str + " where SYS_DDT_RESULT.paperid = " + this.ddlTicketType.SelectedValue + "  ";
            if (strWd == "全国" || strWd == "全渠道")
            {
            }
            else
            {
                str = str + " and surveyobj." + strWd + " is not null ";
            }

            bool bflag = true;
            string strDq = "";
            for (int i = 0; i < dqDownCheckBoxes.Items.Count; i++)
            {
                if (dqDownCheckBoxes.Items[i].Selected)
                {
                    if (bflag)
                    {
                        strDq = strDq + "'" + dqDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {
                        strDq = strDq + ",'" + dqDownCheckBoxes.Items[i].Text + "'";
                    }
                    bflag = false;
                }
            }
            if (strDq != "")
            {
                str = str + " and surveyobj.area in (" + strDq + ")";
            }

            bflag = true;
            string strQy = "";
            for (int i = 0; i < qyDownCheckBoxes.Items.Count; i++)
            {
                if (qyDownCheckBoxes.Items[i].Selected)
                {
                    if (bflag)
                    {
                        strQy = strQy + "'" + qyDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {
                        strQy = strQy + ",'" + qyDownCheckBoxes.Items[i].Text + "'";
                    }
                    bflag = false;
                }
            }
            if (strQy != "")
            {
                str = str + " and surveyobj.col21 in (" + strQy + ")";
            }


            bflag = true;
            string strQd = "";
            for (int i = 0; i < qdDownCheckBoxes.Items.Count; i++)
            {
                if (qdDownCheckBoxes.Items[i].Selected)
                {
                    if (bflag)
                    {
                        strQd = strQd + "'" + qdDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {
                        strQd = strQd + ",'" + qdDownCheckBoxes.Items[i].Text + "'";
                    }
                    bflag = false;
                }
            }
            if (strQd != "")
            {
                str = str + " and surveyobj.col13 in (" + strQd + ")";
            }

            bflag = true;
            string strCity = "";
            for (int i = 0; i < cityDownCheckBoxes.Items.Count; i++)
            {
                if (cityDownCheckBoxes.Items[i].Selected)
                {
                    if (bflag)
                    {
                        strCity = strCity + "'" + cityDownCheckBoxes.Items[i].Text + "'";
                    }
                    else
                    {
                        strCity = strCity + ",'" + cityDownCheckBoxes.Items[i].Text + "'";
                    }
                    bflag = false;
                }
            }
            if (strCity != "")
            {
                str = str + " and surveyobj.city in (" + strCity + ")";
            }

            if (strWd == "全国" || strWd == "全渠道")
            {
                // str = str + " '" + strWd + "',";
            }
            else
            {
                str = str + " group by surveyobj." + strWd + " ";
            }
            return str;
        }

        protected void ddlType1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ddlType2.DataSource = null;
            //ddlType3.DataSource = null;
            //ddlType4.DataSource = null;

            //ddlType2.Items.Clear();
            //ddlType3.Items.Clear();
            //ddlType4.Items.Clear();

            //DataSet select_ds = new DataSet();
            //var select_sql = "";
            //if (InputText.GetConfig("gsk_user").ToString().Trim().ToUpper().Equals(this._LinxSanmpleUserCode.ToUpper().Trim()))
            //{
            //    select_sql = " SELECT DISTINCT  COL26 CODE,COL27 NAME  FROM SURVEYOBJ  WHERE PROJECTID=13 AND COL26 IS NOT NULL ";
            //    if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
            //    {

            //        select_sql = select_sql + " and COL13 = '" + ddlType1.SelectedValue + "' ";

            //        select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            //        if (select_ds != null)
            //        {
            //            ddlType2.DataSource = select_ds;
            //            ddlType2.DataValueField = "code";
            //            ddlType2.DataTextField = "name";
            //            ddlType2.DataBind();
            //        }
            //    }
            //}
            //else
            //{
            //    select_sql = "SELECT DISTINCT  SURVEYOBJ.COL26 CODE,SURVEYOBJ.COL27 NAME  FROM SURVEYOBJ   inner join (select * from V_DDT_USER_SHOP where usercode = '" + this._LinxSanmpleUserCode + "') VUSER on SURVEYOBJ.surveyobjid = VUSER.surveyobjid and  SURVEYOBJ.PROJECTID=13 AND SURVEYOBJ.COL26 IS NOT NULL ";
            //    if (!ddlType1.SelectedItem.Text.Trim().Equals("全部"))
            //    {
            //        select_sql = select_sql + " and SURVEYOBJ.COL13 = '" + ddlType1.SelectedValue + "' ";

            //        select_ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, select_sql);
            //        if (select_ds != null)
            //        {
            //            ddlType2.DataSource = select_ds;
            //            ddlType2.DataValueField = "code";
            //            ddlType2.DataTextField = "name";
            //            ddlType2.DataBind();
            //        }
            //    }
            //}


            //ddlType2.Items.Insert(0, new ListItem("全部", string.Empty));
            //ddlType3.Items.Insert(0, new ListItem("全部", string.Empty));
            //ddlType4.Items.Insert(0, new ListItem("全部", string.Empty));

        }

        protected void ddlType2_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        protected void ddlType3_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnOutPut_Click(object sender, EventArgs e)
        {


            StringBuilder sb = new StringBuilder();
            string strSql = "";

            if (ddldim.SelectedIndex == 0)
            {
                strSql = strSql + "select * from (";
                strSql = strSql + GetSql("全渠道");

                strSql = strSql + " Union  ";

                strSql = strSql + GetSql("col13");

                strSql = strSql + " ) b order by orderid ";
            }
            else if (ddldim.SelectedIndex == 1)
            {
                strSql = strSql + "select * from (";
                strSql = strSql + GetSql("全国");

                strSql = strSql + " Union  ";

                strSql = strSql + GetSql("area");
                strSql = strSql + " ) b order by orderid ";
            }

            else if (ddldim.SelectedIndex == 2)
            {
                strSql = strSql + "select * from (";
                strSql = strSql + GetSql("全国");

                strSql = strSql + " Union  ";

                strSql = strSql + GetSql("col21");
                strSql = strSql + " ) b order by orderid ";
            }

            else if (ddldim.SelectedIndex == 3)
            {
                strSql = strSql + "select * from (";
                strSql = strSql + GetSql("全国");

                strSql = strSql + " Union  ";
                strSql = strSql + GetSql("city");
                strSql = strSql + " ) b order by orderid ";
            }

            else if (ddldim.SelectedIndex == 4)
            {
                strSql = strSql + GetSql("col27");
            }

            else if (ddldim.SelectedIndex == 5)
            {
                strSql = strSql + GetSql("col25");
            }

            else if (ddldim.SelectedIndex == 6)
            {
                strSql = strSql + GetSql("col23");
            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSql.ToString());
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                ExcelOutPut excelOutPut = new ExcelOutPut();
                System.IO.MemoryStream ms = excelOutPut.ExportOutExcel(ds.Tables[0]);
                try
                {
                    byte[] bt = ms.ToArray();
                    //以字符流的形式下载文件  
                    Response.ContentType = "application/vnd.ms-excel";
                    //通知浏览器下载文件而不是打开
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");
                    Response.BinaryWrite(bt);

                    Response.Flush();
                    Response.End();
                    bt = null;
                }
                catch (Exception)
                {

                    throw;
                }
                finally
                {
                    if (ms != null) ms.Dispose();
                }
            }
            else
            {
                JavaScript.Alert(this, "无符合查询条件的数据。");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string fileName = "DDT申诉操作手册.pptx";//客户端保存的文件名
            string filePath = Server.MapPath("/" + "DDT.pptx");//路径

            //以字符流的形式下载文件
            FileStream fs = new FileStream(filePath, FileMode.Open);
            byte[] bytes = new byte[(int)fs.Length];
            fs.Read(bytes, 0, bytes.Length);
            fs.Close();
            Response.ContentType = "application/octet-stream";
            //通知浏览器下载文件而不是打开
            Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}