﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="DDTQG_Hy.aspx.cs" Inherits="JSKWeb.REPORTDDT.DDTQG_Hy" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />

    <script src="../js/echarts.min.js" type="text/javascript"></script>

    <style type="text/css">
        .aline {
            text-decoration: underline;
        }

        select {
            border-color: #cccccc;
            border-width: 1px;
            border-style: solid;
        }

    </style>
    <script language="javascript" type="text/javascript">
        function hiderightMain_content()
        {
            document.getElementById('rightMain_content').style.display='none'
        }

        function QueryChecked()
        {
            //var cgLen =  $("#topTool_SkuDownCheckBoxes_sl input:[type=checkbox]:checked").length
            //if (cgLen == 0) { 
            //    alert("请选择Category！");
            //    return false;
            //}
            var daquLen = $("#topTool_daquDownCheckBoxes_sl input:[type=checkbox]:checked").length;
            var quyuLen = $("#topTool_quYuDownCheckBoxes_sl input:[type=checkbox]:checked").length;
            var chengshiLen = $("#topTool_chengShiDownCheckBoxes_sl input:[type=checkbox]:checked").length; 
            var xiaodaiLen = $("#topTool_xiaoShouDaiBiaoDownCheckBoxes_sl input:[type=checkbox]:checked").length;

            if ((daquLen+quyuLen+chengshiLen+xiaodaiLen)  == 0) {
                alert("请选择MBD！");
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;MBD筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                <tr>
                  
                    <td style="padding: 10px 0px 10px 10px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qsDropDownCheckBoxes" runat="server" OnSelectedIndexChanged="qsDropDownCheckBoxes_SelcetedIndexChanged"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="120" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="问卷期数" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td> 
                   
                    <td style="padding-left: 5px;">
                     
                        <asp:DropDownList ID="ddlWeiDu"  CssClass="ddl_select"   AutoPostBack="true" Width="100" runat="server" OnSelectedIndexChanged="ddlWeiDu_SelcetedIndexChanged" >
                             <asp:ListItem Text="市场划分" Value="市场划分"></asp:ListItem>
                             <asp:ListItem Text="渠道" Value="渠道"></asp:ListItem>
                        </asp:DropDownList>
                    </td>


                    <td style="padding-left: 5px;">
                     <%--   <DropDownCheckBoxes:DropDownCheckBoxes ID="quDaoDownCheckBoxes" runat="server"  OnSelectedIndexChanged="quDaoDownCheckBoxes_SelcetedIndexChanged"
                          Visible="false"   AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>--%>
                        <asp:DropDownList ID="ddlquDao"  CssClass="ddl_select"  Visible="false"  Width="100" runat="server" ></asp:DropDownList>
                    </td>

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="daquDownCheckBoxes" runat="server" OnSelectedIndexChanged="daquDownCheckBoxes_SelcetedIndexChanged"
                           Visible="false"    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="大区" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="quYuDownCheckBoxes" runat="server" OnSelectedIndexChanged="quYuDownCheckBoxes_SelcetedIndexChanged"
                           Visible="false"    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="chengShiDownCheckBoxes" runat="server"  OnSelectedIndexChanged="chengShiDownCheckBoxes_SelcetedIndexChanged"
                           Visible="false"    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="城市" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>

                    <td style="padding-left: 5px;">
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="xiaoShouDaiBiaoDownCheckBoxes" runat="server"  
                             Visible="false"  AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                           
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="销售代表" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td> 
                   
               
                </tr>
            </tbody>
        </table>
        <div style="height: 5px"></div>
     <fieldset>
        <legend>&nbsp;产品筛选&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0" class="">
            <tbody>
                 <tr> 
                    <td style="padding: 10px 0px 10px 10px;">
                        <asp:DropDownList ID="ddlPP"  CssClass="ddl_select"  runat="server" Width="160px" AutoPostBack="True">
                            <asp:ListItem Value="0">舒适达</asp:ListItem>
                            <asp:ListItem Value="1">保丽净</asp:ListItem>
                            <asp:ListItem Value="2">保丽净稳固剂</asp:ListItem>
                            <asp:ListItem Value="3">保丽净清洁片</asp:ListItem>
                        </asp:DropDownList>
                    </td>      <td align="left"  >&nbsp;<asp:Button ID="btnQuery"  CssClass="btn_s" OnClick="btnSearch_Click"  OnClientClick="return QueryChecked()" 
                        runat="server" Text="数据查询" Width="60px" />
                        &nbsp;
                        <asp:Button ID="btnOutPut"   CssClass="btn_s"
                        runat="server" Text="数据导出" Width="60px" OnClick="btnOutput_Click"  OnClientClick="return QueryChecked()"  />
                    </td>
                 </tr>
            </tbody>
        </table>
        <div style="height: 5px"></div>

    </fieldset>

    </fieldset>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">
    <div id="content" runat="server" style="text-align: center">
    </div>
    <div id="main" style="width: 100%; height: 500px; overflow: hidden;"></div>
   
    <script type="text/javascript">
        var s_sum = 10;
        var s_itemName  = <%= this.item_Name %>; 
        if (s_itemName != '[0]' && s_itemName !='') {
            var series_tmp = <%= this.item_series %>;
            var s_legend = <%= this.item_legend %>;  
            var s_itemTitle =[   '活跃评分-'+$("#topTool_ddlPP option:selected").text()];
            var s_xAxis_data =[];
            if(s_itemName.length>10)
                s_sum = 45;
            else 
                s_sum = 0;
            option = {          
                title: {
                    x:'left',
                    text:s_itemTitle
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: s_legend
                },
                toolbox: {
                    show : true,
                    feature : {
                        mark : {show: true},
                        dataView : {show: true, readOnly: false},
                        magicType : {show: true, type: ['line', 'bar']},
                        restore : {show: true},
                        saveAsImage : {show: true}
                    }
                },
                calculable : true,
                xAxis: [
                     {
                         type: 'category',
                         axisLabel: {
                             interval: 0,
                             rotate: s_sum,
                             margin: 2,
                             textStyle: {
                                 color: "#222"
                             }
                         },
                         data: s_itemName
                     }
                ],
                grid: {
                    x: 40,
                    x2: 20,
                    y2: 100,
                },
                yAxis: [
                      {
                          type: 'value',
                          min: 0,
                          max: 12,
                          interval: 3,
                          axisLabel: {
                              formatter: '{value}'
                          }
                      }
                ],
                series: series_tmp    
            };



            var chart = echarts.init(document.getElementById('main'));
       
            chart.showLoading({
                text: '正在加载中..... ',
            });
            chart.hideLoading();

            chart.setOption(option);

            $("#main").resize(function(){
                $("#main").resize();
            })
        }
        
    </script>
    <div id="sqlHtml" runat="server">
    </div>
</asp:Content>
