﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using DBUtility;
using JSKWeb.Code;
using JSKWeb.Utility;
using System.Text;
using System.IO;
using System.Configuration;
using System.Collections;

namespace JSKWeb.REPORTDDT
{
    public partial class DDTQG : System.Web.UI.Page
    {
        public string itemName = "[]";
        public string itemValue = "[]";
        public string itemTitle = "[]";
        public string itemFormate = "[]";
        SqlHelper sqlhelper = new SqlHelper();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                TicketTypeDataBind();
                string strFormType = "";
                //request form type
                if (Request.QueryString["strFormType"] != null)
                {
                    strFormType = Request.QueryString["strFormType"].ToString();
                }

                if (strFormType == "")
                {
                    strFormType = "1";
                }
                if (strFormType == "4"  || strFormType =="6")
                {
                    sku_td.Visible = true;
                    sku_td_ddl.Visible = true;
                    
                    RestSku();
                }
                //ddlDim.SelectedIndex = 0;
            }
        }

        public void TicketTypeDataBind()
        {
            string strFormType = "";
            //request form type
            if (Request.QueryString["strFormType"] != null)
            {
                strFormType = Request.QueryString["strFormType"].ToString();
            }

            if (strFormType == "")
            {
                strFormType = "1";
            }

            var ddlProjectId = InputText.GetConfig("gsk_ddt");
            var ddlTicketTypeSelectSql = String.Format("SELECT paperId, paperTitle FROM Paper WHERE isRPTReleased =1 and projectId={0} order by  paperId desc ", ddlProjectId);
            var ddlTicketTypeDataSet = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, ddlTicketTypeSelectSql);
            ddlTicketType.DataSource = ddlTicketTypeDataSet;
            ddlTicketType.DataValueField = "paperId";
            ddlTicketType.DataTextField = "paperTitle";
            ddlTicketType.DataBind();



            Dictionary<string, string> Ht = new Dictionary<string, string>();

            switch (strFormType)
            {
                //立招陈列评分  
                case "1":
                    Ht.Add("0", "全国");
                    Ht.Add("1", "大区");
                    Ht.Add("2", "区域");
                    Ht.Add("3", "渠道");
                    Ht.Add("4", "城市");
                    Ht.Add("5", "销售代表");
                    break;
                //陈列率（%） 
                case "2":
                    Ht.Add("0", "全国");
                    Ht.Add("1", "大区");
                    Ht.Add("2", "区域");
                    Ht.Add("3", "渠道");
                    Ht.Add("4", "城市");
                    break;
                //产品分销评分 
                case "3":
                    Ht.Add("0", "全国");
                    Ht.Add("1", "大区");
                    Ht.Add("2", "区域");
                    Ht.Add("3", "渠道");
                    Ht.Add("4", "城市");
                    Ht.Add("5", "销售代表");

                    break;
                //产品分销率（%） 
                case "4":
                    Ht.Add("0", "全国");
                    Ht.Add("1", "大区");
                    Ht.Add("2", "区域");
                    Ht.Add("3", "渠道");
                    Ht.Add("4", "城市");
                    ddl_qy.AutoPostBack = true;
                    break;
                //产品活跃评分
                case "5":
                    Ht.Add("0", "全国");
                    Ht.Add("1", "大区");
                    Ht.Add("2", "区域");
                    Ht.Add("3", "渠道");
                    Ht.Add("4", "城市");
                    Ht.Add("5", "销售代表");

                    break;
                //产品活跃率（%）
                case "6":
                    Ht.Add("0", "全国");
                    Ht.Add("1", "大区");
                    Ht.Add("2", "区域");
                    Ht.Add("3", "渠道");
                    Ht.Add("4", "城市");
                    ddl_qy.AutoPostBack = true;
                    break;
                default:
                    break;
            }

            this.ddldim.DataSource = Ht.OrderBy(o => o.Key);
            this.ddldim.DataValueField = "key";
            this.ddldim.DataTextField = "value";
            this.ddldim.DataBind();

        }

        public void RestForm()
        {
            daqu_td.Visible = false;
            daqu_td_ddl.Visible = false;
            quyu_td.Visible = false;
            quyu_td_ddl.Visible = false;

            //sku_td.Visible = false;
            //sku_td_ddl.Visible = false;

            ddl_qy.AutoPostBack = false;
        }

        public DataTable DataLoad()
        {
            DataTable dt = new DataTable();
            string strFormType = "";
            //request form type
            if (Request.QueryString["strFormType"] != null)
            {
                strFormType = Request.QueryString["strFormType"].ToString();
            }

            if (strFormType == "")
            {
                strFormType = "1";
            }

            string daqu = "";

            string quyu = "";

            string sku = "";

            string sql = "";

            string sql_group = "";

            string sqlwhere = " and paperid = " + ddlTicketType.SelectedValue;

            if (ddl_dq.SelectedIndex > 0 && daqu_td_ddl.Visible)
            {
                daqu = ddl_dq.SelectedItem.Text;
                sqlwhere = " and dq='" + daqu + "'";
            }
            if (ddl_qy.SelectedIndex > 0 && quyu_td_ddl.Visible)
            {
                quyu = ddl_qy.SelectedItem.Text;
                sqlwhere += " and xq='" + quyu + "'";
            }
            if (sku_td_ddl.Visible && (strFormType =="4" || strFormType == "6" ))
            {
                sku = ddl_sku.SelectedItem.Value;
                sku = "  "+ sku + " ='有分销' and ";
            }

            string scroe_Str = "";
            string cfg_Str = "";

            #region MyRegion
            switch (strFormType)
            {

                case "1":
                    itemTitle = "'立招陈列评分'";
                    itemFormate = "[]";
                    #region 立招陈列评分
                    if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                    {
                        scroe_Str = "ssdposm_scroe";
                        cfg_Str = "ssdposm_cfg";
                    }
                    else
                    {
                        scroe_Str = "bljposm_score";
                        cfg_Str = "bljposm_cfg";
                    }
                    #region 全国
                    if (ddldim.SelectedItem.Text.Equals("全国"))
                    {
                        sql = GetQuGuo1(scroe_Str, cfg_Str, sqlwhere);
                    }
                    #endregion
                    #region 大区
                    else if (ddldim.SelectedItem.Text.Equals("大区"))
                    {
                        sql = GetQuGuo1(scroe_Str, cfg_Str, sqlwhere);
                        sql += " union all ";
                        sql += GetDaQu1(scroe_Str, cfg_Str, "dq", sqlwhere);
                    }
                    #endregion
                    #region 区域
                    else if (ddldim.SelectedItem.Text.Equals("区域"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo1(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol1(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }

                    }
                    #endregion
                    #region 渠道
                    else if (ddldim.SelectedItem.Text.Equals("渠道"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo1(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol1(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                    }
                    #endregion
                    #region 城市
                    else if (ddldim.SelectedItem.Text.Equals("城市"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo1(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol1(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                    }
                    #endregion
                    #region 销售代表
                    else if (ddldim.SelectedItem.Text.Equals("销售代表"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo1(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol1(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                    }
                    #endregion
                    #endregion
                    break;

                case "2":
                    itemTitle = "'产品陈列率'";
                    itemFormate = "'%'";
                    #region 陈列率（%）
                    if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                    {
                        scroe_Str = "ssdlz";
                        cfg_Str = "ssdposm_cfg";
                    }
                    else
                    {
                        scroe_Str = "bljlz";
                        cfg_Str = "bljposm_cfg";
                    }
                    #region 全国
                    if (ddldim.SelectedItem.Text.Equals("全国"))
                    {
                        sql = GetQuGuo2(scroe_Str, cfg_Str, sqlwhere);
                    }
                    #endregion
                    #region 大区
                    else if (ddldim.SelectedItem.Text.Equals("大区"))
                    {
                        sql = GetQuGuo2(scroe_Str, cfg_Str, sqlwhere);
                        sql += " union all ";
                        sql += GetDaQu2(scroe_Str, cfg_Str, "dq", sqlwhere);
                    }
                    #endregion
                    #region 区域
                    else if (ddldim.SelectedItem.Text.Equals("区域"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo2(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol2(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }
                    }
                    #endregion
                    #region 渠道
                    else if (ddldim.SelectedItem.Text.Equals("渠道"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo2(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol2(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                    }
                    #endregion
                    #region 城市
                    else if (ddldim.SelectedItem.Text.Equals("城市"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo2(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol2(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                    }
                    #endregion
                    #region 销售代表
                    else if (ddldim.SelectedItem.Text.Equals("销售代表"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo2(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol2(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu2(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                    }
                    #endregion
                    #endregion
                    break;
                ///产品分销评分 
                case "3":
                    itemTitle = "'产品分销评分'";
                    itemFormate = "[]";
                    #region 全国
                    if (ddldim.SelectedItem.Text.Equals("全国"))
                    {
                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            //sql = @" select '全国' weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1";
                            sql = GetQuGuo1("ssdfx_scroe", "ssdfx_cfg", sqlwhere);
                        }
                        else
                        {
                            //sql = @" select '全国' weidu, (sum(bljposm_score) /COUNT(1)) fenzhi from RPT_DDT  where bljposm_cfg = 1";
                            sql = GetQuGuo1("bljfx_score", "bljfx_cfg", sqlwhere);
                        }
                    }
                    #endregion
                    #region 大区
                    else if (ddldim.SelectedItem.Text.Equals("大区"))
                    {
                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            //sql = @" select '全国' weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 union 
                            //select dq weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 group by dq ";
                            sql = GetQuGuo1("ssdfx_scroe", "ssdfx_cfg", sqlwhere);
                            sql += " union ";
                            sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "dq", sqlwhere);
                        }
                        else
                        {
                            //sql = @" select '全国' weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT where bljposm_cfg = 1 union
                            //select dq weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT  where bljposm_cfg = 1 group by dq ";
                            sql = GetQuGuo1("bljfx_score", "bljfx_cfg", sqlwhere);
                            sql += " union all ";
                            sql += GetDaQu1("bljfx_score", "bljfx_cfg", "dq", sqlwhere);
                        }
                    }
                    #endregion
                    #region 区域
                    else if (ddldim.SelectedItem.Text.Equals("区域"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            //sql = @" select '全国' weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 union 
                            //select dq weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 group by dq ";
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("ssdfx_scroe", "ssdfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "xq", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("ssdfx_scroe", "ssdfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "xq", sqlwhere);
                            }

                        }
                        else
                        {
                            //sql = @" select '全国' weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT where bljposm_cfg = 1 union
                            //select dq weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT  where bljposm_cfg = 1 group by dq ";
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("bljfx_score", "bljfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "xq", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("bljfx_score", "bljfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "xq", sqlwhere);
                            }
                        }
                    }
                    #endregion
                    #region 渠道
                    else if (ddldim.SelectedItem.Text.Equals("渠道"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("ssdfx_scroe", "ssdfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "shoptype", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("ssdfx_scroe", "ssdfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "shoptype", sqlwhere);
                            }

                        }
                        else
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("bljfx_score", "bljfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "shoptype", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("bljfx_score", "bljfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "shoptype", sqlwhere);
                            }
                        }
                    }
                    #endregion
                    #region 城市
                    else if (ddldim.SelectedItem.Text.Equals("城市"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("ssdfx_scroe", "ssdfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "city", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("ssdfx_scroe", "ssdfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "city", sqlwhere);
                            }

                        }
                        else
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("bljfx_score", "bljfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "city", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("bljfx_score", "bljfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "city", sqlwhere);
                            }
                        }
                    }
                    #endregion
                    #region 销售代表
                    else if (ddldim.SelectedItem.Text.Equals("销售代表"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("ssdfx_scroe", "ssdfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "ddt_name", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("ssdfx_scroe", "ssdfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("ssdfx_scroe", "ssdfx_cfg", "ddt_name", sqlwhere);
                            }

                        }
                        else
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo1("bljfx_score", "bljfx_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "ddt_name", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol1("bljfx_score", "bljfx_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu1("bljfx_score", "bljfx_cfg", "ddt_name", sqlwhere);
                            }
                        }
                    }
                    #endregion                     
                    break;
                ///.产品分销率（%）
                case "4":
                    itemTitle = "'产品分销率'";
                    itemFormate = "'%'";
                    #region 产品分销率（%）
                    if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                    {
                        scroe_Str = sku;
                        cfg_Str = "ssdfx_cfg";
                    }
                    else
                    {
                        scroe_Str = sku;
                        cfg_Str = "bljfx_cfg";
                    }
                    #region 全国
                    if (ddldim.SelectedItem.Text.Equals("全国"))
                    {
                        sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                    }
                    #endregion
                    #region 大区
                    else if (ddldim.SelectedItem.Text.Equals("大区"))
                    {
                        sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                        sql += " union all ";
                        sql += GetDaQu3(scroe_Str, cfg_Str, "dq", sqlwhere);
                    }
                    #endregion
                    #region 区域
                    else if (ddldim.SelectedItem.Text.Equals("区域"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }
                    }
                    #endregion
                    #region 渠道
                    else if (ddldim.SelectedItem.Text.Equals("渠道"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                    }
                    #endregion
                    #region 城市
                    else if (ddldim.SelectedItem.Text.Equals("城市"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                    }
                    #endregion
                    #region 销售代表
                    else if (ddldim.SelectedItem.Text.Equals("销售代表"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                    }
                    #endregion
                    #endregion

                    break;

                ///产品活跃评分
                case "5":
                    itemTitle = "'产品活跃评分'";
                    itemFormate = "[]";
                    #region 全国
                    if (ddldim.SelectedItem.Text.Equals("全国"))
                    {
                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            //sql = @" select '全国' weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1";
                            sql = GetQuGuo4("ssdhr_score", "ssdhr_cfg", sqlwhere);
                        }
                        else
                        {
                            //sql = @" select '全国' weidu, (sum(bljposm_score) /COUNT(1)) fenzhi from RPT_DDT  where bljposm_cfg = 1";
                            sql = GetQuGuo4("bljhr_score", "bljhr_cfg", sqlwhere);
                        }
                    }
                    #endregion
                    #region 大区
                    else if (ddldim.SelectedItem.Text.Equals("大区"))
                    {
                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            //sql = @" select '全国' weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 union 
                            //select dq weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 group by dq ";
                            sql = GetQuGuo4("ssdhr_score", "ssdhr_cfg", sqlwhere);
                            sql += " union ";
                            sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "dq", sqlwhere);
                        }
                        else
                        {
                            //sql = @" select '全国' weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT where bljposm_cfg = 1 union
                            //select dq weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT  where bljposm_cfg = 1 group by dq ";
                            sql = GetQuGuo4("bljhr_score", "bljhr_cfg", sqlwhere);
                            sql += " union all ";
                            sql += GetDaQu4("bljhr_score", "bljhr_cfg", "dq", sqlwhere);
                        }
                    }
                    #endregion
                    #region 区域
                    else if (ddldim.SelectedItem.Text.Equals("区域"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            //sql = @" select '全国' weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 union 
                            //select dq weidu, (sum(ssdposm_scroe) /COUNT(1)) fenzhi from RPT_DDT  where ssdposm_cfg = 1 group by dq ";
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("ssdhr_score", "ssdhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "xq", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("ssdhr_score", "ssdhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "xq", sqlwhere);
                            }

                        }
                        else
                        {
                            //sql = @" select '全国' weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT where bljposm_cfg = 1 union
                            //select dq weidu, (sum(bljposm_score) / COUNT(1)) fenzhi from RPT_DDT  where bljposm_cfg = 1 group by dq ";
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("bljhr_score", "bljhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "xq", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("bljhr_score", "bljhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "xq", sqlwhere);
                            }
                        }
                    }
                    #endregion
                    #region 渠道
                    else if (ddldim.SelectedItem.Text.Equals("渠道"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("ssdhr_score", "ssdhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "shoptype", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("ssdhr_score", "ssdhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "shoptype", sqlwhere);
                            }

                        }
                        else
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("bljhr_score", "bljhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "shoptype", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("bljhr_score", "bljhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "shoptype", sqlwhere);
                            }
                        }
                    }
                    #endregion
                    #region 城市
                    else if (ddldim.SelectedItem.Text.Equals("城市"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("ssdhr_score", "ssdhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "city", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("ssdhr_score", "ssdhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "city", sqlwhere);
                            }

                        }
                        else
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("bljhr_score", "bljhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "city", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("bljhr_score", "bljhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "city", sqlwhere);
                            }
                        }
                    }
                    #endregion
                    #region 销售代表
                    else if (ddldim.SelectedItem.Text.Equals("销售代表"))
                    {

                        if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("ssdhr_score", "ssdhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "ddt_name", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("ssdhr_score", "ssdhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("ssdhr_score", "ssdhr_cfg", "ddt_name", sqlwhere);
                            }

                        }
                        else
                        {
                            if (ddl_dq.SelectedItem.Text.Equals("全部"))
                            {
                                sql = GetQuGuo4("bljhr_score", "bljhr_cfg", sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "ddt_name", sqlwhere);
                            }
                            else
                            {
                                sql += GetSumCol4("bljhr_score", "bljhr_cfg", daqu, sqlwhere);
                                sql += " union ";
                                sql += GetDaQu4("bljhr_score", "bljhr_cfg", "ddt_name", sqlwhere);
                            }
                        }
                    }
                    #endregion                     
                    break;

                ///.产品活跃率（%）
                case "6":
                    itemTitle = "'产品活跃率'";
                    itemFormate = "'%'";

                    #region 产品分销率（%）
                    if (ddlPP.SelectedItem.Text.Equals("舒适达"))
                    {
                        scroe_Str = sku;
                        cfg_Str = "ssdhr_cfg";
                    }
                    else
                    {
                        scroe_Str = sku;
                        cfg_Str = "bljhr_cfg";
                    }
                    #region 全国
                    if (ddldim.SelectedItem.Text.Equals("全国"))
                    {
                        sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                    }
                    #endregion
                    #region 大区
                    else if (ddldim.SelectedItem.Text.Equals("大区"))
                    {
                        sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                        sql += " union all ";
                        sql += GetDaQu3(scroe_Str, cfg_Str, "dq", sqlwhere);
                    }
                    #endregion
                    #region 区域
                    else if (ddldim.SelectedItem.Text.Equals("区域"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "xq", sqlwhere);
                        }
                    }
                    #endregion
                    #region 渠道
                    else if (ddldim.SelectedItem.Text.Equals("渠道"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "shoptype", sqlwhere);
                        }
                    }
                    #endregion
                    #region 城市
                    else if (ddldim.SelectedItem.Text.Equals("城市"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "city", sqlwhere);
                        }
                    }
                    #endregion
                    #region 销售代表
                    else if (ddldim.SelectedItem.Text.Equals("销售代表"))
                    {
                        if (ddl_dq.SelectedItem.Text.Equals("全部"))
                        {
                            sql = GetQuGuo3(scroe_Str, cfg_Str, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                        else
                        {
                            sql += GetSumCol3(scroe_Str, cfg_Str, daqu, sqlwhere);
                            sql += " union ";
                            sql += GetDaQu3(scroe_Str, cfg_Str, "ddt_name", sqlwhere);
                        }
                    }
                    #endregion
                    #endregion

                    break;


                default:
                    content.InnerHtml = "访问错误";
                    return null;
            }
            #endregion

            sql += " order by orderid";

            dt = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql).Tables[0];

            return dt;

        }

        protected void btnChart_Click(object sender, EventArgs e)
        {
            content.InnerHtml = "";
           DataTable dt = DataLoad();
                itemValue = "";
                itemName = "";
                if (dt.Rows.Count >0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                    if (i == 0)
                    {
                        itemValue += "";
                        itemName += "";
                    }
                    else
                    {
                        itemValue += ",";
                        itemName += ",";
                    }
                        itemName = itemName + "'" + dt.Rows[i][0].ToString().Trim() + "'";
                        itemValue = itemValue + "" + (InputText.GetDouble( dt.Rows[i][1]).ToString("##0.0").Trim()) + "";
                    }
                    itemName = "[" + itemName + "]";
                    itemValue = "[" + itemValue + "]";
                }
                else
                {
                    itemValue = "[]";
                    itemName = "[]";
                } 
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            itemName = "''";
            itemValue = "''";
           
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = DataLoad();
                if (dt.Rows.Count == 0)
                {
                    return;
                }
                //动态定义样式
                sb.Append("<table class='tablestyle_x' cellspacing='0' cellpadding='2' style='font-size: 9pt;border-collapse: collapse;'><tbody>");
                sb.Append("<tr class='header'>");
                
                for (int i = 0; i < dt.Columns.Count -1 ; i++)
                {
                     
                    sb.Append("<th align='left' style='width:25%;' scope='col'>" + dt.Columns[i].ToString() + "</th>");
                }
                sb.Append("</tr>");
                for (int i = 0; i < dt.Rows.Count; i++) 
                {
                    DataRow dr = dt.Rows[i];
                    sb.Append("<tr style='cursor:pointer;' >");
                    for (int j = 0; j < dt.Columns.Count - 1; j++)
                    {
                        if (j == 1)
                        {
                            sb.Append("<td class='across' style='width:75%;'>" + InputText.GetDouble(dr[j].ToString()).ToString("##0.0") + itemFormate.Replace("[]","").Replace("'","") + "</td>");
                        }
                        else
                        {
                            sb.Append("<td class='across' style='width:25%;'>" + dr[j].ToString() + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
                sb.Append("</tbody></table>");
               
                content.InnerHtml = sb.ToString();

            }
            catch (Exception)
            {

                throw;
            }



        }

        #region 立招陈列评分 1

        public string GetQuGuo1(string ppScore, string ppConfig, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, (sum(" + ppScore + ") /COUNT(1)) 分值,0 'orderid' from RPT_DDT  where " + ppConfig + " = 1 " + strWhere;

            return sql;
        }

        public string GetSumCol1(string ppScore, string ppConfig, string sumCol, string strWhere)
        {
            string sql = "";
            sql = @" select '" + sumCol + "' 维度, (sum(" + ppScore + ") /COUNT(1)) 分值,0 'orderid' from RPT_DDT  where " + ppConfig + " = 1 " + strWhere;

            return sql;
        }

        public string GetDaQu1(string ppScore, string ppConfig, string DQGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + DQGroup + " 维度, (sum(" + ppScore + ") /COUNT(1)) 分值,1 'orderid' from RPT_DDT  where "
                + ppConfig
                + " = 1 " + strWhere + "group by " + DQGroup;

            return sql;
        }

        #endregion

        #region 陈列率（%） 2
        public string GetQuGuo2(string ppLizhao, string ppConfig, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, ( (select COUNT(1)+0.0  from RPT_DDT where " + ppLizhao + " ='有立招' and " + ppConfig
                + " = 1 "+ strWhere + " ) /COUNT(1))*100 分值,0 'orderid' from RPT_DDT where " + ppConfig + " = 1 " + strWhere;
            return sql;
        }

        public string GetSumCol2(string ppLizhao, string ppConfig, string sumCol, string strWhere)
        {
            string sql = "";
            sql = @" select '" + sumCol + "' 维度, ( (select COUNT(1)+0.0  from RPT_DDT where " + ppLizhao + " ='有立招' and " + ppConfig
                + " = 1 " + strWhere + " ) /COUNT(1)) *100 分值,0 'orderid' from RPT_DDT  where " + ppConfig + " = 1 " + strWhere;

            return sql;
        }

        public string GetDaQu2(string ppLizhao, string ppConfig, string DQGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + DQGroup + " 维度, ( (select COUNT(1)+0.0  from RPT_DDT z where " + ppLizhao + " ='有立招' and " + ppConfig
                + " = 1 " + strWhere + " group by z." + DQGroup + " having z." + DQGroup + " = m." + DQGroup
                + " ) /COUNT(1))*100 分值,1 'orderid' from RPT_DDT m where "
                + ppConfig
                + " = 1 " + strWhere + " group by " + DQGroup;

            return sql;
        }

        #endregion

        #region 陈列率（%） 3 By 单品
        public string GetQuGuo3(string ppFX, string ppConfig, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, ( (select COUNT(1)+0.0  from RPT_DDT where " + ppFX + " " + ppConfig
                + " = 1 " + strWhere + " ) /COUNT(1))*100 分值,0 'orderid' from RPT_DDT where " + ppConfig + " = 1 " + strWhere;
            return sql;
        }

        public string GetSumCol3(string ppFX, string ppConfig, string sumCol, string strWhere)
        {
            string sql = "";
            sql = @" select '" + sumCol + "' 维度, ( (select COUNT(1)+0.0  from RPT_DDT where " + ppFX + " " + ppConfig
                + " = 1 " + strWhere + " ) /COUNT(1)) *100 分值,0 'orderid' from RPT_DDT  where " + ppConfig + " = 1 " + strWhere;

            return sql;
        }

        public string GetDaQu3(string ppFX, string ppConfig, string DQGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + DQGroup + " 维度, ( (select COUNT(1)+0.0  from RPT_DDT z where " + ppFX + " " + ppConfig
                + " = 1 " + strWhere + " group by z." + DQGroup + " having z." + DQGroup + " = m." + DQGroup
                + " ) /COUNT(1))*100 分值,1 'orderid' from RPT_DDT m where "
                + ppConfig
                + " = 1 " + strWhere + " group by " + DQGroup;

            return sql;
        }

        #endregion

        #region 产品活跃评分 

        public string GetQuGuo4(string ppScore, string ppConfig, string strWhere)
        {
            string sql = "";
            sql = @" select '全国' 维度, (sum(" + ppScore + ") /COUNT(1)) 分值,0 'orderid' from RPT_DDT  where " + ppConfig + " = 1 " + strWhere;

            return sql;
        }

        public string GetSumCol4(string ppScore, string ppConfig, string sumCol, string strWhere)
        {
            string sql = "";
            sql = @" select '" + sumCol + "' 维度, (sum(" + ppScore + ") /COUNT(1)) 分值,0 'orderid' from RPT_DDT  where " + ppConfig + " = 1 " + strWhere;

            return sql;
        }

        public string GetDaQu4(string ppScore, string ppConfig, string DQGroup, string strWhere)
        {
            string sql = "";
            sql = @" select " + DQGroup + " 维度, (sum(" + ppScore + ") /COUNT(1)) 分值,1 'orderid' from RPT_DDT  where "
                + ppConfig
                + " = 1 " + strWhere + "group by " + DQGroup;

            return sql;
        }

        #endregion

        /// <summary>
        /// 问卷期数
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTicketType_SelectedIndexChanged(object sender, EventArgs e)
        {
            RestForm();
        }

        /// <summary>
        /// 分析纬度
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddldim_SelectedIndexChanged(object sender, EventArgs e)
        {
            RestForm();
            content.InnerText = "";
            switch (ddldim.SelectedValue)
            {
                ///全国
                case "0":
                    break;
                ///大区
                case "1":
                    break;
                ///区域  筛选条件 ： 大区
                case "2":
                    RestDaQu();

                    daqu_td.Visible = true;
                    daqu_td_ddl.Visible = true;

                    break;
                ///渠道  筛选条件 ： 大区 区域
                case "3":
                    RestDaQu();
                    RestQuYu();

                    daqu_td.Visible = true;
                    daqu_td_ddl.Visible = true;
                    quyu_td.Visible = true;
                    quyu_td_ddl.Visible = true;

                    //sku_td.Visible = true;
                    //sku_td_ddl.Visible = true;
                    //ddl_qy.AutoPostBack = true;
                    break;
                ///城市  筛选条件 ： 大区 区域
                case "4":
                    RestDaQu();
                    RestQuYu();

                    daqu_td.Visible = true;
                    daqu_td_ddl.Visible = true;
                    quyu_td.Visible = true;
                    quyu_td_ddl.Visible = true;
                    break;
                ///销售代表  筛选条件 ： 大区 区域
                case "5":
                    RestDaQu();
                    RestQuYu();

                    daqu_td.Visible = true;
                    daqu_td_ddl.Visible = true;
                    quyu_td.Visible = true;
                    quyu_td_ddl.Visible = true;

                    //sku_td.Visible = true;
                    //sku_td_ddl.Visible = true;
                    //ddl_qy.AutoPostBack = true;
                    break;
                default:
                    break;
            }
        }

        #region 设置大区
        public void RestDaQu()
        {
            string strSQL = String.Format("select distinct dq from RPT_DDT where paperid = {0}", ddlTicketType.SelectedValue);
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);

            this.ddl_dq.DataSource = ds;
            this.ddl_dq.DataValueField = "dq";
            this.ddl_dq.DataTextField = "dq";
            this.ddl_dq.DataBind();

            this.ddl_dq.Items.Insert(0, (new ListItem("全部", "全部")));
            ddl_dq.SelectedIndex = 0;
        }

        protected void ddl_dq_SelectedIndexChanged(object sender, EventArgs e)
        {
            RestQuYu();
        }
        #endregion

        #region 设置区域
        public void RestQuYu()
        {


            string strSQL = String.Format("select distinct xq from RPT_DDT where paperid = {0}", ddlTicketType.SelectedValue);
            if (quyu_td_ddl.Visible && ddl_qy.SelectedIndex > 0)
            {
                strSQL += " and dq='" + ddl_dq.SelectedItem.Text + "'";
            }

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);


            this.ddl_qy.DataSource = ds;
            this.ddl_qy.DataValueField = "xq";
            this.ddl_qy.DataTextField = "xq";
            this.ddl_qy.DataBind();

            this.ddl_qy.Items.Insert(0, (new ListItem("全部", "全部")));
            ddl_qy.SelectedIndex = 0;
        }

        protected void ddl_qy_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (sku_td_ddl.Visible)
            //{
            //    RestSku();
            //}
        }
        #endregion

        #region 设置SKU
        public void RestSku()
        {
            string strSQL = String.Format("select distinct colname, itemname from RPT_DDT_ITEM where paperid = {0} and brandname='{1}'", ddlTicketType.SelectedValue,ddlPP.SelectedItem.Text);
            //if (quyu_td_ddl.Visible && ddl_qy.SelectedIndex > 0)
            //{
            //    strSQL += " and dq='" + ddl_dq.SelectedItem.Text + "'";
            //}

            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, strSQL);


            this.ddl_sku.DataSource = ds;
            this.ddl_sku.DataValueField = "colname";
            this.ddl_sku.DataTextField = "itemname";
            this.ddl_sku.DataBind();

            ddl_sku.SelectedIndex = 0;
        }

        #endregion
    }
}