﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Top.Master" AutoEventWireup="true" CodeBehind="RptDDTSCORE.aspx.cs" Inherits="JSKWeb.REPORTDDT.RptDDTSCORE" %>

<%@ Register Assembly="JSKWeb" Namespace="JSKWeb.Controls" TagPrefix="cc1" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<%@ Register Assembly="DropDownCheckBoxes" Namespace="Saplin.Controls" TagPrefix="DropDownCheckBoxes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <base target="_self" />
    <script src="../js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var lastRowSelected;
        var lastRowClassName;
        function gv_selectRow(row, akey) {
      
        }

        function gv_mouseHover(row) {
            row.style.cursor = 'pointer';
        }

        function quitsystem() {
            if (confirm("Exit system？")) {
                window.location = "../LoginOut.aspx";
            }
        }
        function operate(index) {
           
        }
    
    </script>
    <style type="text/css">
        .aline
        {
            text-decoration: underline;
        }
             select {
            border-color: #cccccc;
            border-width: 1px;
            border-style: solid;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="topTool" runat="server">
    <fieldset>
        <legend>&nbsp;筛选条件&nbsp;</legend>
        <table cellpadding="0" cellspacing="0" border="0"  class="">
            <tbody>
                <tr>
       
                    <td style="padding-left: 5px;" >
                        <asp:DropDownList ID="ddlTicketType"  CssClass="dd_chk_select"  runat="server" Width="110px">
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 5px;" >
                        <asp:DropDownList ID="ddldim" CssClass="dd_chk_select"  runat="server" Width="90px" >
                            <asp:ListItem Value="0">渠道</asp:ListItem>
                            <asp:ListItem Value="1">大区</asp:ListItem>
                            <asp:ListItem Value="2">区域</asp:ListItem>
                            <asp:ListItem Value="3">城市</asp:ListItem>
                            <asp:ListItem Value="4">RDDM</asp:ListItem>
                            <asp:ListItem Value="5">TDDS</asp:ListItem>
                            <asp:ListItem Value="6">DDR</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="padding-left: 5px;" >
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qdDownCheckBoxes" runat="server" 
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="渠道" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                     <td style="padding-left: 5px;" >
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="dqDownCheckBoxes" runat="server"   OnSelectedIndexChanged="dqDownCheckBoxes_SelcetedIndexChanged"  
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="大区" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 5px;" >
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="qyDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="区域" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td style="padding-left: 5px;" >
                        <DropDownCheckBoxes:DropDownCheckBoxes ID="cityDownCheckBoxes" runat="server"
                            AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                            <Style SelectBoxWidth="160" DropDownBoxBoxWidth="160" DropDownBoxBoxHeight="115" />
                            <Texts SelectBoxCaption="城市" />
                        </DropDownCheckBoxes:DropDownCheckBoxes>
                    </td>
                    <td  style="padding-left: 5px;" >
                        <asp:Button ID="btnQuery" OnClick="btnSearch_Click" CssClass="btn" 
                            runat="server"  Text="查询" Width="70px" /> &nbsp;
                        <asp:Button ID="btnOutPut"  CssClass="btn" 
                            runat="server"  Text="导出" Width="70px" OnClick="btnOutPut_Click" /> 
                    </td>
                </tr>
               
            </tbody>
        </table>
    </fieldset>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="rightMain" runat="server">

      <div id="content" runat="server" style="text-align:center">
            
        </div>
</asp:Content>
