﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using DBBase;
using DBUtility;
using System.Data.SqlClient;
namespace JSKWeb
{
    public partial class ChangePwd : System.Web.UI.Page
    {
        //DBUtil ora = new DBUtil();
        SqlHelper sqlhelper = new SqlHelper();   
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Welcome.aspx");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string s_user = "";
           
            if (Request.Cookies["linxsanmpleuser"] != null && Request.Cookies["linxsanmpleuser"].Value != null)
            {
                s_user = Request.Cookies["linxsanmpleuser"].Value.ToString();
            }
            #region 营业所5位编码处理
            string s_role = "";
            if (Request.Cookies["_UserRole"] != null && Request.Cookies["_UserRole"].Value != null)
            {
                s_role = Request.Cookies["_UserRole"].Value.ToString();
            }
         
             //if (s_role == "9")
             //{
             //    s_user = s_user.PadLeft(5, '0');
             //}
             #endregion
             if (txtOldPwd.Text.Trim() == "" || txtPwd.Text.Trim() == "")
            {
                JavaScript.Alert(this, "Password can not be empty");
                return;
            }
          
            if (txtPwd.Text.Trim() != txtRePwd.Text.Trim())
            {
                JavaScript.Alert(this, "Password must be consistent, please input again.");
                return;
            }

            string sql = "select * from sys_user_info where user_code=@user_code and user_pwd=@user_pwd ";
            SqlParameter[] pars = {
                                  new SqlParameter("@user_code",SqlDbType.VarChar),
                                  new SqlParameter ("@user_pwd",SqlDbType.VarChar),
                                  };
            pars[0].Value = s_user;
            pars[1].Value = txtOldPwd.Text.Trim();
            DataSet ds = sqlhelper.ExecuteDataset(SqlHelper.ConnStrEagles, CommandType.Text, sql,pars);


            if (ds.Tables[0].Rows.Count <= 0)
            {
                JavaScript.Alert(this, "OldPassword checked error, please input again.");
                return;
            }
            else
            {
                string sql1 = "UPDATE sys_user_info set user_pwd=@user_pwd  where user_code=@user_code ";
                SqlParameter[] pars1 = {
                                  new SqlParameter("@user_pwd",SqlDbType.VarChar),
                                  new SqlParameter ("@user_code",SqlDbType.VarChar),
                                  };
                pars1[0].Value = this.txtPwd.Text.Trim();
                pars1[1].Value = s_user;

                sqlhelper.ExecuteNonQuery(SqlHelper.ConnStrEagles, CommandType.Text, sql1, pars1);

                JavaScript.Alert(this, "Password Change Success");
            }

        }
    }
}